message(STATUS "MultiSense: Configuring MultiSense tools")

set(VISIONX_MULTISENSE_TOOL_DIR "${PROJECT_ETC_DIR}/script/multisense")

add_custom_target(VisionXScriptMultiSenseStart ALL COMMAND cmake -E create_symlink "${VISIONX_MULTISENSE_TOOL_DIR}/startMultisense.sh" "${ARMARX_BIN_DIR}/startMultisense.sh"
                  SOURCES startMultisense.sh)
 
add_custom_target(VisionXScriptMultiSenseStop ALL COMMAND cmake -E create_symlink "${VISIONX_MULTISENSE_TOOL_DIR}/stopMultisense.sh" "${ARMARX_BIN_DIR}/stopMultisense.sh"
                  SOURCES stopMultisense.sh)
 
