/**
\page VisionX-HowTos-HowTos-implement-pointcloudprocessor VisionX: Implementing PointCloudProcessors

PointCloud processors in VisionX allow to retrieve pointclouds from an
PointCloudProvider and process them. The visionx::PointCloudProcessor
ManagedIceObject is the basic superclass for pointcloud processors. The
superclass provides the following features

\li pointcloud transport via ethernet or shared memory: whether shared memory is
used or not is determined automatically by comparing the IP adresses of provider and processor

\li optional provision of multiple result / visualization pointclouds using embedded
visionx::PointCloudProvider instances

\li usage of multiple pointcloud providers as input

\section VisionX-HowTos-implement-pointcloudprocessor-inherit Creating a pointcloudprocessor

Integrating a new pointcloud processor is achieved by subclassing the
visionx::PointCloudProcessor in the following way:


\code
#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>

namespace visionx
{
    class ExamplePointCloudProcessor :
        virtual public visionx::PointCloudProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        virtual void onInitPointCloudProcessor();

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        virtual void onConnectPointCloudProcessor()  { }

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        virtual void onDisconnectPointCloudProcessor() { }

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        virtual void onExitPointCloudProcessor() { }

        /**
         * @see armarx::Component::getDefaultName()
         */
        virtual std::string getDefaultName() const
        {
            return "ExamplePointCloudProcessor";
        }

        /**
         * Process the vision component.
         *
         * The main loop of the pointcloudprocessor to be implemented in the
         * subclass. Do not block this method. One process should execute
         * exactly one pointcloud processing step.
         */
        virtual void process() = 0;

    };
}

\endcode

The methods that need to be implemented are derived from the armarx::ManagedIceObject:

\li The \b visionx::PointCloudProcessor::onInitPointCloudProcessor method is called once the object is constructred
\li The \b visionx::PointCloudProcessor::onConnectPointCloudProcessor method is called once all network dependencies are resolved
\li the \b visionx::PointCloudProcessor::onDisconnectPointCloudProcessor method is called if a network dependency is lost
\li the \b visionx::PointCloudProcessor::onExitPointCloudProcessor method is called once the application is terminated

As for every ManagedIceObject, the default name of the object needs to be
provided using the \b getDefaultName() method.

The process method should be used to implement the processing of input
pointclouds. It is started in a separate thread once the PointCloudProcessor is
fully connected.

\section VisionX-HowTos-implement-pointcloudprocessor-setup Setting up an pointcloudprocessor

The setup of the pointcloud providers needs to be performed in the onInitPointCloudProvider method:

\code
void ExamplePointCloudProcessor::onInitPointCloudProcessor()
{
    // specify dependency to pointcloud provider
    usingPointCloudProvider("DummyPointCloudProvider");
}


\endcode

Multiple providers can be used by issuing multiple \b usingPointCloudProvider() commands.

\section VisionX-HowTos-implement-pointcloudprocessor-result Enable result pointclouds


To enable a result point cloud processer call \b enableResultPointCloud<PointT>()
 with the point cloud type as an template argument. The name of the point cloud 
provider will be set to ExamplePointCloudProviderResult.  Capacity for the 
shared memory segment is determined by using the template parameter and 
the width and height of the first point cloud provider dependency.


\code
void ExamplePointCloudProvider::onConnectPointCloudProvider()
{
    enableResultPointCloud<pcl::PointXYZ>();
}
\endcode

If you want to specify the capacity manually
call \b enableResultPointCloud(yourCapacity); with your size paramater and
without an template type.  Multiple result point clouds can be enabled via:

\code
void ExamplePointCloudProvider::onConnectPointCloudProvider()
{
    enableResultPointCloud<pcl::PointXYZ>("firstResult");
    enableResultPointCloud<pcl::PointXYZRGBA>("secondResult");
}
\endcode


\section VisionX-HowTos-implement-pointcloudprocessor-retrieve Retrieving pointclouds

In order to process the pointclouds, the pure virtual process method can be
implemented and several utility methods can be used to retrieve the pointclouds.
The process method is called repeatedly, exit checking is done outside the
process method. Consequently, the implementation of the process method should
not block or loop but only trigger one processing step.

There are essentially two types of accessing the pointclouds: polling and
waiting.

\subsection VisionX-HowTos-implement-pointcloudprocessor-retrieve-polling Retrieving pointclouds by polling

For retrieving pointclouds by polling, the visionx::PointCloudProcessor::getPointClouds can be used in the following way:

\code
void ExamplePointCloudProcessor::process()
{
    getPointClouds<pcl::PointXYZ>("DummyPointCloudProvider", pointcloudPtr);
    // process point cloud
}
\endcode

The getPointClouds method will return the most recent pointclouds available
provided by the PointCloudProvider.  This polling method is useful, if the
pointcloud processing usually takes much longer then the framerate of input
pointclouds.

\subsection VisionX-HowTos-implement-pointcloudprocessor-retrieve-waiting Retrieving pointclouds by waiting

For retrieving pointclouds by waiting, the
 \b visionx::PointCloudProcessor::waitForPointClouds() method can be used

\code
void ExamplePointCloudProcessor::process()
{
    if (!waitForPointClouds("DummyPointCloudProvider"))
    {
        ARMARX_WARNING << "Timeout or error in wait for pointclouds";
    }
    else
    {
        // retrieve pointclouds
    }
}
\endcode

The waitForPointClouds method waits until new pointclouds are available from the
provider. This guarantees that a subsequent call to getPointClouds will succeed
in reading the pointclouds.

*/
