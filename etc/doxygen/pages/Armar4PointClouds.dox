/*!
\page VisionX-HowTos-Calculating-point-clouds-with-stereo-vision Calculating point clouds with stereo vision



\section VisionX-HowTos-Calculating-point-clouds-with-stereo-vision-Scenario Creating a new scenario

The calibration file generated in the previous step enables the possibility to calculate point clouds from the two camera images using stereoscopy.
To do this, the components \a StereoCameraProvider and \a StereoImagePointCloudProvider can be used. The \a RealArmar4Vision scenario already contains the \a StereoCameraProvider and can be executed on the head later. Therefore, only a scenario which starts the point cloud provider is required. 

Create a new scenario in the Armar4 package and add the \a StereoImagePointCloudProvider component.

\note This how to is written for the Armar4 head. If you are using a different robot, be sure to adjust the according paths and packages.

\section VisionX-HowTos-Calculating-point-clouds-with-stereo-vision-Configuring Configuring the scenarios

In the configuration of the new scenario, change the \a ImageProviderAdapterName to \a Armar4ImageProvider (since this is the name used in the \a RealArmar4Vision scenario). If you want to use the calibration file created before, open the configuration of the \a RealArmar4Vision scenario and enter the path to the calibration file created before under the \a CalibrationFile variable. Make sure that the camera UIDs are still set correctly (00b09d01009a72f1,00b09d01009a72ee).

\image html Calculating-Point-Clouds-setup.png "The configuration of the new scenario"

\image html Calculating-Point-Clouds-setup-2.png "The configuration of the RealArmar4Vision scenario"

\section VisionX-HowTos-Calculating-point-clouds-with-stereo-vision-Executing Executing the scenarios

Instead of the calibration scenario, now the \a RealArmar4Vision scenario needs to be started on the head computer to access the cameras. The newly created stereo vision scenario should be started from the second pc with the \a ScenarioManager. The widget \a PointCloudViewer can be added to the gui in order to view the resulting point clouds. Pressing ESC should enable the view of the \a PointCloudViewer to be rotatable using the mouse.

Running the two scenarios should now show images in the \a ImageMonitor widget and show point clouds in the \a PointCloudViewer widget. The \a StereoImagePointCloudProvider should calculate a point cloud estimation of the scene using stereoscopy. The quality of course depends on the quality of the calibration, the lighting, the visible correlation points and other potential disturbing factors. Additional components like the \a PointCloudSegmenter can now be added to the new scenario to process the point clouds.

\note Read the how to on VisionX-HowTos-Running-VisionX-Pipeline to learn how the \a PointCloudSegmenter can be employed.

\image html Calculating-Point-Clouds-result.png "The resulting point cloud calculated with sterescopy shown in the PointCloudViewer"



*/
