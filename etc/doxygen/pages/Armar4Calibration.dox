/*!
\page VisionX-HowTos-Calibrating-Armar4-Cameras Calibrating the cameras of the Armar4 head



\section VisionX-HowTos-Calibrating-Armar4-Cameras-Scenario Creating the scenario

To calibrate the stereo setup, a scenario is needed which processes the image data in a calibration component

In the \a ScenarioManager, create a new scenario and add the component called \a CalibrationCreator. The scenario from the how to on \ref VisionX-HowTos-Setting-up-Armar4-head will still be used to provide the images.


\note This how to is written for the Armar4 head. If you are using a different robot, be sure to adjust the according paths and packages.

\section VisionX-HowTos-Calibrating-Armar4-Cameras-Configuration Configuring the scenario

The configuration of the calibration component has to be edited:

Set the \a ImageProviderAdapterName to \a IEEE1394ImageProvider and enter a valid file path as the \a OutputFileName. The calibration is calculated from images of a black and white checkerboard with differing orientations in space. Depending on the calibration checkerboard you are using, the number of \a rows and \a columns, as well as the \a length \a of \a a \a square have to be set. For the usual H2T checkerboard (printed on A3), these values would be \a 7, \a 9 and \a 35.22 respectively.
\note You should put the length of a square in the measuring unit you want to use later, in this case mm

You can increase the \a NumberOfImages value to calibrate with more data and increase the \a WaitingIntervalBetweenImages to give you more time to change the poses between captures (default is only .5 seconds).

\image html Calibration-Scenario-settings.png "Exemplary setup of the calibration component"

\section VisionX-HowTos-Calibrating-Armar4-Cameras-Calibration Calibration

To calibrate, make sure that the image providing scenario is still running on the head pc (you can check with the \a ImageMonitor).Then start the new scenario from the second pc in the \a ScenarioManager. The calibration snapshots are taken after a 10 second waiting time, in which the \a ImageMonitor widget should be arranged to be visible in order to see the images you are capturing during the calibration. Make sure to include many differing poses of the checkerboard in the calibration images.

The calibration parameters should be stored after a short calculation in the location specified as the \a OutputFileName and can be used for \ref VisionX-HowTos-Calculating-point-clouds-with-stereo-vision.

\image html Calibration-Scenario-image-during-calibration.png "The image monitor can be used to check the images provided to the calibration component"

*/
