############## FakePointCloudProvider ###############
ArmarX.FakePointCloudProvider.pointCloudFileName = VisionX/examples/pcd_samples/stairs/

############## PointCloudSegmenter ###############
ArmarX.PointCloudSegmenter.lccpMinimumSegmentSize = 5
ArmarX.PointCloudSegmenter.lccpVoxelResolution = 0.02
ArmarX.PointCloudSegmenter.lccpSeedResolution = 0.1
ArmarX.PointCloudSegmenter.providerName = FakePointCloudProvider
ArmarX.PointCloudSegmenter.segmentationMethod = LCCP


############## PrimitiveExtractor ###############
ArmarX.PrimitiveExtractor.outlierThreshold = 0.02
ArmarX.PrimitiveExtractor.circularDistanceThreshold = 0.01
ArmarX.PrimitiveExtractor.euclideanClusteringTolerance = 0.02
ArmarX.PrimitiveExtractor.planeDistanceThreshold = 0.1
ArmarX.PrimitiveExtractor.maximumSegmentSize = 75000000
ArmarX.PrimitiveExtractor.minimumSegmentSize = 100

