from distutils.core import setup
from setuptools import setup, find_packages
setup(
    name='armarx-facerecognition',
    version='0.1',
    packages=[],
    install_requires=["face-recognition", "face-recognition-models", "zeroc-ice", "numpy", "python-Levenshtein"],
    url='',
    license='',
    author='Mirko Waechter',
    author_email='waechter@kit.edu',
    description='Integration of facerecognition library into ArmarX'
)
