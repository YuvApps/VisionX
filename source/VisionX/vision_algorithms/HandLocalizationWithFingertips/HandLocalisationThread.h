/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
/*
 * @file    HandLocalisationThread.h
 *
 * @author  David Schiebener
 * @date    21.02.2010
 */

/* double include protection */
#pragma once

/* includes */
#include "Threading/Thread.h"
#include "Threading/Threading.h"
#include "Threading/Mutex.h"

#include "Image/ByteImage.h"

#include "HandLocalisation.h"





namespace visionx
{
    /**
     * @class HandLocalisationThread
     *
     */
    class HandLocalisationThread: public CThread
    {
    public:
        HandLocalisationThread(int nNumParticles, int nNumAnnealingRuns, int nPredictionMode, CStereoCalibration* pCalibration, std::string sHandModelFileName);
        ~HandLocalisationThread() override;

        bool IsProcessing();
        bool HasResult();

        void GetResult(double* pEstimatedConfig, double& dConfidenceRating);
        void Localise(const CByteImage* pNewCamImageLeft, const CByteImage* pNewCamImageRight, const double* pSensorConfig);

        int ThreadMethod() override;

        void Stop();

        CHandLocalisation* m_pHandLocalisation;

    private:

        bool m_bIsProcessing;
        bool m_bHasResult;
        bool m_bReadyForLocalising;

        CByteImage* m_pNewCamImageLeft, *m_pNewCamImageRight;
        double* m_pSensorConfig;
        double* m_pEstimatedConfig;
        double m_dConfidenceRating;

        CMutex m_mMutex;

        bool m_bStopped;
    };
}

