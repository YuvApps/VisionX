/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "Math/Math3d.h"

#include "../HandLocalisationConstants.h"


namespace visionx::ConvexPolygonCalculations
{

    struct Polygon
    {
        int nCorners;           // corners which form the convex hull of the polygon. ordered by y (desc).
        Vec3d hull[2 * DSHT_MAX_POLYGON_CORNERS]; // z-value is 1 if the corner belongs to the left part, 2 for the
        // right part, 3 if it belongs to both (highest and lowest point).

        Vec3d hullCircle[2 * DSHT_MAX_POLYGON_CORNERS + 1]; // contains the points of the hull in the order they have
        // on a path going around the edge of the polygon, starting and ending
        // at the highest point, in a clockwise sense
        // +1 reserves space to copy the first entry for hull lines

        // in left and right hull, the entry 0 is the lowest point, 1 the highest, then in descending y-order
        // the other points, and the last entry is the lowest point again.
        int nCornersLeft;           // left part of the hull
        Vec3d hullLeft[2 * DSHT_MAX_POLYGON_CORNERS + 1];

        int nCornersRight;          // right part of the hull
        Vec3d hullRight[2 * DSHT_MAX_POLYGON_CORNERS + 1];

        Vec3d center3d;             // center of the fingertip (3D)

        float minX, maxX, minY, maxY;       // extremest coordinate values in x- and y-direction
    };



    // Sorts the points in the array inout by their X-coordinate (descending)
    void SortByX(Vec3d* inout, int arraylength);
    void SortByY(Vec3d* inout, int arraylength);

    // Returns true if the two consecutive line segments described by the three points
    // make a turn to the left
    bool LeftTurn(Vec3d p1, Vec3d p2, Vec3d p3);
    bool RightTurn(Vec3d p1, Vec3d p2, Vec3d p3);

    // Returns true if Point lies on the right side of the line defined by linePoint1 and linePoint2 (2D)
    bool PointIsOnTheRightSideOfTheLine(Vec3d Point, Vec3d linePoint1, Vec3d linePoint2);



    // Analog zu Sanders-VL Algorithmentechnik, Folien 16 vom 12.02.2008, S.28-32.
    // Calculates the convex hull of the points in "in", the left part of the hull is stored in hull_left,
    // the right part in hull_right. Both start with the lowest point, then the highest point, then the
    // points of the respective hull descending by Y until the lowest point (again).
    // hull_left, hull_right, temp1 and temp2 have to be of size (max number of corners of a polygon) + 1
    void CalcConvexHull(Vec3d* in, int num_all_points, Vec3d* hull_left, int* num_hull_points_left, Vec3d* hull_right, int* num_hull_points_right, Vec3d* temp1, Vec3d* temp2);
    void CalcConvexHull(Vec3d* in, int num_all_points, Vec3d* hull_left, int* num_hull_points_left, Vec3d* hull_right, int* num_hull_points_right);

    // Interface to create the convex polygon pol from the points in the array in
    void CalcConvexHull(Vec3d* in, int num_all_points, Polygon* pol, Vec3d* temp1, Vec3d* temp2);
    void CalcConvexHull(Vec3d* in, int num_all_points, Polygon* pol);

    // Interface to create the convex polygon pol from the points already in pol->hull
    void CalcConvexHull(Polygon* pol, Vec3d* temp1, Vec3d* temp2);
    void CalcConvexHull(Polygon* pol);

    // Creates a convex polygon from the points in hullpoints, assuming that they all belong to the
    // convex hull
    void CreateConvexPolygonFromHullPoints(Vec3d* hullpoints, int nPoints, Polygon* pol);



    // Returns the convex polygon that is the intersection polygon of p1 and p2. pointAccu and boolTable have to
    // be of size 2*(maximal number of points in a normal polygon), clockwiseHullPoly1/2 of size (maximal number
    // of points in a normal polygon)+1
    void GetPolygonIntersection(Polygon* p1, Polygon* p2, Polygon* pInter, Vec3d* pointAccu, bool* boolTable, Vec3d* clockwiseHullPoly1, Vec3d* clockwiseHullPoly2);
    void GetPolygonIntersection(Polygon* p1, Polygon* p2, Polygon* pInter);



    // Returns true if it is possible that the two polygons intersect
    bool PolygonsMightIntersect(Polygon* pol1, Polygon* pol2);

}


