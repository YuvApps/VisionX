# description of the kinematic model of the robot hand
#
#
# base coordinate system: x towards the index etc fingers, y towards the pinky base, z away from the arm
#
#
#
# offset to the thumb base
-40.2	0.0	-40.0
#
# distance from thumb base joint to second joint
-40.2
#
# distance from second thumb joint to fingertip
-23.0
#
#
#
#
# offset from palm to index base
40.2	-14.0	-40.0
#
# distance from index base joint to second joint
40.2
#
# distance from second index joint to fingertip
23.0
#
#
#
# offset from palm to middle base
40.2	13.0	-40.0
#
# distance from middle base joint to second joint
40.2
#
# distance from second middle joint to fingertip
23.0
#
#
# 
# offset from palm to ring base
40.2	37.0	-40.0
#
# distance from ring base joint to second joint
40.2
#
# distance from second ring joint to fingertip
23.0
#
#
#
# offset from palm to pinky base
40.2	59.0	-40.0
#
# distance from pinky base joint to second joint
40.2
#
# distance from second pinky joint to fingertip
17.0
#
#
#
#
# finger tip: number of points
22
#
# offset in z direction such that the tip has z-coordinate 0 at the bottom
6.5
#
# point coordinates
0.0	-3.5	-16.8 	// 1 (Ansatz oben)
0.0	3.5	-16.8
0.0	-10.0	-11.3	// 3 (Ansatz mitte oben)
0.0	10.0	-11.3
0.0	-10.0	-3.5	// 5 (Ansatz mitte unten)
0.0	10.0	-3.5
0.0	-7.6	-0.0	// 7 (Ansatz unten)
0.0	7.6	-0.0
10.5	-2.0	-16.8	// 9 (Oben nahe Ansatz)
10.5	2.0	-16.8
20.0	-7.6	-0.0    // 11 (Unten nahe Spitze)
20.0	7.6	-0.0
21.5	-10.0	-7.5	// 13 (Fast Spitze aussen oben)
21.5	10.0	-7.5
21.5	-10.0	-3.5	// 15 (Fast Spitze aussen unten)
21.5	10.0	-3.5
32.0	-4.3	-13.5	// 17 (Spitze mitte)
32.0	4.3	-13.5
32.0	-7.0	-8.0	// 19 (Spitze aussen)
32.0	7.0	-8.0
24.2	-9.0	-3.5    // 21 (Spitze unten)
24.2	9.0	-3.5
#
#
#
# tracking ball position
-35.0	-45.0	-65.0
#
# tracking ball radius
19.5
