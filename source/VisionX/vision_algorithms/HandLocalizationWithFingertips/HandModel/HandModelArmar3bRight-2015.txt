# description of the kinematic model of the robot hand
#
#
# base coordinate system: x towards the index etc fingers, y towards the pinky base, z away from the arm
#
#
#
# offset from palm to the thumb base
-40.2	0.0	0.0
#
# distance from thumb base joint to second joint
-40.2
#
# distance from second thumb joint to fingertip
-23.0
#
#
#
#
# offset from palm to index base
40.2	-14.0	0.0
#
# distance from index base joint to second joint
40.2
#
# distance from second index joint to fingertip
23.0
#
#
#
# offset from palm to middle base
40.2	13.0	0.0
#
# distance from middle base joint to second joint
40.2
#
# distance from second middle joint to fingertip
23.0
#
#
# 
# offset from palm to ring base
40.2	37.0	0.0
#
# distance from ring base joint to second joint
40.2
#
# distance from second ring joint to fingertip
23.0
#
#
#
# offset from palm to pinky base
40.2	59.0	0.0
#
# distance from pinky base joint to second joint
40.2
#
# distance from second pinky joint to fingertip
17.0
#
#
#
#
# finger tip: number of points
18
#
# offset in z direction such that the tip has z-coordinate 0 at the bottom
10.0
#
# point coordinates. x points towards the end of the tip, y to the side, and z down
#
 2.0   -9.3  -19.5 	//   1 (Ansatz oben)
 2.0    9.3  -19.5
 0.0  -11.5   -7.5	//   3 (Ansatz mitte)
 0.0   11.5   -7.5
 3.0   -9.0    0.0	//   5 (Ansatz unten)
 3.0    9.0    0.0
27.0   -9.3  -17.5	//   7 (Mitte oben)
27.0    9.3  -17.5
27.0  -11.5   -7.5  //   9 (Mitte mitte)
27.0   11.5   -7.5
27.0   -9.0   -1.0	//  11 (Mitte unten)
27.0    9.0   -1.0
37.5   -5.5  -13.0	//  13 (Spitze oben)
37.5    5.5  -13.0
38.0   -7.0   -7.5	//  15 (Spitze mitte)
38.0    7.0   -7.5
35.0   -5.0   -4.5	//  17 (Spitze unten)
35.0    5.0   -4.5
#
#
#
# tracking ball position
-35.0	-45.0	-65.0
#
# tracking ball radius
19.5
