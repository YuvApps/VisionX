armarx_set_target("HandLocalizationWithFingertips Library")

find_package(OpenMP QUIET)
armarx_build_if(OPENMP_FOUND "OpenMP not available")

set(LIB_NAME       HandLocalizationWithFingertips)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
set(LDFLAGS "${LDFLAGS} ${OpenMP_CXX_FLAGS}")

set(LIBS   VisionXInterfaces
           VisionXCore
           VisionXTools
           ArmarXCoreInterfaces
           ArmarXCore
           RobotAPICore
           ArmarXCoreObservers
           RobotAPIRobotStateComponent
           gomp #explicit for ninja compiler
           )

set(LIB_FILES HandLocalisation.cpp
              HandLocalisationThread.cpp
              HandModel/HandModeliCub.cpp
              HandModel/HandModelV2.cpp
              ParticleFilter/ParticleFilterFrameworkParallelized.cpp
              ParticleFilter/ParticleFilterRobotHandLocalisation.cpp
              ParticleFilter/Polygon.cpp
              Visualization/HandModelVisualizer.cpp
              Visualization/MoveMasterModel.cpp
              Visualization/OIFwdKinematicsInterface.cpp
              )

set(LIB_HEADERS HandLocalisationConstants.h
                HandLocalisation.h
                HandLocalisationThread.h
                HandModel/HandModeliCub.h
                HandModel/HandModelV2.h
                ParticleFilter/ParticleFilterFrameworkParallelized.h
                ParticleFilter/ParticleFilterRobotHandLocalisation.h
                ParticleFilter/Polygon.h
                Visualization/HandModelVisualizer.h
                Visualization/MoveMasterModel.h
                Visualization/OIFwdKinematicsInterface.h
                )

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

