/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
// *****************************************************************
// -----------------------------------------------------------------
// HandLocalisation.cpp
// Implementation of class CHandLocalisation
// -----------------------------------------------------------------
// *****************************************************************

// *****************************************************************
// Author:  David Schiebener (Hiwi Kai Welke)
// Date:    11.12.2009
// *****************************************************************


// *****************************************************************
// includes
// *****************************************************************

#include "HandLocalisation.h"
#include "Visualization/HandModelVisualizer.h"

#include <cmath>

// OpenMP
#include <omp.h>

// Core
#include <ArmarXCore/core/logging/Logging.h>



namespace visionx
{
    CHandLocalisation::CHandLocalisation(int nNumParticles, int nNumAnnealingRuns, int nPredictionMode, CStereoCalibration* pCalibration, std::string sHandModelFileName)
    {
        m_pHSVImageLeft = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eRGB24);
        m_pHSVImageRight = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eRGB24);
        m_pColorFilteredImageLeft  = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pColorFilteredImageRight = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pSobelImageLeft  = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pSobelImageRight = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pSobelXImageLeft  = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pSobelXImageRight = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pSobelYImageLeft  = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pSobelYImageRight = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pTempGrayImage1 = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pTempGrayImage2 = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pTempGrayImage3 = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pTempGrayImage4 = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pTempGrayImage5 = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pTempGrayImage6 = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pTrackingBallImageLeft  = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);
        m_pTrackingBallImageRight = new CByteImage(DSHT_IMAGE_WIDTH, DSHT_IMAGE_HEIGHT, CByteImage::eGrayScale);

        m_pFinalConf = new double[DSHT_NUM_PARAMETERS];
        m_pOldFinalConf = new double[DSHT_NUM_PARAMETERS];
        m_pSensorConf = new double[DSHT_NUM_PARAMETERS];
        m_pOldSensorConf = new double[DSHT_NUM_PARAMETERS];
        m_pPFConf = new double[DSHT_NUM_PARAMETERS];
        m_pOldPFConf = new double[DSHT_NUM_PARAMETERS];
        m_pPredictedConf = new double[DSHT_NUM_PARAMETERS];

        for (int i = 0; i < DSHT_NUM_PARAMETERS; i++)
        {
            m_pFinalConf[i] = 0;
            m_pOldFinalConf[i] = 0;
            m_pSensorConf[i] = 0;
            m_pOldSensorConf[i] = 0;
            m_pPFConf[i] = 0;
            m_pOldPFConf[i] = 0;
            m_pPredictedConf[i] = 0;
        }

        m_nNumParticles = nNumParticles;
        m_nNumAnnealingRuns = nNumAnnealingRuns;
        m_nPredictionMode = nPredictionMode;
        m_dVarianceFactor = 1.0;

        const int nNumThreads = omp_get_num_procs();

        m_pParticleFilter = new CParticleFilterRobotHandLocalisation(m_nNumParticles, DSHT_NUM_PARAMETERS, nNumThreads, pCalibration, sHandModelFileName);
        m_pParticleFilter->SetImages(m_pColorFilteredImageLeft, m_pColorFilteredImageRight, m_pSobelImageLeft, m_pSobelImageRight,
                                     m_pSobelXImageLeft, m_pSobelXImageRight, m_pSobelYImageLeft, m_pSobelYImageRight);

        m_pObjectFinderLeft = new CObjectFinder();
        m_pObjectFinderRight = new CObjectFinder();
        m_pdTrBallPosXLeft = new double[m_nMaxTrBallRegions];
        m_pdTrBallPosXRight = new double[m_nMaxTrBallRegions];
        m_pdTrBallPosYLeft = new double[m_nMaxTrBallRegions];
        m_pdTrBallPosYRight = new double[m_nMaxTrBallRegions];
        m_pdTrBallRadiusLeft = new double[m_nMaxTrBallRegions];
        m_pdTrBallRadiusRight = new double[m_nMaxTrBallRegions];

#ifdef DSHT_USE_ICUB
        m_pHandModel = new HandModeliCub(DSHT_HAND_MODEL_PATH, pCalibration);
#else
        m_pHandModel = new CHandModelV2(DSHT_HAND_MODEL_PATH, pCalibration);
#endif

        m_bFirstRun = true;
    }




    CHandLocalisation::~CHandLocalisation()
    {
        delete m_pHSVImageLeft;
        delete m_pHSVImageRight;
        delete m_pColorFilteredImageLeft;
        delete m_pColorFilteredImageRight;
        delete m_pSobelImageLeft;
        delete m_pSobelImageRight;
        delete m_pSobelXImageLeft;
        delete m_pSobelXImageRight;
        delete m_pSobelYImageLeft;
        delete m_pSobelYImageRight;
        delete m_pTempGrayImage1;
        delete m_pTempGrayImage2;
        delete m_pTempGrayImage3;
        delete m_pTempGrayImage4;
        delete m_pTempGrayImage5;
        delete m_pTempGrayImage6;
        delete m_pTrackingBallImageLeft;
        delete m_pTrackingBallImageRight;

        delete[] m_pFinalConf;
        delete[] m_pOldFinalConf;
        delete[] m_pSensorConf;
        delete[] m_pOldSensorConf;
        delete[] m_pPFConf;
        delete[] m_pOldPFConf;
        delete[] m_pPredictedConf;

        delete m_pParticleFilter;

        delete m_pObjectFinderLeft;
        delete m_pObjectFinderRight;
        delete[] m_pdTrBallPosXLeft;
        delete[] m_pdTrBallPosXRight;
        delete[] m_pdTrBallPosYLeft;
        delete[] m_pdTrBallPosYRight;
        delete[] m_pdTrBallRadiusLeft;
        delete[] m_pdTrBallRadiusRight;

        delete m_pHandModel;
    }




    inline void ExtractAnglesFromRotationMatrix(const Mat3d& mat, Vec3d& angles)
    {
        angles.y = asin(mat.r3);
        angles.x = atan2((-mat.r6), (mat.r9));
        angles.z = atan2((-mat.r2), (mat.r1));
    }



    void CHandLocalisation::LocaliseHand(const CByteImage* pNewCamImageLeft, const CByteImage* pNewCamImageRight, const Vec3d vPositionFromSensors, const Mat3d mOrientationFromSensors, const double* pSensorConfigFingers, double* pEstimatedConfig, double& dConfidenceRating)
    {
        double pSensorConfig[DSHT_NUM_PARAMETERS];
        Vec3d vRotationAngles;
        ExtractAnglesFromRotationMatrix(mOrientationFromSensors, vRotationAngles);
        pSensorConfig[0] = vPositionFromSensors.x;
        pSensorConfig[1] = vPositionFromSensors.y;
        pSensorConfig[2] = vPositionFromSensors.z;
        pSensorConfig[3] = vRotationAngles.x;
        pSensorConfig[4] = vRotationAngles.y;
        pSensorConfig[5] = vRotationAngles.z;
        for (int i = 6; i < DSHT_NUM_PARAMETERS; i++)
        {
            pSensorConfig[i] = pSensorConfigFingers[i - 6];
        }

        LocaliseHand(pNewCamImageLeft, pNewCamImageRight, pSensorConfig, pEstimatedConfig, dConfidenceRating);
    }




    Eigen::Matrix4f CHandLocalisation::GetHandPose()
    {
        Eigen::Matrix4f handPose;

        configMutex.lock();

        handPose(0, 3) = m_pFinalConf[0];
        handPose(1, 3) = m_pFinalConf[1];
        handPose(2, 3) = m_pFinalConf[2];

        Mat3d orientation;
        Math3d::SetRotationMat(orientation, m_pFinalConf[3], m_pFinalConf[4], m_pFinalConf[5]);

        configMutex.unlock();

        handPose(0, 0) = orientation.r1;
        handPose(0, 1) = orientation.r2;
        handPose(0, 2) = orientation.r3;
        handPose(1, 0) = orientation.r4;
        handPose(1, 1) = orientation.r5;
        handPose(1, 2) = orientation.r6;
        handPose(2, 0) = orientation.r7;
        handPose(2, 1) = orientation.r8;
        handPose(2, 2) = orientation.r9;

        handPose(3, 0) = 0;
        handPose(3, 1) = 0;
        handPose(3, 2) = 0;
        handPose(3, 3) = 1;

        return handPose;
    }



    std::vector<Vec3d> CHandLocalisation::GetFingertipPositions()
    {
        configMutex.lock();
        m_pHandModel->UpdateHand(m_pFinalConf);
        configMutex.unlock();

        std::vector<Vec3d> ret;
        for (size_t i = 0; i < m_pHandModel->m_aFingerTipCornersInWorldCS.size(); i++)
        {
            Vec3d vAverage = {0, 0, 0};
            if (m_pHandModel->m_aFingerTipCornersInWorldCS.at(i).size() > 0)
            {
                for (size_t j = 0; j < m_pHandModel->m_aFingerTipCornersInWorldCS.at(i).size(); j++)
                {
                    Math3d::AddToVec(vAverage, m_pHandModel->m_aFingerTipCornersInWorldCS.at(i).at(j));
                }
                Math3d::MulVecScalar(vAverage, 1.0f / m_pHandModel->m_aFingerTipCornersInWorldCS.at(i).size(), vAverage);
            }
            ret.push_back(vAverage);
        }
        return ret;
    }



    double* CHandLocalisation::GetResultConfig()
    {
        double* ret = new double[DSHT_NUM_PARAMETERS];
        configMutex.lock();
        for (int i = 0; i < DSHT_NUM_PARAMETERS; i++)
        {
            ret[i] = m_pFinalConf[i];
        }
        configMutex.unlock();
        return ret;
    }



    void CHandLocalisation::SetResultConfig(const Vec3d vPosition, const Mat3d mOrientation, const double* pConfigFingers)
    {
        configMutex.lock();
        Vec3d vRotationAngles;
        ExtractAnglesFromRotationMatrix(mOrientation, vRotationAngles);
        m_pFinalConf[0] = vPosition.x;
        m_pFinalConf[1] = vPosition.y;
        m_pFinalConf[2] = vPosition.z;
        m_pFinalConf[3] = vRotationAngles.x;
        m_pFinalConf[4] = vRotationAngles.y;
        m_pFinalConf[5] = vRotationAngles.z;
        for (int i = 6; i < DSHT_NUM_PARAMETERS; i++)
        {
            m_pFinalConf[i] = pConfigFingers[i - 6];
        }
        configMutex.unlock();
    }



    double* CHandLocalisation::GetSensorConfig()
    {
        double* ret = new double[DSHT_NUM_PARAMETERS];
        configMutex.lock();
        for (int i = 0; i < DSHT_NUM_PARAMETERS; i++)
        {
            ret[i] = m_pSensorConf[i];
        }
        configMutex.unlock();
        return ret;
    }



    void CHandLocalisation::SetSensorConfig(const Vec3d vPosition, const Mat3d mOrientation, const double* pConfigFingers)
    {
        configMutex.lock();
        Vec3d vRotationAngles;
        ExtractAnglesFromRotationMatrix(mOrientation, vRotationAngles);
        m_pSensorConf[0] = vPosition.x;
        m_pSensorConf[1] = vPosition.y;
        m_pSensorConf[2] = vPosition.z;
        m_pSensorConf[3] = vRotationAngles.x;
        m_pSensorConf[4] = vRotationAngles.y;
        m_pSensorConf[5] = vRotationAngles.z;
        for (int i = 6; i < DSHT_NUM_PARAMETERS; i++)
        {
            m_pSensorConf[i] = pConfigFingers[i - 6];
        }
        configMutex.unlock();
    }



    void CHandLocalisation::LocaliseHand(const CByteImage* pNewCamImageLeft, const CByteImage* pNewCamImageRight, const double* pSensorConfig, double* pEstimatedConfig, double& dConfidenceRating)
    {
        Mat3d mTemp1, mTemp2, mTemp3, mTemp4;
        Vec3d vTemp1;
        float fTemp1;
        double pTempConf[DSHT_NUM_PARAMETERS];

        ARMARX_VERBOSE_S << "PF: Preparing";

        // **********************************************************************
        //  update sensor values
        // **********************************************************************

        //ARMARX_VERBOSE_S << "Sensor values:";
        for (int i = 0; i < DSHT_NUM_PARAMETERS; i++)
        {
            ARMARX_VERBOSE_S << pSensorConfig[i];
        }

        configMutex.lock();
        for (int i = 0; i < DSHT_NUM_PARAMETERS; i++)
        {
            m_pSensorConf[i] = pSensorConfig[i];
        }
        configMutex.unlock();

        m_pParticleFilter->SetSensorConfig(m_pSensorConf);


        // **********************************************************************
        //  prepare images
        // **********************************************************************
        #pragma omp parallel sections
        {
            #pragma omp section
            {
                ImageProcessor::CalculateHSVImage(pNewCamImageLeft, m_pHSVImageLeft);
                ImageProcessor::FilterHSV(m_pHSVImageLeft, m_pColorFilteredImageLeft, default_fingertip_hue, default_fingertip_hue_tolerance,
                                          default_fingertip_sat_min, default_fingertip_sat_max, default_fingertip_val_min,
                                          default_fingertip_val_max);
                ImageProcessor::Erode(m_pColorFilteredImageLeft, m_pTempGrayImage1);
                ImageProcessor::Dilate(m_pTempGrayImage1, m_pColorFilteredImageLeft);
            }

            #pragma omp section
            {
                ImageProcessor::CalculateHSVImage(pNewCamImageRight, m_pHSVImageRight);
                ImageProcessor::FilterHSV(m_pHSVImageRight, m_pColorFilteredImageRight, default_fingertip_hue, default_fingertip_hue_tolerance,
                                          default_fingertip_sat_min, default_fingertip_sat_max, default_fingertip_val_min,
                                          default_fingertip_val_max);
                ImageProcessor::Erode(m_pColorFilteredImageRight, m_pTempGrayImage2);
                ImageProcessor::Dilate(m_pTempGrayImage2, m_pColorFilteredImageRight);
            }

            #pragma omp section
            {
                ImageProcessor::ConvertImage(pNewCamImageLeft, m_pTempGrayImage3);
                ImageProcessor::CalculateGradientImageSobel(m_pTempGrayImage3, m_pTempGrayImage4);
                for (int i = 0; i < m_pTempGrayImage4->width * m_pTempGrayImage4->height; i++)
                {
                    m_pTempGrayImage4->pixels[i] = 15 * sqrt(m_pTempGrayImage4->pixels[i]);
                }
                ImageProcessor::GaussianSmooth3x3(m_pTempGrayImage4, m_pSobelImageLeft);
#ifdef DSHT_USE_EDGE_DIRECTION
                ImageProcessor::SobelX(m_pTempGrayImage3, m_pTempGrayImage4, false);
                ImageProcessor::GaussianSmooth3x3(m_pTempGrayImage4, m_pSobelXImageLeft);
                ImageProcessor::SobelY(m_pTempGrayImage3, m_pTempGrayImage4, false);
                ImageProcessor::GaussianSmooth3x3(m_pTempGrayImage4, m_pSobelYImageLeft);
#endif // DSHT_USE_EDGE_DIRECTION
            }

            #pragma omp section
            {
                ImageProcessor::ConvertImage(pNewCamImageRight, m_pTempGrayImage5);
                ImageProcessor::CalculateGradientImageSobel(m_pTempGrayImage5, m_pTempGrayImage6);
                for (int i = 0; i < m_pTempGrayImage6->width * m_pTempGrayImage6->height; i++)
                {
                    m_pTempGrayImage6->pixels[i] = 15 * sqrt(m_pTempGrayImage6->pixels[i]);
                }
                ImageProcessor::GaussianSmooth3x3(m_pTempGrayImage6, m_pSobelImageRight);
#ifdef DSHT_USE_EDGE_DIRECTION
                ImageProcessor::SobelX(m_pTempGrayImage5, m_pTempGrayImage6, false);
                ImageProcessor::GaussianSmooth3x3(m_pTempGrayImage6, m_pSobelXImageRight);
                ImageProcessor::SobelY(m_pTempGrayImage5, m_pTempGrayImage6, false);
                ImageProcessor::GaussianSmooth3x3(m_pTempGrayImage6, m_pSobelYImageRight);
#endif // DSHT_USE_EDGE_DIRECTION
            }



            // **********************************************************************
            //  find tracking ball
            // **********************************************************************

            #pragma omp section
            {
                m_pObjectFinderLeft->PrepareImages(pNewCamImageLeft);
                m_pObjectFinderLeft->FindObjects(pNewCamImageLeft, NULL, eBlue, default_trball_hue, default_trball_hue_tolerance,
                                                 default_trball_sat_min, default_trball_sat_max, default_trball_val_min,
                                                 default_trball_val_max, 500, false);    // 1000=nMinPointsPerRegion
                int nNumRegions = m_pObjectFinderLeft->Finalize();

                // debug
                //ImageProcessor::FilterHSV(m_pHSVImageLeft, m_pTrackingBallImageLeft, default_trball_hue, default_trball_hue_tolerance,
                //                                default_trball_sat_min, default_trball_sat_max, default_trball_val_min, default_trball_val_max);

                MyRegion* pRegion;
                for (int i = 0; i < nNumRegions && i < m_nMaxTrBallRegions; i++)
                {
                    pRegion = &(m_pObjectFinderLeft->m_objectList.at(i).region);
                    m_pdTrBallPosXLeft[i] = 0.5 * (pRegion->min_x + pRegion->max_x);
                    m_pdTrBallPosYLeft[i] = 0.5 * (pRegion->min_y + pRegion->max_y);
                    m_pdTrBallRadiusLeft[i] = (double)(((pRegion->max_x - pRegion->min_x) > (pRegion->max_y - pRegion->min_y)) ? 0.5 * (pRegion->max_x - pRegion->min_x) : 0.5 * (pRegion->max_y - pRegion->min_y));

                    // debug
                    //CHandModelVisualizer::DrawCross(m_pTrackingBallImageLeft, m_pdTrBallPosXLeft[i], m_pdTrBallPosYLeft[i], 150);
                    //printf("Tracking ball pos: (%.1f, %.1f)\n", m_pdTrBallPosXLeft[i], m_pdTrBallPosYLeft[i]);
                }
                m_pParticleFilter->SetTrackingBallPositions(m_pdTrBallPosXLeft, m_pdTrBallPosYLeft, m_pdTrBallRadiusLeft, nNumRegions, true);

                // debug
                //m_pTrackingBallImageLeft->SaveToFile("/home/staff/schieben/TrBall.bmp");
            }

            #pragma omp section
            {
                m_pObjectFinderRight->PrepareImages(pNewCamImageRight);
                m_pObjectFinderRight->FindObjects(pNewCamImageRight, NULL, eBlue, default_trball_hue, default_trball_hue_tolerance,
                                                  default_trball_sat_min, default_trball_sat_max, default_trball_val_min,
                                                  default_trball_val_max, 500, false);    // 1000=nMinPointsPerRegion
                int nNumRegions = m_pObjectFinderRight->Finalize();

                MyRegion* pRegion;
                for (int i = 0; i < nNumRegions && i < m_nMaxTrBallRegions; i++)
                {
                    pRegion = &(m_pObjectFinderRight->m_objectList.at(i).region);
                    m_pdTrBallPosXRight[i] = 0.5 * (pRegion->min_x + pRegion->max_x);
                    m_pdTrBallPosYRight[i] = 0.5 * (pRegion->min_y + pRegion->max_y);
                    m_pdTrBallRadiusRight[i] = (double)(((pRegion->max_x - pRegion->min_x) > (pRegion->max_y - pRegion->min_y)) ? 0.5 * (pRegion->max_x - pRegion->min_x) : 0.5 * (pRegion->max_y - pRegion->min_y));
                }
                m_pParticleFilter->SetTrackingBallPositions(m_pdTrBallPosXRight, m_pdTrBallPosYRight, m_pdTrBallRadiusRight, nNumRegions, false);
            }
        }


        // debug
        //m_pColorFilteredImageLeft->SaveToFile("/home/staff/schieben/ColFilImg.bmp");
        //m_pSobelImageLeft->SaveToFile("/home/staff/schieben/EdgeImg.bmp");


        // **********************************************************************
        //  predict new pose from old estimation
        // **********************************************************************


        // check for nan in old result
        bool bNan = false;
        for (int i = 0; i < DSHT_NUM_PARAMETERS; i++)
        {
            if (std::isnan(m_pOldFinalConf[i]))
            {
                bNan = true;
            }
        }

        if (!m_bFirstRun && !bNan)
        {
            // hand position
            for (int i = 0; i < 3; i++)
            {
                m_pPredictedConf[i] = m_pOldFinalConf[i] + m_pSensorConf[i] - m_pOldSensorConf[i];
            }

            // finger joints
            for (int i = 6; i < DSHT_NUM_PARAMETERS; i++)
            {
                m_pPredictedConf[i] = m_pOldFinalConf[i] + m_pSensorConf[i] - m_pOldSensorConf[i];
                if (m_pPredictedConf[i] < 0)
                {
                    m_pPredictedConf[i] += 2 * M_PI;    // avoid angle overflow
                }
                else if (m_pPredictedConf[i] >= 2 * M_PI)
                {
                    m_pPredictedConf[i] -= 2 * M_PI;
                }
            }

            // hand rotation
            Math3d::SetRotationMat(mTemp1, m_pSensorConf[3], m_pSensorConf[4], m_pSensorConf[5]);
            Math3d::SetRotationMat(mTemp2, m_pOldSensorConf[3], m_pOldSensorConf[4], m_pOldSensorConf[5]);
            Math3d::Invert(mTemp2, mTemp3);
            Math3d::MulMatMat(mTemp1, mTemp3, mTemp4);  // this is the trafo from old to new sensor rotation
            Math3d::SetRotationMat(mTemp1, m_pOldFinalConf[3], m_pOldFinalConf[4], m_pOldFinalConf[5]);
            Math3d::MulMatMat(mTemp4, mTemp1, mTemp2);  // this is the new rotation matrix
            ExtractAnglesFromRotationMatrix(mTemp2, vTemp1);
            m_pPredictedConf[3] = vTemp1.x;
            m_pPredictedConf[4] = vTemp1.y;
            m_pPredictedConf[5] = vTemp1.z;
        }


        // **********************************************************************
        //  apply particle filter
        // **********************************************************************

        if ((!m_bFirstRun)  && (m_nPredictionMode >= 1)  && !bNan)
        {
            m_pParticleFilter->SetConfigOfATenthOfTheParticles(0, m_pSensorConf);
            m_pParticleFilter->SetConfigOfATenthOfTheParticles(1, m_pSensorConf);
            m_pParticleFilter->SetConfigOfATenthOfTheParticles(2, m_pSensorConf);
            m_pParticleFilter->SetConfigOfATenthOfTheParticles(3, m_pSensorConf);
            m_pParticleFilter->SetConfigOfATenthOfTheParticles(4, m_pSensorConf);
            m_pParticleFilter->SetConfigOfATenthOfTheParticles(5, m_pPredictedConf);
            m_pParticleFilter->SetConfigOfATenthOfTheParticles(6, m_pPredictedConf);
            m_pParticleFilter->SetConfigOfATenthOfTheParticles(7, m_pPredictedConf);
            m_pParticleFilter->SetConfigOfATenthOfTheParticles(8, m_pPredictedConf);
            m_pParticleFilter->SetConfigOfATenthOfTheParticles(9, m_pPredictedConf);
        }
        else
        {
            m_pParticleFilter->SetParticleConfig(m_pSensorConf);
        }


        double dAnnFact = 1.0; // 1.0
        double dAnnRedFactor = 0.5;
        if (m_nNumAnnealingRuns > 4)
        {
            dAnnRedFactor = pow(0.1, 1 / (m_nNumAnnealingRuns - 1));    // => annealing factor is never < 0.1
        }
        for (int i = 0; i < m_nNumAnnealingRuns; i++)
        {
            ARMARX_VERBOSE_S << "PF: Iteration " << i + 1 << " of " << m_nNumAnnealingRuns;
            m_pParticleFilter->ParticleFilter(m_pPFConf, m_dVarianceFactor * dAnnFact);
            dAnnFact *= dAnnRedFactor;
        }

        //m_pParticleFilter->GetConfiguration(m_pPFConf, 1.0); // get average of particles that are better than 1.0 times the average
        //m_pParticleFilter->GetBestConfiguration(m_pPFConf);

        m_pParticleFilter->GetDetailedRating(m_pPFConf, &m_PFRating);
        m_dRating = (0.5 + 0.5 * m_PFRating.dConfidenceOfRating) * m_PFRating.dRating;


        // **********************************************************************
        //  if prediction is used, calculate weighted combination of
        //  PF estimation and predicted config
        // **********************************************************************

        configMutex.lock();

        if (m_nPredictionMode == 0 || bNan)
        {
            for (int i = 0; i < DSHT_NUM_PARAMETERS; i++)
            {
                m_pFinalConf[i] = m_pPFConf[i];
            }
        }
        else if (m_nPredictionMode == 1 || m_nPredictionMode == 2)
        {
            ARMARX_VERBOSE_S << "Refining particle filter results with prediction";

            m_dOldRating *= 0.9;

            // TODO: include movement credibility rating
            double dMovementRating = 1;


            if (m_bFirstRun)
            {
                for (int i = 0; i < DSHT_NUM_PARAMETERS; i++)
                {
                    m_pFinalConf[i] = m_pPFConf[i];
                }
                if (m_dRating == 0)
                {
                    m_dRating = 0.001;
                }
            }
            else if (m_dRating < 0.05 && false) // if new estimation is bad, use predicted config
            {
                for (int i = 0; i < DSHT_NUM_PARAMETERS; i++)
                {
                    m_pFinalConf[i] = m_pPredictedConf[i];
                }
            }
            else    // else use weighted combination of new and predicted config
            {
                const float fWeightingFactor = 3.0;
                double dWeight = (fWeightingFactor * m_dRating * dMovementRating / (m_dOldRating + fWeightingFactor * m_dRating * dMovementRating));

                // hand position
                for (int i = 0; i < 3; i++)
                {
                    m_pFinalConf[i] = dWeight * m_pPFConf[i] + (1 - dWeight) * m_pPredictedConf[i];
                }

                // hand rotation
                Math3d::SetRotationMat(mTemp1, m_pPredictedConf[3], m_pPredictedConf[4], m_pPredictedConf[5]);
                Math3d::SetRotationMat(mTemp2, m_pPFConf[3], m_pPFConf[4], m_pPFConf[5]);
                Math3d::Invert(mTemp1, mTemp3);
                Math3d::MulMatMat(mTemp2, mTemp3, mTemp4); // this is the trafo from predicted to PF rotation
                Math3d::GetAxisAndAngle(mTemp4, vTemp1, fTemp1);
                fTemp1 *= dWeight;
                Math3d::SetRotationMatAxis(mTemp3, vTemp1, fTemp1); // trafo with reduced angle
                Math3d::MulMatMat(mTemp3, mTemp1, mTemp2);      // this is the new rotation matrix
                ExtractAnglesFromRotationMatrix(mTemp2, vTemp1);
                m_pFinalConf[3] = vTemp1.x;
                m_pFinalConf[4] = vTemp1.y;
                m_pFinalConf[5] = vTemp1.z;

                // finger joints
                for (int i = 6; i < DSHT_NUM_PARAMETERS; i++)
                {
                    m_pFinalConf[i] = dWeight * m_pPFConf[i] + (1 - dWeight) * m_pPredictedConf[i];
                }

            }


            if (m_nPredictionMode == 2)
            {
                // refine result
                m_pParticleFilter->SetParticleConfig(m_pFinalConf);
                m_pParticleFilter->ParticleFilter((double*)pTempConf, 0.05, m_pParticleFilter->GetNumberOfParticles() / 4);
                for (int i = 0; i < DSHT_NUM_PARAMETERS; i++)
                {
                    m_pFinalConf[i] = 0.7 * pTempConf[i] + 0.3 * m_pFinalConf[i];
                }
                // update rating
                m_pParticleFilter->GetDetailedRating(m_pFinalConf, &m_PFRating);
                m_dRating = (0.5 + 0.5 * m_PFRating.dConfidenceOfRating) * m_PFRating.dRating;
            }

        }



        // **********************************************************************
        //  return estimation, update old configs
        // **********************************************************************

        for (int i = 0; i < DSHT_NUM_PARAMETERS; i++)
        {
            pEstimatedConfig[i] = m_pFinalConf[i];
            m_pOldFinalConf[i] = m_pFinalConf[i];
            m_pOldSensorConf[i] = m_pSensorConf[i];
            m_pOldPFConf[i] = m_pPFConf[i];
        }
        m_dOldRating = m_dRating;

        configMutex.unlock();

        m_bFirstRun = false;

    }
}
