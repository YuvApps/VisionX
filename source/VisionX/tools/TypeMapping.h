/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <string>

#include <Eigen/Eigen>

// IVT
#include <Structs/Structs.h>
#include <Structs/ObjectDefinitions.h>
#include <Image/ImageProcessor.h>
#include <Interfaces/VideoCaptureInterface.h>

// VisionXInterface
#include <VisionX/interface/core/DataTypes.h>
#include <VisionX/interface/components/Calibration.h>
#include <VisionX/interface/units/ObjectRecognitionUnit.h>

#include "exceptions/local/InvalidImageTypeNameException.h"
#include "exceptions/local/UnsupportedImageTypeException.h"
#include "exceptions/local/InvalidFrameRateException.h"

class CByteImage;
class CCalibration;
class CStereoCalibration;

namespace visionx::tools
{
    /*
     * Converts a vector of the type T into an array of the Type T
     *
     * @param data                      Data vector
     *
     * @return                          T* filled with the elements taken from <em>data</em>
     */
    template <typename T>
    T* vectorToArray(const std::vector<T>& dataVector)
    {
        T* dataArray = new T[dataVector.size()];

        memcpy(dataArray, &dataVector[0], sizeof(T) * dataVector.size());

        return dataArray;
    }

    /*
     * Converts an array of the type T into a vector of the type T
     *
     * @param data                      Input data array
     * @param data                      Output vec
     */
    template <typename T>
    void arrayToVector(const T* dataArray, const int size, std::vector<T>& vec)
    {
        vec.clear();
        vec.resize(size);

        memcpy(&vec[0], dataArray, sizeof(T) * size);
    }

    /**
     * Converts an image type name as string into an ImageType
     * integer.
     *
     * @param imageTypeName        Image type string
     *
     * @return                     VisionX::ImageType if the image type name is valid
     */
    ImageType typeNameToImageType(const std::string& imageTypeName);

    /**
     * Converts an image type into a string
     * integer.
     *
     * @param imageType        Image type
     *
     * @return                 Type name string
     */
    std::string imageTypeToTypeName(const ImageType imageType);


    /**
     * Converts a VisionX image type into an image type of IVT's
     * ByteImage.
     *
     * @param visionXImageType     VisionX image type
     *
     * @return                     ByteImage::ImageType
     */
    CByteImage::ImageType convert(const ImageType visionxImageType);

    /**
     * Converts a VisionX BayerPatternType into a IVT's image processor BayerPatternType
     *
     * @param visionxBayerPatternType       VisionX Bayer pattern type
     *
     * @return                              ImageProcessor::BayerPatternType
     */
    ::ImageProcessor::BayerPatternType convert(const BayerPatternType visionxBayerPatternType);

    /**
     * Converts a VisionX image dimension object into an IVT VideoMode of IVT's
     * CVideoCaptureInterface.
     *
     * @param imageDimension     VisionX image dimension which consist of image width and height
     *
     * @return                   According CVideoCaptureInterface::VideoMode if a valid dimension was passed
     *                           otherwise CVideoCaptureInterface::eNone is returned.
     */
    CVideoCaptureInterface::VideoMode convert(const ImageDimension& imageDimension);

    /**
     * Converts frame rate floats into distinct IVT FrameRates by applying quantization.
     *
     * @param frameRate         frame rate
     *
     * @return                  Closest CVideoCaptureInterface::FrameRate
     */
    CVideoCaptureInterface::FrameRate convert(const float frameRate);

    /**
     * Converts a VisionX Vector (float STL Vector) into an IVT Vec2d
     *
     * @param vec               VisionX vector <em>pointer</em>!
     *
     * @return                  IVT 2D Vector
     */
    Vec2d convert(const visionx::types::Vec* pVec);

    /**
     * Converts a VisionX Vector (float STL Vector) into an IVT Vec3d
     *
     * @param vec               VisionX vector <em>reference</em>
     *
     * @return                  IVT 3D Vector
     */
    Vec3d convert(const visionx::types::Vec& vec);

    /**
     * Converts a VisionX Matrix (float STL Vectors) into an IVT Mat3d
     *
     * @param vec               VisionX matrix
     *
     * @return                  IVT 3D Matrix
     */
    Mat3d convert(const visionx::types::Mat& mat);

    /**
     * Converts a VisionX StereoCalibration object into an IVT CStereoCalibration
     *
     * @param stereoCalibration         VisionX Stereo Calibration struct
     *
     * @return                          IVT CStereoCalibration
     */
    CStereoCalibration* convert(const visionx::StereoCalibration& stereoCalibration);

    /**
     * Converts a VisionX MonocularCalibration object into an IVT CCalibration
     *
     * @param calibration               VisionX Stereo Calibration struct
     *
     * @return                          IVT CCalibration
     */
    CCalibration* convert(const visionx::MonocularCalibration& calibration);

    /**
     * Mapps an IVT Object type id onto a VisionX object type id.
     *
     * @param objectType        VisionX object type id
     *
     * @return                  IVT object type id
     */
    ObjectType convert(const visionx::types::ObjectType& objectType);

    /**
     * Mapps an IVT Object color id onto a VisionX object color id.
     *
     * @param objectColor       VisionX object color id
     *
     * @return                  IVT object color id
     */
    ObjectColor convert(const visionx::types::ObjectColor& objectColor);

    /**
     * Converts a VisionX MyRegion object into an IVT MyRegion instance
     *
     * @parma region                    VisionX MyRegion object
     *
     * @return                          IVT MyRegion instance
     */
    MyRegion convert(const visionx::types::Region& region);

    /**
     * Converts a VisionX Transformation3d object into an IVT Transformation3d object
     *
     * @parma transformation3d          VisionX Transformation3d object
     *
     * @return                          IVT Transformation3d instance
     */
    Transformation3d convert(const visionx::types::Transformation3d& transformation3d);

    /**
     * Converts a VisionX ObjectEntry object into an IVT Object3DEntry instance
     *
     * @param objectEntry       VisionX ObjectEntry object
     *
     * @return                  IVT Object3DEntry object
     */
    Object3DEntry convert(const visionx::types::Object3DEntry& object3DEntry);

    /**
     * Converts an IVT Object3DList object into a VisionX Object3DEntry instance
     *
     * @param objectList        VisionX Object3DEntry instance
     *
     * @return                  IVT Object3DList instance
     */
    Object3DList convert(const visionx::types::Object3DList& object3DList);

    /**
     * Converts an IVT Vec2d Vector into VisionX Vector (float STL Vector)
     *
     * @param vec               IVT 2D Vector
     *
     * @return                  VisionX vector
     */
    visionx::types::Vec convert(const Vec2d& vec);

    /**
     * Converts an IVT Vec3d Vector into VisionX Vector (float STL Vector)
     *
     * @param vec               IVT 3D Vector
     *
     * @return                  VisionX vector
     */
    visionx::types::Vec convert(const Vec3d& vec);

    /**
     * Converts an IVT Mat3d into VisionX Matrix (float STL Vectors)
     *
     * @param vec               IVT 3D Matrix
     *
     * @return                  VisionX matrix
     */
    visionx::types::Mat convert(const Mat3d& mat);

    /**
     * Converts an IVT CStereoCalibration into a VisionX StereoCalibration struct
     *
     * @param ivtStereoCalibration      IVT StereoCalibration
     *
     * @return                          VisionX StereoCalibration object
     */
    visionx::StereoCalibration convert(const CStereoCalibration& ivtStereoCalibration);

    /**
     * Converts an IVT CCalibration into a VisionX MonocularCalibration struct
     *
     * @param ivtCalibration            IVT Calibration
     *
     * @return                          VisionX StereoCalibration object
     */
    visionx::MonocularCalibration convert(const CCalibration& ivtCalibration);

    /**
     * Converts an IVT MyRegion into a VisionX MyRegion
     *
     * @param ivtRegion                 IVT MyRegion struct instance
     *
     * @return                          VisionX MyRegion object
     */
    visionx::types::Region convert(const MyRegion& ivtRegion);

    /**
     * Converts an IVT Transformation3d into a VisionX Transformation3d
     *
     * @param ivtTransformation3d       IVT Transformation3d struct instance
     *
     * @return                          VisionX Transformation3d object
     */
    visionx::types::Transformation3d convert(const Transformation3d& ivtTransformation3d);

    /**
     * Mapps an IVT Object color id onto a VisionX object color id.
     *
     * @param ivtObjectColor    IVT Object color id
     *
     * @return                  VisionX Object color id
     */
    visionx::types::ObjectColor convert(const ObjectColor& ivtObjectColor);

    /**
     * Mapps an IVT Object type id onto a VisionX object type id.
     *
     * @param ivtObjectType     IVT Object type id
     *
     * @return                  VisionX Object type id
     */
    visionx::types::ObjectType convert(const ObjectType& ivtObjectType);

    /**
     * Converts an IVT Object3DEntry id into a VisionX ObjectEntry
     *
     * @param ivtObject3DEntry  IVT Object3DEntry
     *
     * @return                  VisionX ObjectEntry
     */
    visionx::types::Object3DEntry convert(const Object3DEntry& ivtObject3DEntry);

    /**
     * Converts an IVT Object3DList object into an VisionX ObjectList
     *
     * @param ivtObject3DList   IVT Object3DList instance
     *
     * @return                  VisionX ObjectList
     */
    visionx::types::Object3DList convert(const Object3DList& ivtObject3DList);

    Eigen::Matrix3f convertIVTtoEigen(const Mat3d m);
    Eigen::Vector3f convertIVTtoEigen(const Vec3d v);

    visionx::types::Vec convertEigenVecToVisionX(Eigen::VectorXf v);

    visionx::types::Mat convertEigenMatToVisionX(Eigen::MatrixXf m);
}


