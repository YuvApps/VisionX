#include "OpenCVUtil.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <Image/ByteImage.h>
#include <cstring>

namespace visionx
{

    void copyCvMatToIVT(cv::Mat const& input, CByteImage* output)
    {
        ARMARX_CHECK_NOT_NULL(input.data);
        ARMARX_CHECK_NOT_NULL(output);
        ARMARX_CHECK_EXPRESSION(input.isContinuous());
        ARMARX_CHECK_EQUAL((int)input.elemSize(), output->bytesPerPixel);

        int imageSize = output->width * output->height * output->bytesPerPixel;
        ARMARX_CHECK_EQUAL((int)(input.total() * input.elemSize()), imageSize);

        std::memcpy(output->pixels, input.data, imageSize);
    }

    void copyCvDepthToGrayscaleIVT(cv::Mat const& inputInMeters, CByteImage* output)
    {
        ARMARX_CHECK_NOT_NULL(inputInMeters.data);
        ARMARX_CHECK_NOT_NULL(output);
        ARMARX_CHECK_EQUAL(inputInMeters.type(), CV_32FC1);
        ARMARX_CHECK_EQUAL(output->type, CByteImage::eRGB24);
        ARMARX_CHECK_EQUAL(inputInMeters.rows, output->height);
        ARMARX_CHECK_EQUAL(inputInMeters.cols, output->width);

        uchar* outRow = output->pixels;
        int outStride = 3 * output->width;

        double minD, maxD;
        cv::minMaxLoc(inputInMeters, &minD, &maxD);

        float invRangeD = 1.0 / (maxD - minD);

        for (int y = 0; y < inputInMeters.rows; ++y)
        {
            float const* depthRow = inputInMeters.ptr<float>(y);
            for (int x = 0; x < inputInMeters.cols; ++x)
            {
                float depth = depthRow[x];
                float normalizedDepth = (depth - minD) * invRangeD;
                int pixelValue = std::floor(255.0f - 255.0f * normalizedDepth);

                outRow[3 * x + 0] = pixelValue;
                outRow[3 * x + 1] = pixelValue;
                outRow[3 * x + 2] = pixelValue;
            }

            outRow += outStride;
        }
    }

    cv::Mat convertWeirdArmarXToDepthInMeters(CByteImage const* input)
    {
        ARMARX_CHECK_NOT_NULL(input);
        ARMARX_CHECK_EQUAL(input->type, CByteImage::eRGB24);

        int width = input->width;
        int height = input->height;
        cv::Mat result(height, width, CV_32FC1);

        unsigned char* inputRow = input->pixels;
        for (int y = 0; y  < height; ++y)
        {
            float* outputRow = result.ptr<float>(y);
            for (int x = 0; x < width; ++x)
            {
                // Depth is stored in mm (lower 8 bits in r, higher 8 bits g)
                int r = inputRow[x * 3 + 0];
                int g = inputRow[x * 3 + 1];
                int depthInMM = (g << 8) + r;
                outputRow[x] = depthInMM / 1000.0f;
            }

            inputRow += width * 3;
        }

        return result;
    }

    void convertDepthInMetersToWeirdArmarX(const cv::Mat& input, CByteImage* output)
    {
        ARMARX_CHECK_NOT_NULL(output);
        ARMARX_CHECK_EQUAL(output->type, CByteImage::eRGB24);
        ARMARX_CHECK_EQUAL(output->width, input.cols);
        ARMARX_CHECK_EQUAL(output->height, input.rows);
        ARMARX_CHECK_EQUAL(CV_32FC1, input.type());

        const auto height = output->height;
        const auto width = output->width;

        unsigned char* outputRow = output->pixels;
        for (int y = 0; y  < height; ++y)
        {
            const float* inputRow = input.ptr<float>(y);
            for (int x = 0; x < width; ++x)
            {
                const auto depthInMM = inputRow[x] * 1000;
                // Depth is stored in mm (lower 8 bits in r, higher 8 bits g)
                outputRow[x * 3 + 0] = static_cast<unsigned char>(std::fmod(depthInMM, 256));
                outputRow[x * 3 + 1] = static_cast<unsigned char>(std::fmod(depthInMM / 256, 256));
                outputRow[x * 3 + 2] = static_cast<unsigned char>(std::fmod(depthInMM / 256 / 256, 256));
            }

            inputRow += width * 3;
        }
    }
}
