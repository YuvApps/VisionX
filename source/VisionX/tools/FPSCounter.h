/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <list>
#include <boost/thread/mutex.hpp>

namespace visionx
{
    ///////////////////////////////////////////
    // definition of class FPSCounter
    ///////////////////////////////////////////
    /**
    * The FPSCounter class provides methods for calculating the frames per second (FPS) count
    * in periodic tasks. Further synchronization to a given FPS is supported.
    */
    class FPSCounter
    {
    public:
        /**
        * Constructs a new FPS counter
        *
        * @param nDelayFrames number of frames to use for FPS calculation. The
        *        FPS calculation will be delayed by this amount of frames
        */
        FPSCounter(int nDelayFrames = 10);

        ///////////////////////////////////////////
        // control
        ///////////////////////////////////////////
        /**
        * Resets the FPS counter to its initial state
        */
        void reset();

        /**
        * Updates the FPS counter
        *
        * Using this method in a periodic task will measure the time elapsed from one call
        * to the next. Based on this time, the FPS is calculated.
        */
        void update();

        /**
        * recalculates the FPS statistics
        */
        void recalculate();

        /**
        * Synchronize to FPS
        *
        * Using this method in a periodic task will synchronize the task to the
        * specified FPS.
        * Update is called internally, so do not call update at a different position.
        *
        * @param fFrameRate frames per second to use for synchronization
        */
        void assureFPS(float fFrameRate);

        ///////////////////////////////////////////
        // getter
        ///////////////////////////////////////////
        /**
        * Get if calculated values are valid
        *
        * Calculated values are valid if the minimum amount of delay frames has passed (m_nDelayFrames, see constructor)
        * @return calculated values valid
        */
        bool getValid();

        /**
        * Get frames per second
        *
        * @return frames per second measure over the last nDelayTime frames (see constructor)
        */
        float getFPS();

        /**
        * Get number of updates
        *
        * @return number of updates or assureFPS calls performed since start or last reset
        */
        int getUpdates();

        /**
        * Get mean cycle time over last 10 frames
        *
        * @return mean cycle time in ms
        */
        float getMeanCycleTimeMS();

        /**
        * Get minimum cycle time since start
        *
        * @return minimum cycle time in ms
        */
        float getMinCycleTimeMS();

        /**
        * Get maximum cycle time since start
        *
        * @return maximum cycle time in ms
        */
        float getMaxCycleTimeMS();

    private:
        // private methods
        void updateMembers(int nCycleTime);
        void recalculateFPS(int nCycleTime);
        void recalculateStats(int nCycleTime);

        int calculateTimeDiff(long& nSec, long& nUSec, bool bSetTime = false);

        // settings
        int                m_nDelayFrames;

        // times
        long            m_nCycleSec;
        long            m_nCycleUSec;
        int                m_nIntervalTime;

        // results
        int                m_nUpdates;
        float            m_fFPS;
        float            m_fMinCycleTimeMS;
        float            m_fMaxCycleTimeMS;
        float            m_fLastCycleTimesMS[10];
    };
}

