/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools::Tests
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::tools::TypeMapping::VectorToArray
#define ARMARX_BOOST_TEST
#include <VisionX/Test.h>

// STL
#include <vector>

// VisionXTools
#include <VisionX/tools/TypeMapping.h>

// Fixture use
struct VectorArrayTestFixture
{
    VectorArrayTestFixture() :
        intVector(2), intArray(0)
    {
        BOOST_TEST_MESSAGE("setup VectorArrayTestFixture");

        intVector[0] = 0;
        intVector[1] = 1;

        intArray = visionx::tools::vectorToArray(intVector);
    }

    ~VectorArrayTestFixture()
    {
        BOOST_TEST_MESSAGE("teardown VectorArrayTestFixture");

        if (intArray)
        {
            delete intArray;
        }
    }

    std::vector<int> intVector;
    int* intArray;
};


BOOST_FIXTURE_TEST_CASE(ArrayContentTest, VectorArrayTestFixture)
{
    BOOST_CHECK(intArray[0] == 0);
    BOOST_CHECK(intArray[1] == 1);
}


BOOST_FIXTURE_TEST_CASE(ArrayVectorEqualityTest, VectorArrayTestFixture)
{
    BOOST_CHECK(intArray[0] == intVector[0]);
    BOOST_CHECK(intArray[1] == intVector[1]);
}
