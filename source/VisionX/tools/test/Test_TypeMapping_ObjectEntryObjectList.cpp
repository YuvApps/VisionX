/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools::Tests
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#define BOOST_TEST_MODULE VisionX::tools::TypeMapping::ObjectEntry
#define ARMARX_BOOST_TEST
#include <VisionX/Test.h>

// IVT
#include <Structs/Structs.h>
#include <Structs/ObjectDefinitions.h>

// VisionXInterface
#include <VisionX/interface/units/ObjectRecognitionUnit.h>

// VisionXTools
#include <VisionX/tools/TypeMapping.h>

// Fixture use
struct Object3DEntryFixture
{
    Object3DEntryFixture()
    {
        BOOST_TEST_MESSAGE("setup Object3DEntryFixture");

        ivtObject3DEntryInstance.class_id = 1;

        ivtObject3DEntryInstance.color = eYellow3;

        ivtObject3DEntryInstance.orientation.x = 1;
        ivtObject3DEntryInstance.orientation.y = 2;
        ivtObject3DEntryInstance.orientation.z = 3;

        ivtObject3DEntryInstance.pose.translation.x = 11;
        ivtObject3DEntryInstance.pose.translation.y = 12;
        ivtObject3DEntryInstance.pose.translation.z = 13;
        ivtObject3DEntryInstance.pose.rotation.r1 = 101;
        ivtObject3DEntryInstance.pose.rotation.r2 = 102;
        ivtObject3DEntryInstance.pose.rotation.r3 = 103;
        ivtObject3DEntryInstance.pose.rotation.r4 = 104;
        ivtObject3DEntryInstance.pose.rotation.r5 = 105;
        ivtObject3DEntryInstance.pose.rotation.r6 = 106;
        ivtObject3DEntryInstance.pose.rotation.r7 = 107;
        ivtObject3DEntryInstance.pose.rotation.r8 = 108;
        ivtObject3DEntryInstance.pose.rotation.r9 = 109;

        ivtObject3DEntryInstance.quality = 0.55f;
        ivtObject3DEntryInstance.quality2 = 0.98f;

        ivtObject3DEntryInstance.region_id_left  = 1001;
        ivtObject3DEntryInstance.region_id_right = 1002;

        ivtObject3DEntryInstance.region_left.centroid.x = 10001;
        ivtObject3DEntryInstance.region_left.centroid.y = 10002;
        ivtObject3DEntryInstance.region_left.min_x = 10003;
        ivtObject3DEntryInstance.region_left.max_x = 10004;
        ivtObject3DEntryInstance.region_left.min_y = 10005;
        ivtObject3DEntryInstance.region_left.max_y = 10006;
        ivtObject3DEntryInstance.region_left.nPixels = 2;
        ivtObject3DEntryInstance.region_left.pPixels = new int[2];
        ivtObject3DEntryInstance.region_left.pPixels[0] = 10007;
        ivtObject3DEntryInstance.region_left.pPixels[1] = 10007;
        ivtObject3DEntryInstance.region_left.ratio = 0.33f;

        ivtObject3DEntryInstance.region_right.centroid.x = 100001;
        ivtObject3DEntryInstance.region_right.centroid.y     = 100002;
        ivtObject3DEntryInstance.region_right.min_x = 100003;
        ivtObject3DEntryInstance.region_right.max_x = 100004;
        ivtObject3DEntryInstance.region_right.min_y = 100005;
        ivtObject3DEntryInstance.region_right.max_y = 100006;
        ivtObject3DEntryInstance.region_right.nPixels = 2;
        ivtObject3DEntryInstance.region_right.pPixels = new int[2];
        ivtObject3DEntryInstance.region_right.pPixels[0] = 100007;
        ivtObject3DEntryInstance.region_right.pPixels[1] = 100007;
        ivtObject3DEntryInstance.region_right.ratio = 0.66f;

        ivtObject3DEntryInstance.sName = "TestObject3DEntryObject";

        ivtObject3DEntryInstance.type = eCompactObject;

        ivtObject3DEntryInstance.world_point.x = 1000001;
        ivtObject3DEntryInstance.world_point.y = 1000002;
        ivtObject3DEntryInstance.world_point.z = 1000003;

        ivtObject3DListInstance.resize(1);
        ivtObject3DListInstance[0] = ivtObject3DEntryInstance;
    }

    ~Object3DEntryFixture()
    {
        BOOST_TEST_MESSAGE("teardown Object3DEntryFixture");
    }

    Object3DEntry ivtObject3DEntryInstance;
    Object3DList ivtObject3DListInstance;


    visionx::types::Object3DEntry object3DEntry;
    visionx::types::Object3DEntry object3DEntryTemp;
    Object3DEntry ivtObject3DEntry;

    visionx::types::Object3DList object3DList;
    visionx::types::Object3DList object3DListTemp;
    Object3DList ivtObject3DList;
};


BOOST_FIXTURE_TEST_CASE(IvtObject3DEntryToVisionXObject3DEntry, Object3DEntryFixture)
{
    object3DEntry = visionx::tools::convert(ivtObject3DEntryInstance);

    BOOST_CHECK(object3DEntry.classId == 1);

    BOOST_CHECK(object3DEntry.color == visionx::types::eYellow3);

    BOOST_CHECK(object3DEntry.orientation[0] == 1);
    BOOST_CHECK(object3DEntry.orientation[1] == 2);
    BOOST_CHECK(object3DEntry.orientation[2] == 3);

    BOOST_CHECK(object3DEntry.pose.translation[0] == 11);
    BOOST_CHECK(object3DEntry.pose.translation[1] == 12);
    BOOST_CHECK(object3DEntry.pose.translation[2] == 13);
    BOOST_CHECK(object3DEntry.pose.rotation[0][0] == 101);
    BOOST_CHECK(object3DEntry.pose.rotation[0][1] == 102);
    BOOST_CHECK(object3DEntry.pose.rotation[0][2] == 103);
    BOOST_CHECK(object3DEntry.pose.rotation[1][0] == 104);
    BOOST_CHECK(object3DEntry.pose.rotation[1][1] == 105);
    BOOST_CHECK(object3DEntry.pose.rotation[1][2] == 106);
    BOOST_CHECK(object3DEntry.pose.rotation[2][0] == 107);
    BOOST_CHECK(object3DEntry.pose.rotation[2][1] == 108);
    BOOST_CHECK(object3DEntry.pose.rotation[2][2] == 109);

    BOOST_CHECK(object3DEntry.quality == 0.55f);
    BOOST_CHECK(object3DEntry.quality2 == 0.98f);

    BOOST_CHECK(object3DEntry.regionIdLeft  == 1001);
    BOOST_CHECK(object3DEntry.regionIdRight == 1002);

    BOOST_CHECK(object3DEntry.regionLeft.centroid[0] == 10001);
    BOOST_CHECK(object3DEntry.regionLeft.centroid[1] == 10002);
    BOOST_CHECK(object3DEntry.regionLeft.minX == 10003);
    BOOST_CHECK(object3DEntry.regionLeft.maxX == 10004);
    BOOST_CHECK(object3DEntry.regionLeft.minY == 10005);
    BOOST_CHECK(object3DEntry.regionLeft.maxY == 10006);
    BOOST_CHECK(object3DEntry.regionLeft.nPixels == 2);
    BOOST_CHECK(object3DEntry.regionLeft.pixels[0] == 10007);
    BOOST_CHECK(object3DEntry.regionLeft.pixels[1] == 10007);
    BOOST_CHECK(object3DEntry.regionLeft.ratio == 0.33f);

    BOOST_CHECK(object3DEntry.regionRight.centroid[0] == 100001);
    BOOST_CHECK(object3DEntry.regionRight.centroid[1] == 100002);
    BOOST_CHECK(object3DEntry.regionRight.minX == 100003);
    BOOST_CHECK(object3DEntry.regionRight.maxX == 100004);
    BOOST_CHECK(object3DEntry.regionRight.minY == 100005);
    BOOST_CHECK(object3DEntry.regionRight.maxY == 100006);
    BOOST_CHECK(object3DEntry.regionRight.nPixels == 2);
    BOOST_CHECK(object3DEntry.regionRight.pixels[0] == 100007);
    BOOST_CHECK(object3DEntry.regionRight.pixels[1] == 100007);
    BOOST_CHECK(object3DEntry.regionRight.ratio == 0.66f);

    BOOST_CHECK(object3DEntry.name.compare("TestObject3DEntryObject") == 0);

    BOOST_CHECK(object3DEntry.type == visionx::types::eCompactObject);

    BOOST_CHECK(object3DEntry.worldPoint[0] == 1000001);
    BOOST_CHECK(object3DEntry.worldPoint[1] == 1000002);
    BOOST_CHECK(object3DEntry.worldPoint[2] == 1000003);
}


BOOST_FIXTURE_TEST_CASE(VisionXObject3DEntryToIvtObject3DEntry, Object3DEntryFixture)
{
    object3DEntryTemp = visionx::tools::convert(ivtObject3DEntryInstance);
    ivtObject3DEntry = visionx::tools::convert(object3DEntryTemp);

    BOOST_CHECK(ivtObject3DEntry.class_id == 1);

    BOOST_CHECK(ivtObject3DEntry.color == eYellow3);

    BOOST_CHECK(ivtObject3DEntry.orientation.x == 1);
    BOOST_CHECK(ivtObject3DEntry.orientation.y == 2);
    BOOST_CHECK(ivtObject3DEntry.orientation.z == 3);

    BOOST_CHECK(ivtObject3DEntry.pose.translation.x == 11);
    BOOST_CHECK(ivtObject3DEntry.pose.translation.y == 12);
    BOOST_CHECK(ivtObject3DEntry.pose.translation.z == 13);
    BOOST_CHECK(ivtObject3DEntry.pose.rotation.r1 == 101);
    BOOST_CHECK(ivtObject3DEntry.pose.rotation.r2 == 102);
    BOOST_CHECK(ivtObject3DEntry.pose.rotation.r3 == 103);
    BOOST_CHECK(ivtObject3DEntry.pose.rotation.r4 == 104);
    BOOST_CHECK(ivtObject3DEntry.pose.rotation.r5 == 105);
    BOOST_CHECK(ivtObject3DEntry.pose.rotation.r6 == 106);
    BOOST_CHECK(ivtObject3DEntry.pose.rotation.r7 == 107);
    BOOST_CHECK(ivtObject3DEntry.pose.rotation.r8 == 108);
    BOOST_CHECK(ivtObject3DEntry.pose.rotation.r9 == 109);

    BOOST_CHECK(ivtObject3DEntry.quality == 0.55f);
    BOOST_CHECK(ivtObject3DEntry.quality2 == 0.98f);

    BOOST_CHECK(ivtObject3DEntry.region_id_left  == 1001);
    BOOST_CHECK(ivtObject3DEntry.region_id_right == 1002);

    BOOST_CHECK(ivtObject3DEntry.region_left.centroid.x == 10001);
    BOOST_CHECK(ivtObject3DEntry.region_left.centroid.y == 10002);
    BOOST_CHECK(ivtObject3DEntry.region_left.min_x == 10003);
    BOOST_CHECK(ivtObject3DEntry.region_left.max_x == 10004);
    BOOST_CHECK(ivtObject3DEntry.region_left.min_y == 10005);
    BOOST_CHECK(ivtObject3DEntry.region_left.max_y == 10006);
    BOOST_CHECK(ivtObject3DEntry.region_left.nPixels == 2);
    BOOST_CHECK(ivtObject3DEntry.region_left.pPixels[0] == 10007);
    BOOST_CHECK(ivtObject3DEntry.region_left.pPixels[1] == 10007);
    BOOST_CHECK(ivtObject3DEntry.region_left.ratio == 0.33f);

    BOOST_CHECK(ivtObject3DEntry.region_right.centroid.x == 100001);
    BOOST_CHECK(ivtObject3DEntry.region_right.centroid.y == 100002);
    BOOST_CHECK(ivtObject3DEntry.region_right.min_x == 100003);
    BOOST_CHECK(ivtObject3DEntry.region_right.max_x == 100004);
    BOOST_CHECK(ivtObject3DEntry.region_right.min_y == 100005);
    BOOST_CHECK(ivtObject3DEntry.region_right.max_y == 100006);
    BOOST_CHECK(ivtObject3DEntry.region_right.nPixels == 2);
    BOOST_CHECK(ivtObject3DEntry.region_right.pPixels[0] == 100007);
    BOOST_CHECK(ivtObject3DEntry.region_right.pPixels[1] == 100007);
    BOOST_CHECK(ivtObject3DEntry.region_right.ratio == 0.66f);

    BOOST_CHECK(ivtObject3DEntry.sName.compare("TestObject3DEntryObject") == 0);

    BOOST_CHECK(ivtObject3DEntry.type == eCompactObject);

    BOOST_CHECK(ivtObject3DEntry.world_point.x == 1000001);
    BOOST_CHECK(ivtObject3DEntry.world_point.y == 1000002);
    BOOST_CHECK(ivtObject3DEntry.world_point.z == 1000003);
}


BOOST_FIXTURE_TEST_CASE(IvtObject3DLisToVisionXObject3DList, Object3DEntryFixture)
{
    object3DList = visionx::tools::convert(ivtObject3DListInstance);

    BOOST_CHECK(object3DList.size() == 1);
    BOOST_CHECK(object3DList[0].name.compare("TestObject3DEntryObject") == 0);
}


BOOST_FIXTURE_TEST_CASE(VisionXObject3DLisToIvtObject3DList, Object3DEntryFixture)
{
    object3DListTemp = visionx::tools::convert(ivtObject3DListInstance);
    ivtObject3DList = visionx::tools::convert(object3DListTemp);

    BOOST_CHECK(ivtObject3DList.size() == 1);
    BOOST_CHECK(ivtObject3DList[0].sName.compare("TestObject3DEntryObject") == 0);
}

