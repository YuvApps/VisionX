/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools::Tests
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#define BOOST_TEST_MODULE VisionX::tools::TypeMapping::Transformation3d
#define ARMARX_BOOST_TEST
#include <VisionX/Test.h>

// IVT
#include <Structs/Structs.h>
#include <Structs/ObjectDefinitions.h>

// VisionXTools
#include <VisionX/tools/TypeMapping.h>

// VisionXInterface
#include <VisionX/interface/units/ObjectRecognitionUnit.h>

// Fixture use
struct Transformation3DFixture
{
    Transformation3DFixture()
    {
        BOOST_TEST_MESSAGE("setup Transformation3DFixture");

        ivtTransformatio3DInstance.translation.x = 1;
        ivtTransformatio3DInstance.translation.y = 2;
        ivtTransformatio3DInstance.translation.z = 3;

        ivtTransformatio3DInstance.rotation.r1 = 11;
        ivtTransformatio3DInstance.rotation.r2 = 12;
        ivtTransformatio3DInstance.rotation.r3 = 13;
        ivtTransformatio3DInstance.rotation.r4 = 14;
        ivtTransformatio3DInstance.rotation.r5 = 15;
        ivtTransformatio3DInstance.rotation.r6 = 16;
        ivtTransformatio3DInstance.rotation.r7 = 17;
        ivtTransformatio3DInstance.rotation.r8 = 18;
        ivtTransformatio3DInstance.rotation.r9 = 19;
    }

    ~Transformation3DFixture()
    {
        BOOST_TEST_MESSAGE("teardown Transformation3DFixture");
    }

    Transformation3d ivtTransformatio3DInstance;
};


BOOST_FIXTURE_TEST_CASE(Transformation3dIvtToVisionX, Transformation3DFixture)
{
    visionx::types::Transformation3d transformation3d = visionx::tools::convert(ivtTransformatio3DInstance);

    BOOST_CHECK(transformation3d.translation[0] == 1);
    BOOST_CHECK(transformation3d.translation[1] == 2);
    BOOST_CHECK(transformation3d.translation[2] == 3);
    BOOST_CHECK(transformation3d.rotation[0][0] == 11);
    BOOST_CHECK(transformation3d.rotation[0][1] == 12);
    BOOST_CHECK(transformation3d.rotation[0][2] == 13);
    BOOST_CHECK(transformation3d.rotation[1][0] == 14);
    BOOST_CHECK(transformation3d.rotation[1][1] == 15);
    BOOST_CHECK(transformation3d.rotation[1][2] == 16);
    BOOST_CHECK(transformation3d.rotation[2][0] == 17);
    BOOST_CHECK(transformation3d.rotation[2][1] == 18);
    BOOST_CHECK(transformation3d.rotation[2][2] == 19);
}


BOOST_FIXTURE_TEST_CASE(Transformation3dVisionXToIvt, Transformation3DFixture)
{
    visionx::types::Transformation3d transformation3dTmp = visionx::tools::convert(ivtTransformatio3DInstance);
    Transformation3d ivtTransforamation3d = visionx::tools::convert(transformation3dTmp);

    BOOST_CHECK(ivtTransforamation3d.translation.x == 1);
    BOOST_CHECK(ivtTransforamation3d.translation.y == 2);
    BOOST_CHECK(ivtTransforamation3d.translation.z == 3);
    BOOST_CHECK(ivtTransforamation3d.rotation.r1 == 11);
    BOOST_CHECK(ivtTransforamation3d.rotation.r2 == 12);
    BOOST_CHECK(ivtTransforamation3d.rotation.r3 == 13);
    BOOST_CHECK(ivtTransforamation3d.rotation.r4 == 14);
    BOOST_CHECK(ivtTransforamation3d.rotation.r5 == 15);
    BOOST_CHECK(ivtTransforamation3d.rotation.r6 == 16);
    BOOST_CHECK(ivtTransforamation3d.rotation.r7 == 17);
    BOOST_CHECK(ivtTransforamation3d.rotation.r8 == 18);
    BOOST_CHECK(ivtTransforamation3d.rotation.r9 == 19);
}
