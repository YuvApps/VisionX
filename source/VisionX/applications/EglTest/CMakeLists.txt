armarx_component_set_name("EglTestApp")


find_package(OpenGL COMPONENTS OpenGL EGL)
armarx_build_if(OPENGL_FOUND "No OpenGL EGL found")


set(COMPONENT_LIBS
    OpenGL::EGL
    )

armarx_add_component_executable(main.cpp)
