/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar3::applications
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/application/Application.h>
#include <VisionX/components/AffordancePipelineVisualization/AffordancePipelineVisualization.h>

namespace armar3::applications
{
    class AffordancePipelineVisualizationApp :
        virtual public armarx::Application
    {
        void setup(const armarx::ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) override
        {
            registry->addObject(armarx::Component::create<armarx::AffordancePipelineVisualization>(properties));
        }
    };
}


