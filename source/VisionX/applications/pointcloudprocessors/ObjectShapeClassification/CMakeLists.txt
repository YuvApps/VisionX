armarx_component_set_name(ObjectShapeClassification)
set(COMPONENT_LIBS VisionXObjectShapeClassification)
set(EXE_SOURCE main.cpp ObjectShapeClassificationApp.h )
armarx_add_component_executable("${EXE_SOURCE}")
