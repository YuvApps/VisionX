/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::applications
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// STD/STL
#include <iostream>
#include <string>

// Boost
#include <filesystem>
#include <boost/program_options.hpp>

// OpenCV
#include <opencv2/core/core.hpp>

// VisionX
#include <VisionX/libraries/record.h>
#include <VisionX/libraries/playback.h>


/**
 * @brief Parses argv and writes the results to the out parameters description, help, progress, in_path, out_path
 * @param argc Number of arguments
 * @param argv Arguments array
 * @param description Description of the program, listing all arguments etc
 * @param help True, if help was requested while invoking the program
 * @param progress True, if progress reporting was requested while invoking the program
 * @param in_path Input path defined when invoking the program
 * @param out_path Output path defined when invoking the program
 */
void init_args(int argc, char* argv[], std::string& description, bool& help, bool& progress, std::filesystem::path& in_path, std::filesystem::path& out_path);


/**
 * @brief Performs the actual conversion
 * @param in Path to the input file
 * @param out Path to the output file
 */
void convert(const std::filesystem::path& in, const std::filesystem::path& out, bool print_progress);


int
main(int argc, char* argv[])
{
    std::string description = "";
    const std::string usage = "Usage: cvtrec input_file output_file\n"
                              "  input_file:  Path to input file\n"
                              "  output_file: Path to output file";
    bool help = false;
    bool progress = false;
    std::filesystem::path in_path = "";
    std::filesystem::path out_path = "";

    // Initialise arguments and options
    ::init_args(argc, argv, description, help, progress, in_path, out_path);

    // Print options for --help and exit
    if (help)
    {
        std::cout << "cvtrec - VisionX utility to convert recordings" << std::endl << std::endl;
        std::cout << usage << std::endl << std::endl;
        std::cout << "Example: (To convert a PNG image sequence to AVI)" << std::endl;
        std::cout << "  `cvtrec /path/to/img/seq/ ./videofile.avi`" << std::endl << std::endl;
        std::cout << description << std::endl;
        return EXIT_SUCCESS;
    }

    // Ensure mandatory arguments were set
    if (in_path == "" or out_path == "")
    {
        std::cerr << usage << std::endl;
        return EXIT_FAILURE;
    }

    // Ensure that target format is different from source format
    if (in_path.extension() == out_path.extension())
    {
        std::cerr << "Source and target formats are the same" << std::endl;
        return EXIT_FAILURE;
    }

    // Try running conversion
    try
    {
        ::convert(in_path, out_path, progress);
        return EXIT_SUCCESS;
    }
    catch (const std::exception& e)
    {
        if (progress)
        {
            std::cerr << std::endl;
        }
        std::cerr << "Error while converting: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}


void
init_args(int argc, char* argv[], std::string& description, bool& help, bool& progress, std::filesystem::path& in_path, std::filesystem::path& out_path)
{
    boost::program_options::options_description desc("Arguments and options");
    boost::program_options::variables_map vm;

    // Define options
    desc.add_options()
    ("help,h", "Show this message")
    ("progress,p", "Print information about the conversion progress")
    ("in", boost::program_options::value<std::filesystem::path>(&in_path), "Path to input file which should be converted")
    ("out", boost::program_options::value<std::filesystem::path>(&out_path), "Path to output file where the converted recording should be written to");

    // Designate positional parameters
    boost::program_options::positional_options_description pos;
    pos.add("in", 1);
    pos.add("out", 1);

    // Parse argv
    boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).positional(pos).run(), vm);
    boost::program_options::notify(vm);

    // Set description
    std::stringstream description_stream;
    desc.print(description_stream);
    description = description_stream.str();

    // Set help flag
    help = vm.find("help") != vm.end();

    // Set progress flag
    progress = vm.find("progress") != vm.end();
}


void
convert(const std::filesystem::path& in, const std::filesystem::path& out, bool print_progress)
{
    // Initialise playback, recording and frame_buffer
    visionx::playback::Playback playback = visionx::playback::newPlayback(in);
    if (playback == nullptr)
    {
        throw std::runtime_error("Could not read input file format");
    }
    visionx::record::Recording recording = visionx::record::newRecording(out, playback->getFps());
    cv::Mat frame_buffer;
    const unsigned int frame_count = playback->getFrameCount();

    // Loop over playback and write each frame into recording
    for (unsigned int i = 0; i < frame_count; ++i)
    {
        const bool success = playback->getNextFrame(frame_buffer);
        if (!success)
        {
            throw std::runtime_error("Could not read frame");
        }
        recording->recordFrame(frame_buffer);

        // Print progress
        if (print_progress)
        {
            std::cerr << "\r" << std::flush;
            std::cerr << "Converting... " << (i * 100 / (frame_count - 1)) << "%" << std::flush;
        }
    }
    recording->stopRecording();

    if (print_progress)
    {
        std::cerr << std::endl;
    }
}
