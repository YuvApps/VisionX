#include "OOBB.h"

#include "OOBB.hpp"

#ifdef CGAL_FOUND


simox::OrientedBox<float> armarx::calculate2dOOBB(const std::vector<Eigen::Vector3f>& points, const Eigen::Vector3f& dir)
{
    return calculate2dOOBB<Eigen::Vector3f>(points, dir.cast<double>()).cast<float>();
}
simox::OrientedBox<double> armarx::calculate2dOOBB(const std::vector<Eigen::Vector3d>& points, const Eigen::Vector3d& dir)
{
    return calculate2dOOBB<Eigen::Vector3d>(points, dir);
}

simox::OrientedBox<float> armarx::calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZ>& cloud, const Eigen::Vector3f& dir)
{
    return calculate2dOOBB<pcl::PointXYZ>(cloud.points, dir.cast<double>()).cast<float>();
}
simox::OrientedBox<double> armarx::calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZ>& cloud, const Eigen::Vector3d& dir)
{
    return calculate2dOOBB<pcl::PointXYZ>(cloud.points, dir);
}

simox::OrientedBox<float> armarx::calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZL>& cloud, const Eigen::Vector3f& dir)
{
    return calculate2dOOBB<pcl::PointXYZL>(cloud.points, dir.cast<double>()).cast<float>();
}
simox::OrientedBox<double> armarx::calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZL>& cloud, const Eigen::Vector3d& dir)
{
    return calculate2dOOBB<pcl::PointXYZL>(cloud.points, dir);
}

simox::OrientedBox<float> armarx::calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZRGB>& cloud, const Eigen::Vector3f& dir)
{
    return calculate2dOOBB<pcl::PointXYZRGB>(cloud.points, dir.cast<double>()).cast<float>();
}
simox::OrientedBox<double> armarx::calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZRGB>& cloud, const Eigen::Vector3d& dir)
{
    return calculate2dOOBB<pcl::PointXYZRGB>(cloud.points, dir);
}

simox::OrientedBox<float> armarx::calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZRGBA>& cloud, const Eigen::Vector3f& dir)
{
    return calculate2dOOBB<pcl::PointXYZRGBA>(cloud.points, dir.cast<double>()).cast<float>();
}
simox::OrientedBox<double> armarx::calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZRGBA>& cloud, const Eigen::Vector3d& dir)
{
    return calculate2dOOBB<pcl::PointXYZRGBA>(cloud.points, dir);
}

simox::OrientedBox<float> armarx::calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZRGBL>& cloud, const Eigen::Vector3f& dir)
{
    return calculate2dOOBB<pcl::PointXYZRGBL>(cloud.points, dir.cast<double>()).cast<float>();
}
simox::OrientedBox<double> armarx::calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZRGBL>& cloud, const Eigen::Vector3d& dir)
{
    return calculate2dOOBB<pcl::PointXYZRGBL>(cloud.points, dir);
}


#endif
