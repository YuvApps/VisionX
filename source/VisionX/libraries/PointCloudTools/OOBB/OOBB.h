#pragma once

#ifdef CGAL_FOUND

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <SimoxUtility/shapes/OrientedBox.h>
#include <SimoxUtility/shapes/XYConstrainedOrientedBox.h>


namespace armarx
{

    simox::OrientedBox<float> calculate2dOOBB(const std::vector<Eigen::Vector3f>& points, const Eigen::Vector3f& dir);
    simox::OrientedBox<double> calculate2dOOBB(const std::vector<Eigen::Vector3d>& points, const Eigen::Vector3d& dir);

    simox::OrientedBox<float> calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZ>& cloud, const Eigen::Vector3f& dir);
    simox::OrientedBox<double> calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZ>& cloud, const Eigen::Vector3d& dir);

    simox::OrientedBox<float> calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZL>& cloud, const Eigen::Vector3f& dir);
    simox::OrientedBox<double> calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZL>& cloud, const Eigen::Vector3d& dir);

    simox::OrientedBox<float> calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZRGB>& cloud, const Eigen::Vector3f& dir);
    simox::OrientedBox<double> calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZRGB>& cloud, const Eigen::Vector3d& dir);

    simox::OrientedBox<float> calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZRGBA>& cloud, const Eigen::Vector3f& dir);
    simox::OrientedBox<double> calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZRGBA>& cloud, const Eigen::Vector3d& dir);

    simox::OrientedBox<float> calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZRGBL>& cloud, const Eigen::Vector3f& dir);
    simox::OrientedBox<double> calculate2dOOBB(const pcl::PointCloud<pcl::PointXYZRGBL>& cloud, const Eigen::Vector3d& dir);

}


#endif
