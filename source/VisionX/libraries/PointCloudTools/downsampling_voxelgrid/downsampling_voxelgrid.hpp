#pragma once

#include <pcl/point_cloud.h>
#include <pcl/filters/voxel_grid.h>


namespace visionx::tools
{

    namespace detail
    {

        template <typename PointT, typename IndicesPtrT = pcl::IndicesConstPtr>
        typename pcl::PointCloud<PointT>::Ptr
        downsampleByVoxelGrid(typename pcl::PointCloud<PointT>::ConstPtr inputCloud,
                              float leafSize = 5.0,
                              const IndicesPtrT& indices = nullptr)
        {
            pcl::VoxelGrid<PointT> vg;

            vg.setLeafSize(leafSize, leafSize, leafSize);

            vg.setInputCloud(inputCloud);
            if (indices)
            {
                vg.setIndices(indices);
            }

            typename pcl::PointCloud<PointT>::Ptr output(new pcl::PointCloud<PointT>);
            vg.filter(*output);
            return output;
        }

    }

    template <typename PointT>
    typename pcl::PointCloud<PointT>::Ptr
    downsampleByVoxelGrid(typename pcl::PointCloud<PointT>::ConstPtr inputCloud, float leafSize = 5.0,
                          pcl::IndicesConstPtr indices = nullptr)
    {
        return detail::downsampleByVoxelGrid<PointT>(inputCloud, leafSize, indices);
    }
    template <typename PointT>
    typename pcl::PointCloud<PointT>::Ptr
    downsampleByVoxelGrid(typename pcl::PointCloud<PointT>::ConstPtr inputCloud, float leafSize = 5.0,
                          pcl::PointIndicesConstPtr indices = nullptr)
    {
        return detail::downsampleByVoxelGrid<PointT>(inputCloud, leafSize, indices);
    }

}
