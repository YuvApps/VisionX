#pragma once

#include <functional>
#include <type_traits>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <ArmarXCore/core/exceptions/local/UnexpectedEnumValueException.h>

#include <VisionX/interface/core/DataTypes.h>


namespace visionx::tools::detail
{
    /// Base class of functors with return value.
    struct FunctorWithReturnType {};


    template <PointContentType pointContentType>
    struct PclPointType
    {
        using PointT = pcl::PointXYZ;
    };
    template <> struct PclPointType<PointContentType::ePoints>
    {
        using PointT = pcl::PointXYZ;
    };
    template <> struct PclPointType<PointContentType::eIntensity>
    {
        using PointT = pcl::PointXYZI;
    };
    template <> struct PclPointType<PointContentType::eColoredPoints>
    {
        using PointT = pcl::PointXYZRGBA;
    };
    template <> struct PclPointType<PointContentType::eLabeledPoints>
    {
        using PointT = pcl::PointXYZL;
    };
    template <> struct PclPointType<PointContentType::eOrientedPoints>
    {
        using PointT = pcl::PointNormal;
    };
    template <> struct PclPointType<PointContentType::eColoredLabeledPoints>
    {
        using PointT = pcl::PointXYZRGBL;
    };
    template <> struct PclPointType<PointContentType::eColoredOrientedPoints>
    {
        using PointT = pcl::PointXYZRGBNormal;
    };
}

namespace visionx::tools
{
    /**
     * @brief Base class for functors with return value.
     * @param ReturnT The return type.
     */
    template <typename ReturnT = void>
    struct FunctorWithReturnType : public detail::FunctorWithReturnType
    {
        using ReturnType = ReturnT;
    };


    /**
     * @brief Call `functor` with the correct pcl point type according to `type`.
     *
     * The passed `functor` must have a member functor template, taking the
     * pcl point type as first template argument:
     *
     * @snippet VisionX/libraries/PointCloudTools/test/CallWithPointTypeTest.cpp FunctorDocumentation ExampleFunctor
     *
     * Construct your functor and pass it to `callWithPointType()` along with
     * the respective `PointContentType`:
     * @snippet VisionX/libraries/PointCloudTools/test/CallWithPointTypeTest.cpp FunctorDocumentation ExampleFunctor_usage
     *
     * Depending on the passed `PointContentType`, the functor's `operator()`
     * is called with the according `pcl::Point...` type as template argument.
     * This allows you to create `pcl::PointCloud<PointT>` with the correct type.
     *
     * In addition to `functor` and `type`, `callWithPointType()` can take
     * more arguments which will be passed to the functor's `operator()`:
     *
     * @snippet VisionX/libraries/PointCloudTools/test/CallWithPointTypeTest.cpp FunctorDocumentation ExampleFunctorTakingArguments
     * Usage:
     * @snippet VisionX/libraries/PointCloudTools/test/CallWithPointTypeTest.cpp FunctorDocumentation ExampleFunctorTakingArguments_usage
     *
     * If your functor returns a result, have it derive from `FunctorWithReturnType`,
     * specifying the return type as template argument:
     *
     * @snippet VisionX/libraries/PointCloudTools/test/CallWithPointTypeTest.cpp FunctorDocumentation ExampleFunctorReturningInt
     * Usage:
     * @snippet VisionX/libraries/PointCloudTools/test/CallWithPointTypeTest.cpp FunctorDocumentation ExampleFunctorReturningInt_usage
     *
     * @param functor The functor to be called.
     * @param type The point content type.
     * @param args Arguments passed to functor call.
     */
    template <class FunctorT, class... Args>
    typename std::enable_if_t < !std::is_base_of<detail::FunctorWithReturnType, FunctorT>::value, void >
    callWithPointType(FunctorT& functor, PointContentType type, Args... args)
    {
        switch (type)
        {
            case PointContentType::ePoints:
                functor.template operator()<detail::PclPointType<ePoints>::PointT> (args...);
                break;
            case PointContentType::eIntensity:
                functor.template operator()<detail::PclPointType<eIntensity>::PointT>(args...);
                break;
            case PointContentType::eColoredPoints:
                functor.template operator()<detail::PclPointType<eColoredPoints>::PointT>(args...);
                break;
            case PointContentType::eLabeledPoints:
                functor.template operator()<detail::PclPointType<eLabeledPoints>::PointT>(args...);
                break;
            case PointContentType::eOrientedPoints:
                functor.template operator()<detail::PclPointType<eOrientedPoints>::PointT>(args...);
                break;
            case PointContentType::eColoredLabeledPoints:
                functor.template operator()<detail::PclPointType<eColoredLabeledPoints>::PointT>(args...);
                break;
            case PointContentType::eColoredOrientedPoints:
                functor.template operator()<detail::PclPointType<eColoredOrientedPoints>::PointT>(args...);
                break;
        }
    }


    /// Overload of `visitWithPointType()` for functors passed by const reference.
    template <class FunctorT, class... Args>
    typename std::enable_if_t < !std::is_base_of<detail::FunctorWithReturnType, FunctorT>::value, void >
    callWithPointType(const FunctorT& functor, PointContentType type, Args... args)
    {
        switch (type)
        {
            case PointContentType::ePoints:
                functor.template operator()<detail::PclPointType<ePoints>::PointT> (args...);
                break;
            case PointContentType::eIntensity:
                functor.template operator()<detail::PclPointType<eIntensity>::PointT>(args...);
                break;
            case PointContentType::eColoredPoints:
                functor.template operator()<detail::PclPointType<eColoredPoints>::PointT>(args...);
                break;
            case PointContentType::eLabeledPoints:
                functor.template operator()<detail::PclPointType<eLabeledPoints>::PointT>(args...);
                break;
            case PointContentType::eOrientedPoints:
                functor.template operator()<detail::PclPointType<eOrientedPoints>::PointT>(args...);
                break;
            case PointContentType::eColoredLabeledPoints:
                functor.template operator()<detail::PclPointType<eColoredLabeledPoints>::PointT>(args...);
                break;
            case PointContentType::eColoredOrientedPoints:
                functor.template operator()<detail::PclPointType<eColoredOrientedPoints>::PointT>(args...);
                break;
        }
    }


    /// Overload of `visitWithPointType()` for functors with return value.
    template <class VisitorT, class... Args>
    typename VisitorT::ReturnType
    callWithPointType(VisitorT& functor, PointContentType type, Args... args)
    {
        switch (type)
        {
            case PointContentType::ePoints:
                return functor.template operator()<detail::PclPointType<ePoints>::PointT> (args...);
                break;
            case PointContentType::eIntensity:
                return functor.template operator()<detail::PclPointType<eIntensity>::PointT>(args...);
                break;
            case PointContentType::eColoredPoints:
                return functor.template operator()<detail::PclPointType<eColoredPoints>::PointT>(args...);
                break;
            case PointContentType::eLabeledPoints:
                return functor.template operator()<detail::PclPointType<eLabeledPoints>::PointT>(args...);
                break;
            case PointContentType::eOrientedPoints:
                return functor.template operator()<detail::PclPointType<eOrientedPoints>::PointT>(args...);
                break;
            case PointContentType::eColoredLabeledPoints:
                return functor.template operator()<detail::PclPointType<eColoredLabeledPoints>::PointT>(args...);
                break;
            case PointContentType::eColoredOrientedPoints:
                return functor.template operator()<detail::PclPointType<eColoredOrientedPoints>::PointT>(args...);
                break;
        }
        // By throwing an exception, we do not need to provide a return value.
        ARMARX_UNEXPECTED_ENUM_VALUE(PointContentType, type)
    }


    /// Overload of `visitWithPointType()` for functors with return value passed by const reference.
    template <class VisitorT, class... Args>
    typename VisitorT::ReturnType
    callWithPointType(const VisitorT& functor, PointContentType type, Args... args)
    {
        switch (type)
        {
            case PointContentType::ePoints:
                return functor.template operator()<detail::PclPointType<ePoints>::PointT> (args...);
                break;
            case PointContentType::eIntensity:
                return functor.template operator()<detail::PclPointType<eIntensity>::PointT>(args...);
                break;
            case PointContentType::eColoredPoints:
                return functor.template operator()<detail::PclPointType<eColoredPoints>::PointT>(args...);
                break;
            case PointContentType::eLabeledPoints:
                return functor.template operator()<detail::PclPointType<eLabeledPoints>::PointT>(args...);
                break;
            case PointContentType::eOrientedPoints:
                return functor.template operator()<detail::PclPointType<eOrientedPoints>::PointT>(args...);
                break;
            case PointContentType::eColoredLabeledPoints:
                return functor.template operator()<detail::PclPointType<eColoredLabeledPoints>::PointT>(args...);
                break;
            case PointContentType::eColoredOrientedPoints:
                return functor.template operator()<detail::PclPointType<eColoredOrientedPoints>::PointT>(args...);
                break;
        }
        // By throwing an exception, we do not need to provide a return value.
        ARMARX_UNEXPECTED_ENUM_VALUE(PointContentType, type)
    }


    /// Functor creating a `pcl::PointCloud<PointT>` and passing it to `function`.
    struct PointCloudFunctor
    {
        template <class PointT, class FunctionT, class... Args>
        void operator()(FunctionT& function, Args... args) const
        {
            pcl::PointCloud<PointT> pointCloud;
            function(pointCloud, args...);
        }
        template <class PointT, class FunctionT, class... Args>
        void operator()(const FunctionT& function, Args... args) const
        {
            pcl::PointCloud<PointT> pointCloud;
            function(pointCloud, args...);
        }
    };

    /// Functor creating a `pcl::PointCloud<PointT>::Ptr` and passing it to `function`.
    struct PointCloudPtrFunctor
    {
        template <class PointT, class FunctionT, class... Args>
        void operator()(FunctionT& function, Args... args) const
        {
            typename pcl::PointCloud<PointT>::Ptr pointCloud(new pcl::PointCloud<PointT>);
            function(pointCloud, args...);
        }
        template <class PointT, class FunctionT, class... Args>
        void operator()(const FunctionT& function, Args... args) const
        {
            typename pcl::PointCloud<PointT>::Ptr pointCloud(new pcl::PointCloud<PointT>);
            function(pointCloud, args...);
        }
    };

    /**
     * @brief Call `function` with a `pcl::PointCloud<PointT>` with the
     * adequate `PointT` according to `type`.
     *
     * The passed `function` may be a lambda taking the point cloud by `auto`
     * as its first argument: `[](auto pointCloud) { pointCloud.size(); } `
     *
     * @param function The function to be called.
     * @param type The point content type.
     * @param args Optional additional arguments passed to the called function.
     */
    template <class FunctionT, class... Args>
    void callWithPointCloud(FunctionT& function, PointContentType type, Args... args)
    {
        callWithPointType(PointCloudFunctor{}, type, function, args...);
    }

    /// Overload of `callWithPointCloud()` for functions passed by const reference.
    template <class FunctionT, class... Args>
    void callWithPointCloud(const FunctionT& function, PointContentType type, Args... args)
    {
        callWithPointType(PointCloudFunctor{}, type, function, args...);
    }

    /**
     * @brief Call `function` with a (non-null) `pcl::PointCloud<PointT>::Ptr`
     * with the adequate `PointT` according to `type`.
     *
     * The passed `function` may be a lambda taking the point cloud by `auto`
     * as its first argument: `[](auto pointCloud) { pointCloud->size(); } `
     *
     * @param function The function to be called.
     * @param type The point content type.
     * @param args Optional additional arguments passed to the called function.
     */
    template <class FunctionT, class... Args>
    void callWithPointCloudPtr(FunctionT& function, PointContentType type, Args... args)
    {
        callWithPointType(PointCloudPtrFunctor{}, type, function, args...);
    }

    /// Overload of `callWithPointCloudPtr()` for functions passed by const reference.
    template <class FunctionT, class... Args>
    void callWithPointCloudPtr(const FunctionT& function, PointContentType type, Args... args)
    {
        callWithPointType(PointCloudPtrFunctor{}, type, function, args...);
    }

}
