#include "plane_fitting_ransac.h"

#include "plane_fitting_ransac.hpp"


using namespace visionx;


#define DEFINE_fitPlaneRansac_PointT(PointT) \
    std::optional<tools::PlaneFittingResult>                                              \
    tools::fitPlaneRansac(pcl::PointCloud< PointT >::Ptr cloud, double distanceThreshold, \
                          pcl::PointIndices::Ptr indices)                                 \
    {                                                                                     \
        return fitPlaneRansac< PointT >(cloud, distanceThreshold, indices);               \
    }

DEFINE_fitPlaneRansac_PointT(pcl::PointXYZ)
DEFINE_fitPlaneRansac_PointT(pcl::PointXYZRGB)
DEFINE_fitPlaneRansac_PointT(pcl::PointXYZRGBA)
DEFINE_fitPlaneRansac_PointT(pcl::PointXYZRGBL)


#undef DEFINE_fitPlaneRansac_PointT
