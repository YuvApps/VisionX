/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudTools
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::ArmarXLibraries::PointCloudTools

#define ARMARX_BOOST_TEST

#include <VisionX/Test.h>
#include "../call_with_point_type.h"
#include <VisionX/components/pointcloud_core/PointCloudConversions.h>

#include <iostream>

#include <pcl/point_cloud.h>


using namespace visionx;


static const std::vector<visionx::PointContentType> TYPES =
{
    PointContentType::ePoints, PointContentType::eIntensity, PointContentType::eColoredPoints,
    PointContentType::eLabeledPoints, PointContentType::eOrientedPoints,
    PointContentType::eColoredLabeledPoints, PointContentType::eColoredOrientedPoints
};

PointContentType getPointContentType()
{
    return PointContentType::ePoints;
}


/// [FunctorDocumentation ExampleFunctor]
struct ExampleFunctor
{
    template <class PointT>
    void operator()()
    {
        pcl::PointCloud<PointT> pointCloud;
        // Do stuff with `pointCloud` ...
        (void) pointCloud;
    }
};
/// [FunctorDocumentation ExampleFunctor]

BOOST_AUTO_TEST_CASE(test_example)
{
    /// [FunctorDocumentation ExampleFunctor_usage]
    PointContentType type = getPointContentType();

    ExampleFunctor functor;
    visionx::tools::callWithPointType(functor, type);
    /// [FunctorDocumentation ExampleFunctor_usage]
}


/// [FunctorDocumentation ExampleFunctorTakingArguments]
struct ExampleFunctorTakingArguments
{
    template <class PointT>
    void operator()(int myInt, float myFloat)
    {
        (void) myInt;
        (void) myFloat;
    }
};
/// [FunctorDocumentation ExampleFunctorTakingArguments]

BOOST_AUTO_TEST_CASE(test_example_taking_arguments)
{
    /// [FunctorDocumentation ExampleFunctorTakingArguments_usage]
    PointContentType type = getPointContentType();

    ExampleFunctorTakingArguments functor;
    int myInt = 42;
    float myFloat = 15;
    visionx::tools::callWithPointType(functor, type, myInt, myFloat);
    /// [FunctorDocumentation ExampleFunctorTakingArguments_usage]
}


/// [FunctorDocumentation ExampleFunctorReturningInt]
struct ExampleFunctorReturningInt : visionx::tools::FunctorWithReturnType<int>
{
    template <class PointT>
    int operator()(int myArg)
    {
        return myArg + 42;
    }
};
/// [FunctorDocumentation ExampleFunctorReturningInt]

BOOST_AUTO_TEST_CASE(test_example_returning_int)
{
    /// [FunctorDocumentation ExampleFunctorReturningInt_usage]
    PointContentType type = getPointContentType();

    ExampleFunctorReturningInt functor;
    int myInt = 42;
    int result = visionx::tools::callWithPointType(functor, type, myInt);
    /// [FunctorDocumentation ExampleFunctorReturningInt_usage]
    (void) result;
}



struct TestFunctor
{
    bool called = false;

    template <class PointT>
    void operator()()
    {
        called = true;
    }

    template <class PointT>
    void operator()(int typeAsInt, PointContentType originalType)
    {
        BOOST_CHECK_EQUAL(visionx::tools::getPointContentType<PointT>(), originalType);
        BOOST_CHECK_EQUAL(typeAsInt, static_cast<int>(originalType));
        called = true;
    }
};


BOOST_AUTO_TEST_CASE(test_callWithPointType_no_return_no_custom_args)
{
    for (PointContentType type : TYPES)
    {
        TestFunctor f;
        BOOST_CHECK(!f.called);
        visionx::tools::callWithPointType(f, type);
        BOOST_CHECK(f.called);
    }
}


BOOST_AUTO_TEST_CASE(test_callWithPointType_no_return_with_custom_args)
{
    for (PointContentType type : TYPES)
    {
        TestFunctor f;
        BOOST_CHECK(!f.called);
        visionx::tools::callWithPointType(f, type, static_cast<int>(type), type);
        BOOST_CHECK(f.called);
    }
}


struct TestReturnFunctor : visionx::tools::FunctorWithReturnType<int>
{
    bool called = false;

    template <class PointT>
    int operator()()
    {
        called = true;
        return 1;
    }

    template <class PointT>
    int operator()(int typeAsInt, PointContentType originalType)
    {
        BOOST_CHECK_EQUAL(visionx::tools::getPointContentType<PointT>(), originalType);
        BOOST_CHECK_EQUAL(typeAsInt, static_cast<int>(originalType));
        called = true;
        return 2;
    }
};


BOOST_AUTO_TEST_CASE(test_callWithPointType_with_return_no_custom_args)
{
    for (PointContentType type : TYPES)
    {
        TestReturnFunctor f;
        BOOST_CHECK(!f.called);
        visionx::tools::callWithPointType(f, type);
        BOOST_CHECK(f.called);
    }
}


BOOST_AUTO_TEST_CASE(test_callWithPointType_with_return_with_custom_args)
{
    for (PointContentType type : TYPES)
    {
        TestReturnFunctor f;
        BOOST_CHECK(!f.called);
        visionx::tools::callWithPointType(f, type, static_cast<int>(type), type);
        BOOST_CHECK(f.called);
    }
}



BOOST_AUTO_TEST_CASE(test_callWithPointCloud_lambda)
{
    for (PointContentType type : TYPES)
    {
        bool called = false;

        auto function = [&called](const auto & pointCloud, PointContentType type, int typeAsInt)
        {
            BOOST_CHECK(pointCloud.empty());
            called = true;
        };

        visionx::tools::callWithPointCloud(function, type, type, static_cast<int>(type));
        BOOST_CHECK(called);

        // Anonymous lambda.
        called = false;
        visionx::tools::callWithPointCloud(
            [&called](auto cloud)
        {
            BOOST_CHECK(cloud.empty());
            called = true;
        }, type);

        BOOST_CHECK(called);
    }
}


BOOST_AUTO_TEST_CASE(test_callWithPointCloudPtr_lambda)
{
    for (PointContentType type : TYPES)
    {
        bool called = false;

        auto function = [&called](const auto & pointCloud, PointContentType type, int typeAsInt)
        {
            BOOST_CHECK(pointCloud);
            BOOST_CHECK(pointCloud->empty());
            BOOST_CHECK_EQUAL(typeAsInt, static_cast<int>(type));
            called = true;
        };

        visionx::tools::callWithPointCloudPtr(function, type, type, static_cast<int>(type));
        BOOST_CHECK(called);

        // Anonymous lambda.
        called = false;
        visionx::tools::callWithPointCloudPtr(
            [&called](auto cloud)
        {
            BOOST_CHECK(cloud);
            called = true;
        }, type);

        BOOST_CHECK(called);
    }
}






