#pragma once

#include <Eigen/Core>

#include <pcl/point_cloud.h>
#include <pcl/PointIndices.h>
#include <pcl/common/io.h>

#include <SimoxUtility/shapes/AxisAlignedBoundingBox.h>


namespace visionx::tools
{

    template <typename PointT>
    pcl::PointIndices::Ptr getCropIndices(const pcl::PointCloud<PointT>& inputCloud,
                                          Eigen::Vector3f min, Eigen::Vector3f max)
    {
        const simox::AxisAlignedBoundingBox aabb(min, max);
        pcl::PointIndices::Ptr indices(new pcl::PointIndices);
        for (size_t i = 0; i < inputCloud.size(); ++i)
        {
            const PointT& p = inputCloud[i];
            if (aabb.is_inside(p))
            {
                indices->indices.push_back(int(i));
            }
        }
        return indices;
    }

    template <typename PointT>
    typename pcl::PointCloud<PointT>::Ptr
    getCropped(const pcl::PointCloud<PointT>& inputCloud,
               Eigen::Vector3f min, Eigen::Vector3f max)
    {
        pcl::PointIndices::Ptr indices = getCropIndices(inputCloud, min, max);

        typename pcl::PointCloud<PointT>::Ptr result(new pcl::PointCloud<PointT>);
        pcl::copyPointCloud(inputCloud, indices, *result);

        return result;
    }

}
