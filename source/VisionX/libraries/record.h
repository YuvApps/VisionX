/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// VisionX
#include <VisionX/libraries/record/AbstractRecordingStrategy.h>
#include <VisionX/libraries/record/RecordingMethod.h>
#include <VisionX/libraries/record/public_api.h>



/**
 * This package provides an API to record image streams frame by frame for various formats.
 *
 * The format is determined by the concrete strategy used. Available formats are:
 *
 *  - Video formats
 *    - *.h264 (H264RecordingStrategy) (Requires libx264 and libswscale)
 *    - *.avi (AVIRecordingStrategy)
 *  - Individual image formats
 *    - *.bmp (BMPRecordingStrategy)
 *    - *.png (PNGRecordingStrategy)
 *    - *.jpg (JPGRecordingStrategy)
 *
 * Required header:
 *
 * ```cpp
 * // Include the required header. NOTE: Other header files are not part of the public API and may be subject to change
 * #include <VisionX/libraries/record.h>
 * ```
 *
 * Example (Record AVI video):
 *
 * ```cpp
 * // Initialise recording
 * const std::filesystem::path path = "/home/anon/recording_2010-10-10.avi";
 * const unsigned int fps = 30;
 * visionx::record::Recording rec = visionx::record::newRecording(path, fps);
 *
 * // Record images
 * for (CByteImage* img : images) // typeof(images) = CByteImages** (IVT)
 * {
 *     rec->recordFrame(img);
 * }
 *
 * // Stop recording
 * rec->stopRecording();
 *
 * // {rec} is now uninitialised and should be destroyed
 * ```
 *
 * Example (Taking a snapshot):
 *
 * ```cpp
 * const std::filesystem::path path = "/home/anon/recording_2010-10-10";
 * const cv::Mat snapshot = somehow_get_an_image();
 * visionx::record::takeSnapshot(snapshot, path); // Note: Snapshots are always in the PNG format
 * ```
 *
 * @package visionx::record
 * @author Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date 2018
 */
namespace visionx::record
{
    // pass
}


