/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKitArmarX
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <AffordanceKit/Scene.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>

namespace AffordanceKitArmarX
{

    class SceneArmarX : public AffordanceKit::Scene
    {
    public:
        SceneArmarX();
        SceneArmarX(const AffordanceKit::PrimitiveSetPtr& primitives, const std::vector<AffordanceKit::UnimanualAffordancePtr>& unimanualAffordances, const std::vector<AffordanceKit::BimanualAffordancePtr>& bimanualAffordances);
        SceneArmarX(memoryx::EnvironmentalPrimitiveSegmentBasePrx& primitiveSegment, memoryx::AffordanceSegmentBasePrx& affordanceSegment);

        void writeToMemory(const memoryx::EnvironmentalPrimitiveSegmentBasePrx& primitiveSegment, const memoryx::AffordanceSegmentBasePrx& affordanceSegment) const;

    protected:
        void generateAffordanceTypes();

    protected:
        std::map<std::string, memoryx::AffordanceType> affordanceTypes;
    };

    using SceneArmarXPtr = std::shared_ptr<SceneArmarX>;

}



