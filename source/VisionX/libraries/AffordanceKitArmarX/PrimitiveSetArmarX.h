/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    AffordanceKitArmarX
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <AffordanceKit/PrimitiveSet.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>

namespace AffordanceKitArmarX
{
    class PrimitiveSetArmarX : public AffordanceKit::PrimitiveSet
    {
    public:
        PrimitiveSetArmarX();
        PrimitiveSetArmarX(const AffordanceKit::PrimitiveSetPtr& primitiveSet);
        PrimitiveSetArmarX(const memoryx::EnvironmentalPrimitiveSegmentBasePrx& segment, long timestamp = 0);

        void writeToMemory(const memoryx::EnvironmentalPrimitiveSegmentBasePrx& segment) const;
    };

    using PrimitiveSetArmarXPtr = std::shared_ptr<PrimitiveSetArmarX>;

}




