armarx_set_target("AffordanceKitArmarX")

find_package(AffordanceKit QUIET)
armarx_build_if(AffordanceKit_FOUND "AffordanceKit not available")

find_package(OpenMP QUIET)
if(OPENMP_FOUND)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()


set(CXX_FLAGS -UBOOST_ENABLE_ASSERT_HANDLER)

set(SOURCES
    PrimitiveSetArmarX.cpp
    SceneArmarX.cpp
    UnimanualAffordanceArmarX.cpp
    BimanualAffordanceArmarX.cpp
)
    
set(HEADERS 
    PrimitiveSetArmarX.h
    SceneArmarX.h
    UnimanualAffordanceArmarX.h
    BimanualAffordanceArmarX.h
)

set(COMPONENT_LIBS
    ArmarXCore
    MemoryXMemoryTypes
    ${AffordanceKit_LIBRARIES}
)

armarx_add_library(AffordanceKitArmarX "${SOURCES}" "${HEADERS}" "${COMPONENT_LIBS}")

if(AffordanceKit_FOUND)
    target_include_directories(AffordanceKitArmarX PUBLIC ${AffordanceKit_INCLUDE_DIRS})
endif()
