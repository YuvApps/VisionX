#pragma once


// STD/STL
#include <map>
#include <string>
#include <vector>

// RobotAPI
#include <RobotAPI/components/ArViz/Client/Layer.h>

// VisionX
#include <VisionX/interface/components/OpenPoseEstimationInterface.h>


namespace visionx::viz
{

    class HumanPoseBody25
    {

    public:

        static
        const std::map<unsigned int, std::string> parts;

        static
        const std::vector<std::pair<unsigned int, unsigned int>> pairs;

    public:

        void
        static addPoseToLayer(const armarx::Keypoint3DMap& pose, armarx::viz::Layer& layer);

    };

}
