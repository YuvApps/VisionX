#include <VisionX/libraries/ArViz/HumanPoseBody25.h>


#include <RobotAPI/components/ArViz/Client/Elements.h>


const std::map<unsigned int, std::string>
visionx::viz::HumanPoseBody25::parts =
{
    {0,  "Nose"},
    {1,  "Neck"},
    {2,  "RShoulder"},
    {3,  "RElbow"},
    {4,  "RWrist"},
    {5,  "LShoulder"},
    {6,  "LElbow"},
    {7,  "LWrist"},
    {8,  "MidHip"},
    {9,  "RHip"},
    {10, "RKnee"},
    {11, "RAnkle"},
    {12, "LHip"},
    {13, "LKnee"},
    {14, "LAnkle"},
    {15, "REye"},
    {16, "LEye"},
    {17, "REar"},
    {18, "LEar"},
    {19, "LBigToe"},
    {20, "LSmallToe"},
    {21, "LHeel"},
    {22, "RBigToe"},
    {23, "RSmallToe"},
    {24, "RHeel"},
    {25, "Background"}
};


const std::vector<std::pair<unsigned int, unsigned int>>
        visionx::viz::HumanPoseBody25::pairs =
{
    {1, 8}, {1, 2}, {1, 5}, {2, 3}, {3, 4}, {5, 6}, {6, 7}, {8, 9}, {9, 10}, {10, 11}, {8, 12},
    {12, 13}, {13, 14}, {1, 0}, {0, 15}, {15, 17}, {0, 16}, {16, 18}, {14, 19}, {19, 20}, {14, 21},
    {11, 22}, {22, 23}, {11, 24}
};


void
visionx::viz::HumanPoseBody25::addPoseToLayer(const armarx::Keypoint3DMap& pose,
        armarx::viz::Layer& layer)
{
    // Segments.
    for (const auto& [index1, index2] : HumanPoseBody25::pairs)
    {
        const std::string name1 = parts.at(index1);
        const std::string name2 = parts.at(index2);

        // Only draw segment if start end end are in keypoint map.
        if (pose.find(name1) == pose.end() or pose.find(name2) == pose.end())
        {
            continue;
        }

        const armarx::Keypoint3D p1 = pose.at(name1);
        const armarx::Keypoint3D p2 = pose.at(name2);

        const Eigen::Vector3f pos1(p1.x, p1.y, p1.z);
        const Eigen::Vector3f pos2(p2.x, p2.y, p2.z);
        const Eigen::Vector3f dir = pos2 - pos1;

        const armarx::viz::Color color = armarx::viz::Color::fromRGBA(p2.dominantColor.r,
                                         p2.dominantColor.g,
                                         p2.dominantColor.b);

        armarx::viz::Arrow line_helper = armarx::viz::Arrow(p1.label + "_" + p2.label)
                                         .position(pos1)
                                         .direction(dir.normalized());
        armarx::viz::Cylinder line = armarx::viz::Cylinder(p1.label + "_" + p2.label)
                                     .radius(10)
                                     .height(dir.norm())
                                     .color(color);
        line.data_->pose = line_helper.data_->pose;

        layer.add(line);
    }

    // Keypoints.
    for (auto const& [name, keypoint] : pose)
    {
        const Eigen::Vector3f pos(keypoint.x, keypoint.y, keypoint.z);

        const armarx::viz::Color color = armarx::viz::Color::fromRGBA(keypoint.dominantColor.r,
                                         keypoint.dominantColor.g,
                                         keypoint.dominantColor.b);
        armarx::viz::Sphere sphere = armarx::viz::Sphere(name)
                                     .position(pos)
                                     .radius(15)
                                     .color(color);
        layer.add(sphere);
    }
}
