//
// Created by christoph on 03.06.19.
//

#ifndef ROBDEKON_POINTXYZRGBLNORMAL_H
#define ROBDEKON_POINTXYZRGBLNORMAL_H

#define PCL_NO_PRECOMPILE
#include <pcl/point_types.h>
#include <pcl/pcl_macros.h>
#include <bitset>
#include <pcl/register_point_struct.h>

// We're doing a lot of black magic with Boost here, so disable warnings in Maintainer mode, as we will never
// be able to fix them anyway
#if defined _MSC_VER
#pragma warning(disable: 4201)
#endif
//#pragma warning(push, 1)
#if defined __GNUC__
#  pragma GCC system_header
#endif

namespace pcl
{
    /** \brief Members: float x, y, z, rgb, uint32_t label, normal[3], curvature
    * \ingroup common
    */
    struct PointXYZRGBLNormal;
}

#include "PointXYZRGBLNormal.hpp"

POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::_PointXYZRGBLNormal,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (uint32_t, rgba, rgba)
                                  (uint32_t, label, label)
                                  (float, normal_x, normal_x)
                                  (float, normal_y, normal_y)
                                  (float, normal_z, normal_z)
                                  (float, curvature, curvature)
                                 )
POINT_CLOUD_REGISTER_POINT_WRAPPER(pcl::PointXYZRGBLNormal, pcl::_PointXYZRGBLNormal)


#if defined _MSC_VER
#pragma warning(default: 4201)
#endif


#endif //ROBDEKON_POINTXYZRGBLNORMAL_H
