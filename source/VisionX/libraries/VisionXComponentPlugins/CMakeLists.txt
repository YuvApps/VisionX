set(LIB_NAME       VisionXComponentPlugins)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

find_package(SemanticObjectRelations QUIET)
armarx_build_if(SemanticObjectRelations_FOUND "SemanticObjectRelations not available")

set(LIBS
    VisionXCore
    VisionXInterfaces
    VisionXSemanticObjectRelations
)

set(LIB_FILES
    SemanticGraphStorageComponentPlugin.cpp
)
set(LIB_HEADERS
    SemanticGraphStorageComponentPlugin.h
)

armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

