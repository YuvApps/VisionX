#include <VirtualRobot/VirtualRobot.h>

#include "SemanticGraphStorageComponentPlugin.h"

#include <VisionX/libraries/SemanticObjectRelations/ice_serialization.h>

namespace armarx
{
    namespace plugins
    {

        void SemanticGraphStorageComponentPlugin::preOnInitComponent()
        {
            parent<Component>().offeringTopicFromProperty(PROPERTY_NAME);
        }

        void SemanticGraphStorageComponentPlugin::preOnConnectComponent()
        {
            parent<Component>().getTopicFromProperty(topic, PROPERTY_NAME);
        }

        void SemanticGraphStorageComponentPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineOptionalProperty<std::string>(
                    PROPERTY_NAME,
                    "SemanticGraphTopic",
                    "Name of the SemanticGraphTopic");
            }
        }

    }

    SemanticGraphStorageComponentPluginUser::SemanticGraphStorageComponentPluginUser()
    {
        addPlugin(plugin);
    }

    const semantic::GraphStorageTopicPrx& SemanticGraphStorageComponentPluginUser::getGraphStorageTopic()
    {
        return plugin->topic;
    }

    void armarx::SemanticGraphStorageComponentPluginUser::storeGraph(std::string const& name, const semrel::AttributedGraph& graph)
    {
        armarx::semantic::data::Graph iceGraph = armarx::semantic::toIce(graph);
        getGraphStorageTopic()->reportGraph(name, iceGraph);
    }

}
