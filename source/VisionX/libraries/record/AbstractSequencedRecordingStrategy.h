/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <fstream>
#include <set>
#include <string>

// VisionX
#include <VisionX/libraries/record/AbstractRecordingStrategy.h>


namespace visionx::record
{
    /**
     * An object of this class behaves likee a normal recording, but is in fact a sequence of images
     */
    class AbstractSequencedRecordingStrategy;
}



class visionx::record::AbstractSequencedRecordingStrategy :
    public visionx::record::AbstractRecordingStrategy
{

private:

    /**
     * @brief Frame number which is additionally displayed in the filename
     */
    unsigned int sequenceNumber = 0;

    /**
     * @brief Name of the current chunk (part of the path to overcome file system limitations)
     */
    std::filesystem::path framesChunk;

    /**
     * @brief File stream for the metadata file
     */
    std::ofstream metadataFile;

    /**
     * @brief List of already written metadata variable names
     */
    std::set<std::string> writtenMetadataVariables;

public:

    /**
     * @brief Default constructor to manually start the recording
     */
    AbstractSequencedRecordingStrategy();

    /**
     * @brief Constructor for any recording strategy, immediately starting the recording
     * @param filePath Path to where the recording file should be written to
     * @param fps Amount of frames being recorded per second
     */
    AbstractSequencedRecordingStrategy(const std::filesystem::path& filePath, unsigned int fps);

    /**
     * @brief Destruct the recording strategy
     */
    virtual ~AbstractSequencedRecordingStrategy() override;

    /**
     * @brief Starts the recording manually if constructed empty
     * @param filePath Path to where the recording file should be written to
     * @param fps Amount of frames being recorded per second
     */
    virtual void startRecording(const std::filesystem::path& filePath, unsigned int fps) override;

    /**
     * @brief Adds the given frame to the recording
     * @param frame Frame to be added
     */
    virtual void recordFrame(const CByteImage& frame) final override;

    /**
     * @brief Adds the given frame to the recording
     * @param frame Frame to be added
     */
    virtual void recordFrame(const cv::Mat& frame) final override;

    /**
     * @brief Stops the recording
     */
    virtual void stopRecording() override;

protected:

    /**
     * @brief Adds the given frame to the recording and saves it at given path
     * @param frame Frame to be added
     * @param fullPath Path to where the frame should be written to
     */
    virtual void recordFrame(const CByteImage& frame, const std::filesystem::path& fullPath);

    /**
     * @brief Adds the given frame to the recording and saves it at given path
     * @param frame Frame to be added
     * @param fullPath Path to where the frame should be written to
     */
    virtual void recordFrame(const cv::Mat& frame, const std::filesystem::path& fullPath);

    /**
     * @brief Writes a file to the metadata file if writing is enabled
     * @param varName Name of the variable to be written
     * @param varType Type of the variable to be written
     * @param varValue Value of the variable to be written
     */
    virtual void writeMetadataLine(const std::string& varName, const std::string& varType, const std::string& varValue);

private:

    /**
     * @brief Returns the next sequenced full path and increments the sequence number
     * @return Next sequenced full path
     */
    std::filesystem::path getNextFullPath();

};
