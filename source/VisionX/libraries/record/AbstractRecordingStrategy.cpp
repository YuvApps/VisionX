/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/record/AbstractRecordingStrategy.h>


// Boost
#include <filesystem>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

// VisionX
#include <VisionX/libraries/record/helper.h>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>


visionx::record::AbstractRecordingStrategy::AbstractRecordingStrategy()
{
    this->recording = false;
    this->filePath = "";
    this->fps = 0;
}


visionx::record::AbstractRecordingStrategy::AbstractRecordingStrategy(const std::filesystem::path& filePath, unsigned int fps)
{
    this->recording = false;
    this->startRecording(filePath, fps);
}


visionx::record::AbstractRecordingStrategy::~AbstractRecordingStrategy()
{
    this->stopRecording();
}


bool
visionx::record::AbstractRecordingStrategy::isRecording() const
{
    return this->recording;
}


void
visionx::record::AbstractRecordingStrategy::startRecording(const std::filesystem::path& filePath, unsigned int fps)
{
    ARMARX_CHECK_EXPRESSION_W_HINT(!this->recording, "Instance has already been initialised for recording");
    ARMARX_CHECK_EXPRESSION_W_HINT(fps != 0, "FPS of a recording cannot be zero");
    ARMARX_CHECK_EXPRESSION_W_HINT(!std::filesystem::exists(filePath), "File already exists");

    this->filePath = filePath;
    this->fps = fps;

    std::filesystem::create_directories(this->filePath.parent_path());

    this->recording = true;
}


void
visionx::record::AbstractRecordingStrategy::recordFrame(const CByteImage& frame)
{
    // Default implementations of recordFrame just convert the parameter and call the other recordFrame method.
    // This way only one method needs to be overridden in the interface-implementing classes

    // Call cv::Mat variant of recordFrame(...)
    cv::Mat cv_frame;
    visionx::record::convert(frame, cv_frame);
    this->recordFrame(cv_frame);
}


void
visionx::record::AbstractRecordingStrategy::recordFrame(const cv::Mat& frame)
{
    // Default implementations of recordFrame just convert the parameter and call the other recordFrame method.
    // This way only one method needs to be overridden in the interface-implementing classes

    // Call CByteImage variant of recordFrame(...)
    CByteImage ivt_frame;
    visionx::record::convert(frame, ivt_frame);
    this->recordFrame(ivt_frame);
}


void
visionx::record::AbstractRecordingStrategy::stopRecording()
{
    this->recording = false;
}


std::filesystem::path
visionx::record::AbstractRecordingStrategy::getFilePath() const
{
    return this->filePath;
}


unsigned int
visionx::record::AbstractRecordingStrategy::getFps() const
{
    return this->fps;
}


std::filesystem::path
visionx::record::AbstractRecordingStrategy::getPath() const
{
    return this->filePath.parent_path();
}


std::filesystem::path
visionx::record::AbstractRecordingStrategy::getStem() const
{
    return this->filePath.stem();
}


std::filesystem::path
visionx::record::AbstractRecordingStrategy::getDotExtension() const
{
    return this->filePath.extension();
}
