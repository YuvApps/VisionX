/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/record/public_api.h>


// VisionX
#include <VisionX/libraries/record/RecordingMethodRegistry.h>
#include <VisionX/libraries/record/strats/PNGRecordingStrategy.h>


visionx::record::Recording
visionx::record::newRecording(const std::filesystem::path& filePath, unsigned int fps)
{
    return visionx::record::RecordingMethodRegistry::newRecording(filePath, fps);
}


void
visionx::record::registerRecordingMethod(const visionx::record::RecordingMethod& recordingMethod)
{
    return visionx::record::RecordingMethodRegistry::registerRecordingMethod(recordingMethod);
}


visionx::record::RecordingMethod
visionx::record::getRecordingMethod(const std::string& fileExtension)
{
    return visionx::record::RecordingMethodRegistry::getRecordingMethod(fileExtension);
}


std::vector<visionx::record::RecordingMethod>
visionx::record::getAvailableRecordingMethods()
{
    return visionx::record::RecordingMethodRegistry::getAvailableRecordingMethods();
}


void
visionx::record::takeSnapshot(const CByteImage& image, const std::filesystem::path& filePath)
{
    visionx::record::RecordingMethodRegistry::takeSnapshot(image, filePath);
}


void
visionx::record::takeSnapshot(const cv::Mat& image, const std::filesystem::path& filePath)
{
    visionx::record::RecordingMethodRegistry::takeSnapshot(image, filePath);
}
