/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Mirko Waechter <mirko dot waechter at kit dot edu>
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/record/strats/H264LegacyRecordingStrategy.h>


// STD/STL
#include <cstring>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

// IVT
#include <Image/ImageProcessor.h>


visionx::record::strats::H264RecordingStrategy::H264RecordingStrategy()
{
    // pass
}


visionx::record::strats::H264RecordingStrategy::H264RecordingStrategy(const boost::filesystem::path& filePath, unsigned int fps) :
    visionx::record::AbstractRecordingStrategy(filePath, fps)
{
    // pass
}


visionx::record::strats::H264RecordingStrategy::~H264RecordingStrategy()
{
    // pass
}


void
visionx::record::strats::H264RecordingStrategy::recordFrame(const CByteImage& frame)
{
    armarx::ScopedLock lock(this->mutex);
    if (!this->fp or !this->initialized)
    {
        this->initH264Data(frame.width, frame.height, static_cast<int>(this->getFps()), "veryfast", "Main", 25.f);

        if (!this->fp)
        {
            ARMARX_ERROR << deactivateSpam() << "Could not open the output video file '" << this->getFilePath().string() << "' for writing. FRAME DROPPED!";
            return;
        }

        if (!this->initialized)
        {
            ARMARX_ERROR << deactivateSpam() << "Could not initialise H264 video codecs. FRAME DROPPED!";
            return;
        }
    }

    ARMARX_CHECK_EXPRESSION(this->h264Data.encoder);
    ARMARX_CHECK_EXPRESSION(this->h264Data.convertCtx);

    const CByteImage* tmpImagePtr = &frame;
    if (frame.type == CByteImage::eGrayScale)
    {
        //ARMARX_INFO << "Converting image";
        ::ImageProcessor::ConvertImage(&frame, &rgbImage);
        tmpImagePtr = &this->rgbImage;
    }

    // and encode and store into pic_out
    this->h264Data.pic_in.i_pts = this->h264Data.pts;

    int srcstride = tmpImagePtr->width * 3; //RGB stride is just 3*width
    const uint8_t* pixels = tmpImagePtr->pixels;
    //ARMARX_INFO << "Scaling image";
    sws_scale(this->h264Data.convertCtx, &pixels, &srcstride, 0, frame.height, this->h264Data.pic_in.img.plane, this->h264Data.pic_in.img.i_stride);
    //x264_nal_t* nals;
    int i_nals;
    //ARMARX_INFO << "encoding image";
    int frameSize = x264_encoder_encode(this->h264Data.encoder, &this->h264Data.nals, &i_nals, &this->h264Data.pic_in, &this->h264Data.pic_out);
    if (frameSize)
    {
        //ARMARX_INFO << "Writing image";
        if (!fwrite(this->h264Data.nals[0].p_payload, static_cast<unsigned int>(frameSize), 1, this->fp))
        {
            throw armarx::LocalException("Error while trying to write Network Abstraction Layer data to file");
        }
    }
    this->h264Data.pts++;
}


void
visionx::record::strats::H264RecordingStrategy::stopRecording()
{
    armarx::ScopedLock lock(mutex);
    ARMARX_DEBUG << "stopRecording";
    int count = 0;
    /* Flush delayed frames */
    while (x264_encoder_delayed_frames(this->h264Data.encoder))
    {
        int i_nal;
        int bytes = x264_encoder_encode(this->h264Data.encoder, &this->h264Data.nals, &i_nal, nullptr, &this->h264Data.pic_out);
        ARMARX_INFO << " writing delayed frame " << count <<  " of size " << bytes << " bytes";
        if (bytes > 0)
        {
            fwrite(this->h264Data.nals->p_payload, static_cast<unsigned int>(bytes), 1, this->fp);
        }
    }

    if (this->h264Data.encoder)
    {
        x264_picture_clean(&this->h264Data.pic_in);
        std::memset(static_cast<void*>(&this->h264Data.pic_in), 0, sizeof(this->h264Data.pic_in));
        std::memset(static_cast<void*>(&this->h264Data.pic_out), 0, sizeof(this->h264Data.pic_out));

        x264_encoder_close(this->h264Data.encoder);
        this->h264Data.encoder = nullptr;
    }

    if (this->h264Data.convertCtx)
    {
        sws_freeContext(this->h264Data.convertCtx);
        this->h264Data.convertCtx = nullptr;
    }

    if (this->fp)
    {
        fclose(this->fp);
    }

    this->visionx::record::AbstractRecordingStrategy::stopRecording();
}


void
visionx::record::strats::H264RecordingStrategy::initH264Data(int width, int height, int fps, std::string h264Preset, std::string h264Profile, float compressionRate)
{
    x264_param_default_preset(&this->h264Data.param, h264Preset.c_str(), "zerolatency");
    this->h264Data.param.i_threads = 1;
    this->h264Data.param.i_width = width;
    this->h264Data.param.i_height = height;
    this->h264Data.param.i_fps_num = static_cast<unsigned int>(fps);
    this->h264Data.param.i_fps_den = 1;
    // Intra refres:
    this->h264Data.param.i_keyint_max = fps;
    this->h264Data.param.b_intra_refresh = 1;
    // Rate control:
    //this->h264Data.param.rc.i_qp_constant = 51;
    this->h264Data.param.rc.i_rc_method = X264_RC_CRF;
    this->h264Data.param.rc.f_rf_constant = compressionRate;
    this->h264Data.param.rc.f_rf_constant_max = this->h264Data.param.rc.f_rf_constant * 1.4f;

    //this->h264Data.param.rc.i_qp_constant = 18;
    //this->h264Data.param.rc.i_qp_min = 18;
    //this->h264Data.param.rc.i_qp_max = 18;

    // For streaming:
    this->h264Data.param.b_repeat_headers = 1;
    this->h264Data.param.b_annexb = 1;

    if (x264_param_apply_profile(&this->h264Data.param, h264Profile.c_str()) != 0)
    {
        ARMARX_WARNING << "Could not set '" << h264Profile << "' profile for x264 codec";
    }

    this->h264Data.encoder = x264_encoder_open(&this->h264Data.param);
    if (!this->h264Data.encoder)
    {
        ARMARX_ERROR << "Could not open h264 encoder!";
        return;
    }

    x264_picture_alloc(&this->h264Data.pic_in, X264_CSP_I420, width, height);

    this->h264Data.convertCtx = sws_getContext(width, height, AV_PIX_FMT_RGB24, width, height, AV_PIX_FMT_YUV420P, SWS_FAST_BILINEAR, nullptr, nullptr, nullptr);

    this->fp = fopen(this->getFilePath().c_str(), "w+b");
    if (!this->fp)
    {
        ARMARX_ERROR << "Cannot open the h264 destination file: " << this->getFilePath().string();
        return;
    }

    int r = 0;
    int nheader = 0;
    int header_size = 0;

    // write headers
    r = x264_encoder_headers(this->h264Data.encoder, &this->h264Data.nals, &nheader);
    if (r < 0)
    {
        ARMARX_ERROR << "x264_encoder_headers() failed";
        return;
    }

    header_size = this->h264Data.nals[0].i_payload + this->h264Data.nals[1].i_payload + this->h264Data.nals[2].i_payload;

    if (!fwrite(this->h264Data.nals[0].p_payload, static_cast<unsigned int>(header_size), 1, this->fp))
    {
        ARMARX_ERROR << "Cannot write headers";
        return;
    }

    this->initialized = true;
}
