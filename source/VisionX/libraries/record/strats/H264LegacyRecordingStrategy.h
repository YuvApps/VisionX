/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <string>

// libx264 + libswscale
extern "C"
{
#include <x264.h>
#include <libswscale/swscale.h>
}

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>

// VisionX
#include <VisionX/libraries/record/AbstractRecordingStrategy.h>


namespace visionx::record::strats
{
    /**
     * Concrete strategy for a H264 video recording
     */
    class H264RecordingStrategy;
}


class visionx::record::strats::H264RecordingStrategy :
    public visionx::record::AbstractRecordingStrategy
{

protected:

    struct H264Data
    {
        x264_param_t param;
        x264_t* encoder = nullptr;
        x264_picture_t pic_in, pic_out;
        SwsContext* convertCtx = nullptr;
        x264_nal_t* nals = nullptr;
        int pts = 0;
    };

    visionx::record::strats::H264RecordingStrategy::H264Data h264Data;
    FILE* fp = nullptr;
    bool initialized = false;
    CByteImage rgbImage;
    armarx::Mutex mutex;

    void initH264Data(int width, int height, int fps, std::string h264Preset, std::string h264Profile, float compressionRate);

public:

    H264RecordingStrategy();
    H264RecordingStrategy(const std::filesystem::path& filePath, unsigned int fps);
    virtual ~H264RecordingStrategy() override;
    virtual void recordFrame(const CByteImage& frame) override;
    virtual void stopRecording() override;

};
