/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// PNG compression; Default value: 9; Allowed int values: [1, 9]; Higher compression means lower file size at the cost of computation time (lossless)
#define PNG_COMPRESSION 9


#include <VisionX/libraries/record/strats/PNGRecordingStrategy.h>


// STD/STL
#include <vector>

// Boost
#include <filesystem>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

// VisionX
#include <VisionX/libraries/record/helper.h>


visionx::record::strats::PNGRecordingStrategy::PNGRecordingStrategy(bool snapshotMode) :
    snapshotMode(snapshotMode)
{
    // pass
}


visionx::record::strats::PNGRecordingStrategy::PNGRecordingStrategy(const std::filesystem::path& filePath, unsigned int fps) :
    visionx::record::AbstractSequencedRecordingStrategy(filePath, fps),
    snapshotMode(false)
{
    // pass
}


visionx::record::strats::PNGRecordingStrategy::~PNGRecordingStrategy()
{
    // pass
}


bool
visionx::record::strats::PNGRecordingStrategy::isRecording() const
{
    // In snapshot mode, the instance is always recording
    return this->snapshotMode or this->AbstractSequencedRecordingStrategy::isRecording();
}


void
visionx::record::strats::PNGRecordingStrategy::startRecording(const std::filesystem::path& filePath, unsigned int fps)
{
    // In snapshot mode, startRecording will do nothing
    if (!this->snapshotMode)
    {
        this->visionx::record::AbstractSequencedRecordingStrategy::startRecording(filePath, fps);
    }
}


void
visionx::record::strats::PNGRecordingStrategy::stopRecording()
{
    // In snapshot mode, stopRecording will do nothing
    if (!this->snapshotMode)
    {
        this->visionx::record::AbstractSequencedRecordingStrategy::stopRecording();
    }
}


void
visionx::record::strats::PNGRecordingStrategy::recordSnapshot(const cv::Mat& image, const std::filesystem::path& path)
{
    std::filesystem::path snapshotPath(path);

    // Make sure that path does exist, ensure PNG extension
    ARMARX_CHECK_EXPRESSION_W_HINT(std::filesystem::exists(path.parent_path()), "Cannot take snapshot, path '" + path.parent_path().string() + "' does ot exist");

    if (snapshotPath.extension() != ".png")
    {
        snapshotPath += ".png";
    }

    this->recordFrame(image, snapshotPath);
}


void
visionx::record::strats::PNGRecordingStrategy::recordSnapshot(const CByteImage& image, const std::filesystem::path& path)
{
    // Covert to OpenCV image and run the CV method
    cv::Mat cv_image;
    visionx::record::convert(image, cv_image);
    this->recordSnapshot(cv_image, path);
}


void
visionx::record::strats::PNGRecordingStrategy::recordFrame(const cv::Mat& frame, const std::filesystem::path& fullPath)
{
    std::vector<int> params {CV_IMWRITE_PNG_COMPRESSION, PNG_COMPRESSION,
                             CV_IMWRITE_PNG_STRATEGY, CV_IMWRITE_PNG_STRATEGY_RLE};
    cv::imwrite(fullPath.string(), frame, params);
}


void
visionx::record::strats::PNGRecordingStrategy::writeMetadataLine(const std::string& varName, const std::string& varType, const std::string& varValue)
{
    // In snapshot mode, no metadata file should be written
    if (!this->snapshotMode)
    {
        this->AbstractSequencedRecordingStrategy::writeMetadataLine(varName, varType, varValue);
    }
}
