/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/record/strats/AVIRecordingStrategy.h>


visionx::record::strats::AVIRecordingStrategy::AVIRecordingStrategy()
{
    // pass
}


visionx::record::strats::AVIRecordingStrategy::AVIRecordingStrategy(const std::filesystem::path& filePath, unsigned int fps) :
    visionx::record::AbstractRecordingStrategy(filePath, fps)
{
    // pass
}


visionx::record::strats::AVIRecordingStrategy::~AVIRecordingStrategy()
{
    // pass
}


void
visionx::record::strats::AVIRecordingStrategy::recordFrame(const cv::Mat& frame)
{
    if (!this->aviVideoWriter.isOpened())
    {
        const std::string filePath = this->getFilePath().string();
        const int fourcc = CV_FOURCC('M', 'J', 'P', 'G');
        this->aviVideoWriter.open(filePath, fourcc, this->getFps(), frame.size());

        if (!this->aviVideoWriter.isOpened())
        {
            ARMARX_ERROR << deactivateSpam() << "Could not open the output video file '" << filePath << "' for writing. FRAME DROPPED!";
            return;
        }
    }

    this->aviVideoWriter.write(frame);
}
