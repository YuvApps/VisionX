/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/record/strats/BMPRecordingStrategy.h>


visionx::record::strats::BMPRecordingStrategy::BMPRecordingStrategy()
{
    // pass
}


visionx::record::strats::BMPRecordingStrategy::BMPRecordingStrategy(const std::filesystem::path& filePath, unsigned int fps) :
    visionx::record::AbstractSequencedRecordingStrategy(filePath, fps)
{
    // pass
}


visionx::record::strats::BMPRecordingStrategy::~BMPRecordingStrategy()
{
    // pass
}


void
visionx::record::strats::BMPRecordingStrategy::recordFrame(const CByteImage& frame, const std::filesystem::path& fullPath)
{
    frame.SaveToFile(fullPath.c_str());
}
