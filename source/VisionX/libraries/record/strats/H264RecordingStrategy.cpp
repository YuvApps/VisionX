/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/record/strats/H264RecordingStrategy.h>


visionx::record::strats::H264RecordingStrategy::H264RecordingStrategy()
{
    // pass
}


visionx::record::strats::H264RecordingStrategy::H264RecordingStrategy(const std::filesystem::path& filePath, unsigned int fps) :
    visionx::record::AbstractRecordingStrategy(filePath, fps)
{
    // pass
}


visionx::record::strats::H264RecordingStrategy::~H264RecordingStrategy()
{
    // pass
}


void
visionx::record::strats::H264RecordingStrategy::recordFrame(const cv::Mat& frame)
{
    if (!this->h264VideoWriter.isOpened())
    {
        const int fourcc = CV_FOURCC('X', '2', '6', '4');
        this->h264VideoWriter.open(this->getFilePath().string(), fourcc, this->getFps(), frame.size());

        if (!this->h264VideoWriter.isOpened())
        {
            ARMARX_ERROR << deactivateSpam() << "Could not open the output video file '" << this->getFilePath().string() << "' for writing. FRAME DROPPED!";
            return;
        }
    }

    this->h264VideoWriter.write(frame);
}
