/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// JPG quality; Default value: 95; Allowed int values: [0, 100]; Lower quality means lower file size at the cost of quality (lossy)
#define JPG_QUALITY 95


#include <VisionX/libraries/record/strats/JPGRecordingStrategy.h>


// STD/STL
#include <vector>


visionx::record::strats::JPGRecordingStrategy::JPGRecordingStrategy()
{
    // pass
}


visionx::record::strats::JPGRecordingStrategy::JPGRecordingStrategy(const std::filesystem::path& filePath, unsigned int fps) :
    visionx::record::AbstractSequencedRecordingStrategy(filePath, fps)
{
    // pass
}


visionx::record::strats::JPGRecordingStrategy::~JPGRecordingStrategy()
{
    // pass
}


void
visionx::record::strats::JPGRecordingStrategy::recordFrame(const cv::Mat& frame, const std::filesystem::path& fullPath)
{
    std::vector<int> params {CV_IMWRITE_JPEG_QUALITY, JPG_QUALITY};
    cv::imwrite(fullPath.string(), frame, params);
}
