/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/record/RecordingMethodRegistry.h>


// VisionX
#include <VisionX/libraries/record/strats/AVIRecordingStrategy.h>
#include <VisionX/libraries/record/strats/BMPRecordingStrategy.h>
#include <VisionX/libraries/record/strats/JPGRecordingStrategy.h>
#include <VisionX/libraries/record/strats/PNGRecordingStrategy.h>
#ifdef LEGACY_X264_SUPPORT
#include <VisionX/libraries/record/strats/H264LegacyRecordingStrategy.h>
#else
#include <VisionX/libraries/record/strats/H264RecordingStrategy.h>
#endif


/**
 * New default recording methods may be registered in this anonymous namespace
 */
namespace
{
    // Registry object for PNG
    const visionx::record::RecordingMethod PNG_RE
    {
        ".png",
        visionx::record::Flag::COMPRESSED,
        &visionx::record::genericRecordingConstructor<visionx::record::strats::PNGRecordingStrategy>
    };

    // Registry object for BMP
    const visionx::record::RecordingMethod BMP_RE
    {
        ".bmp",
        visionx::record::Flag::NONE,
        &visionx::record::genericRecordingConstructor<visionx::record::strats::BMPRecordingStrategy>
    };

    // Registry object for JPG
    const visionx::record::RecordingMethod JPG_RE
    {
        ".jpg",
        visionx::record::Flag::COMPRESSED | visionx::record::Flag::LOSSY,
        &visionx::record::genericRecordingConstructor<visionx::record::strats::JPGRecordingStrategy>
    };

    // Registry object for H264
    const visionx::record::RecordingMethod H264_RE
    {
        ".h264",
        visionx::record::Flag::VIDEO | visionx::record::Flag::COMPRESSED | visionx::record::Flag::LOSSY,
        &visionx::record::genericRecordingConstructor<visionx::record::strats::H264RecordingStrategy>
    };

    // Registry object for AVI
    const visionx::record::RecordingMethod AVI_RE
    {
        ".avi",
        visionx::record::Flag::VIDEO | visionx::record::Flag::COMPRESSED | visionx::record::Flag::LOSSY, // Lossy depends on codec
        &visionx::record::genericRecordingConstructor<visionx::record::strats::AVIRecordingStrategy>
    };

    // Default registry
    const std::map<std::string, visionx::record::RecordingMethod> ANONYMOUS_DEFAULT_REGISTRY = []() -> std::map<std::string, visionx::record::RecordingMethod>
    {
        std::vector<visionx::record::RecordingMethod> defaultList = {PNG_RE, BMP_RE, JPG_RE, H264_RE, AVI_RE};
        std::map<std::string, visionx::record::RecordingMethod> registry;

        for (const visionx::record::RecordingMethod& recordingStrategyMeta : defaultList)
        {
            registry.insert(std::make_pair(recordingStrategyMeta.getFileExtension(), recordingStrategyMeta));
        }

        return registry;
    }();

    // Snapshot recording strategy
    visionx::record::strats::PNGRecordingStrategy snapshotRec;
}


const std::map<std::string, visionx::record::RecordingMethod>
visionx::record::RecordingMethodRegistry::defaultRecordingMethods = ANONYMOUS_DEFAULT_REGISTRY;


std::map<std::string, visionx::record::RecordingMethod>
visionx::record::RecordingMethodRegistry::recordingMethods {};


visionx::record::Recording
visionx::record::RecordingMethodRegistry::newRecording(const std::filesystem::path& filePath, unsigned int fps)
{
    const std::string fileExtension = filePath.extension().string();
    return visionx::record::RecordingMethodRegistry::getMergedRecordingMethods().at(fileExtension).newRecording(filePath, fps);
}


void
visionx::record::RecordingMethodRegistry::registerRecordingMethod(const visionx::record::RecordingMethod& recordMethod)
{
    visionx::record::RecordingMethodRegistry::recordingMethods.insert(std::make_pair(recordMethod.getFileExtension(), recordMethod));
}


visionx::record::RecordingMethod
visionx::record::RecordingMethodRegistry::getRecordingMethod(const std::string& fileExtension)
{
    return visionx::record::RecordingMethodRegistry::getMergedRecordingMethods().at(fileExtension);
}


std::vector<visionx::record::RecordingMethod>
visionx::record::RecordingMethodRegistry::getAvailableRecordingMethods()
{
    // Init output list and get available methods
    std::vector<visionx::record::RecordingMethod> availableRecordingMethods;
    std::map<std::string, visionx::record::RecordingMethod> availableRecordingMethodsMap = visionx::record::RecordingMethodRegistry::getMergedRecordingMethods();

    // Add all available methods to output list
    for (const std::pair<std::string, visionx::record::RecordingMethod>& item : availableRecordingMethodsMap)
    {
        availableRecordingMethods.push_back(item.second);
    }

    return availableRecordingMethods;
}


std::map<std::string, visionx::record::RecordingMethod>
visionx::record::RecordingMethodRegistry::getMergedRecordingMethods()
{
    // Initialise with user-defined methods
    std::map<std::string, visionx::record::RecordingMethod> availableRecordMethods = visionx::record::RecordingMethodRegistry::recordingMethods;

    // Add default methods (insert does not overwrite the map element, if the key already exists)
    for (const std::pair<std::string, visionx::record::RecordingMethod>& item : visionx::record::RecordingMethodRegistry::defaultRecordingMethods)
    {
        availableRecordMethods.insert(std::make_pair(item.first, item.second));
    }

    return availableRecordMethods;
}


void
visionx::record::RecordingMethodRegistry::takeSnapshot(const CByteImage& image, const std::filesystem::path& path)
{
    snapshotRec.recordSnapshot(image, path);
}


void
visionx::record::RecordingMethodRegistry::takeSnapshot(const cv::Mat& image, const std::filesystem::path& path)
{
    snapshotRec.recordSnapshot(image, path);
}
