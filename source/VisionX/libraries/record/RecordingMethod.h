/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#define NUM_AVAILABLE_FLAGS 3


// STD/STL
#include <bitset>
#include <functional>
#include <string>

// VisionX
#include <VisionX/libraries/record/AbstractRecordingStrategy.h>


namespace visionx::record
{
    /**
     * Meta object of a recording strategy.
     *
     * Offers an API to create new instances and get general information about the concrete strategy, like file extension or if it compresses the output
     */
    class RecordingMethod;

    /**
     * Alias for the used bitset, in case more flags are added later on
     */
    using Flags = std::bitset<NUM_AVAILABLE_FLAGS>;
}
/**
 * Flag namespace which acts like an enum for the individual flags and their bit positions
 */
namespace visionx::record::Flag
{
    const constexpr Flags NONE = 0;
    const constexpr Flags LOSSY = 1 << 0;
    const constexpr Flags COMPRESSED = 1 << 1;
    const constexpr Flags VIDEO = 1 << 2;
}




class visionx::record::RecordingMethod
{

protected:

    /**
     * @brief File extension of the recording method
     */
    const std::string fileExtension;

    /**
     * @brief Properties of the recording method like lossy, compressed, ...
     */
    visionx::record::Flags flags;

    /**
     * @brief Factory function of how to construct a recording of this method
     */
    visionx::record::RecordingConstructor constructor;

public:

    /**
     * @brief Default constructor of a recording method
     * @param fileExtension File extension of the recording method
     * @param flags Properties of the recording method like lossy, compressed, ...
     * @param constructor Factory function of how to construct a recording of this method
     */
    RecordingMethod(const std::string& fileExtension, visionx::record::Flags flags, visionx::record::RecordingConstructor constructor);

    /**
     * @brief Gets the file extension of the recording method
     * @return File extension of the recording method
     */
    std::string getFileExtension() const;

    /**
     * @brief Checks whether the recording method compresses
     * @return true if compressed, false if not
     */
    bool isCompressed() const;

    /**
     * @brief Checks whether the recording method is lossy
     * @return true if lossy, false if lossless
     */
    bool isLossy() const;

    /**
     * @brief Checks whether the recording method is a video recording
     * @return true if video recording, false if image sequence
     */
    bool isVideo() const;

    /**
     * @brief Returns a human readable description of the file format
     * @return Human readable description of the file format
     */
    std::string getHumanReadableDescription() const;

    /**
     * @brief Creates a new recording given the file extension and additional parameters
     * @param filePath Path to where the recording file should be written to
     * @param fps Amount of frames being recorded per second
     * @return Recording object
     */
    visionx::record::Recording newRecording(const std::filesystem::path& filePath, unsigned int fps) const;

};
