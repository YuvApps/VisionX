/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/record/helper.h>


// STD/STL
#include <cstring>

// OpenCV
#include <opencv2/imgproc/imgproc.hpp>


void
visionx::record::convert(const CByteImage& in, cv::Mat& out)
{
    // Initialise RGB OpenCV image from CByteImage
    cv::Mat cv_frame(cv::Size(in.width, in.height), CV_8UC3, CV_MAT_CN(CV_8UC3));
    std::memcpy(cv_frame.data, in.pixels, static_cast<unsigned int>(in.width * in.height * in.bytesPerPixel));

    // Convert to BGR OpenCV image (OpenCV assumes this format by default)
    cv::cvtColor(cv_frame, cv_frame, cv::COLOR_RGB2BGR);
    out = cv_frame;
}


void
visionx::record::convert(const cv::Mat& in, CByteImage& out)
{
    // Create a cv::Mat copy and convert to RGB format
    cv::Mat frame_cpy(in.size(), in.type());
    cv::cvtColor(in, frame_cpy, cv::COLOR_BGR2RGB);

    // Initialise CByteImage and copy pixel data
    out.Set(in.size().width, in.size().height, ::CByteImage::ImageType::eRGB24);
    std::memcpy(out.pixels, frame_cpy.data, static_cast<unsigned int>(in.size().height * in.size().width * in.channels()));
}
