/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// Boost
#include <filesystem>

// OpenCV
#include <opencv2/core/core.hpp>

// IVT
#include <Image/ByteImage.h>

// VisionX
#include <VisionX/libraries/record/AbstractRecordingStrategy.h>
#include <VisionX/libraries/record/RecordingMethod.h>


namespace visionx::record
{

    /**
     * @brief Creates a new recording given the file extension and additional parameters
     * @param filePath Path to where the recording file should be written to
     * @param fps Amount of frames being recorded per second
     * @return Recording object
     */
    visionx::record::Recording newRecording(const std::filesystem::path& filePath, unsigned int fps);

    /**
     * @brief Registers a new publicly available recording method
     *
     * Note that default recording methods may be overridden that way
     *
     * @param recordingMethod RecordingMethod object containing the knowledge of how to construct a recording of that type and meta data like file extension
     */
    void registerRecordingMethod(const visionx::record::RecordingMethod& recordingMethod);

    /**
     * @brief Gets a specific recording method registered by the given file extension
     * @param fileExtension File extension
     * @return Recording method
     */
    visionx::record::RecordingMethod getRecordingMethod(const std::string& fileExtension);

    /**
     * @brief Gets a list of all available recording methods, including user-defined recordings
     *
     * Note that default recordings may be shadowed by user-defined recordings
     *
     * @return List of all available recording methods
     */
    std::vector<visionx::record::RecordingMethod> getAvailableRecordingMethods();

    /**
     * @brief Takes a snapshot using the default recording method for snapshots
     * @param filePath Path to where the recording file should be written to
     * @param image The image to be saved
     */
    void takeSnapshot(const CByteImage& image, const std::filesystem::path& filePath);

    /**
     * @brief Takes a snapshot using the default recording method for snapshots
     * @param filePath Path to where the recording file should be written to
     * @param image The image to be saved
     */
    void takeSnapshot(const cv::Mat& image, const std::filesystem::path& filePath);

}

