/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <map>
#include <string>
#include <tuple>
#include <vector>

// Boost
#include <filesystem>

// OpenCV
#include <opencv2/core/core.hpp>

// IVT
#include <Image/ByteImage.h>

// VisionX
#include <VisionX/libraries/record/AbstractRecordingStrategy.h>
#include <VisionX/libraries/record/RecordingMethod.h>


namespace visionx::record
{
    class RecordingMethodRegistry;
}



class visionx::record::RecordingMethodRegistry
{

private:

    static const std::map<std::string, visionx::record::RecordingMethod> defaultRecordingMethods;
    static std::map<std::string, visionx::record::RecordingMethod> recordingMethods;

private:

    RecordingMethodRegistry();

    static std::map<std::string, visionx::record::RecordingMethod> getMergedRecordingMethods();

public:

    static visionx::record::Recording newRecording(const std::filesystem::path& filePath, unsigned int fps);
    static void registerRecordingMethod(const visionx::record::RecordingMethod& recordMethod);
    static visionx::record::RecordingMethod getRecordingMethod(const std::string& fileExtension);
    static std::vector<visionx::record::RecordingMethod> getAvailableRecordingMethods();
    static void takeSnapshot(const CByteImage& image, const std::filesystem::path& path);
    static void takeSnapshot(const cv::Mat& image, const std::filesystem::path& path);

};
