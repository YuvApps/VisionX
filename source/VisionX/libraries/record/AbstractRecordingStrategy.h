/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <memory>
#include <string>
#include <functional>
#include <filesystem>

// OpenCV 2
#include <opencv2/opencv.hpp>

// IVT
#include <Image/ByteImage.h>


namespace visionx::record
{
    /**
     * Abstract interface of a recording strategy
     */
    class AbstractRecordingStrategy;

    /**
     * Convenience alias for any recording strategy
     */
    using Recording = std::shared_ptr<AbstractRecordingStrategy>;

    /**
     * Type of a function with the same signature as the constructor of any recording strategy
     */
    using RecordingConstructor = std::function<visionx::record::Recording(const std::filesystem::path& filePath, unsigned int fps)>;

    /**
     * Generic recording constructor which can be used as a factory method in the registry
     */
    template <typename T>
    visionx::record::Recording genericRecordingConstructor(const std::filesystem::path& filePath, unsigned int fps)
    {
        return std::make_shared<T>(filePath, fps);
    }
}



class visionx::record::AbstractRecordingStrategy
{

private:

    /**
     * @brief Flag to indicate whether the recording already started
     */
    bool recording;

    /**
     * @brief Path to where the recording file should be written to
     */
    std::filesystem::path filePath;

    /**
     * @brief Amount of frames being recorded per second
     */
    unsigned int fps;

public:

    /**
     * @brief Default constructor to start the recording manually
     */
    AbstractRecordingStrategy();

    /**
     * @brief Constructor for any recording strategy, immediately starting the recording
     * @param filePath Path to where the recording file should be written to
     * @param fps Amount of frames being recorded per second
     */
    AbstractRecordingStrategy(const std::filesystem::path& filePath, unsigned int fps);

    /**
     * @brief Destructor
     */
    virtual ~AbstractRecordingStrategy();

    /**
     * @brief Indicates whether this instance is already initialised for recording
     * @return True if it is initialised and ready to record, false if not
     */
    virtual bool isRecording() const;

    /**
     * @brief Starts the recording manually if constructed empty
     * @param filePath Path to where the recording file should be written to
     * @param fps Amount of frames being recorded per second
     */
    virtual void startRecording(const std::filesystem::path& filePath, unsigned int fps);

    /**
     * @brief Adds the given frame to the recording
     * @param frame Frame to be added
     */
    virtual void recordFrame(const CByteImage& frame);

    /**
     * @brief Adds the given frame to the recording
     * @param frame Frame to be added
     */
    virtual void recordFrame(const cv::Mat& frame);

    /**
     * @brief Stops the recording
     */
    virtual void stopRecording();

    /**
     * @brief Gets the raw file path for the recording as configured
     * @return Raw file path for the recording as configureds
     */
    virtual std::filesystem::path getFilePath() const;

    /**
     * @brief Gets the configured FPS for the recording
     * @return Configured FPS for the recording
     */
    virtual unsigned int getFps() const;

    /**
     * @brief Gets the path to the recording without filename
     * @return Path to the recording without filename
     */
    virtual std::filesystem::path getPath() const;

    /**
     * @brief Gets the stem of the configured file (filename without extension)
     * @return Stem of the configured file
     */
    virtual std::filesystem::path getStem() const;

    /**
     * @brief Gets the extension plus preceeded dot of the configured file (e.g. ".avi")
     * @return Extention plus preceeded dot of the configured file
     */
    virtual std::filesystem::path getDotExtension() const;

};
