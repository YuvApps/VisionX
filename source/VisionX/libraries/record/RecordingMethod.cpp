/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::record
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/record/RecordingMethod.h>


visionx::record::RecordingMethod::RecordingMethod(const std::string& fileExtension, visionx::record::Flags flags, visionx::record::RecordingConstructor constructor) :
    fileExtension(fileExtension), flags(flags), constructor(constructor)
{
    // pass
}


std::string
visionx::record::RecordingMethod::getFileExtension() const
{
    return this->fileExtension;
}


bool
visionx::record::RecordingMethod::isCompressed() const
{
    return (this->flags & visionx::record::Flag::COMPRESSED).any();
}


bool
visionx::record::RecordingMethod::isLossy() const
{
    return (this->flags & visionx::record::Flag::LOSSY).any();
}


bool
visionx::record::RecordingMethod::isVideo() const
{
    return (this->flags & visionx::record::Flag::VIDEO).any();
}


std::string
visionx::record::RecordingMethod::getHumanReadableDescription() const
{
    std::string label = std::string(this->isVideo() ? "Video" : "Individual images") + " (*" + this->getFileExtension() + ")";
    return label;
}


visionx::record::Recording
visionx::record::RecordingMethod::newRecording(const std::filesystem::path& filePath, unsigned int fps) const
{
    return this->constructor(filePath, fps);
}
