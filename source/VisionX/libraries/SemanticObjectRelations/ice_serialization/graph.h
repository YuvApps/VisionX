#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <SemanticObjectRelations/RelationGraph/json/AttributedGraph.h>

#include <VisionX/interface/libraries/SemanticObjectRelations/Graph.h>


namespace armarx::semantic
{

    data::Graph toIce(const semrel::AttributedGraph& input);
    semrel::AttributedGraph fromIce(const semantic::data::Graph& graph);


    template <typename VA, typename EA, typename GA>
    data::Graph toIce(const semrel::RelationGraph<VA, EA, GA>& graph)
    {
        return toIce(semrel::toAttributedGraph(graph));
    }

    template <typename VA, typename EA, typename GA>
    data::Graph toIce(const semrel::RelationGraph<VA, EA, GA>& graph, const semrel::ShapeMap& objects)
    {
        return toIce(semrel::toAttributedGraph(graph, objects));
    }

    template <typename GraphT>
    GraphT fromIce(const semantic::data::Graph& graph)
    {
        return fromIce(graph).toGraph<GraphT>();
    }

}
