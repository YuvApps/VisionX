#include "shape.h"

#include <SemanticObjectRelations/Shapes/json.h>


namespace armarx
{

    semantic::data::Shape semantic::toIce(const semrel::Shape& shape)
    {
        semantic::data::Shape result;
        result.json = nlohmann::json(shape).dump();
        return result;
    }

    semrel::ShapePtr semantic::fromIce(const semantic::data::Shape& shape)
    {
        return nlohmann::json::parse(shape.json).get<semrel::ShapePtr>();
    }

    semantic::data::ShapeList semantic::toIce(const semrel::ShapeList& shapes)
    {
        semantic::data::ShapeList result;
        result.reserve(shapes.size());
        for (const auto& shape : shapes)
        {
            result.push_back(toIce(*shape));
        }
        return result;
    }

    semantic::data::ShapeList semantic::toIce(const semrel::ShapeMap& shapes)
    {
        semantic::data::ShapeList result;
        result.reserve(shapes.size());
        for (const auto& [id, shape] : shapes)
        {
            result.push_back(toIce(*shape));
        }
        return result;
    }

    semrel::ShapeList semantic::fromIce(const semantic::data::ShapeList& shapes)
    {
        semrel::ShapeList result;
        result.reserve(shapes.size());
        for (const auto& shape : shapes)
        {
            result.push_back(fromIce(shape));
        }
        return result;
    }

}
