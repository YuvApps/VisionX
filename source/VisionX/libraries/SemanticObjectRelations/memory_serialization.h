#pragma once

#include <SemanticObjectRelations/RelationGraph/json/AttributedGraph.h>

#include <MemoryX/interface/memorytypes/MemoryEntities.h>


namespace armarx::semantic
{


    memoryx::RelationList toMemory(const semrel::AttributedGraph& graph);

    /**
     * @brief Creates an attributed graph from a relation list.
     *
     * The relation list can be retrieved from the working memory.
     * The attributed graph only contains the JSON attributes and not
     * the deserialized shape objects.
     *
     * @param relations List of relations used to create the attributed graph.
     * @return Attributed graph.
     */
    semrel::AttributedGraph fromMemory(const memoryx::RelationList& relations);

}
