#pragma once

#include <SemanticObjectRelations/Hooks/Log.h>

#include <ArmarXCore/core/logging/Logging.h>


namespace armarx::semantic
{

    class ArmarXLog : public semrel::LogInterface, public armarx::Logging
    {

    public:
        static void setAsImplementation(const std::string& tag);

        ArmarXLog(const std::string& tag = "");

        void log(semrel::LogMetaInfo info, const std::string& message) override;

    private:
        armarx::LogSenderPtr getLogSender(const semrel::LogMetaInfo& info);
        armarx::MessageType logLevelToMessageType(semrel::LogLevel level);

    };

}

