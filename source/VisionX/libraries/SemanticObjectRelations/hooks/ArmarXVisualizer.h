#pragma once

#include <SemanticObjectRelations/Hooks/VisualizerInterface.h>

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>


namespace armarx::semantic
{

    /**
     * @brief Implementation of semrel::VisualizerInterface for ArmarX
     * (using the DebugDrawer).
     */
    class ArmarXVisualizer : public semrel::VisualizerInterface
    {
    public:
        static void setAsImplementation(const armarx::DebugDrawerInterfacePrx& debugDrawer);

        ArmarXVisualizer(armarx::DebugDrawerInterfacePrx const& debugDrawer);

        virtual void clearAll() override;
        virtual void clearLayer(const std::string& layer) override;

        virtual void drawLine(semrel::VisuMetaInfo id, const Eigen::Vector3f& start, const Eigen::Vector3f& end, float lineWidth, semrel::DrawColor color) override;
        virtual void drawArrow(semrel::VisuMetaInfo id, const Eigen::Vector3f& origin, const Eigen::Vector3f& direction, float length, float width, semrel::DrawColor color) override;
        virtual void drawBox(semrel::VisuMetaInfo id, const semrel::Box& box, semrel::DrawColor color) override;
        virtual void drawCylinder(semrel::VisuMetaInfo id, const semrel::Cylinder& cylinder, semrel::DrawColor color) override;
        virtual void drawSphere(semrel::VisuMetaInfo id, const semrel::Sphere& sphere, semrel::DrawColor color) override;
        virtual void drawPolygon(semrel::VisuMetaInfo id, const std::vector<Eigen::Vector3f>& polygonPoints, float lineWidth, semrel::DrawColor colorInner, semrel::DrawColor colorBorder) override;
        virtual void drawTriMesh(semrel::VisuMetaInfo id, const semrel::TriMesh& mesh, semrel::DrawColor color) override;
        virtual void drawText(semrel::VisuMetaInfo id, const std::string& text, const Eigen::Vector3f& position, float size, semrel::DrawColor color) override;
        virtual void drawPointCloud(semrel::VisuMetaInfo id, const std::vector<Eigen::Vector3f>& cloud, float pointSize, semrel::DrawColor color) override;

    private:
        armarx::DebugDrawerInterfacePrx drawer;
    };
}

namespace armarx::semantic::properties::defaults
{
    extern const std::string visualizationLevelName;
    extern const std::string visualizationLevelDescription;
}

namespace armarx::semantic::properties
{

    void defineVisualizationLevel(armarx::ComponentPropertyDefinitions& defs,
                                  const std::string& propertyName = defaults::visualizationLevelName,
                                  semrel::VisuLevel defaultLevel = semrel::VisuLevel::RESULT,
                                  const std::string& description = defaults::visualizationLevelDescription);

    semrel::VisuLevel getVisualizationLevel(
        armarx::PropertyUser& defs,
        const std::string& propertyName = defaults::visualizationLevelName);

    void setMinimumVisuLevel(
        armarx::PropertyUser& defs,
        const std::string& propertyName = defaults::visualizationLevelName);

}


