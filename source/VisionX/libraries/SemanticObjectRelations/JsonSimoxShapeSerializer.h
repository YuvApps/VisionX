#pragma once

#include <memory>

#include <SimoxUtility/json/json.hpp>


namespace memoryx
{
    class ObjectClassSegmentWrapper;
}

namespace armarx::semantic
{
    class SimoxObjectShape;


    /**
     * @brief A serializer for `SimoxObjectShape` using the `memoryx::ObjectClassSegment`.
     */
    class JsonSimoxShapeSerializer
    {
    public:

        JsonSimoxShapeSerializer(const memoryx::ObjectClassSegmentWrapper* classSegment = nullptr);

        void to_json(nlohmann::json& j, const SimoxObjectShape& object) const;
        void from_json(const nlohmann::json& j, SimoxObjectShape& object) const;


        /**
         * @brief Register the given serializer in `semrel::json::ShapeSerializers`.
         *
         * The passed instance is held alive by capturing lambdas stored in
         * `semrel::json::ShapeSerializers`.
         *
         * @see `semrel::json::ShapeSerializers::registerSerializer()`
         */
        static void registerSerializer(const std::shared_ptr<JsonSimoxShapeSerializer>& instance,
                                       bool overwrite = false);


    private:

        const memoryx::ObjectClassSegmentWrapper* classSegment = nullptr;

    };

}
