#pragma once

#include <VisionX/libraries/VoxelGridCore/Visualizer.hpp>

#include "VoxelGrid.h"


namespace visionx::voxelgrid::LabelDensity
{

    /**
     * @brief Visualizer for label density voxel grids.
     *
     * Draws voxels as multi-colored boxes, where the proportion of each color
     * corresponds to the proportion of the respective label.
     */
    class Visualizer : public voxelgrid::Visualizer<Voxel>
    {

    public:

        Visualizer();
        Visualizer(const armarx::DebugDrawerTopic& drawer,
                   const std::string& layer = "LabelDensityVoxelGrid");

        virtual ~Visualizer() override;


        /// Get the alpha.
        float getAlpha() const;
        /// Set the alpha. Must be in [0, 1].
        void setAlpha(float value);


    protected:

        virtual bool isVisible(const VoxelVisuData& voxelVisu) const override;
        virtual void drawVoxel(const VoxelVisuData& voxelVisu) override;


        float alpha = 0.75;

    };

}
