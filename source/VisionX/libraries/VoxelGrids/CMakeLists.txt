set(LIB_NAME       VisionXVoxelGrids)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")


find_package(SemanticObjectRelations QUIET)
armarx_build_if(SemanticObjectRelations_FOUND "SemanticObjectRelations not available")
if(SemanticObjectRelations_FOUND)
    #target_link_libraries(${LIB_NAME} PUBLIC SemanticObjectRelations)
endif()


set(LIBS
    ArmarXCore
    RobotAPICore
    VisionXCore VisionXVoxelGridCore
    VirtualRobot
    SemanticObjectRelations
)

set(LIB_FILES
    LabelDensity/VoxelGrid.cpp
    LabelDensity/Visualizer.cpp
    
    LabelOccupancy/VoxelGrid.cpp
    LabelOccupancy/Visualizer.cpp
    
    utils/point_cloud.cpp
)

set(LIB_HEADERS
    VoxelGrids.h
    types.h
    
    LabelDensity.h
    LabelDensity/VoxelGrid.h
    LabelDensity/Visualizer.h
    
    LabelOccupancy.h
    LabelOccupancy/VoxelGrid.h
    LabelOccupancy/Visualizer.h
    
    utils.h
    utils/point_cloud.h
)


armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

find_package(PCL 1.8 QUIET COMPONENTS common)
armarx_build_if(PCL_FOUND "PCL not available")
if(PCL_FOUND)
    target_include_directories(${LIB_NAME} PUBLIC ${PCL_INCLUDE_DIRS})
    target_link_libraries(${LIB_NAME} PUBLIC ${PCL_LIBRARIES})
endif()




# add unit tests
add_subdirectory(test)
