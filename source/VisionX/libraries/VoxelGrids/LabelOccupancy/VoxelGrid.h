#pragma once

#include <filesystem>
#include <set>

#include <VirtualRobot/Util/json/json.hpp>

#include <VisionX/libraries/VoxelGridCore/VoxelGridCore.h>

#include "../types.h"
#include "../LabelDensity/VoxelGrid.h"


namespace visionx::voxelgrid::LabelOccupancy
{

    // VOXEL TYPE

    /**
     * @brief A voxel storing whether it is occupied or free and the object
     * label it is occupied by.
     */
    class Voxel
    {
    public:

        /// Construct an unoccupied voxel.
        Voxel();
        /// Construct an occupied voxel with given label.
        Voxel(Label label);

        /// Indicate whether this voxel is free (i.e. it is not occupied).
        bool isFree() const;
        /// Indicate whether this voxel is occupied.
        bool isOccupied() const;

        /// Get the label. Voxel must be occupied.
        Label getLabel() const;

        /// Set the voxel to free, i.e. unoccupied.
        void setFree();
        /// Set the voxel to occupied and set its label.
        void setOccupied(Label label);


    private:

        /// Whether this voxel is occupied.
        bool _isOccupied = true;
        /// If occupied, the object label occupying this object.
        Label _label = 0;

    };


    /// Set `json` to the voxel's label if occupied, and to -1 if free.
    void to_json(nlohmann::json& json, const Voxel& voxel);
    /// Set this voxel free, if `json` is < 0.
    /// Otherwise, set `voxel`'s label to `json`.
    void from_json(const nlohmann::json& json, Voxel& voxel);



    // VOXEL GRID

    /**
     * @brief Voxel grid storing the number of points per label in each voxel.
     */
    class VoxelGrid : public visionx::VoxelGrid<Voxel>
    {
        using Base = voxelgrid::VoxelGrid<Voxel>;

    public:

        /**
         * @brief Make a label occupancy grid from the given density grid.
         * @see `setByLabelDensity()`
         */
        static VoxelGrid fromLabelDensity(const LabelDensity::VoxelGrid& density);


        using Base::VoxelGrid;


        /// Get the set of unique labels in the grid.
        std::set<Label> getUniqueLabels() const;



        /**
         * @brief Set the voxels by selecting the label with most points
         * for each voxel.
         *
         * This overwrites any information previously stored in this grid.
         *
         * @param density The label density grid.
         * @param adoptStructure
         *      If false, throws an exception if `density`'s structure is not
         *      equal to the structure of `*this`. If you wish to adopt
         *      `density`s structure, pass true.
         *
         * @throw `voxelgrid::error::InvalidStructure`
         *      If `density`'s structure is different to `getStructure()`
         *      and `adoptStructure` is false.
         */
        void setByLabelDensity(const LabelDensity::VoxelGrid& density,
                               bool adoptStructure = false);


        // Serialization

        /// Write this voxel grid to JSON.
        void writeJson(std::ostream& os) const;
        void writeJson(const std::filesystem::path& file) const;

        /// Read this voxel grid from JSON.
        void readJson(std::istream& is);
        void readJson(const std::filesystem::path& file);


    public:

        /// Name of the single voxel array in JSON.
        static const std::string JSON_VOXEL_ARRAY_NAME;

    };


    /// Write the voxel grid to JSON.
    void to_json(nlohmann::json& json, const VoxelGrid& grid);
    /// Read the voxel grid from JSON.
    void from_json(const nlohmann::json& json, VoxelGrid& grid);

}
