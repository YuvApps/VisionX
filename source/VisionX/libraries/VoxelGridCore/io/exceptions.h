#pragma once

#include <ios>  // for std::ios_base::failure

#include "../exceptions.h"


namespace visionx::voxelgrid::error::io
{

    /**
     * @brief Base class for exceptions in this library.
     */
    class VoxelGridIOError : public VoxelGridError
    {
    public:
        using VoxelGridError::VoxelGridError;
    };


    /**
     * @brief Indicates that voxel data with an invalid number of voxels
     * has been passed to a VoxelGrid.
     */
    class BinaryIOError : public VoxelGridIOError
    {
    public:
        BinaryIOError(const std::string& operation, const std::string& path,
                      const std::string& reason, const std::ios_base::failure& causing);
    };


    class BinaryReadError : public VoxelGridIOError
    {
    public:
        BinaryReadError(const std::string& message, const std::ios_base::failure& causing);
    };


}


