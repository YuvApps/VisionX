#include "BinaryIO.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace visionx::voxelgrid::io
{

    void BinaryIO::checkPositive(int value)
    {
        ARMARX_CHECK_POSITIVE(value);
    }

    void BinaryIO::checkPositive(float value)
    {
        ARMARX_CHECK_POSITIVE(value);
    }

}


