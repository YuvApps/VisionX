#pragma once

#include <fstream>
#include <functional>
#include <iostream>
#include <map>

#include <VirtualRobot/Util/json/json.hpp>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "../VoxelGrid.hpp"
#include "to_from_json.h"


namespace visionx::voxelgrid::io
{

    /// Function to get an attribute of a voxel.
    template <class VoxelT>
    using VoxelAttributeGetter = std::function<nlohmann::json(const VoxelT& voxel)>;
    /// Function to set an attribute of a voxel.
    template <class VoxelT>
    using VoxelAttributeSetter = std::function<void(const nlohmann::json& j, VoxelT& voxel)>;

    /// Map of attribute name to attribute getter.
    template <class VoxelT>
    using VoxelAttributeGetterMap = std::map<std::string, VoxelAttributeGetter<VoxelT>>;
    /// Map of attribute name to attribute setter.
    template <class VoxelT>
    using VoxelAttributeSetterMap = std::map<std::string, VoxelAttributeSetter<VoxelT>>;


    /// Make an attribute getter getting a member variable.
    /// Usage: `makeGetter(&MyVoxel::myVar)`
    template <class VoxelT, class MemberT>
    static VoxelAttributeGetter<VoxelT> makeGetter(MemberT VoxelT::*memberVariable)
    {
        return [memberVariable](const VoxelT & v)
        {
            return nlohmann::json(v.*memberVariable);
        };
    }
    /// Make an attribute getter calling a member function.
    /// Usage: `makeGetter(&MyVoxel::myGetter)`
    template <class VoxelT, class MemberT>
    static VoxelAttributeGetter<VoxelT> makeGetter(MemberT(VoxelT::*memberFunc)() const)
    {
        return [memberFunc](const VoxelT & v)
        {
            return (v.*memberFunc)();
        };
    }

    /// Make an attribute setter setting a member variable.
    /// Usage: `makeSetter(&MyVoxel::myVariable)`
    template <class VoxelT, class MemberT>
    static VoxelAttributeSetter<VoxelT> makeSetter(MemberT VoxelT::*memberVariable)
    {
        return [memberVariable](const nlohmann::json & j, VoxelT & v)
        {
            return v.*memberVariable = j.get<MemberT>();
        };
    }

    /// Make an attribute setter calling a member function.
    /// Usage: `makeSetter(&MyVoxel::mySetter)`
    template <class VoxelT, class MemberT>
    static VoxelAttributeSetter<VoxelT> makeSetter(void(VoxelT::*memberFunc)(const MemberT&))
    {
        return [memberFunc](const nlohmann::json & j, VoxelT & v)
        {
            return (v.*memberFunc)(j.get<MemberT>());
        };
    }

    /**
     * @brief Class to store voxel grids in a vectorized JSON format.
     *
     * The methods in class can be used to store voxel grids in JSON format.
     * A voxel grid essentially consists of its structure (grid and voxel
     * size, pose) and its voxel data.
     *
     * In the format produced by this class, there is one top-level JSON
     * element for each voxel attribute to store. Each of this elements
     * is an array storing the attribute value of each voxel.
     *
     * For example, if a voxel has two attributes, `int foo` and `float bar`,
     * the produces JSON object would look like this:
     * @code
     * {
     *   "structure: { ... },
     *   "foo": [0 1 1 2 ... -1 0],
     *   "bar": [0.0, 1.0, 1.1, ... , 0.0, -1.0]
     * }
     * @endcode
     *
     * This is done to reduce the redundancy of JSON for a large number of voxels.
     */
    class JsonIO
    {
    public:

        // WRITE

        /// Write a voxel grid to file.
        template <class VoxelT>
        static void write(
            const std::string& filename,
            const VoxelGrid<VoxelT>& grid,
            const VoxelAttributeGetterMap<VoxelT>& attributeMap,
            int indent = -1, char indentChar = ' ')
        {
            writeJson(filename, toJson(grid, attributeMap), indent, indentChar);
        }

        /// Write a voxel grid to `os`.
        template <class VoxelT>
        static void write(
            std::ostream& os,
            const VoxelGrid<VoxelT>& grid,
            const VoxelAttributeGetterMap<VoxelT>& attributeMap,
            int indent = -1, char indentChar = ' ')
        {
            writeJson(os, toJson(grid, attributeMap), indent, indentChar);
        }


        // READ

        /// Read a voxel grid from file.
        template <class VoxelT>
        static void read(
            const std::string& filename,
            VoxelGrid<VoxelT>& grid,
            const VoxelAttributeGetterMap<VoxelT>& attributeMap)
        {
            fromJson(readJson(filename), grid, attributeMap);
        }

        /// Read a voxel grid from file.
        template <class VoxelT>
        static VoxelGrid<VoxelT> read(
            const std::string& filename,
            const VoxelAttributeGetterMap<VoxelT>& attributeMap);

        /// Read a voxel grid from `is`.
        template <class VoxelT>
        static void read(
            std::istream& is,
            VoxelGrid<VoxelT>& grid,
            const VoxelAttributeGetterMap<VoxelT>& attributeMap)
        {
            fromJson(readJson(is), grid, attributeMap);
        }

        /// Read a voxel grid from `is`.
        template <class VoxelT>
        static VoxelGrid<VoxelT> read(
            std::istream& is,
            const VoxelAttributeGetterMap<VoxelT>& attributeMap);


        // Grid -> JSON

        /// Serialize a voxel grid to JSON.
        template <class VoxelT>
        static void toJson(
            nlohmann::json& j,
            const VoxelGrid<VoxelT>& grid,
            const VoxelAttributeGetterMap<VoxelT>& attributeMap);

        /// Serialize a voxel grid to JSON.
        template <class VoxelT>
        static nlohmann::json toJson(
            const VoxelGrid<VoxelT>& grid,
            const VoxelAttributeGetterMap<VoxelT>& attributeMap);

        /// Serialize a voxel grid to JSON with plain voxel array (with name `voxelArrayName`).
        /// VoxelT must provide an implementation of `to_json()`.
        template <class VoxelT>
        static void toJson(
            nlohmann::json& j,
            const VoxelGrid<VoxelT>& grid,
            const std::string& voxelArrayName = "voxels");

        /// Serialize a voxel grid to JSON with plain voxel array (with name `voxelArrayName`).
        /// VoxelT must provide an implementation of `to_json()`.
        template <class VoxelT>
        static nlohmann::json toJson(
            const VoxelGrid<VoxelT>& grid,
            const std::string& voxelArrayName = "voxels");


        // JSON -> Grid

        /// Deserialize a voxel grid from JSON.
        template <class VoxelT>
        static void fromJson(
            const nlohmann::json& j,
            VoxelGrid<VoxelT>& grid,
            const VoxelAttributeSetterMap<VoxelT>& attributeMap);


        /// Deserialize a voxel grid from JSON with plain voxel array (with name `voxelArrayName`).
        /// VoxelT must provide an implementation of `from_json()`.
        template <class VoxelT>
        static void fromJson(
            const nlohmann::json& j,
            VoxelGrid<VoxelT>& grid,
            const std::string& voxelArrayName = "voxels");


        /// Write JSON json to file.
        static void writeJson(const std::string& filename, const nlohmann::json& j,
                              int indent = -1, char indentChar = ' ');
        /// Write JSON json to `os`.
        static void writeJson(std::ostream& os, const nlohmann::json& j,
                              int indent = -1, char indentChar = ' ');

        /// Read JSON from file.
        static nlohmann::json readJson(const std::string& filename);
        /// Read JSON json `is`.
        static nlohmann::json readJson(std::istream& is);

    };

    template<class VoxelT>
    VoxelGrid<VoxelT> JsonIO::read(
        const std::string& filename,
        const VoxelAttributeGetterMap<VoxelT>& attributeMap)
    {
        VoxelGrid<VoxelT> grid;
        read(filename, grid, attributeMap);
        return grid;
    }

    template<class VoxelT>
    VoxelGrid<VoxelT> JsonIO::read(std::istream& is,
                                   const VoxelAttributeGetterMap<VoxelT>& attributeMap)
    {
        VoxelGrid<VoxelT> grid;
        read(is, grid, attributeMap);
        return grid;
    }


    template<class VoxelT>
    nlohmann::json JsonIO::toJson(
        const VoxelGrid<VoxelT>& grid,
        const VoxelAttributeGetterMap<VoxelT>& attributeMap)
    {
        nlohmann::json j;
        toJson(j, grid, attributeMap);
        return j;
    }

    template<class VoxelT>
    void JsonIO::toJson(
        nlohmann::json& j,
        const VoxelGrid<VoxelT>& grid,
        const VoxelAttributeGetterMap<VoxelT>& attributeMap)
    {
        // Serialize structure.
        j["structure"] = grid.getStructure();

        // Serialize voxel data.

        // Insert array elements.
        for (const auto& item : attributeMap)
        {
            j[item.first] = nlohmann::json::array();
        }

        // Add data.
        for (const VoxelT& voxel : grid)
        {
            for (const auto& item : attributeMap)
            {
                // Fetch serialized attribute from voxel.
                j[item.first].push_back(item.second(voxel));
            }
        }
    }

    template<class VoxelT>
    void JsonIO::toJson(nlohmann::json& j, const VoxelGrid<VoxelT>& grid,
                        const std::string& voxelArrayName)
    {
        // Serialize structure.
        j["structure"] = grid.getStructure();
        // Serialize voxel data.
        j[voxelArrayName] = grid.getVoxels();
    }

    template<class VoxelT>
    nlohmann::json JsonIO::toJson(const VoxelGrid<VoxelT>& grid, const std::string& voxelArrayName)
    {
        nlohmann::json j;
        toJson(j, grid, voxelArrayName);
        return j;
    }


    template<class VoxelT>
    void JsonIO::fromJson(
        const nlohmann::json& j,
        VoxelGrid<VoxelT>& grid,
        const VoxelAttributeSetterMap<VoxelT>& attributeMap)
    {
        // Deserialize structure.
        grid.resetStructure(j.at("structure").get<VoxelGridStructure>());

        ARMARX_CHECK_EQUAL(grid.getVoxels().size(), grid.getStructure().getNumVoxels());

        // Deserialize voxel data.
        for (std::size_t i = 0; i < grid.numVoxels(); ++i)
        {
            VoxelT& voxel = grid[i];
            for (const auto& item : attributeMap)
            {
                // Deserialize and set voxel attribute.
                item.second(j[item.first].at(i), voxel);
            }
        }
    }


    template<class VoxelT>
    void JsonIO::fromJson(
        const nlohmann::json& j,
        VoxelGrid<VoxelT>& grid,
        const std::string& voxelArrayName)
    {
        // Deserialize structure.
        grid.resetStructure(j.at("structure").get<VoxelGridStructure>());
        ARMARX_CHECK_EQUAL(grid.getVoxels().size(), grid.getStructure().getNumVoxels());

        // Deserialize voxel data.
        grid.setVoxels(j.at(voxelArrayName).get<std::vector<VoxelT>>());
    }

}

