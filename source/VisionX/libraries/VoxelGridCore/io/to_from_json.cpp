#include "to_from_json.h"

#include <VirtualRobot/Util/json/eigen_conversion.hpp>


namespace visionx::voxelgrid
{


    void to_json(nlohmann::json& json, const VoxelGridStructure& structure)
    {
        json["gridSize"] = structure.getGridSize();
        json["voxelSize"] = structure.getVoxelSize();
        json["origin"] = structure.getOrigin();
        json["orientation"] = structure.getOrientation();
    }

    void from_json(const nlohmann::json& json, VoxelGridStructure& structure)
    {
        structure.setGridSize(json.at("gridSize").get<Eigen::Vector3i>());
        structure.setVoxelSize(json.at("voxelSize").get<Eigen::Vector3f>());
        structure.setOrigin(json.at("origin").get<Eigen::Vector3f>());
        structure.setOrientation(json.at("orientation").get<Eigen::Quaternionf>());
    }


}
