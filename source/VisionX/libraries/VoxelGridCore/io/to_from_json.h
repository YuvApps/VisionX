#pragma once

#include <VirtualRobot/Util/json/json.hpp>

#include "../VoxelGridStructure.h"


namespace visionx::voxelgrid
{

    void to_json(nlohmann::json& json, const VoxelGridStructure& structure);
    void from_json(const nlohmann::json& json, VoxelGridStructure& structure);

}
