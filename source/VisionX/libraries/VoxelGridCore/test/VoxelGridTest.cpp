/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RelationsForManipulation::ArmarXObjects::SupportVoxelGrid
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::ArmarXLibraries::VoxelGridCore

#define ARMARX_BOOST_TEST

#include <VisionX/Test.h>
#include "../VoxelGrid.hpp"

#include <iostream>

using namespace visionx::voxelgrid;

struct Fixture
{
    Fixture()
    {
    }

    VoxelGrid<int> grid = { { 8, 1.0 } };
};


BOOST_FIXTURE_TEST_SUITE(VoxelGridTestSuite, Fixture)


BOOST_AUTO_TEST_CASE(test_getBar_with_foo)
{
    for (std::size_t i = 0; i < grid.numVoxels(); ++i)
    {
        grid.getVoxel(i) = static_cast<int>(2 * i + 1);
    }

    for (std::size_t i = 0; i < grid.numVoxels(); ++i)
    {
        BOOST_CHECK_EQUAL(grid.getVoxel(i), static_cast<int>(2 * i + 1));
    }
}




BOOST_AUTO_TEST_SUITE_END()
