/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RelationsForManipulation::ArmarXObjects::SupportVoxelGrid
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::ArmarXLibraries::VoxelGridCore

#define ARMARX_BOOST_TEST

#include <VisionX/Test.h>
#include "../VoxelLine.h"

#include <iostream>

using namespace visionx;


struct Fixture
{
    static const Eigen::IOFormat iof;

    Eigen::Vector3i start {0, 0, 0};
    Eigen::Vector3i end {1, 1, 1};

    std::vector<Eigen::Vector3i> points;

    void draw()
    {
        points = VoxelLine::getLineVoxels(start, end);
    }

    void draw(bool includeStart, bool includeEnd)
    {
        points = VoxelLine::getLineVoxels(start, end, includeStart, includeEnd);
    }

    void draw(const Eigen::Vector3i& start, const Eigen::Vector3i& end)
    {
        this->start = start;
        this->end = end;
        draw();
    }

    void draw(const Eigen::Vector3i& start, const Eigen::Vector3i& end,
              bool includeStart, bool includeEnd)
    {
        this->start = start;
        this->end = end;
        draw(includeStart, includeEnd);
    }
};

const Eigen::IOFormat Fixture::iof{3, 0, " ", " ", "", "", "[", "]"};


#define BOOST_CHECK_EQUAL_VECTOR(lhs, rhs) \
    BOOST_CHECK_MESSAGE((lhs) == (rhs), "("#lhs ") == (" #rhs ") failed (" \
                        << lhs.format(iof) << " != " << rhs.format(iof) << ")")


BOOST_FIXTURE_TEST_SUITE(VoxelLineDrawingSuite, Fixture)


BOOST_AUTO_TEST_CASE(test_start_equals_end)
{
    draw({1, 1, 1}, {1, 1, 1});

    BOOST_REQUIRE_EQUAL(points.size(), 1);

    BOOST_TEST_MESSAGE("  " << points.front().format(iof));
    BOOST_CHECK_EQUAL_VECTOR(points.front(), Eigen::Vector3i(1, 1, 1));
}

BOOST_AUTO_TEST_CASE(test_start_equals_end_include_start_exlude_end)
{
    draw({1, 1, 1}, {1, 1, 1}, true, false);

    BOOST_REQUIRE_EQUAL(points.size(), 1);
    BOOST_CHECK_EQUAL_VECTOR(points.front(), Eigen::Vector3i(1, 1, 1));
}

BOOST_AUTO_TEST_CASE(test_start_equals_end_exclude_start_include_end)
{
    draw({1, 1, 1}, {1, 1, 1}, false, true);

    BOOST_REQUIRE_EQUAL(points.size(), 1);

    BOOST_TEST_MESSAGE("  " << points.front().format(iof));
    BOOST_CHECK_EQUAL_VECTOR(points.front(), Eigen::Vector3i(1, 1, 1));
}

BOOST_AUTO_TEST_CASE(test_start_equals_end_include_neither_start_nor_end)
{
    draw({1, 1, 1}, {1, 1, 1}, false, false);
    BOOST_CHECK_EQUAL(points.size(), 0);
}

BOOST_AUTO_TEST_CASE(test_one_step)
{
    draw({0, 0, 0}, {1, 0, 0});

    BOOST_REQUIRE_EQUAL(points.size(), 2);
    BOOST_CHECK_EQUAL_VECTOR(points.front(), start);
    BOOST_CHECK_EQUAL_VECTOR(points.back(), end);
}

BOOST_AUTO_TEST_CASE(test_one_step_include_start_exclude_end)
{
    draw({0, 0, 0}, {1, 0, 0}, true, false);

    BOOST_REQUIRE_EQUAL(points.size(), 1);
    BOOST_CHECK_EQUAL_VECTOR(points.front(), start);
}

BOOST_AUTO_TEST_CASE(test_one_step_exclude_start_include_end)
{
    draw({0, 0, 0}, {1, 0, 0}, false, true);

    BOOST_REQUIRE_EQUAL(points.size(), 1);
    BOOST_CHECK_EQUAL_VECTOR(points.back(), end);
}

BOOST_AUTO_TEST_CASE(test_one_step_include_start_include_end)
{
    draw({0, 0, 0}, {1, 0, 0}, false, false);

    BOOST_REQUIRE_EQUAL(points.size(), 0);
}

BOOST_AUTO_TEST_CASE(test_axis_aligned)
{
    for (int axis = 0; axis < 3; ++axis)
    {
        for (int sign :
             {
                 -1, 1
                 })
        {
            BOOST_TEST_MESSAGE("Axis: " << axis << ", sign: " << sign);
            end.setZero();
            end(axis) = 5 * sign;
            draw();

            for (std::size_t i = 0; i < points.size(); ++i)
            {
                Eigen::Vector3i truth = Eigen::Vector3i::Zero();
                truth(axis) = static_cast<int>(i) * sign;
                BOOST_TEST_MESSAGE("  " << points[i].format(iof));
                BOOST_CHECK_EQUAL_VECTOR(points[i], truth);
            }
        }
    }
}

BOOST_AUTO_TEST_CASE(test_45_degree)
{
    for (int signX :
         {
             -1, 1
         }) for (int signY :
    {
        -1, 1
    }) for (int signZ :
    {
        -1, 1
        })
    {
        Eigen::Vector3i dir(signX, signY, signZ);
        BOOST_TEST_MESSAGE("Dir: " << dir.format(iof));
        draw(start, 5 * dir);

        for (std::size_t i = 0; i < points.size(); ++i)
        {
            int j = static_cast<int>(i);
            BOOST_TEST_MESSAGE("  " << points[i].format(iof));
            BOOST_CHECK_EQUAL_VECTOR(points[i], (j * dir));
        }
    }
}


BOOST_AUTO_TEST_CASE(test_positive_slant)
{
    draw({0, 0, 0}, {2 - 1, 4 - 1, 8 - 1});

    for (std::size_t i = 0; i < points.size(); ++i)
    {
        int j = static_cast<int>(i);
        BOOST_TEST_MESSAGE("  " << points[i].format(iof));
        BOOST_CHECK_EQUAL_VECTOR(points[i], Eigen::Vector3i(j / 4, j / 2, j));
    }
}


BOOST_AUTO_TEST_SUITE_END()
