
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore RoboceptionUtility)
 
armarx_add_test(RoboceptionUtilityTest RoboceptionUtilityTest.cpp "${LIBS}")