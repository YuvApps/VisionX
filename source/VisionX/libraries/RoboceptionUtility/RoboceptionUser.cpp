/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RoboceptionUser
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <stdlib.h>

#include <rc_genicam_api/system.h>
#include <rc_genicam_api/interface.h>
#include <rc_genicam_api/buffer.h>
#include <rc_genicam_api/image.h>
#include <rc_genicam_api/config.h>
#include <rc_genicam_api/pixel_formats.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <VisionX/tools/TypeMapping.h>


#include <Calibration/Calibration.h>
#include <Calibration/StereoCalibration.h>
#include <Image/ImageProcessor.h>

#include "RoboceptionUser.h"

using namespace armarx;


namespace
{

    const char* const GC_COMPONENT_ENABLE = "ComponentEnable";
    const char* const GC_COMPONENT_SELECTOR = "ComponentSelector";

}

bool visionx::RoboceptionUser::initDevice(const std::string& dname)
{
    if (!getenv("GENICAM_GENTL64_PATH"))
    {
        setenv("GENICAM_GENTL64_PATH", "/usr/lib/rc_genicam_api/", 0);
    }
    dev = rcg::getDevice(dname.c_str());
    if (!dev)
    {
        //this randomly gets added by the rc driver
        const auto dname2 = "devicemodul" + dname;
        dev = rcg::getDevice(dname2.c_str());
    }

    if (dev)
    {
        ARMARX_INFO << "Connecting to device: " << dname;
        dev->open(rcg::Device::EXCLUSIVE);
        nodemap = dev->getRemoteNodeMap();
        return true;
    }
    ARMARX_WARNING << "Unable to connect to device: " << dname;

    std::vector<std::shared_ptr<rcg::Device> > devices = rcg::getDevices();
    ARMARX_INFO << "found " << devices.size() << " devices";

    for (const rcg::DevicePtr& d : devices)
    {
        if (dname == d->getID())
        {
            ARMARX_ERROR << "Device: " << d->getID() << " !!!(THIS IS THE DEVICE YOU REQUESTED)!!!";
        }
        else
        {
            ARMARX_INFO << "Device: " << d->getID();
        }
        ARMARX_INFO << "Vendor: " << d->getVendor();
        ARMARX_INFO << "Model: " << d->getModel();
        ARMARX_INFO << "TL type: " << d->getTLType();
        ARMARX_INFO << "Display name: " << d->getDisplayName();
        ARMARX_INFO << "Access status:" << d->getAccessStatus();
        ARMARX_INFO << "User name: " << d->getUserDefinedName();
        ARMARX_INFO << "Serial number: " << d->getSerialNumber();
        ARMARX_INFO << "Version: " << d->getVersion();
        ARMARX_INFO << "TS Frequency: " << d->getTimestampFrequency();
    }

    ARMARX_ERROR << "Please specify the device id. Aborting";

    return false;
}

void visionx::RoboceptionUser::enableIntensity(bool enabled)
{
    rcg::setString(nodemap, GC_COMPONENT_SELECTOR, "Intensity", true);
    rcg::setString(nodemap, GC_COMPONENT_ENABLE, enabled ? "1" : "0", true);
}

void visionx::RoboceptionUser::enableIntensityCombined(bool enabled)
{
    rcg::setString(nodemap, GC_COMPONENT_SELECTOR, "IntensityCombined", true);
    rcg::setString(nodemap, GC_COMPONENT_ENABLE, enabled ? "1" : "0", true);
}

void visionx::RoboceptionUser::enableDisparity(bool enabled)
{
    rcg::setString(nodemap, GC_COMPONENT_SELECTOR, "Disparity", true);
    rcg::setString(nodemap, GC_COMPONENT_ENABLE, enabled ? "1" : "0", true);
}

void visionx::RoboceptionUser::enableConfidence(bool enabled)
{
    rcg::setString(nodemap, GC_COMPONENT_SELECTOR, "Confidence", true);
    rcg::setString(nodemap, GC_COMPONENT_ENABLE, enabled ? "1" : "0", true);
}

void visionx::RoboceptionUser::enableError(bool enabled)
{
    rcg::setString(nodemap, GC_COMPONENT_SELECTOR, "Error", true);
    rcg::setString(nodemap, GC_COMPONENT_ENABLE, enabled ? "1" : "0", true);
}

void visionx::RoboceptionUser::setupStreamAndCalibration(float scalefactor, const std::string& calibSavePath)
{
    numImages = 2;

    if (!rcg::setBoolean(nodemap, "GevIEEE1588", true))
    {
        ARMARX_WARNING << "Could not activate Precision Time Protocol (PTP) client!";
    }

    dimensions.height = rcg::getInteger(nodemap, "Height") / 2.0;
    dimensions.width = rcg::getInteger(nodemap, "Width");
    ARMARX_INFO << VAROUT(dimensions.height) << VAROUT(dimensions.width);

    baseline = rcg::getFloat(nodemap, "Baseline", 0, 0, true);
    focalLengthFactor = rcg::getFloat(nodemap, "FocalLengthFactor", 0, 0, false);
    float focalLength = focalLengthFactor * dimensions.width;
    ARMARX_INFO << VAROUT(focalLengthFactor) << "\t" << VAROUT(focalLength);


    ARMARX_INFO << "Get stream";
    {
        std::vector<rcg::StreamPtr> availableStreams = dev->getStreams();

        for (const rcg::StreamPtr& s : availableStreams)
        {
            ARMARX_INFO << "Stream: " << s->getID();
        }

        if (!availableStreams.size())
        {
            ARMARX_FATAL << "Unable to open stream";
            return;
        }
        else
        {
            this->stream = availableStreams[0];
        }
    }


    cameraImages = new CByteImage*[numImages];
    for (size_t i = 0 ; i < numImages; i++)
    {
        cameraImages[i] = new CByteImage(dimensions.width, dimensions.height, CByteImage::eRGB24);
    }

    scaleFactor = scalefactor;
    if (scaleFactor > 1.0)
    {
        dimensions.height /= scaleFactor;
        dimensions.width /= scaleFactor;
        focalLength /= scaleFactor;
    }

    smallImages = new CByteImage*[numImages];
    for (size_t i = 0 ; i < numImages; i++)
    {
        smallImages[i] = new CByteImage(dimensions.width, dimensions.height, CByteImage::eRGB24);
    }

    CCalibration c;
    c.SetCameraParameters(focalLength, focalLength, dimensions.width / 2.0, dimensions.height / 2.0,
                          0.0, 0.0, 0.0, 0.0,
                          Math3d::unit_mat, Math3d::zero_vec, dimensions.width, dimensions.height);

    CStereoCalibration ivtStereoCalibration;
    ivtStereoCalibration.SetSingleCalibrations(c, c);
    ivtStereoCalibration.rectificationHomographyRight = Math3d::unit_mat;
    ivtStereoCalibration.rectificationHomographyLeft = Math3d::unit_mat;

    Vec3d right_translation;
    right_translation.x = baseline * -1000.0;
    right_translation.y = 0.0;
    right_translation.z = 0.0;
    ivtStereoCalibration.SetExtrinsicParameters(Math3d::unit_mat, Math3d::zero_vec, Math3d::unit_mat, right_translation, false);

    ARMARX_INFO << "Saving calibration to: " << calibSavePath;
    ivtStereoCalibration.SaveCameraParameters(calibSavePath.c_str());
    stereoCalibration = visionx::tools::convert(ivtStereoCalibration);
}

void visionx::RoboceptionUser::startRC()
{
    ARMARX_INFO << "Start streaming Roboception Images";
    // rcg::setString(nodemap, "AcquisitionFrameRate", std::to_string(framesPerSecond), true);
    stream->open();
    stream->startStreaming();
}

void visionx::RoboceptionUser::stopRC()
{
    ARMARX_INFO << "Stop streaming Roboception Images";
    stream->stopStreaming();
    stream->close();
}

void visionx::RoboceptionUser::cleanupRC()
{
    if (stream)
    {
        stream->stopStreaming();
        stream->close();
    }

    if (dev)
    {
        dev->close();
    }

    for (size_t i = 0; i < numImages; i++)
    {
        delete cameraImages[i];
        delete smallImages[i];
    }

    delete [] cameraImages;
    delete [] smallImages;
}

void visionx::RoboceptionUser::printNodeMap() const
{
    GenApi::NodeList_t nodeList;
    nodemap->_GetNodes(nodeList);
    ARMARX_INFO << "RC config nodes";
    for (const auto& node : nodeList)
    {
        ARMARX_INFO << "---- properties of node '" << node->GetName(true) << '\'';
        GENICAM_NAMESPACE::gcstring_vector properties;
        node->GetPropertyNames(properties);
        for (const auto& string : properties)
        {
            ARMARX_INFO << "-------- '" << string << '\'';
        }
    }
}
