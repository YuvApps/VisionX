/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RoboceptionUser
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <rc_genicam_api/device.h>
#include <rc_genicam_api/stream.h>

#include <Image/ImageProcessor.h>

#include <VisionX/interface/components/Calibration.h>

namespace rcg
{

    using DevicePtr = std::shared_ptr<rcg::Device>;
    using StreamPtr = std::shared_ptr<rcg::Stream> ;
}

namespace GENAPI_NAMESPACE
{
    class CNodeMapRef;
}
namespace visionx
{
    /**
    * @defgroup Library-RoboceptionUtility RoboceptionUtility
    * @ingroup Component-RCImageProvider
    * A description of the library RoboceptionUtility.
    *
    * @class RoboceptionUser
    * @ingroup Library-RoboceptionUtility
    * @brief This class contains common implementation for
    * \ref RCImageProvider and \ref RCPointCloudProvider
    */
    class RoboceptionUser
    {
        //Functions: setup
    protected:
        bool initDevice(const std::string& dname);

        void enableIntensity(bool enabled);
        void enableIntensityCombined(bool enabled);
        void enableDisparity(bool enabled);
        void enableConfidence(bool enabled);
        void enableError(bool enabled);

        void setupStreamAndCalibration(float scalefactor, const std::string& calibSavePath);

        //Functions: runtime
    protected:
        void startRC();
        void stopRC();
        void cleanupRC();

        void printNodeMap() const;
        //Data
    protected:
        //rc
        rcg::DevicePtr dev;
        rcg::StreamPtr stream;
        std::shared_ptr<GenApi::CNodeMapRef> nodemap;
        //settings
        size_t numImages = 0;
        float scaleFactor = 1;
        double focalLengthFactor = 1.0;
        double baseline = 1.0;
        //VisionX
        CByteImage** cameraImages{nullptr};
        CByteImage** smallImages{nullptr};
        visionx::StereoCalibration stereoCalibration;
        visionx::ImageDimension dimensions;
    };

}
