/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SelectableImageViewer
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/gui-plugins/ImageMonitor/ImageViewerArea.h>

#include <QWidget>
#include <QMouseEvent>

namespace armarx
{
    /**
    * @defgroup Library-SelectableImageViewer SelectableImageViewer
    * @ingroup VisionX-Components
    * Offers the SelectableImageViewer widget.
    *
    * @class SelectableImageViewer
    * @ingroup VisionX-Libraries
    * @brief A clickable, i.e. selectable, visionx::ImageViewerArea.
    *
    * The SelectableImageViewer is a visionx::ImageViewerArea which can be clicked
    * on to make a selection in the displayed image
    */
    class SelectableImageViewer : public visionx::ImageViewerArea
    {
        Q_OBJECT
    public:
        explicit SelectableImageViewer(QWidget* parent = 0);

        void mousePressEvent(QMouseEvent* ev) override;

    signals:

        /**
         * @brief Signals that a pixel was selected in the image viewer.
         */
        void selected();

        /**
         * @brief Signals that a pixel was selected in the image viewer.
         * @param imageIndex the index of the selected image, in [0 .. numImages-1]
         * @param x the relative x coordinate, in [0 .. 1)
         * @param y the relative y coordinate, in [0 .. 1)
         */
        void selected(int imageIndex, float x, float y);

    public slots:




    };

}
