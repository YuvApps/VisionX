/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SelectableImageViewer
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SelectableImageViewer.h"


using namespace armarx;

SelectableImageViewer::SelectableImageViewer(QWidget* parent) :
    visionx::ImageViewerArea(parent)
{
}

void SelectableImageViewer::mousePressEvent(QMouseEvent* ev)
{
    int clickedX = ev->x();
    int clickedY = ev->y();

    Vec2d dimensions = getScaledImageDimensions();

    // should probably not happen, but check it just in case
    if (clickedX < 0 || clickedX > dimensions.x
        || clickedY < 0 || clickedY > dimensions.y)
    {
        return; // ignore
    }


    float relX = std::min(float(clickedX), dimensions.x - 1); // in [0 .. dims.x - 1]
    float relY = std::min(float(clickedY), dimensions.y - 1); // in [0 .. dims.y - 1]

    relX /= dimensions.x; // in [0 .. 1)
    relY /= dimensions.y; // in [0 .. 1)

    // find corresponsing image and adapt relX accordingly

    int numberImages = getNumberImages();
    int imageIndex = int(relX * numberImages); // in [0 .. numImages]

    // move relX to [0, 1/numImages)
    relX -= float(imageIndex) / numberImages;
    // e.g. 1st (idx 0) of 2 imgs: subtract 0/2 = 0.0   => relX in [0 .. 0.5)
    // e.g. 3rd (idx 2) of 3 imgs: subtract 2/3 = 0.67  => relX in [0 .. 0.33)

    // scale up to [0, 1)
    relX *= numberImages;

    emit selected();
    emit selected(imageIndex, relX, relY);
}

