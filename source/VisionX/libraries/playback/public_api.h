/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::playback
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <string>

// Boost
#include <filesystem>

// VisionX
#include <VisionX/libraries/playback/AbstractPlaybackStrategy.h>


namespace visionx::playback
{

    /**
     * @brief Instantiates and returns a new playback strategy which is capable of replaying the file or collection at path
     * @param path Path to the recording file or collection (folder, glob, ...)
     * @return Concrete playback strategy capable of replaying the recording at path
     */
    visionx::playback::Playback newPlayback(const std::filesystem::path& path);
}
