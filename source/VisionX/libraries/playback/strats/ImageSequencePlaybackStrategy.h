/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::playback
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <string>
#include <vector>

// Boost
#include <filesystem>

// VisionX
#include <VisionX/libraries/playback/AbstractPlaybackStrategy.h>


namespace visionx::playback::strats
{
    class ImageSequencePlaybackStrategy;
}



class visionx::playback::strats::ImageSequencePlaybackStrategy :
    public visionx::playback::AbstractPlaybackStrategy
{

private:

    /**
     * @brief Flag to indicate whether the instance is configured for playback or not
     */
    bool playingBack;

    /**
     * @brief Index of the current frame
     */
    unsigned int currentFrame;

    /**
     * @brief Total amount of frames
     */
    unsigned int frameCount;

    /**
     * @brief Base path of the recordig frame files
     */
    std::filesystem::path basePath;

    /**
     * @brief Prefix of an image file which must match to be considered a frame
     */
    std::string requiredPrefix;

    /**
     * @brief Suffix of an image file which must match to be considered a frame
     */
    std::string requiredSuffix;

    /**
     * @brief Sorted list of each frame in the image sequence
     */
    std::vector<std::filesystem::path> framePaths;

    /**
     * @brief Height of an individual frame in pixel
     */
    unsigned int frameHeight;

    /**
     * @brief Width of an individual frame in pixel
     */
    unsigned int frameWidth;

public:

    /**
     * @brief Default constructor to manually setup later
     */
    ImageSequencePlaybackStrategy();

    /**
     * @brief Constructor initialising the playback immediately
     * @param filePath Path to the recording
     */
    ImageSequencePlaybackStrategy(const std::filesystem::path& filePath);

    /**
     * @brief Destructor
     */
    virtual ~ImageSequencePlaybackStrategy() override;

    /**
     * @brief Indicates whether the instance is configured to be able to play back
     * @return True if it is playing back, false otherwise
     */
    virtual bool isPlayingBack() const override;

    /**
     * @brief Gets the amount of frames per second of the recording
     * @return Amount of frames per second of the recording
     */
    virtual unsigned int getFps() const override;

    /**
     * @brief Gets the total amout of frames in the recording
     * @return Total amount of frames in the recording
     */
    virtual unsigned int getFrameCount() const override;

    /**
     * @brief Gets the height of a frame in pixel
     * @return Height of a frame in pixel
     */
    virtual unsigned int getFrameHeight() const override;

    /**
     * @brief Gets the width of a frame in pixel
     * @return Width of a frame in pixel
     */
    virtual unsigned int getFrameWidth() const override;

    /**
     * @brief Sets the frame from there the playback should resume afterwards (seek)
     * @param frame Frame from where the playback should be resumed (e.g. "0" to replay from the beginning)
     */
    virtual void setCurrentFrame(unsigned int frame) override;

    /**
     * @brief Gets the current frame index of the playback
     * @return The current frame index
     */
    virtual unsigned int getCurrentFrame() const override;

    /**
     * @brief Indicates whether the recording has a consecutive frame
     * @return True, if there is a consecutive frame, false otherwise
     */
    virtual bool hasNextFrame() const override;

    /**
     * @brief Starts the playback
     * @param filePath Path to the recording to play back
     */
    virtual void startPlayback(const std::filesystem::path& filePath) override;

    /**
     * @brief Writes the next frame into a buffer of any form (RGB)
     * @param buffer Output parameter where the frame data of the next frame should be written to
     * @return True on success, false otherwise (no consecutive frame, error while loading frame)
     */
    virtual bool getNextFrame(void* buffer) override;

    /**
     * @brief Writes the next frame into an IVT CByteImage buffer (RGB)
     * @param buffer Output parameter where the frame data of the next frame should be written to
     * @return True on success, false otherwise (no consecutive frame, error while loading frame)
     */
    virtual bool getNextFrame(::CByteImage& buffer) override;

    /**
     * @brief Writes the next frame into an OpenCV Mat buffer (BGR)
     * @param buffer Output parameter where the frame data of the next frame should be written to
     * @return True on success, false otherwise (no consecutive frame, error while loading frame)
     */
    virtual bool getNextFrame(cv::Mat& buffer) override;

    /**
     * @brief Stops the playback
     */
    virtual void stopPlayback() override;

private:

    /**
     * @brief Initialises the base path, and the required prefix and suffix for each frame file
     * @param filePath Given path
     */
    void initBasePathPrefixSuffix(const std::filesystem::path& filePath);

    /**
     * @brief Initialises each frame path given the requirements
     */
    void initFramePaths();

    /**
     * @brief Initialise the height and width of an individual frame
     */
    void initFrameDimensions();

};
