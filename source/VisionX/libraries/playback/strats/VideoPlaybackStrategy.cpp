/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::playback
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/libraries/playback/strats/VideoPlaybackStrategy.h>


// STD/STL
#include <cstring>

// Boost
#include <filesystem>

// OpenCV 2
#include <opencv2/imgproc/imgproc.hpp>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


visionx::playback::strats::VideoPlaybackStrategy::VideoPlaybackStrategy()
{
    this->filePath = "";
}


visionx::playback::strats::VideoPlaybackStrategy::VideoPlaybackStrategy(const std::filesystem::path& filePath)
{
    this->startPlayback(filePath);
}


visionx::playback::strats::VideoPlaybackStrategy::~VideoPlaybackStrategy()
{
    this->stopPlayback();
}


bool
visionx::playback::strats::VideoPlaybackStrategy::isPlayingBack() const
{
    return this->videoCapture.isOpened();
}


unsigned int
visionx::playback::strats::VideoPlaybackStrategy::getFps() const
{
    return this->fps;
}


unsigned int
visionx::playback::strats::VideoPlaybackStrategy::getFrameCount() const
{
    return this->frameCount;
}


unsigned int
visionx::playback::strats::VideoPlaybackStrategy::getFrameHeight() const
{
    return this->frameHeight;
}


unsigned int
visionx::playback::strats::VideoPlaybackStrategy::getFrameWidth() const
{
    return this->frameWidth;
}


void
visionx::playback::strats::VideoPlaybackStrategy::setCurrentFrame(unsigned int frame)
{
    ARMARX_CHECK_LESS_W_HINT(frame, this->frameCount, "Desired frame number to set is above the number of frames in total");
    if (frame != currentFrame)
    {
        this->videoCapture.set(CV_CAP_PROP_POS_FRAMES, static_cast<double>(frame));
        this->currentFrame = frame;
    }
}


unsigned int
visionx::playback::strats::VideoPlaybackStrategy::getCurrentFrame() const
{
    return this->currentFrame;
}


bool
visionx::playback::strats::VideoPlaybackStrategy::hasNextFrame() const
{
    return this->currentFrame < this->frameCount;
}


void
visionx::playback::strats::VideoPlaybackStrategy::startPlayback(const std::filesystem::path& filePath)
{
    this->filePath = std::filesystem::canonical(filePath);
    this->videoCapture.open(this->filePath.string());

    ARMARX_CHECK_EXPRESSION_W_HINT(this->videoCapture.isOpened(), "Could not open the output video file '" + this->filePath.string() + "' for writing");

    this->fps = static_cast<unsigned int>(this->videoCapture.get(CV_CAP_PROP_FPS));
    this->frameHeight = static_cast<unsigned int>(this->videoCapture.get(CV_CAP_PROP_FRAME_HEIGHT));
    this->frameWidth = static_cast<unsigned int>(this->videoCapture.get(CV_CAP_PROP_FRAME_WIDTH));
    this->frameCount = static_cast<unsigned int>(this->videoCapture.get(CV_CAP_PROP_FRAME_COUNT));
    this->currentFrame = 0;
}


bool
visionx::playback::strats::VideoPlaybackStrategy::getNextFrame(void* buffer)
{
    ARMARX_CHECK_EXPRESSION_W_HINT(this->videoCapture.isOpened(), "Could not open the output video file '" + this->filePath.string() + "' for writing");

    cv::Mat frame;
    const bool success = this->videoCapture.read(frame);
    ++this->currentFrame;

    // Convert OpenCV BGR to common sense RGB and copy to buffer
    if (success)
    {
        cv::cvtColor(frame, frame, cv::COLOR_BGR2RGB);
        std::memcpy(buffer, frame.data, this->frameHeight * this->frameWidth * static_cast<unsigned int>(frame.channels()));
        return true;
    }

    return false;
}


bool
visionx::playback::strats::VideoPlaybackStrategy::getNextFrame(::CByteImage& buffer)
{
    const bool success = this->getNextFrame(buffer.pixels);
    ++this->currentFrame;
    return success;
}


bool
visionx::playback::strats::VideoPlaybackStrategy::getNextFrame(cv::Mat& buffer)
{
    ARMARX_CHECK_EXPRESSION_W_HINT(this->videoCapture.isOpened(), "Could not open the output video file '" + this->filePath.string() + "' for writing");

    return this->videoCapture.read(buffer);
}


void
visionx::playback::strats::VideoPlaybackStrategy::stopPlayback()
{
    if (this->videoCapture.isOpened())
    {
        this->videoCapture.release();
    }
}
