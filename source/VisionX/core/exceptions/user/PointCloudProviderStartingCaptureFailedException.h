/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Core
* @author     David Gonzalez <david dot gonzalez at kit dot edu>s
* @copyright  2014 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>

#include <VisionX/interface/core/PointCloudProviderInterface.h>

namespace visionx::exceptions::user
{
    class PointCloudProviderStartingCaptureFailedException: public visionx::PointCloudProviderStartingCaptureFailedException
    {
    public:

        PointCloudProviderStartingCaptureFailedException(std::string reason)
            : visionx::PointCloudProviderStartingCaptureFailedException(reason)
        {
        }

        ~PointCloudProviderStartingCaptureFailedException() throw()
        {
        }
    };
};


