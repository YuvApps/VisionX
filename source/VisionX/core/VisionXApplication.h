/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/application/Application.h>

namespace visionx
{
    /**
     * Base Class for all ArmarX applications. Implements the run method from
     * the Ice::Application interface which is called by the automatically
     * generated main.cpp.
     */
    class ARMARXCORE_IMPORT_EXPORT VisionXApplication :
        virtual public armarx::Application
    {
    protected:
        /**
         * Retrieve the domain name. Overwrites the default "ArmarX" with
         * "VisionX"
         *
         * @return domain name. Defaults to "VisionX".
         */
        std::string getDomainName() override
        {
            return "VisionX";
        }
    };
}

