/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @author     Jan Issac (jan dot issac at gmail dot com)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <VisionX/core/ImageProcessor.h>

#include <algorithm>
#include <opencv2/opencv.hpp>
#include <boost/thread/thread.hpp>

// IVT
#include <Image/ImageProcessor.h>

// ArmarXCore
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/util/OnScopeExit.h>
#include <ArmarXCore/core/system/Synchronization.h>
// VisionXTools
#include <VisionX/tools/TypeMapping.h>
#include <VisionX/tools/ImageUtil.h>
#include <VisionX/tools/HardwareId.h>
#include <VisionX/tools/exceptions/local/UnsupportedImageConversionException.h>

using namespace armarx;

namespace visionx
{
    std::string ImageProcessor::ListenerSuffix = ".ImageListener";
    void ImageProcessor::onInitComponent()
    {
        if (hasProperty("CompressionType") && hasProperty("CompressionQuality"))
        {
            setCompressionType(getProperty<CompressionType>("CompressionType").getValue(), getProperty<int>("CompressionQuality").getValue());
        }
        // call setup of vision component
        onInitImageProcessor();
        float cycleTime = desiredFps == 0 ? 0 : 1000.f / desiredFps;
        processorTask = new PeriodicTask<ImageProcessor>(this, &ImageProcessor::runProcessor, cycleTime, false, "ImageProcessingThread", false);
        if (cycleTime == 0)
        {
            processorTask->setDelayWarningTolerance(1000);
        }
        else
        {
            processorTask->setDelayWarningTolerance(cycleTime * 5);
        }
    }

    void ImageProcessor::onConnectComponent()
    {
        onConnectImageProcessor();

        // create processor task
        if (startProcessorTask)
        {
            processorTask->start();
        }
    }

    void ImageProcessor::onDisconnectComponent()
    {
        processorTask->stop();
        onDisconnectImageProcessor();
    }

    void ImageProcessor::onExitComponent()
    {
        ARMARX_VERBOSE << "ImageProcessor::onExitComponent()";
        onExitImageProcessor();

        cleanup();
    }

    PropertyDefinitionsPtr ImageProcessor::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new ImageProcessorPropertyDefinitions(
                getConfigIdentifier()));
    }

    void ImageProcessor::runProcessor()
    {


        // call process method of sub class
        process();

    }

    std::string ImageProcessor::getImageListenerTopicName(std::string providerName) const
    {
        return providerName + ListenerSuffix;
    }

    void ImageProcessor::usingImageProvider(std::string providerName)
    {
        // use image event topic
        ARMARX_VERBOSE << "Using ImageProvider " << providerName;
        usingTopic(getImageListenerTopicName(providerName));

        // create shared memory consumer
        armarx::IceSharedMemoryConsumer<unsigned char>::pointer_type consumer = new IceSharedMemoryConsumer<unsigned char>(this, providerName, "ImageProvider");
        if (hasProperty("ForceIceTransfer") && getProperty<bool>("ForceIceTransfer").getValue())
        {
            consumer->setTransferMode(armarx::eIce);
        }
        // insert into map
        std::pair<std::string, armarx::IceSharedMemoryConsumer<unsigned char>::pointer_type> consumer_info;
        consumer_info.first = providerName;
        consumer_info.second = consumer;

        usedImageProviders.insert(consumer_info);
    }

    void ImageProcessor::releaseImageProvider(std::string providerName)
    {

        auto it = usedImageProviders.find(providerName);
        if (it != usedImageProviders.end())
        {
            removeProxyDependency(providerName);
            armarx::IceSharedMemoryConsumer<unsigned char>::pointer_type consumer = it->second;
            removeProxyDependency(consumer->getMemoryName());
            unsubscribeFromTopic(getImageListenerTopicName(providerName));
            imageProviderInfoMap.erase(providerName);
            usedImageProviders.erase(it);
        }
    }

    ImageProviderInfo ImageProcessor::getImageProvider(std::string providerName, ImageType destinationImageType, bool waitForProxy)
    {
        // create imageproviderinfo
        ImageProviderInfo provider_info;

        boost::upgrade_lock<boost::shared_mutex> lock(imageProviderInfoMutex);

        //////////////////////////
        // image format handling
        //////////////////////////
        // get proxy for image polling
        provider_info.proxy = getProxy<ImageProviderInterfacePrx>(providerName, waitForProxy);

        provider_info.imageFormat = provider_info.proxy->getImageFormat();
        provider_info.numberImages = provider_info.proxy->getNumberImages();
        provider_info.destinationImageType = destinationImageType;
        provider_info.buffer.resize(provider_info.numberImages * provider_info.imageFormat.dimension.width * provider_info.imageFormat.dimension.height * provider_info.imageFormat.bytesPerPixel);

        //////////////////////////
        // synchronisation
        //////////////////////////
        provider_info.imageAvailableEvent.reset(new boost::condition_variable);
        provider_info.imageAvailable = false;

        // update image provider info (and lock mutex exclusively for writing)
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        std::pair<std::string, ImageProviderInfo> entry(providerName, provider_info);
        imageProviderInfoMap.insert(entry);

        // find image provider by name
        ImageProviderMap::iterator iter = usedImageProviders.find(providerName);

        if (iter == usedImageProviders.end())
        {
            usingImageProvider(providerName);
        }

        if (!provider_info.proxy->hasSharedMemorySupport())
        {
            ARMARX_WARNING << "shared memory not available for provider " << providerName;
            usedImageProviders[providerName]->setTransferMode(eIce);
            imageProviderInfoMap[providerName].imageTransferMode = eIceTransfer;
            imageProviderInfoMap[providerName].info = new MetaInfoSizeBase(0, 0, TimeUtil::GetTime().toMicroSeconds());
            removeProxyDependency(usedImageProviders[providerName]->getMemoryName());
        }
        else
        {
            //////////////////////////
            // start communication
            //////////////////////////
            usedImageProviders[providerName]->start();

            auto transferMode = usedImageProviders[providerName]->getTransferMode();
            switch (transferMode)
            {
                case eIce:
                    imageProviderInfoMap[providerName].imageTransferMode = eIceTransfer;
                    break;
                case eSharedMem:
                    imageProviderInfoMap[providerName].imageTransferMode = eSharedMemoryTransfer;
                    break;
            }
        }

        boost::mutex::scoped_lock lock2(statisticsMutex);
        statistics[providerName].pollingFPS.reset();
        statistics[providerName].imageProviderFPS.reset();

        return provider_info;
    }

    ImageProviderInfo ImageProcessor::getImageProvider(std::string name, bool waitForProxy, ImageType destinationImageType)
    {
        return getImageProvider(name, destinationImageType, waitForProxy);
    }

    void ImageProcessor::enableResultImages(int numberImages, ImageDimension imageDimension, ImageType imageType, const std::string& name)
    {
        if (!resultImageProvider)
        {
            ARMARX_VERBOSE << "Enabling ResultImageProvider with " << numberImages << " result images.";
            resultImageProvider = Component::create<ResultImageProvider>();
            resultImageProvider->setName(name.empty() ? getName() + "Result" : name);

            getArmarXManager()->addObject(resultImageProvider);

            resultImageProvider->setNumberResultImages(numberImages);
            resultImageProvider->setResultImageFormat(imageDimension, imageType);

            // wait for resultImageProvider
            resultImageProvider->getObjectScheduler()->waitForObjectState(eManagedIceObjectStarted);
        }
    }

    void ImageProcessor::provideResultImages(CByteImage** images, armarx::MetaInfoSizeBasePtr info)
    {
        if (resultImageProvider)
        {
            if (info)
            {
                resultImageProvider->provideResultImages(images, info->timeProvided);
            }
            else
            {
                resultImageProvider->provideResultImages(images, armarx::TimeUtil::GetTime().toMicroSeconds());
            }
        }
    }

    void ImageProcessor::provideResultImages(const std::vector<CByteImageUPtr >& images, MetaInfoSizeBasePtr info)
    {
        if (resultImageProvider)
        {
            if (info)
            {
                resultImageProvider->provideResultImages(images, info->timeProvided);
            }
            else
            {
                resultImageProvider->provideResultImages(images, armarx::TimeUtil::GetTime().toMicroSeconds());
            }
        }
    }

    bool ImageProcessor::waitForImages(int milliseconds)
    {
        if (imageProviderInfoMap.size() == 0)
        {
            ARMARX_ERROR << "Calling getImages without ImageProvider name, and no ImageProvider is available - did you forget to call useImageProvider?";
            return false;
        }
        else if (imageProviderInfoMap.size() > 1)
        {
            std::string outputText = "Several image providers are available - using the first one in the list:";
            std::map<std::string, ImageProviderInfo>::iterator iter;

            for (iter = imageProviderInfoMap.begin(); iter != imageProviderInfoMap.end(); iter++)
            {
                outputText << " " << iter->first;
            }

            ARMARX_VERBOSE << outputText;
        }

        return waitForImages(imageProviderInfoMap.begin()->first, milliseconds);
    }

    bool ImageProcessor::waitForImages(std::string providerName, int milliseconds)
    {
        std::shared_ptr<boost::condition_variable> cond;
        {
            // lock image provider info mutex for reading
            armarx::ScopedSharedLock lock(imageProviderInfoMutex);
            // find image provider by name
            std::map<std::string, ImageProviderInfo>::iterator iter = imageProviderInfoMap.find(providerName);

            if (iter == imageProviderInfoMap.end())
            {
                ARMARX_ERROR << "Trying to wait for images from unknown image provider. Call useImageProvider before";
                return false;
            }
            cond = iter->second.imageAvailableEvent;
        }


        ARMARX_DEBUG << "Waiting for images from provider proxy " << providerName;

        // wait for conditionale
        boost::mutex mut;
        boost::unique_lock<boost::mutex> lock(mut);
        boost::posix_time::time_duration td = boost::posix_time::milliseconds(milliseconds);

        return cond->timed_wait(lock, td);
    }

    bool ImageProcessor::waitForImages(std::string providerName, IceUtil::Time waitTime)
    {
        return waitForImages(providerName, static_cast<int>(waitTime.toMilliSeconds()));
    }

    bool ImageProcessor::isNewImageAvailable()
    {
        return isNewImageAvailable(imageProviderInfoMap.begin()->first);
    }

    bool ImageProcessor::isNewImageAvailable(const std::string& providerName)
    {
        armarx::ScopedSharedLock lock(imageProviderInfoMutex);
        std::map<std::string, ImageProviderInfo>::iterator iter = imageProviderInfoMap.find(providerName);

        if (iter == imageProviderInfoMap.end())
        {
            ARMARX_ERROR << "Trying access unknown image provider. Call useImageProvider before";
            return false;
        }

        bool result = iter->second.imageAvailable;
        return result;
    }

    int ImageProcessor::getImages(CByteImage** ppImages)
    {
        int numberOfImages = getNumberOfImages();

        if (!ppImages)
        {
            ARMARX_ERROR << "ppImages must not be NULL!";
        }
        else for (int i = 0; i < numberOfImages; ++i)
            {
                if (!ppImages[i])
                {
                    ARMARX_ERROR << "ppImages[i] must not be NULL!";
                }
            }

        void** ppBuffer = new void* [numberOfImages];

        for (int i = 0; i < numberOfImages; ++i)
        {
            ppBuffer[i] = (void*) ppImages[i]->pixels;
        }

        numberOfImages = getImages(ppBuffer);
        delete[] ppBuffer;

        return numberOfImages;
    }

    int ImageProcessor::getImages(CFloatImage** ppImages)
    {
        int numberOfImages = getNumberOfImages();

        void** ppBuffer = new void* [numberOfImages];

        for (int i = 0; i < numberOfImages; ++i)
        {
            ppBuffer[i] = (void*) ppImages[i]->pixels;
        }

        numberOfImages = getImages(ppBuffer);
        delete[] ppBuffer;

        return numberOfImages;
    }

    int ImageProcessor::getImages(void** ppBuffer)
    {
        if (imageProviderInfoMap.size() == 0)
        {
            ARMARX_ERROR << "Calling getImages without ImageProvider name, and no ImageProvider is available - did you forget to call useImageProvider?";
            return false;
        }
        else if (imageProviderInfoMap.size() > 1)
        {
            std::string outputText = "Several image providers are available - using the first one in the list:";
            std::map<std::string, ImageProviderInfo>::iterator iter;

            for (iter = imageProviderInfoMap.begin(); iter != imageProviderInfoMap.end(); iter++)
            {
                outputText << " " << iter->first;
            }

            ARMARX_VERBOSE << outputText;
        }
        std::string providerName;
        {
            armarx::ScopedSharedLock lock(imageProviderInfoMutex);
            providerName = imageProviderInfoMap.begin()->first;
        }


        armarx::MetaInfoSizeBasePtr info;
        return getImages(providerName, ppBuffer, info);
    }

    int ImageProcessor::getImages(std::string providerName, CByteImage** ppImages, armarx::MetaInfoSizeBasePtr& info)
    {
        int numberOfImages = getNumberOfImages(providerName);

        void** ppBuffer = new void* [numberOfImages];

        for (int i = 0; i < numberOfImages; ++i)
        {
            ppBuffer[i] = (void*) ppImages[i]->pixels;
        }

        numberOfImages = getImages(providerName, ppBuffer, info);
        delete[] ppBuffer;

        return numberOfImages;
    }

    int ImageProcessor::getImages(std::string providerName, const std::vector<CByteImageUPtr >& ppImages, MetaInfoSizeBasePtr& info)
    {
        int numberOfImages = getNumberOfImages(providerName);

        void** ppBuffer = new void* [numberOfImages];

        for (int i = 0; i < numberOfImages; ++i)
        {
            ppBuffer[i] = (void*) ppImages.at(i)->pixels;
        }

        numberOfImages = getImages(providerName, ppBuffer, info);
        delete[] ppBuffer;

        return numberOfImages;
    }

    int ImageProcessor::getImages(std::string providerName, CFloatImage** ppImages, armarx::MetaInfoSizeBasePtr& info)
    {
        int numberOfImages = getNumberOfImages(providerName);

        void** ppBuffer = new void* [numberOfImages];

        for (int i = 0; i < numberOfImages; ++i)
        {
            ppBuffer[i] = (void*) ppImages[i]->pixels;
        }

        numberOfImages = getImages(providerName, ppBuffer, info);
        delete[] ppBuffer;

        return numberOfImages;
    }

    int ImageProcessor::getImages(std::string providerName, void** ppBuffer, armarx::MetaInfoSizeBasePtr& info)
    {
        int numberImages = -1;
        {
            // lock image provider info mutex for reading (and changing imageAvailable);

            armarx::ScopedSharedLock lock(imageProviderInfoMutex);

            // find image provider
            std::map<std::string, ImageProviderInfo>::iterator iter = imageProviderInfoMap.find(providerName);

            if (iter == imageProviderInfoMap.end())
            {
                ARMARX_ERROR << "Trying to retrieve images from unknown image provider. Call useImageProvider before";
                return 0;
            }

            numberImages = iter->second.numberImages;

            // check if new images are available
            if (!iter->second.imageAvailable)
            {
                ARMARX_IMPORTANT << "No image available";
                return 0;
            }


            ImageProviderInfo& providerInfo = iter->second;
            if (providerInfo.imageTransferMode == eIceTransfer)
            {
                CompressedImageProviderInterfacePrx compressedImageProvider;
                if (compressionType != eNoCompression)
                {
                    compressedImageProvider = CompressedImageProviderInterfacePrx::checkedCast(iter->second.proxy);
                    if (!compressedImageProvider)
                    {
                        ARMARX_WARNING << deactivateSpam(10000000, iter->first) <<  "Trying to use image provider as CompressedImageProviderInterfacePrx, but it is not of this type: " << iter->second.proxy->ice_id();
                    }
                }
                if (compressionType != eNoCompression && compressedImageProvider)
                {
                    ARMARX_DEBUG << deactivateSpam(10) << "Using compressed images";
                    auto blob = compressedImageProvider->getCompressedImagesAndMetaInfo(compressionType, compressionQuality, providerInfo.info);
                    ARMARX_CHECK_GREATER(blob.size(), 0);
                    ARMARX_DEBUG << deactivateSpam(10) << "Got image blob of size " << blob.size();
                    cv::Mat mat = cv::imdecode(blob, CV_LOAD_IMAGE_COLOR);
                    ARMARX_CHECK_GREATER(mat.size().area(), 0);
                    ARMARX_DEBUG << deactivateSpam(10) << "decoded image to size  " << mat.size().width << "x" << mat.size().height << ", bpp: " << mat.channels();
                    providerInfo.buffer = Blob(mat.datastart, mat.dataend);
                    ARMARX_DEBUG << deactivateSpam(10) << "decoded blob size  " << providerInfo.buffer.size();
                }
                else
                {
                    Blob blob;
                    try
                    {
                        blob = iter->second.proxy->getImagesAndMetaInfo(providerInfo.info);
                        ARMARX_DEBUG << deactivateSpam(1) << "The image is " << (TimeUtil::GetTime() - IceUtil::Time::microSeconds(providerInfo.info->timeProvided)).toMilliSecondsDouble() << " ms old, timestamp: " << providerInfo.info->timeProvided;
                    }
                    catch (...)
                    {
                        ARMARX_INFO << deactivateSpam(10) << "using fall back remote procedure call without timestamp!";

                        providerInfo.info->timeProvided = TimeUtil::GetTime().toMicroSeconds();
                        blob = iter->second.proxy->getImages();
                    }

                    blob.swap(providerInfo.buffer);
                }
            }
            else
            {
                usedImageProviders[providerName]->getData(providerInfo.buffer, providerInfo.info);
            }
            // poll images
            int imageSize = providerInfo.imageFormat.dimension.width * providerInfo.imageFormat.dimension.height * providerInfo.imageFormat.bytesPerPixel;

            // copy images
            for (int i = 0 ; i < numberImages ; i++)
            {
                visionx::tools::convertImage(iter->second, &iter->second.buffer[0] + i * imageSize, ppBuffer[i]);
            }

            info = iter->second.info;

            // set image available to false
            iter->second.imageAvailable = false;
        }



        // update statistics
        boost::mutex::scoped_lock lock(statisticsMutex);
        statistics[providerName].pollingFPS.update();

        return numberImages;
    }


    int ImageProcessor::getNumberOfImages(const std::string& providerName)
    {
        if (imageProviderInfoMap.size() == 0 && providerName.empty())
        {
            ARMARX_ERROR << "Calling getNumberOfImages() without ImageProvider name, and no ImageProvider is available - did you forget to call useImageProvider?";
            return 0;
        }
        else if (imageProviderInfoMap.size() > 1 && providerName.empty())
        {
            std::string outputText = "Several image providers are available and no name was specified when calling getNumberOfImages() - using the first one in the list:";
            std::map<std::string, ImageProviderInfo>::iterator iter;

            for (iter = imageProviderInfoMap.begin(); iter != imageProviderInfoMap.end(); iter++)
            {
                outputText << " " << iter->first;
            }

            ARMARX_VERBOSE << outputText;
        }

        int numberOfImages = 0;

        if (providerName.empty())
        {
            armarx::ScopedSharedLock lock(imageProviderInfoMutex);

            numberOfImages = imageProviderInfoMap.begin()->second.numberImages;
        }
        else
        {
            armarx::ScopedSharedLock lock(imageProviderInfoMutex);

            // find image provider
            std::map<std::string, ImageProviderInfo>::iterator iter = imageProviderInfoMap.find(providerName);

            if (iter == imageProviderInfoMap.end())
            {
                ARMARX_ERROR << "Trying to retrieve images from unknown image provider. Call useImageProvider before";
                return 0;
            }

            numberOfImages = iter->second.numberImages;
        }

        return numberOfImages;
    }


    ImageTransferStats ImageProcessor::getImageTransferStats(std::string providerName, bool resetStats)
    {
        boost::mutex::scoped_lock lock(statisticsMutex);

        std::map<std::string, ImageTransferStats>::iterator iter = statistics.find(providerName);

        if (iter == statistics.end())
        {
            ARMARX_ERROR << "Requesting statistics for unknown image provider (" << providerName << ")";
            return ImageTransferStats();
        }

        ImageTransferStats stats = iter->second;

        if (resetStats)
        {
            iter->second.imageProviderFPS.reset();
            iter->second.pollingFPS.reset();
        }
        else
        {
            iter->second.imageProviderFPS.recalculate();
            iter->second.pollingFPS.recalculate();
        }

        return stats;
    }

    MetaInfoSizeBasePtr ImageProcessor::getImageMetaInfo(const std::string& imageProviderName) const
    {

        ARMARX_WARNING << "deprecated. Use getImages() instead.";

        if (usedImageProviders.empty())
        {
            ARMARX_WARNING << "usedImageProviders is empty";
            return NULL;
        }
        if (imageProviderName.empty())
        {
            return usedImageProviders.begin()->second->getMetaInfo();
        }
        else
        {
            auto it = usedImageProviders.find(imageProviderName);
            if (it != usedImageProviders.end())
            {
                return it->second->getMetaInfo();
            }
            else
            {
                ARMARX_ERROR << "Requesting meta info for unknown image provider (" << imageProviderName << ")";
                return NULL;
            }
        }
    }

    void ImageProcessor::setFramerate(float fps)
    {
        this->desiredFps = fps;
        if (processorTask)
        {
            processorTask->changeInterval(1000.f / fps);
        }
    }

    float ImageProcessor::getFramerate() const
    {
        return desiredFps;
    }

    void ImageProcessor::setCompressionType(CompressionType compressionType, int compressionQuality)
    {
        ARMARX_VERBOSE << "Setting compression to " << (int)compressionType << " and quality " << compressionQuality;
        this->compressionType = compressionType;
        this->compressionQuality = compressionQuality;
    }

    void ImageProcessor::reportImageAvailable(const std::string& providerName, const Ice::Current& c)
    {
        {
            // lock mutex for reading
            armarx::ScopedSharedLock lock(imageProviderInfoMutex);
            // find provider
            std::map<std::string, ImageProviderInfo>::iterator iter = imageProviderInfoMap.find(providerName);

            if (iter == imageProviderInfoMap.end())
            {
                ARMARX_ERROR << deactivateSpam(10, providerName) << "Received notification from unknown imageprovider (" << providerName << ")";

                return;
            }

            iter->second.imageAvailable = true;
            iter->second.imageAvailableEvent->notify_all();
            ARMARX_DEBUG << "Got notification of image provider " << providerName;
        }

        // update statistics
        boost::mutex::scoped_lock lock(statisticsMutex);
        statistics[providerName].imageProviderFPS.update();
    }

    void ImageProcessor::cleanup()
    {
        std::map<std::string, ImageProviderInfo>::iterator iter = imageProviderInfoMap.begin();

        while (iter != imageProviderInfoMap.end())
        {
            iter++;
        }

        if (resultImageProvider)
        {
            getArmarXManager()->removeObjectBlocking(resultImageProvider);
        }
    }

    void ImageProcessor::componentPropertiesUpdated(const std::set<std::string>& changedProperties)
    {
        //        ARMARX_INFO << VAROUT(changedProperties);
        if (changedProperties.count("CompressionType"))
        {
            setCompressionType(getProperty<CompressionType>("CompressionType").getValue());
        }
        if (changedProperties.count("CompressionQuality"))
        {
            setCompressionType(getProperty<CompressionType>("CompressionType").getValue(), getProperty<int>("CompressionQuality").getValue());
        }
    }



    int ImageProcessor::getCompressionQuality() const
    {
        return compressionQuality;
    }

    CompressionType ImageProcessor::getCompressionType() const
    {
        return compressionType;
    }
}
