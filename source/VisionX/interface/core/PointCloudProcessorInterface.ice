/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Core
* @author     David Gonzalez <david dot gonzalez at kit dot edu>
* @copyright  2014 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/DataTypes.ice>
#include <VisionX/interface/core/PointCloudProviderInterface.ice>
#include <VisionX/interface/core/ImageProcessorInterface.ice>

/**
 * Definition of all generic VisionX interfaces
 */
module visionx
{
        /**
         * A point cloud processor component listener interface.
         * A listener is able to receive point clouds update notifications from multiple point clouds providers.
         * This supports point cloud transfer via shared memory if the listener and provider reside on the same machine.
         */
        interface PointCloudProcessorInterface
        {
            /**
             * Point cloud update notification callback invoked by the respective registerd provider.
             *
             * @param providerName name of the notifying provider.
             *        In case of multiple provider this can be used to discriminate the image source.
             */
            void reportPointCloudAvailable(string providerName);
        };

        interface PointCloudAndImageProcessorInterface extends PointCloudProcessorInterface, ImageProcessorInterface
        {
        };
};

