/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Core
* @author     Kai Welke <welke at kit dot edu>
* @author     Jan Issac <jan dot issac at gmx dot de>
* @copyright  2010 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/DataTypes.ice>
#include <RobotAPI/interface/units/UnitInterface.ice>
#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/SharedMemory.ice>

/**
 * Definition of all generic VisionX interfaces
 */
module visionx
{    
    interface ProviderWithSharedMemorySupportInterface
    {
        /**
         * Check if provider has shared memory support.
         */
        bool hasSharedMemorySupport();
    };

    /**
     * An image provider component captures images from cameras, files, ...
     **/
    interface ImageProviderInterface extends armarx::SensorActorUnitInterface, ProviderWithSharedMemorySupportInterface
    {
        /**
         * Retrives provider's images
         */
        armarx::Blob getImages();
        armarx::Blob getImagesAndMetaInfo(out armarx::MetaInfoSizeBase info);

        /**
         * Retrives provider's image format information via Ice.
         */
        ImageFormatInfo getImageFormat();

        /**
         * Retrives provider's image format information via Ice.
         */
        int getNumberImages();
    };


    exception FrameRateNotSupportedException extends armarx::UserException
    {
        float frameRate;
    };
    
    exception StartingCaptureFailedException extends armarx::UserException
    {
    
    };

    enum CompressionType
    {
        eNoCompression,
        ePNG,
        eJPEG
    };

    /**
     * An image provider component captures images from cameras, files, ...
     **/
    interface CompressedImageProviderInterface extends ImageProviderInterface
    {
        /**
         * Retrieves provider's images
         */
        armarx::Blob getCompressedImagesAndMetaInfo(CompressionType compressionType, int compressionQuality, out armarx::MetaInfoSizeBase info);
    };


    interface CapturingImageProviderInterface extends CompressedImageProviderInterface
    {
        /**
         * Initiate image capturing.
         *
         * @param framesPerSecond FPS that has to be ensure if the synchronization
         *        mode is set to eFpsSynchronization, otherwise this won't have any
         *        effect.
         */
        void startCapture(float framesPerSecond) throws FrameRateNotSupportedException,
                                                        StartingCaptureFailedException;
        
        /**
         * Suspends image capturing and hence image update notification
         * broadcasts
         */
        void stopCapture();
    };

};

