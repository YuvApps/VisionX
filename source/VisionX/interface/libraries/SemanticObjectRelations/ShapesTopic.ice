#pragma once

#include <VisionX/interface/libraries/SemanticObjectRelations/Shape.ice>


module armarx
{
    module semantic
    {

        interface ShapesTopic
        {
            void reportShapes(string name, data::ShapeList shapes);
        };

    };
};
