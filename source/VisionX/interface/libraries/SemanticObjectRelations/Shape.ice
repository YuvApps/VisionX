#pragma once

//#include <RobotAPI/interface/core/PoseBase.ice>


module armarx
{
    module semantic
    {
        module data
        {

            struct Shape
            {
                string json;
            };

            sequence<Shape> ShapeList;

        };
    };
};
