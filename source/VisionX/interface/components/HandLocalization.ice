/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Core
* @author     David Schiebener <schiebener at kit dot edu>
* @copyright  2013 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/DataTypes.ice>
#include <VisionX/interface/core/ImageProcessorInterface.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>

/**
 * Definition of all generic hand localization interfaces
 */
module visionx
{
    interface HandLocalizationInterface extends ImageProcessorInterface
    {
        armarx::FramedPoseBase getHandPose();
    };

    sequence<armarx::FramedPositionBase> FramedPositionBaseList;
    interface HandLocalizationWithFingertipsInterface extends HandLocalizationInterface
    {
    	FramedPositionBaseList getFingertipPositions();
    };
};

