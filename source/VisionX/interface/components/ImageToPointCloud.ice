#pragma once
/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * package    ActiveVision::ImageToPointCloud
 * author     Markus Grotz ( markus dot grotz at kit dot edu )
 * date       2019
 * copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *            GNU General Public License
 */

#include <ArmarXCore/interface/core/BasicTypes.ice>

#include <VisionX/interface/components/Calibration.ice>
#include <VisionX/interface/core/ImageProcessorInterface.ice>
#include <VisionX/interface/core/ImageProviderInterface.ice>
#include <VisionX/interface/core/PointCloudProviderInterface.ice>



module armarx
{

	interface ImageToPointCloudInterface extends visionx::ImageProcessorInterface, visionx::PointCloudProviderInterface
    {

    };
};
