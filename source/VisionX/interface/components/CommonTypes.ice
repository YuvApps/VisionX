/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    
 * @author     
 * @date       
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

module armarx
{
    struct LocationEstimate
    {
	float x;
	float y;
	float alpha;
    };

    struct Particle
    {
	LocationEstimate locEst;
	float neckRollError;
	float neckPitchError;
	float neckYawError;
	float importanceWeight;
    };

    struct Direction
    {
	float x;
	float y;
    };

    struct MeanScatteringDirections
    {
	float x;
	float y;
    };

    struct PlaneScatteringDirections
    {
	Direction direction1;
	MeanScatteringDirections meanScattDir;
	float sigma1;
	float sigma2;
    };

    sequence<float> vect;

    struct BoundingBox
    {
	vect min;
	vect max;
    };

    sequence<Particle> ParticleVector;
};

