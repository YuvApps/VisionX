#pragma once

#include <VisionX/interface/core/PointCloudProcessorInterface.ice>
#include <VisionX/interface/core/DataTypes.ice>


module visionx
{

    interface UserAssistedSegmenterInterface extends PointCloudProcessorInterface
    {
        void publishSegmentation(ColoredLabeledPointCloud pointCloud);
    };

    interface UserAssistedSegmenterListener
    {
        void reportSegmentation(ColoredLabeledPointCloud pointCloud);
    };

};

