#pragma once

#include <VisionX/interface/libraries/SemanticObjectRelations/Shape.ice>

#include <VisionX/interface/core/PointCloudProcessorInterface.ice>


module visionx
{

    interface SegmentRansacShapeExtractorInterface extends visionx::PointCloudProcessorInterface
    {
        /**
         * Performs shape extraction from the latest received point cloud and
         * returns the result.
         */
        armarx::semantic::data::ShapeList extractShapes();

        /**
         * Returns the previously extracted shapes, if any.
         * (Call `extractShapes()` to trigger shape extraction).
         */
        armarx::semantic::data::ShapeList getExtractedShapes();
    };

};

