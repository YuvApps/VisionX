/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionXPlugins::DarknetObjectDetection
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// ArmarX
#include <RobotAPI/interface/visualization/DebugDrawerInterface.ice>
#include <VisionX/interface/core/DataTypes.ice>


module visionx { module yolo
{

struct ClassCandidate
{
    string className;
    int classIndex;
    float certainty;
    armarx::DrawColor24Bit color;
};
sequence<ClassCandidate> ClassCandidateList;


struct DetectedObject
{
    ClassCandidateList candidates;
    int classCount;
    string objectName = "";
    visionx::BoundingBox2D boundingBox;
};
sequence<DetectedObject> DetectedObjectList;


interface ObjectListener
{
    /**
     * @brief Called when Yolo is given a frame to process.
     *
     * Can be used to fetch the frame Darknet picked to run processing in parallel while waiting
     * for the outputs.  Timestamps of announceDetectedObjects and reportDetectedObjects will match.
     *
     * @param timestamp Timestamp of the frame
     */
    void announceDetectedObjects(long timestamp);

    /**
     * @brief Called when Yolo processed a frame
     * @param dol Detected objects
     * @param timestamp Timestamp of the frame
     */
    void reportDetectedObjects(DetectedObjectList dol, long timestamp);
};


};};
