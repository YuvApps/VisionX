/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <VisionX/interface/core/PointCloudProcessorInterface.ice>
#include <ArmarXCore/interface/core/BasicVectorTypes.ice>

module visionx
{
    interface VoxelGridProviderInterface extends visionx::PointCloudProcessorInterface
    {
        ["cpp:const"]
        armarx::Vector3fSeq getFilledGridPositions();
        void startCollectingPointClouds();
        void stopCollectingPointClouds();
        void reset();
        ["cpp:const"]
        float getGridSize();
    };
};

