/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Component
* @author     Sebas Mendez, David Schiebener <schiebener at kit dot edu>
* @copyright  2014 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/DataTypes.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>

/**
 * Definition of all generic hand localization interfaces
 */
module visionx
{
	interface ObjectShapeClassificationInterface
	{
		string FindSimilarKnownObject(types::PointList segmentedObjectPoints);
	};

	class FeatureBase extends armarx::VariantDataClass
	{
		//["cpp:const"]
		//string name();
		//["protected"]
		//string m_name;
	};

};

