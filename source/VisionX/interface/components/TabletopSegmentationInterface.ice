/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX
* @author     Simon Ottenhaus <simon dot ottenhaus at kit dot edu>
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <VisionX/interface/core/PointCloudProcessorInterface.ice>


module visionx
{
    class TabletopSegmentationPlane
    {
        armarx::Vector3Base center;
        armarx::Vector3Base normal;
    };

    interface TabletopSegmentationInterface extends PointCloudProcessorInterface
    {
        TabletopSegmentationPlane getTablePlane();
    };
};

