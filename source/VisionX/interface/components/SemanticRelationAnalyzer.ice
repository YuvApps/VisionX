#pragma once

//#include <RobotAPI/interface/core/PoseBase.ice>

#include <VisionX/interface/libraries/SemanticObjectRelations/Graph.ice>


module armarx
{

    interface SemanticRelationAnalyzerInterface
    {
        semantic::data::Graph extractSupportGraphFromWorkingMemory();
    };

};
