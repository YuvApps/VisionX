/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX
* @author     Peter Kaiser (peter dot kaiser at kit dot edu)
* @copyright  2015 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/components/PointCloudSegmenter.ice>
#include <VisionX/interface/components/AffordanceExtraction.ice>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.ice>

module armarx
{
    enum AffordanceVisualizationStyle
    {
        eAffordanceLabels,
        eAffordanceBeliefFunctions
    };

    interface AffordancePipelineVisualizationInterface extends visionx::PointCloudSegmentationListener, AffordanceExtractionListener, DebugDrawerListener
    {
        void selectPrimitive(string id);
        void deselectPrimitive(string id);
        void deselectAllPrimitives();

        void triggerVisualization();

        void enableVisualization(bool enablePrimitives, bool enableAffordances, bool enableDebugInformation);

        void configureAffordanceVisualization(AffordanceVisualizationStyle style, Ice::StringSeq primitives, Ice::StringSeq affordances, float minExpectedProbability);

        void exportVisualizationAsIV(string filename);
        void exportVisualizationAsJSON(string filename);

        void clearVisualization();
    };

    interface AffordancePipelineVisualizationListener
    {
        void reportAffordanceVisualizationUpdate();
        void reportPrimitiveVisualizationUpdate();
        void reportPrimitiveSelectionChanged(Ice::StringSeq selectedPrimitives);
    };
};



