/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX
* @author     Eren Aksoy (eren dot aksoy at kit dot edu)
* @copyright  2014 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/DataTypes.ice>
#include <ArmarXCore/interface/observers/ObserverInterface.ice>
#include <RobotAPI/interface/core/PoseBase.ice>


#include <VisionX/interface/core/PointCloudProcessorInterface.ice>

module visionx
{
    interface PrimitiveExtractorInterface extends PointCloudProcessorInterface
    {
        visionx::PrimitiveData getPrimitiveData();
    };
};

