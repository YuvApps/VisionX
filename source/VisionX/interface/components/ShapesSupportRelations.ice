#pragma once

#include <Ice/BuiltinSequences.ice>

#include <VisionX/interface/libraries/SemanticObjectRelations/Graph.ice>
#include <VisionX/interface/libraries/SemanticObjectRelations/ShapesTopic.ice>


module armarx
{
    module semantic
    {

        interface SupportRelationsFromShapesInterface
        {
            semantic::data::Graph extractSupportGraph(data::ShapeList objects, Ice::LongSeq safeObjectIDs);
        };

        interface ShapesSupportRelationsInterface extends
                SupportRelationsFromShapesInterface,
                ShapesTopic
        {
        }

    }
};
