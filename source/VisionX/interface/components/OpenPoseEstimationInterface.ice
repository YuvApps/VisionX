/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::OpenCVImageStabilizer
* @author     Stefan Reither
* @date       2018 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/ImageProcessorInterface.ice>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.ice>
#include <ArmarXCore/interface/core/TextInterface.ice>

module armarx
{
    // Image Server Interface from SecondHands
    module ImageBroadcasting {

        sequence<byte> ImageData;

        enum ColourSpace {
            BGR,
            RGB,
            Grayscale
        };

        struct Image {
            ImageData data;
            int width;
            int height;
            int sequenceId;
            bool isGreyscale;
            long timestamp;
            ColourSpace colourType;
        };

        interface ImageBroadcaster {
            void onMessage(Image message);
        };
    };


    interface OpenPoseEstimationInterface extends visionx::ImageProcessorInterface, Texting::TextMessenger
    {
        void start();
        void stop();
        void start3DPoseEstimation();
        void stop3DPoseEstimation();
    };

    struct Keypoint2D
    {
        string label;
        float x;
        float y;
        float confidence;
        DrawColor24Bit dominantColor;
    };

    dictionary<string, Keypoint2D> Keypoint2DMap;
    sequence<Keypoint2DMap> Keypoint2DMapList;

    struct Keypoint3D
    {
        string label;
        float x;
        float y;
        float z;
        float confidence;
        DrawColor24Bit dominantColor;
    };

    struct ActionRecognitionResult
    {
        string label;
        long timestamp;
    };

    dictionary<string, Keypoint3D> Keypoint3DMap;
    sequence<Keypoint3DMap> Keypoint3DMapList;

    interface OpenPose2DListener
    {
        void report2DKeypoints(Keypoint2DMapList list, long timestamp);
        void report2DKeypointsNormalized(Keypoint2DMapList list, long timestamp);
    };

    interface OpenPoseHand2DListener
    {
        void report2DHandKeypoints(Keypoint2DMapList list, long timestamp);
        void report2DHandKeypointsNormalized(Keypoint2DMapList list, long timestamp);
    };

    interface OpenPose3DListener
    {
        void report3DKeypoints(Keypoint3DMapList list, long timestamp);
    };

    interface OpenPoseCombinedListener extends OpenPose3DListener, OpenPose2DListener
    {
    };

    interface PoseBasedActionRecognitionListener
    { 
	void reportPoseClassified(string label);
    };

    interface PoseBasedActionRecognitionInterface
    { 
    void startRecognition();
    void stopRecognition();
    ActionRecognitionResult getLatestRecognition();
    };
  



};
