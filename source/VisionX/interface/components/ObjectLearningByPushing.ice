/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::Core
* @author     David Schiebener <schiebener at kit dot edu>
* @copyright  2014 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/ObserverInterface.ice>
#include <VisionX/interface/core/DataTypes.ice>
#include <VisionX/interface/core/PointCloudProcessorInterface.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>


/**
 * Definition of all generic hand localization interfaces
 */
module visionx
{
    interface ObjectLearningByPushingInterface extends PointCloudAndImageProcessorInterface
	{
		void CreateInitialObjectHypotheses();
		void ValidateInitialObjectHypotheses();
		void RevalidateConfirmedObjectHypotheses();
		types::PointList getObjectHypothesisPoints();
		types::PointList getScenePoints();
        armarx::Vector3Base getUpwardsVector();
        string getReferenceFrameName();
        armarx::PoseBase getLastObjectTransformation();
		void recognizeObject(string objectName);
	};

    interface ObjectLearningByPushingListener extends armarx::ObserverInterface
    {
    	void reportInitialObjectHypothesesCreated(bool hypothesesCreated);
    	void reportObjectHypothesesValidated(bool hypothesesValidated);
		void resetHypothesesStatus();
    	void reportObjectHypothesisPosition(armarx::FramedPositionBase objectPosition, float objectExtent, 
											armarx::Vector3Base principalAxis1, armarx::Vector3Base principalAxis2, 
											armarx::Vector3Base principalAxis3, armarx::Vector3Base eigenValues);
    };
};

