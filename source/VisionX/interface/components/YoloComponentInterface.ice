/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionXPlugins::DarknetObjectDetection
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// Ice
#include <Ice/BuiltinSequences.ice>

// ArmarX
#include <VisionX/interface/core/ImageProcessorInterface.ice>


module visionx { module yolo
{


interface ComponentInterface extends visionx::ImageProcessorInterface
{
    idempotent void restoreDefaults();

    idempotent double getThresh();
    idempotent double getHierThresh();
    idempotent double getNms();
    idempotent float getFpsCap();

    idempotent void setThresh(double thresh);
    idempotent void setHierThresh(double hierThresh);
    idempotent void setNms(double nms);
    idempotent void setFpsCap(float fpsCap);

    idempotent Ice::StringSeq getClasses();
};


};};
