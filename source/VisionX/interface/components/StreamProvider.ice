/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    
 * @author     
 * @date       
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/interface/components/TopicRecorderInterface.ice>
#include <VisionX/interface/core/ImageProcessorInterface.ice>
#include <VisionX/interface/core/ImageProviderInterface.ice>

module Stream {
	/**
	 * a data chunk. This contains the raw data that was received from
	 * the encoder. Clients are recommended not to fiddle with it but 
	 * rather to feed it directly into the decoder.
	 */
	sequence<byte> DataChunk;


    enum CodecType
    {
        eH264
    };
	
    /**
     * parameters for setting the compression rate of the encoder.
     * because encoders differ in how the compresion rate is set
     * (most codecs allow you to set a bitrate, but some others
     * recommend setting a quality value, for which some use a integer,
     * while others use a fixed-point decimal value...) we abstract
     * from these settings and offer compresion rate choices to the
     * client.
     * the server maps these to the respective codec configurations.
     */
    enum CompressionRate {

        /**
         * use a high compression rate, i.e. do not care about quality
         * and try to get a high frame rate on messy connections.
         */
        COMPRESSIONHIGH,

        /**
         * use a medium compression rate, i.e. provide decent quality
         * over decent connections.
         */
        COMPRESSIONMED,

        /**
         * use a low compression rate, i.e. provide excellent quality
         * over very good connections.
         */
        COMPRESSIONLOW

    };

    const int zeroInt = 0;


    interface StreamListenerInterface extends visionx::CompressedImageProviderInterface
    {
        void reportNewStreamData(DataChunk newData, long imageTimestamp);
    };


	/**
	 * Interface for accessing the Streaming Subsystem.
	 * 
	 * when a client wants to use the Streaming subsystem, it 
	 * has to call startCapture() first. This will initialize
	 * the Streaming subsystem. 
	 */
	interface StreamProvider {

		/**
		 * Stop capturing. The internal chunk queues will be 
		 * cleared and the allocated ressources are freed.
		 */
        void stopCapture();
        bool startCapture();

        void getImageInformation(out int imageWidth, out int imageHeight,out int imageType);

        /**
		 * change the compression rate of the stream. This method 
		 * can only be called if the stream is currently not 
		 * initialized, because not all codecs support changing 
		 * compression rate on the fly.
		 * @param cam the camera for which to set the compression rate.
		 * @param rate the compression rate to use.
		 */
        void setCompressionRate( CompressionRate rate);
        CodecType getCodecType();
        int getNumberOfImages();
		
	};

    interface StreamProviderImageProcessorInterface extends StreamProvider, visionx::ImageProcessorInterface, armarx::TopicRecorderListenerInterface
    {

    };

    struct StreamMetaData
    {
        string streamName;
        int width = -1;
        int height = -1;
        bool captureActive = false;
//        StreamSourceType type = eVIDEO;
        float fpsActual = zeroInt;
        float bitrateActual = zeroInt;
    };
};

