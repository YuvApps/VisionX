/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::ObjectMemory
* @author     David Schiebener <david dot schiebener at kit dot edu>
* @copyright  2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/DataTypes.ice>

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <RobotAPI/interface/core/RobotState.ice>



/**
 * Definition of all generic VisionX interfaces
 */
module visionx
{
    /**
     * The MotionModel updates the pose and uncertainty of an object during the time between
     * localizations.
     */
    interface MotionModelInterface
    {
        /**
         * Add an object to the memory that has to be localized and observed
         *
         * @param objectName the name of the object
         * @param initialPose an initial guess of the pose of the object that may significantly speed up the search for it
         * @param initialPriority the priority of the object
         */
        void updatePoseAndUncertainties(types::ObjectInformation oldObjectInfo, armarx::SharedRobotInterface* robotStateAtLastLocalization, 
                                        armarx::SharedRobotInterface* robotStateNow, long timeSinceLastLocalizationInMs,
                                        out types::ObjectInformation updatedObjectInfo);
    };


};



