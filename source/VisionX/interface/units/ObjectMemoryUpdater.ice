/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX::ObjectMemoryUpdater
* @author     David Schiebener <david dot schiebener at kit dot edu>
* @copyright  2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <VisionX/interface/core/DataTypes.ice>

#include <RobotAPI/interface/core/PoseBase.ice>
#include <RobotAPI/interface/units/UnitInterface.ice>

/**
 * Definition of all generic VisionX interfaces
 */
module visionx
{
    /**
     * An ObjectMemoryUpdater is a component that updates the information about an object, that may be
     * done through visual localisation or the use of other sensorial input, e.g. kinematics
     */
    interface ObjectMemoryUpdater
    {
        /**
         * Initiates object recognition for a specific object.
         *
         * @param name the name of the object that should be localized
         * @param nameForCallback must be set to the same object name, used for identification at
         * asynchronous callback
         * @newPose the object pose as determined by the localization
         * @recognitionCertainty the certainty that the recognition was correct
         */
        void localizeObject(string name, out string nameForCallback, out armarx::PoseBase newPose, out float recognitionCertainty);

    };
};

