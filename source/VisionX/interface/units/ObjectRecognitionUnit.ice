/**
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     Jan Issac <jan dot issac at gmx dot de>
 * @copyright  2010 Humanoids Group, HIS, KIT
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/interface/core/DataTypes.ice>
#include <VisionX/interface/core/ImageProcessorInterface.ice>
#include <VisionX/interface/units/ObjectMemoryUpdater.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/SharedMemory.ice>
#include <RobotAPI/interface/units/UnitInterface.ice>

module visionx
{
    module types
    {
        /**
         * Different object colors for homogeneously
         * mono colored object such as markers.
         */
        enum ObjectColor
        {
            eNone,
            eBlue,
            eBlue2,
            eBlue3,
            eColored,
            eGreen,
            eGreen2,
            eGreen3,
	        eOrange,
	        eOrange2,
	        eOrange3,
	        eRed,
	        eRed2,
            eRed3,
	        eSkin,
            eWhite,
            eYellow,
            eYellow2,
            eYellow3,
            eNumberOfColors
        };

        /**
         * Object appearance and shape types  
         */
        enum ObjectType
        {
            eCompactObject,
            eTexturedObject,
            eHead,
            eLeftHand,
            eRightHand,
            eUnknownObject
        };

        /**
         * 3D Transformation which consists of a rotation 
         * matrix and a translation vector.
         */
        struct Transformation3d
        {
            Mat rotation;
            Vec translation;
        };
    
        /**
         * Object state inforamtion 
         */
        struct Object3DEntry
        {
            Region regionLeft;
            Region regionRight;
            int regionIdLeft;
            int regionIdRight;
            ObjectType type;
            ObjectColor color;
            Transformation3d pose;
            Vec worldPoint;
            Vec orientation;
            string name;
            int classId;
            float quality;
            float quality2;
        };
    
        /**
         * Result list of found objects. Each object entry contains
         * state information of a found object.
         */
        sequence<Object3DEntry> Object3DList;
    };

    exception ObjectNotFoundException extends armarx::UserException
    {
        string objectName;
    };

    /**
     * Object recognition interface
     */
    interface ObjectRecognitionUnitInterface extends armarx::SensorActorUnitInterface, ImageProcessorInterface, ObjectMemoryUpdater
    {
           
        /**
         * Initiates recognition of all object within the current field of view.
         */
        //void recognizeObjects();
        
        /**
         * Returns the object information of the specified object, if exists.
         *
         * @param name  Requested object name
         *
         * @return      An object entry of requested object, if exists
         *
         * @throw       ObjectNotFoundException
         */
        //visionx::types::Object3DEntry getObject(string name) throws ObjectNotFoundException;
        
        /**
         * Returns an object list of all found objects
         *
         * @return      A list of found objects
         */
        //visionx::types::Object3DList getFoundObjects();
    };
    
};
 
