/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::PointCloud
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ResumeProvider.h"
#include "PointCloudStatechartContext.generated.h"
namespace armarx::PointCloud
{
    // DO NOT EDIT NEXT LINE
    ResumeProvider::SubClassRegistry ResumeProvider::Registry(ResumeProvider::GetName(), &ResumeProvider::CreateInstance);



    ResumeProvider::ResumeProvider(const XMLStateConstructorParams& stateData) :
        XMLStateTemplate<ResumeProvider>(stateData),  ResumeProviderGeneratedBase<ResumeProvider>(stateData)
    {
    }

    void ResumeProvider::onEnter()
    {
        std::string providerName = in.getProviderName();
        PointCloudStatechartContext* context = getContext<PointCloudStatechartContext>();


        visionx::CapturingPointCloudProviderInterfacePrx providerPrx;

        try
        {
            providerPrx = context->getIceManager()->getProxy<visionx::CapturingPointCloudProviderInterfacePrx>(providerName);
        }
        catch (...)
        {
        }

        if (providerPrx)
        {
            providerPrx->startCapture();
            emitSuccess();
        }
        else
        {
            emitFailure();
        }
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr ResumeProvider::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new ResumeProvider(stateData));
    }
}
