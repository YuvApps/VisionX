/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::PointCloud
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ResumeProcessor.h"

using namespace armarx;
using namespace PointCloud;

// DO NOT EDIT NEXT LINE
ResumeProcessor::SubClassRegistry ResumeProcessor::Registry(ResumeProcessor::GetName(), &ResumeProcessor::CreateInstance);



ResumeProcessor::ResumeProcessor(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<ResumeProcessor>(stateData),  ResumeProcessorGeneratedBase<ResumeProcessor>(stateData)
{
}

void ResumeProcessor::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void ResumeProcessor::run()
{
    // put your user code for the execution-phase here
    // runs in seperate thread, thus can do complex operations
    // should check constantly whether isRunningTaskStopped() returns true

    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    //    while (!isRunningTaskStopped()) // stop run function if returning true
    //    {
    //        // do your calculations
    //    }

}

void ResumeProcessor::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void ResumeProcessor::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr ResumeProcessor::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new ResumeProcessor(stateData));
}

