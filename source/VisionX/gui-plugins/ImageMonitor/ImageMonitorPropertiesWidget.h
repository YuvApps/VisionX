/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#pragma once


// STD/STL
#include <vector>

// Qt
#include <QComboBox>
#include <QDialog>
#include <QFileDialog>
#include <QString>

// Ice
#include <Ice/Ice.h>

// IVT
#include <Image/ByteImage.h>

// AramrX
#include <ArmarXCore/core/IceManager.h>

// VisionX
#include <VisionX/gui-plugins/ImageMonitor/ui_ImageMonitorPropertiesWidget.h>
#include <VisionX/gui-plugins/ImageMonitor/ImageMonitorWidgetController.h>
#include <VisionX/libraries/record.h>


namespace armarx
{
    template <typename ProxyType>
    class IceProxyFinder;
}


namespace visionx
{
    class ImageMonitorPropertiesWidget :
        public QDialog
    {
        Q_OBJECT

    public:
        ImageMonitorPropertiesWidget();
        ~ImageMonitorPropertiesWidget() override;

        void update(ImageMonitorProperties properties, armarx::IceManagerPtr iceManager);
        ImageMonitorProperties getProperties();

    public slots:
        void chooseOutputPath();
        void setOutputPath();
        void onValidProxySelected(const QString& proxyName);
        void refreshRecordingWidgets();
        void compressionTypeChanged(int index);

    signals:
        // pass

    private:
        armarx::IceManagerPtr iceManager;
        Ui::ImageMonitorPropertiesWidget ui;
        QFileDialog* fileDialog;
        unsigned int numImageSources = 0;
        std::vector<QComboBox*> recordingMethodComboBoxes;
        armarx::IceProxyFinder<ImageProviderInterfacePrx>* proxyFinder;

    };
}
