/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ObserverTest::
* @author     ( at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#pragma once


// STD/STL
#include <queue>
#include <set>
#include <string>
#include <vector>

// Boost
#include <boost/thread.hpp>
#include <boost/circular_buffer.hpp>

// OpenCV 2
#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat)

// Qt
#include <QString>
#include <QToolBar>

// ArmarX
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

// IVT
#include <Image/ByteImage.h>

// VisionX
#include <ArmarXCore/observers/filters/rtfilters/TimeWindowAverageFilter.h>
#include <ArmarXCore/observers/filters/rtfilters/AverageFilter.h>
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/gui-plugins/ImageMonitor/ImageMonitorWidget.h>
#include <VisionX/libraries/record.h>
#include <VisionX/tools/FPSCounter.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>


namespace visionx
{
    /**
    \page VisionX-GuiPlugins-ImageMonitor ImageMonitor
    \brief ...

    \image html ImageMonitorGuiPlugin.bmp "The Image Monitor gui plugin"

    API Documentation \ref ImageMonitorWidgetController

    \see ImageMonitorGuiPlugin
    */

    /**
     * \class ImageMonitorProperties
     * \brief ImageMonitorProperties brief one line description
     *
     * Detailed description
     */
    class ImageMonitorProperties
    {
    public:

        ImageMonitorProperties()
        {
            providerName = "";
            frameRate = -1.0f;

            outputPath = armarx::ArmarXDataPath::getHomePath();

            imageBufferSize = 100;
            bufferFps = 5;
            controlsHidden = false;
            depthImageIndex = -1;
            maxDepthmm = 5000;
            compressionType = eNoCompression;
            compressionQuality = 9;
        }

        std::string providerName;
        int frameRate;
        std::string outputPath;
        int imageBufferSize;
        float bufferFps;
        std::set<size_t> imagesToShow;
        bool controlsHidden;
        bool recordDepthRaw;
        QStringList recordingMethods;
        int depthImageIndex;
        int maxDepthmm;
        CompressionType compressionType;
        int compressionQuality;
    };

    using CByteImagePtr = std::shared_ptr<CByteImage>;
    using ImageContainer = std::vector<CByteImagePtr>;

    /*!
    * \class ImageMonitorWidgetController
    * \brief
    */
    class ARMARXCOMPONENT_IMPORT_EXPORT ImageMonitorWidgetController :
        public armarx::ArmarXComponentWidgetControllerTemplate<ImageMonitorWidgetController>,
        public visionx::ImageProcessor
    {
        Q_OBJECT

    public:
        ImageMonitorWidgetController();

        // inherited from ArmarXWidgetController
        QPointer<QWidget> getWidget() override;
        static QString GetWidgetName()
        {
            return "VisionX.ImageMonitor";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon {"://icons/image_monitor.svg"};
        }
        static QIcon GetWidgetCategoryIcon()
        {
            return QIcon {"://icons/eye.svg"};
        }

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

        // inherited from ImageProcessor
        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onExitImageProcessor() override;
        void process() override;

        // properties
        void applyProperties(ImageMonitorProperties properties);
        ImageMonitorProperties getProperties();

        // getters
        ImageTransferStats getStatistics();
        ImageProviderInfo getImageProviderInfo()
        {
            return imageProviderInfo;
        }
        const ImageContainer& getBufferedImage(unsigned int position, unsigned int& realPosition);
        unsigned int getBufferedImageCount()
        {
            return imageBuffer.size();
        }

        // setters
        void setPlaying(bool playing)
        {
            this->playing = playing;
        }
        void setBuffering(bool buffering)
        {
            this->writeImageBuffer = buffering;
        }

        void createSnapshot();

        // ice
        using Component::getIceManager;

        /**
         * @brief Start recording of the images to bmp-files. Each frame of
         * each camera is one bmp-file. The names of the files contain a
         * increasing sequence_number.
         * @param fps Specifies the framerate of the recording. If set to -1
         * the framerate of the source is used.
         */
        void startRecording();
        void stopRecording();

        // Qt signals
    signals:
        void imageProviderConnected(bool connected);
        void recordingBufferEmpty(bool isEmpty);
        void statisticsUpdated(const QString& statisticsString);

    private:
        void connectToProvider();
        void disconnectFromProvider();
        void updateStatistics();
        void recordFrame();

    private slots:
        void hideControls(bool hide = true);

    private:
        // ui
        QPointer<ImageMonitorWidget> widget;

        // settings
        ImageMonitorProperties      properties;

        // images
        CByteImage**                images;
        unsigned int numberImages;

        // provider
        ImageProviderInterfacePrx   imageProviderPrx;
        visionx::ImageProviderInfo  imageProviderInfo;

        // recording
        armarx::PeriodicTask<ImageMonitorWidgetController>::pointer_type recordingTask;

        // image buffer
        bool                        writeImageBuffer;
        int                         bufferSize;
        float                       bufferFps;
        IceUtil::Time               lastBufferTime;
        boost::circular_buffer<ImageContainer > imageBuffer;
        std::queue<std::vector<CByteImage*>> recordingBuffer;
        armarx::RecursiveMutex recordingBufferMutex;
        bool writeRecordingBuffer = false;

        // internals
        bool                        connected;
        bool                        playing;

        armarx::RecursiveMutex      imageMutex;
        FPSCounter                  fpsCounter;

        QPointer<QToolBar> customToolbar;
        QAction* viewingModeAction;

        IceUtil::Time timeProvided, timeReceived;
        armarx::rtfilters::TimeWindowAverageFilter displayDelayFilter;
        std::vector<cv::Mat> outputBuffer;
        std::vector<visionx::record::Recording> recorders;

        std::map < float, std::vector<boost::optional<armarx::DrawColor24Bit>>> depthToRgbLookUpTables;

        // ArmarXWidgetController interface
    public:
        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent) override;
        std::string getAbsoluteOutputPath();
        void convertToHeatMapDepthImage(CByteImage* image);

    };
}
