/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#include <VisionX/gui-plugins/ImageMonitor/ImageMonitorPropertiesWidget.h>


// STD/STL
#include <map>
#include <string>

// Qt
#include <QApplication>

// ArmarXGui
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>


using namespace armarx;


namespace visionx
{
    ImageMonitorPropertiesWidget::ImageMonitorPropertiesWidget()
    {
        ui.setupUi(this);
        proxyFinder = new armarx::IceProxyFinder<ImageProviderInterfacePrx>(this);
        proxyFinder->setSearchMask("*Provider|*Result");
        ui.horizontalLayout->addWidget(proxyFinder);
        fileDialog = new QFileDialog(this);

        connect(ui.pushButtonOutputPath, SIGNAL(clicked()), this, SLOT(chooseOutputPath()));
        connect(fileDialog, SIGNAL(accepted()), this, SLOT(setOutputPath()));
        connect(proxyFinder, SIGNAL(validProxySelected(QString)), this, SLOT(onValidProxySelected(QString)));
        connect(ui.spinBoxDepthImageIndex, SIGNAL(valueChanged(int)), this, SLOT(refreshRecordingWidgets()));
        connect(ui.comboBoxRecordDepthRaw, SIGNAL(currentIndexChanged(int)), this, SLOT(refreshRecordingWidgets()));
        connect(ui.comboBoxCompressionType, SIGNAL(currentIndexChanged(int)), this, SLOT(compressionTypeChanged(int)));

        // Initialise options for frame rate dropdown
        this->ui.comboBoxFrameRate->addItem("Source framerate", -1);
        for (unsigned int i = 30; i > 0; i -= 5)
        {
            this->ui.comboBoxFrameRate->addItem(QString::number(i) + " fps", static_cast<int>(i));
        }
        this->ui.comboBoxFrameRate->addItem("1 fps", 1);

        // Initialise options for recording depth raw dropdown
        this->ui.comboBoxRecordDepthRaw->addItem("Record raw depth data (for replaying actual depth data)", true);
        this->ui.comboBoxRecordDepthRaw->addItem("Record depth-interpreted data (for visualisation only)", false);

        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        this->setSizePolicy(sizePolicy);
    }

    ImageMonitorPropertiesWidget::~ImageMonitorPropertiesWidget()
    {
        // pass
    }

    void ImageMonitorPropertiesWidget::update(ImageMonitorProperties properties, IceManagerPtr iceManager)
    {
        this->iceManager = iceManager;
        proxyFinder->setIceManager(iceManager, proxyFinder->getSelectedProxyName().isEmpty());
        proxyFinder->setDefaultSelectedProxy(QString::fromStdString(properties.providerName));

        // select framerate
        this->ui.comboBoxFrameRate->setCurrentIndex(this->ui.comboBoxFrameRate->findData(properties.frameRate));

        // output path
        ui.lineEditOutputPath->setText(QString(properties.outputPath.c_str()));

        ui.doubleSpinBoxBufferFps->setValue(static_cast<double>(properties.bufferFps));
        ui.spinBoxImageBuffer->setValue(properties.imageBufferSize);

        QStringList l;
        for (auto number : properties.imagesToShow)
        {
            l << QString::number(number);
        }
        ui.lineEditImagesToShow->setText(l.join(","));

        ui.spinBoxMaxDepth->setValue(properties.maxDepthmm);
        ui.spinBoxDepthImageIndex->setValue(properties.depthImageIndex);

        ui.comboBoxCompressionType->setCurrentIndex((int)properties.compressionType);
        //        compressionTypeChanged((int)properties.compressionType);
        ui.spinBoxCompressionQuality->setValue(properties.compressionQuality);

        // Restore settings for depth raw recording combobox
        this->ui.comboBoxRecordDepthRaw->setCurrentIndex(this->ui.comboBoxRecordDepthRaw->findData(properties.recordDepthRaw));

        // Restore settings for recording methods
        for (unsigned int i = 0; i < static_cast<unsigned int>(properties.recordingMethods.size()); i++)
        {
            const QString ext = properties.recordingMethods.at(static_cast<int>(i));
            int index = this->recordingMethodComboBoxes.at(i)->findData(ext);

            if (index < 0)
            {
                ARMARX_WARNING << "Could not restore default values for recording output formats combobox. Caching inconsistencies? Using first item as fallback";
                index = 1; // Index 0 is "Ignore / don't record". Better safe than sorry
            }

            this->recordingMethodComboBoxes.at(i)->setCurrentIndex(index);
        }
    }

    ImageMonitorProperties ImageMonitorPropertiesWidget::getProperties()
    {
        ImageMonitorProperties properties;

        // retrieve provider name
        QString text = proxyFinder->getSelectedProxyName();
        std::string providerName = text.toStdString();
        properties.providerName = providerName;

        // set more properties
        properties.outputPath = ui.lineEditOutputPath->text().toStdString();

        // set framerate
        properties.frameRate = this->ui.comboBoxFrameRate->itemData(this->ui.comboBoxFrameRate->currentIndex()).toInt();

        // buffer properties
        properties.imageBufferSize = ui.spinBoxImageBuffer->value();
        properties.bufferFps = static_cast<float>(ui.doubleSpinBoxBufferFps->value());
        QStringList list = ui.lineEditImagesToShow->text().split(",");

        for (QString& s : list)
        {
            bool ok;
            auto value = s.toULong(&ok);
            if (ok)
            {
                properties.imagesToShow.insert(value);
            }
        }
        for (auto i : properties.imagesToShow)
        {
            ARMARX_INFO_S << i;
        }
        properties.depthImageIndex = ui.spinBoxDepthImageIndex->value();
        properties.maxDepthmm = ui.spinBoxMaxDepth->value();

        properties.compressionType = static_cast<CompressionType>(ui.comboBoxCompressionType->currentIndex());
        properties.compressionQuality = ui.spinBoxCompressionQuality->value();


        properties.recordDepthRaw = this->ui.comboBoxRecordDepthRaw->itemData(this->ui.comboBoxRecordDepthRaw->currentIndex()).toBool();

        // Find and set recording methods for each image source
        {
            QStringList recordingMethods;

            for (unsigned int i = 0; i < this->recordingMethodComboBoxes.size(); ++i)
            {
                const QComboBox* cb = this->recordingMethodComboBoxes.at(i);
                QString ext = cb->itemData(cb->currentIndex()).toString();
                recordingMethods.push_back(ext);
            }

            properties.recordingMethods = recordingMethods;
        }

        return properties;
    }

    void ImageMonitorPropertiesWidget::chooseOutputPath()
    {
        fileDialog->setDirectory(ui.lineEditOutputPath->text());
        fileDialog->setFileMode(QFileDialog::Directory);
        fileDialog->setModal(true);
        fileDialog->show();
    }

    void ImageMonitorPropertiesWidget::setOutputPath()
    {
        QDir dir = fileDialog->directory();
        dir.makeAbsolute();

        ui.lineEditOutputPath->setText(dir.path());
    }

    void
    ImageMonitorPropertiesWidget::onValidProxySelected(const QString& proxyName)
    {
        try
        {
            // Try to get the object proxy "objPrx" represented by "proxyName"
            IceGrid::AdminPrx admin = this->iceManager->getIceGridSession()->getAdmin();
            Ice::Identity objectIceId = Ice::stringToIdentity(proxyName.toStdString());
            visionx::ImageProviderInterfacePrx imageProviderPrx = visionx::ImageProviderInterfacePrx::checkedCast(admin->getObjectInfo(objectIceId).proxy);
            unsigned int numImages = static_cast<unsigned int>(imageProviderPrx->getNumberImages());
            this->numImageSources = numImages;
            this->refreshRecordingWidgets();
        }
        catch (...)
        {
            // pass
        }
    }

    void ImageMonitorPropertiesWidget::refreshRecordingWidgets()
    {
        /* Factory function to create a new initialised combobox to pick recording modalties depending
         * on the type of source. For depth sources, lossy recording methods are excluded */
        auto recordingMethodsComboboxFactory = [](bool forDepthSource) -> QComboBox *
        {
            QComboBox* cb = new QComboBox();
            const std::vector<visionx::record::RecordingMethod>& availableMethods = visionx::record::getAvailableRecordingMethods();

            cb->addItem("** Ignore / don't record **", "");

            for (const visionx::record::RecordingMethod& availableMethod : availableMethods)
            {
                // Skip if the recording methods is lossy and the combobox is for a depth source
                if (availableMethod.isLossy() and forDepthSource)
                {
                    continue;
                }

                const QString label = QString::fromStdString(availableMethod.getHumanReadableDescription());
                const QString ext = QString::fromStdString(availableMethod.getFileExtension());

                cb->addItem(label, ext);
            }

            cb->setCurrentIndex(1); // Set to first item *after* "Ignore / don't record"

            return cb;
        };

        QGridLayout* layout = static_cast<QGridLayout*>(this->ui.recordingOutputs->layout());
        const int depthImageIndex = this->ui.spinBoxDepthImageIndex->value();

        // Enable/disable depth handling combobox
        this->ui.comboBoxRecordDepthRaw->setEnabled(depthImageIndex >= 0);

        // Rebuild individual dropdowns per image source
        {
            // Determine if depth data should be recorded as raw data
            const bool recordDepthRaw = this->ui.comboBoxRecordDepthRaw->itemData(this->ui.comboBoxRecordDepthRaw->currentIndex()).toBool();

            // Delete everything
            {
                QLayoutItem* item;

                // Delete all elements inside the layout without deleting the layout itself
                while ((item = layout->takeAt(0)) != nullptr)
                {
                    delete item->widget();
                    delete item;
                }

                // Clear list (only dangling pointers left after the loop above)
                this->recordingMethodComboBoxes.clear();
            }

            // Repopulate widget
            if (this->numImageSources == 0)
            {
                QLabel* label = new QLabel("<i>Select an image provider first</i>");
                layout->addWidget(label, 0, 0);
            }
            else
            {
                for (unsigned int i = 0; i < this->numImageSources; ++i)
                {
                    const bool isDepthSource = depthImageIndex >= 0 and i == static_cast<unsigned int>(depthImageIndex);
                    const QString labelText = "camera index " + QString::number(i) + (isDepthSource ? " (depth)" : "");

                    QLabel* label = new QLabel(labelText);
                    QComboBox* cb = recordingMethodsComboboxFactory(isDepthSource and recordDepthRaw);
                    this->recordingMethodComboBoxes.push_back(cb);

                    layout->addWidget(label, static_cast<int>(i), 0);
                    layout->addWidget(cb, static_cast<int>(i), 1);
                }
            }

            // Resize properties widget to display everything
            QApplication::processEvents();
            this->adjustSize();
        }
    }

    void ImageMonitorPropertiesWidget::compressionTypeChanged(int index)
    {
        ui.spinBoxCompressionQuality->setEnabled(index != 0);
        if (index == 1)
        {
            ui.spinBoxCompressionQuality->setValue(9);
        }
        else if (index == 2)
        {
            ui.spinBoxCompressionQuality->setValue(95);
        }
    }
}
