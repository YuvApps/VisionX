/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Kai Welke ( welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ImageMonitorWidget.h"
#include "ImageMonitorWidgetController.h"
#include "ImageMonitorPropertiesWidget.h"
#include "ImageMonitorStatisticsWidget.h"
#include "ImageViewerArea.h"

using namespace armarx;

namespace visionx
{
    // *******************************************************
    // construction / destruction
    // *******************************************************
    ImageMonitorWidget::ImageMonitorWidget(ImageMonitorWidgetController* controller)
    {
        qRegisterMetaType<CByteImage**>("CByteImage**");

        // members
        this->controller = controller;

        // ui elements
        ui.setupUi(this);
        imageViewer = new ImageViewerArea();
        QSizePolicy sizePoli(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePoli.setVerticalStretch(15);
        imageViewer->setSizePolicy(sizePoli);
        ui.imageViewerLayout->addWidget(imageViewer);
        imageViewer->show();

        imageMonitorPropertiesWidget = new ImageMonitorPropertiesWidget();
        imageMonitorPropertiesWidget->hide();

        imageMonitorStatisticsWidget = new ImageMonitorStatisticsWidget();
        imageMonitorStatisticsWidget->hide();

        // signals and slots
        connect(ui.settingsButton, SIGNAL(clicked(bool)), this, SLOT(settingsButtonClicked(bool)));
        connect(ui.statisticsButton, SIGNAL(clicked(bool)), this, SLOT(statisticsButtonClicked(bool)));
        connect(ui.snapshotButton, SIGNAL(clicked(bool)), this, SLOT(snapshotButtonClicked(bool)));
        connect(ui.playButton, SIGNAL(toggled(bool)), this, SLOT(playButtonToggled(bool)));
        connect(ui.btnRecord, SIGNAL(toggled(bool)), this, SLOT(recordButtonToggled(bool)));
        connect(ui.cbBufferImages, SIGNAL(toggled(bool)), this, SLOT(bufferImagesToggled(bool)));
        connect(ui.sliderImageBuffer, SIGNAL(valueChanged(int)), this, SLOT(sliderPositionChanged(int)));
        connect(ui.buttonShowBufferImages, SIGNAL(toggled(bool)), this, SLOT(bufferImagesPaneChanged(bool)));

        connect(imageMonitorPropertiesWidget, SIGNAL(accepted()), this, SLOT(propertiesAccepted()));
        connect(imageMonitorStatisticsWidget, SIGNAL(accepted()), this, SLOT(statisticsAccepted()));

        ui.frameImageBuffer->hide();

        setConnected(false);
    }

    ImageMonitorWidget::~ImageMonitorWidget()
    {
        delete imageMonitorPropertiesWidget;
    }

    void ImageMonitorWidget::drawImages(int numberImages, CByteImage** images, IceUtil::Time imageTimestamp, IceUtil::Time receiveTimestamp)
    {
        if (!images)
        {
            return;
        }

        for (int i = 0; i < numberImages; ++i)
        {
            if (!images[i])
            {
                return;
            }
        }

        imageViewer->setImages(numberImages, images, imageTimestamp, receiveTimestamp);
    }

    void ImageMonitorWidget::hideControlWidgets(bool hide)
    {
        ui.controlWidgetsContainer->setVisible(!hide);
    }

    // *******************************************************
    // Qt slots
    // *******************************************************
    void ImageMonitorWidget::settingsButtonClicked(bool toggled)
    {
        // update the properties dialog
        imageMonitorPropertiesWidget->update(controller->getProperties(), controller->getIceManager());

        // display dialog
        imageMonitorPropertiesWidget->setModal(true);
        imageMonitorPropertiesWidget->show();
    }

    void ImageMonitorWidget::statisticsButtonClicked(bool toggled)
    {
        // update the properties dialog
        imageMonitorStatisticsWidget->update(controller->getProperties().providerName, controller->getImageProviderInfo(), controller->getStatistics());

        // display dialog
        imageMonitorStatisticsWidget->setModal(true);
        imageMonitorStatisticsWidget->show();
    }


    void ImageMonitorWidget::propertiesAccepted()
    {
        // apply properties to controller
        controller->applyProperties(imageMonitorPropertiesWidget->getProperties());

        ui.sliderImageBuffer->setMaximum(imageMonitorPropertiesWidget->getProperties().imageBufferSize);
    }

    void ImageMonitorWidget::statisticsAccepted()
    {
    }

    void ImageMonitorWidget::recordButtonToggled(bool toggled)
    {
        if (toggled)
        {
            controller->startRecording();
        }
        else
        {
            controller->stopRecording();
        }
    }

    void ImageMonitorWidget::bufferImagesToggled(bool toggled)
    {
        controller->setBuffering(toggled);

        ui.sliderImageBuffer->setEnabled(toggled);
    }

    void ImageMonitorWidget::sliderPositionChanged(int pos)
    {
        if (ui.playButton->isChecked())
        {
            ui.playButton->setChecked(false);
        }

        unsigned int realPos = 0;
        const ImageContainer images = controller->getBufferedImage(pos, realPos);
        auto imagesToShow = controller->getProperties().imagesToShow;

        ui.lblSliderPosition->setText(QString::number(realPos + 1) + "/" + QString::number(controller->getBufferedImageCount()));

        if (images.size() == 0)
        {
            return;
        }

        std::vector<CByteImage*> selectedImages;

        for (unsigned int i = 0; i < images.size(); ++i)
        {
            if (imagesToShow.size() > 0 && imagesToShow.count(i) == 0)
            {
                continue;
            }
            if (!images[i].get())
            {
                ARMARX_WARNING_S << "No Image in Buffer found!" << std::endl;
                return;
            }

            selectedImages.push_back(images[i].get());
        }
        if (selectedImages.size() > 0)
        {
            drawImages(selectedImages.size(), &selectedImages[0], IceUtil::Time(), IceUtil::Time());
        }
    }

    void ImageMonitorWidget::bufferImagesPaneChanged(bool toggled)
    {
        if (toggled)
        {
            ui.buttonShowBufferImages->setArrowType(Qt::DownArrow);
        }
        else
        {
            ui.buttonShowBufferImages->setArrowType(Qt::LeftArrow);
        }
    }

    ImageViewerArea* ImageMonitorWidget::getImageViewer() const
    {
        return imageViewer;
    }

    void ImageMonitorWidget::snapshotButtonClicked(bool toggled)
    {
        controller->createSnapshot();
    }

    void ImageMonitorWidget::playButtonToggled(bool playing)
    {
        controller->setPlaying(playing);
        ui.playButton->setChecked(playing);
        ui.btnRecord->setEnabled(playing);

    }

    void ImageMonitorWidget::setConnected(bool connected)
    {
        ui.playButton->setEnabled(connected);
        ui.statisticsButton->setEnabled(connected);
        ui.snapshotButton->setEnabled(connected);
        ui.cbBufferImages->setEnabled(connected);

        if (!connected) // only disable, enabling is only done in playButtonToggled
        {
            ui.btnRecord->setEnabled(false);
            ui.sliderImageBuffer->setEnabled(false);
        }
        playButtonToggled(connected);
    }

    void ImageMonitorWidget::updateStatistics(const QString& statisticsStr)
    {
        ui.imageViewerStats->setText(statisticsStr);
    }

    void ImageMonitorWidget::updateRecordButton(bool isEnabled)
    {
        if (isEnabled)
        {
            this->ui.btnRecord->setToolTip("Record the current image sources as specified in the preferences");
        }
        else
        {
            this->ui.btnRecord->setToolTip("Still saving buffered images... Please stand by...");
        }
        this->ui.btnRecord->setEnabled(isEnabled);
    }
}
