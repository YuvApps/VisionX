#ifndef HSVIMAGESEGMENTATION_H
#define HSVIMAGESEGMENTATION_H


// ivt
#include <Image/ImageProcessor.h>

#include <VisionX/core/ImageProcessor.h>

namespace armarx
{

    /**
     * @brief The HsvImageSegmentation class
     *
     * Allows for HSV color segmentation of images (e.g. in an image processor).
     *
     * Pointer ownership: All memory for images is allocated and freed by this class.
     * You may not and need not to allocate or delete memory for images.
     *
     * All color images are of CByteImage::eRGB24 type.
     * All grey images are of CByteImage::eGrayScale type.
     */
    class HsvImageSegmentation
    {
    public:

        /**
         * @brief No-initialization constructor.
         * Does not allocate any space. Overwrite an initialized object
         * by overwriting it with a non-initalized object.
         */
        HsvImageSegmentation();

        /**
         * @brief Construct from the given image parameters.
         * Allocates space for the given number of images of given width and height.
         *
         * @param numImages number of images
         * @param width image width
         * @param height image height
         *
         * @throws std::invalid_argument if numImages <= 0
         */
        HsvImageSegmentation(int numImages, int width, int height, CByteImage::ImageType colorType);
        /**
         * @brief Shortcut constructor for usage from an image processor.
         * @param imageProviderInfo the image provider info
         *
         * @throws std::invalid_argument if imageProviderInfo.numberImages <= 0
         */
        HsvImageSegmentation(const visionx::ImageProviderInfo& imageProviderInfo);

        /**
         * Frees all allocated memory.
         */
        ~HsvImageSegmentation();


        /// move constructor
        HsvImageSegmentation(HsvImageSegmentation&& other);

        HsvImageSegmentation& operator =(HsvImageSegmentation&& other);


        /**
         * @brief Get the input images (RGB) (buffer).
         * Write into this buffer to set new input images.
         * @return input images RGB (buffer)
         */
        CByteImage** getInputImagesRgb() const;
        /**
         * @brief Get the input images in HSV.
         * Only valid after calling processInputImages().
         * @return the input images in HSV
         */
        CByteImage** getInputImagesHsv() const;
        /**
         * @brief Get input visualization images.
         * Only valid after calling processInputImages().
         * These are the same as input images, but may be modified
         * for visualization without affecting the input images.
         * @return modifiable input images for visualization
         */
        CByteImage** getInputVisuImages() const;
        /**
         * @brief Get the output images in gray scale.
         * Only valid after calling processInputImages().
         * @return the output images (Gray)
         */
        CByteImage** getOutputImagesGray() const;
        /**
         * @brief Get the output images in RGB (from gray scale).
         * Only valid after calling processInputImages().
         * @return the output images RGB
         */
        CByteImage** getOutputImagesRgb() const;

        /**
         * @return the number of images
         */
        int getNumImages() const;

        /**
         * @brief Processes the current input images.
         * Copies input RGB images to input visu images.
         * Converts input RGB images to input HSV images.
         * Performs segmentation on HSV image.
         * Converts output grey image to output RGB image.
         */
        void processInputImages(int hue, int hueTol, int satMin, int satMax, int valMin, int valMax);


    private:

        /// Allocates image buffers from the given image provider info.
        void allocate(const visionx::ImageProviderInfo& imageProviderInfo);
        /// If allocated, frees the image buffers.
        void deallocate();
        /// Steals state and image buffers from other and resets other.
        void moveFrom(HsvImageSegmentation& other);


        /// Number of images.
        int numImages = 0;
        /// Width of each image.
        int width = 0;
        /// Height of each image.
        int height = 0;

        /// Input images (RGB24)
        CByteImage** inputImagesRgb = nullptr;
        /// Input images with stuff drawn on them (RGB24)
        CByteImage** inputVisuImages = nullptr;

        /// Input images in HSV space (RGB24, but HSV values)
        CByteImage** inputImagesHsv = nullptr;

        /// Output images (GrayScale)
        CByteImage** outputImagesGray = nullptr;
        /// Output images for visualization (RGB24)
        CByteImage** outputImagesRgb = nullptr;


    };

}

#endif // HSVIMAGESEGMENTATION_H
