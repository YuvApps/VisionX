/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::HsvColorSegmentationWidgetController
 * @author     Rainer Kartmann ( rainer dot kartmann at student dot kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include <VisionX/gui-plugins/HsvColorSegmentation/ui_HsvColorSegmentationWidget.h>


// ivt
#include <Color/ColorParameterSet.h>


#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <VisionX/core/ImageProcessor.h>

#include <VisionX/gui-plugins/ImageMonitor/ImageViewerArea.h>
#include <VisionX/libraries/SelectableImageViewer/SelectableImageViewer.h>


#include "HsvImageSegmentation.h"


namespace armarx
{

    /**
    \page VisionX-GuiPlugins-HsvColorSegmentation HsvColorSegmentation
    \brief The HsvColorSegmentation allows image segmentation by HSV color and fast parameter selection.

    \image html HsvColorSegmentation.png
    The user can connect to any image provider for segmentation.
    Once connected, the providers image stream is shown in the GUI.
    The user can select colors from the image to be included in the segmented image.
    Segmentation parameters can be automatically generated from the selected colors,
    or manually refined.
    The segmented image is shown live inside the GUI, and is provided for usage by
    other image processors.

    API Documentation \ref HsvColorSegmentationWidgetController

    \see HsvColorSegmentationGuiPlugin
    */

    /**
     * \class HsvColorSegmentationWidgetController
     * \brief HsvColorSegmentationWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        HsvColorSegmentationWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < HsvColorSegmentationWidgetController >,
        public visionx::ImageProcessor
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit HsvColorSegmentationWidgetController();

        /**
         * Controller destructor
         */
        virtual ~HsvColorSegmentationWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "VisionX.HsvColorSegmentation";
        }

    protected:

        // ImageProcessor interface
        virtual void onInitImageProcessor() override;
        virtual void onConnectImageProcessor() override;
        virtual void onDisconnectImageProcessor() override;
        virtual void onExitImageProcessor() override;

        virtual void process() override;

    public slots:
        /* QT slot declarations */

        // provider settings
        void clickedButtonConnect(bool toggled);
        void clickedButtonDisconnect(bool toggled);
        void clickedButtonPlayPause(bool toggled);

        // selection
        void addColorSelection(int imageIndex, float x, float y);
        void clearColorSelection();


        // color settings

        void onColorSettingsChanged();

        void onColorIndexChanged(int value);

        void setManualHueMid(int value);
        void setManualHueTol(int value);
        void setManualSatMin(int value);
        void setManualSatMax(int value);
        void setManualValMin(int value);
        void setManualValMax(int value);

        void onToggledCheckBoxAutoAll(bool enabled);
        void onToggledCheckBoxAuto(bool enabled);
        void onChangedAddTolerance(int value);

        void resetCurrentColorSettings();

        // color parameter set

        /// Loads the color parameter set from file and update the gui
        void loadColorParameterSetFromFile(bool showMsgBoxes = true);
        void saveColorParameterSetToFile();

        void searchColorParameterFile();
        void resetColorParameterFile();

        // UI updates
        void updateImageMonitorUI();

    signals:
        /* QT signal declarations */
        void imageProviderConnected(bool connected);

    private:

        enum Param
        {
            HUE_MID, HUE_TOL, SAT_MIN, SAT_MAX, VAL_MIN, VAL_MAX, NUM_PARAMS
        };

        // utils
        static void rgbToHsv(int r, int g, int b, int& h, int& s, int& v);

        static std::string getHomeDirectory();
        static QString getDefaultColorParameterFile();

        const static QStringList COLOR_PARAMETER_NAMES;
        /// Default color parameter file, relative to home directory.
        const static std::string DEFAULT_COLOR_PARAMETER_FILE;

        // connenctivity
        void reconnect();
        void connectToProvider();
        void disconnectFromProvider();

        // ui value getters
        int value(int param);
        int hueMin();
        int hueMax();
        int satMid();
        int valMid();

        bool isAutoEnabled(int param);
        int additionalTolerance();

        std::string colorParameterFile();


        // procedures
        void recomputeAutoValues();
        void onSelectedPointsChanged();

        void drawSelectedPoints();
        void runSegmentation();


        /// Store the current settings to the local color parameter set.
        /// Does not store anything to disc.
        void saveCurrentColorParameters(int colorIndex);
        /// Load the parameters of the currently selected color to the gui.
        void loadCurrentColorParameters(int colorIndex);

        // ui updates
        void updatePausePlayButtonText();

        void setUiParamValue(int param, int value);
        void updateAutoValuesUI();

        void updateColorVisualization();

        void markCurrentColorDirty();
        void markCurrentColorClean();
        void markAllColorsClean();


        /// Widget Form
        Ui::HsvColorSegmentationWidget widget;


        // provider
        armarx::IceProxyFinder<visionx::ImageProviderInterfacePrx>* proxyFinder;

        std::string imageProviderName;
        visionx::ImageProviderInfo imageProviderInfo;
        visionx::ImageProviderInterfacePrx& imageProviderPrx = imageProviderInfo.proxy;
        int& numImages = imageProviderInfo.numberImages;
        int& imageWidth = imageProviderInfo.imageFormat.dimension.width;
        int& imageHeight = imageProviderInfo.imageFormat.dimension.height;

        bool connected = false;
        bool isPaused = true;


        armarx::RecursiveMutex imageMutex;
        HsvImageSegmentation segmentation;

        SelectableImageViewer* imageViewerInput;
        visionx::ImageViewerArea* imageViewerOutput;


        class SelectedPoint
        {
        public:

            SelectedPoint(int imageIndex, float xRel, float yRel, int red, int green, int blue) :
                imageIndex(imageIndex), xRel(xRel), yRel(yRel),
                red(red), green(green), blue(blue)
            {
                rgbToHsv(red, green, blue, hue, sat, val);
            }

            int imageIndex;
            float xRel, yRel;
            int red, green, blue;
            int hue, sat, val;
        };

        std::vector<SelectedPoint> selectedPoints;
        std::vector<int> autoValues;


        int currentColorIndex = 0;
        std::set<int> dirtyColors;
        CColorParameterSet colorParameterSet;




        std::vector<QCheckBox*> guiAutoCheckBoxes;
        std::vector<QSpinBox*> guiValueSpinBoxes;
        std::vector<QSlider*> guiValueSliders;

    };
}


