/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::PrimitiveExtractionWidgetController
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/gui-plugins/PrimitiveExtraction/ui_PrimitiveExtractionWidget.h>
#include "PrimitiveExtractionConfigDialog.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>

#include <VisionX/interface/components/FakePointCloudProviderInterface.h>
#include <VisionX/interface/components/PointCloudSegmenter.h>
#include <VisionX/interface/components/PrimitiveMapper.h>
#include <VisionX/interface/components/AffordancePipelineVisualization.h>

#include <QListWidgetItem>

namespace armarx
{
    /**
    \page VisionX-GuiPlugins-PrimitiveExtraction PrimitiveExtraction
    \brief The PrimitiveExtractionGui provides a convenient way for evaluating parameter
           setups for the segmentation of point clouds into geometric primitives.

    API Documentation \ref PrimitiveExtractionWidgetController

    \see PrimitiveExtractionGuiPlugin
    */

    /**
     * \class PrimitiveExtractionWidgetController
     * \brief PrimitiveExtractionWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        PrimitiveExtractionWidgetController:
        public ArmarXComponentWidgetControllerTemplate<PrimitiveExtractionWidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit PrimitiveExtractionWidgetController();

        /**
         * Controller destructor
         */
        virtual ~PrimitiveExtractionWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override;

        void configured() override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "VisionX.PrimitiveExtractionConfig";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;
        void onDisconnectComponent() override;

    public slots:
        void loadPointCloud();
        void run();
        void setParameters();
        void visualizationOptionsChanged();
        void applyPointCloudTransformation();
        void importConfig();
        void exportConfig();
        void applyAutoRotation();

    signals:
        void providersChanged();

    private:
        /**
         * Widget Form
         */
        Ui::PrimitiveExtractionWidget widget;
        QPointer<PrimitiveExtractionConfigDialog> configDialog;

        std::string pointCloudProviderName;
        std::string pointCloudSegmenterName;
        std::string primitiveExtractorName;
        std::string pipelineVisualizationName;
        std::string workingMemoryName;

        visionx::PointCloudProviderInterfacePrx pointCloudProvider;
        visionx::PointCloudSegmenterInterfacePrx pointCloudSegmenter;
        visionx::PrimitiveMapperInterfacePrx primitiveExtractor;
        AffordancePipelineVisualizationInterfacePrx pipelineVisualization;
        memoryx::WorkingMemoryInterfacePrx workingMemory;
        DebugDrawerInterfacePrx debugDrawer;
    };
}

