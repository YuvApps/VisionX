armarx_set_target("PrimitiveExtractionGuiPlugin")

set(SOURCES PrimitiveExtractionConfigDialog.cpp PrimitiveExtractionGuiPlugin.cpp PrimitiveExtractionWidgetController.cpp)
set(HEADERS PrimitiveExtractionConfigDialog.h   PrimitiveExtractionGuiPlugin.h   PrimitiveExtractionWidgetController.h)

set(GUI_MOC_HDRS PrimitiveExtractionConfigDialog.h PrimitiveExtractionGuiPlugin.h PrimitiveExtractionWidgetController.h)
set(GUI_UIS PrimitiveExtractionConfigDialog.ui PrimitiveExtractionWidget.ui)

set(COMPONENT_LIBS
    ArmarXGuiBase
    RobotAPICore
    VisionXCore
    AffordanceKitArmarX
    ${PCL_IO_LIBRARY}
)

if(ArmarXGui_FOUND)
	armarx_gui_library(PrimitiveExtractionGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
        if(PCL_FOUND)
            target_include_directories(PrimitiveExtractionGuiPlugin SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})
        endif()
endif()
