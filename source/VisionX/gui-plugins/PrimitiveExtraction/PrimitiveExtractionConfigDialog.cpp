/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX
* @author     Peter Kaiser (peter dot kaiser at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "PrimitiveExtractionConfigDialog.h"
#include <VisionX/gui-plugins/PrimitiveExtraction/ui_PrimitiveExtractionConfigDialog.h>

#include <QTimer>
#include <QPushButton>
#include <QMessageBox>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

#include <VisionX/interface/components/FakePointCloudProviderInterface.h>
#include <VisionX/interface/components/PointCloudSegmenter.h>
#include <VisionX/interface/components/PrimitiveMapper.h>
#include <VisionX/interface/components/AffordancePipelineVisualization.h>

#include <IceUtil/UUID.h>

namespace armarx
{
    PrimitiveExtractionConfigDialog::PrimitiveExtractionConfigDialog(QWidget* parent) :
        QDialog(parent),
        ui(new Ui::PrimitiveExtractionConfigDialog),
        uuid(IceUtil::generateUUID())
    {
        ui->setupUi(this);

        connect(this->ui->buttonBox, SIGNAL(accepted()), this, SLOT(verifyConfig()));
        ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);

        pointCloudProviderProxyFinder = new IceProxyFinder<visionx::PointCloudProviderInterfacePrx>(this);
        pointCloudSegmenterProxyFinder = new IceProxyFinder<visionx::PointCloudSegmenterInterfacePrx>(this);
        primitiveExtractorProxyFinder = new IceProxyFinder<visionx::PrimitiveMapperInterfacePrx>(this);
        pipelineVisualizationProxyFinder = new IceProxyFinder<AffordancePipelineVisualizationInterfacePrx>(this);

        pointCloudProviderProxyFinder->setSearchMask("*");
        pointCloudSegmenterProxyFinder->setSearchMask("*");
        primitiveExtractorProxyFinder->setSearchMask("*");
        pipelineVisualizationProxyFinder->setSearchMask("*");

        ui->gridLayout->addWidget(pointCloudProviderProxyFinder, 0, 1, 1, 2);
        ui->gridLayout->addWidget(pointCloudSegmenterProxyFinder, 1, 1, 1, 2);
        ui->gridLayout->addWidget(primitiveExtractorProxyFinder, 2, 1, 1, 2);
        ui->gridLayout->addWidget(pipelineVisualizationProxyFinder, 3, 1, 1, 2);
    }

    PrimitiveExtractionConfigDialog::~PrimitiveExtractionConfigDialog()
    {
        delete ui;
    }

    void PrimitiveExtractionConfigDialog::onInitComponent()
    {
        pointCloudProviderProxyFinder->setIceManager(getIceManager());
        pointCloudSegmenterProxyFinder->setIceManager(getIceManager());
        primitiveExtractorProxyFinder->setIceManager(getIceManager());
        pipelineVisualizationProxyFinder->setIceManager(getIceManager());
    }

    void PrimitiveExtractionConfigDialog::onConnectComponent()
    {
    }

    void PrimitiveExtractionConfigDialog::onExitComponent()
    {
        QObject::disconnect();
    }

    void PrimitiveExtractionConfigDialog::verifyConfig()
    {
        unsigned int l1 = pointCloudProviderProxyFinder->getSelectedProxyName().trimmed().length();
        unsigned int l3 = primitiveExtractorProxyFinder->getSelectedProxyName().trimmed().length();
        unsigned int l4 = pipelineVisualizationProxyFinder->getSelectedProxyName().trimmed().length();

        if (!l1 || !l3 || !l4)
        {
            QMessageBox::critical(this, "Invalid Configuration", "The proxy names must not be empty");
            return;
        }

        this->accept();
    }
}
