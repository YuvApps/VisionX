/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "Manager.h"

namespace visionx
{

    Manager::Manager()
    {
    }

    Manager::~Manager()
    {
    }

    void Manager::registerPointCloud(const std::string& name)
    {
        armarx::ScopedLock lock(mutex);
        if (clouds.find(name) == clouds.end())
        {
            clouds.emplace(name, nullptr);
        }
    }

    void Manager::unregisterPointCloud(const std::string& name)
    {
        armarx::ScopedLock lock(mutex);
        auto find = clouds.find(name);
        if (find != clouds.end())
        {
            if (find->second != nullptr)
            {
                int position = this->findChild(find->second);
                if (position != -1)
                {
                    this->removeChild(position);
                }
            }
            clouds.erase(find);
        }
    }

    std::vector<std::string> Manager::getRegisteredPointClouds()
    {
        armarx::ScopedLock lock(mutex);

        std::vector<std::string> cloud_list;
        for (const auto& [name, _] : clouds)
        {
            cloud_list.push_back(name);
        }
        return cloud_list;
    }

    CoinPointCloud* Manager::getCloudByName(const std::string& name)
    {
        armarx::ScopedLock lock(mutex);
        auto find = clouds.find(name);
        return find != clouds.end() ? find->second : nullptr;
    }

    void Manager::updatePointCloud(const std::string& name, CoinPointCloud& cloud)
    {
        armarx::ScopedLock lock(mutex);

        auto item = clouds.find(name);
        if (item != clouds.end())
        {
            if (item->second != nullptr)
            {
                // This will also bring ref counter of cloud to zero and free memory.
                int position = this->findChild(item->second);
                if (position != -1)
                {
                    this->removeChild(position);
                }
            }
            item->second = &cloud;
            this->addChild(&cloud);
        }
    }
}
