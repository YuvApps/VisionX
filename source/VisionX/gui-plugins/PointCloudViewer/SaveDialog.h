/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QDialog>
#include <QFileDialog>

#include "CoinPointCloud.h"

namespace Ui
{
    class SaveDialog;
}

namespace visionx
{
    class SaveDialog : public QDialog
    {
        Q_OBJECT

    public:

        explicit SaveDialog(QWidget* parent = 0);
        ~SaveDialog() override;

        void setPointCloudToSave(CoinPointCloud* cloud);

        void releaseBuffer();

        /// After calling the dialog, this contains the chosen file name
        std::string getResultFileName();

    public slots:

        void openFile();

    private:

        Ui::SaveDialog* ui;

        /// Save file dialog
        QFileDialog fileDialog;

    };
}
