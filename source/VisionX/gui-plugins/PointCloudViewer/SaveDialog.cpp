/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "SaveDialog.h"
#include <VisionX/gui-plugins/PointCloudViewer/ui_SaveDialog.h>

namespace visionx
{
    SaveDialog::SaveDialog(QWidget* parent) :
        QDialog(parent),
        ui(new Ui::SaveDialog),
        fileDialog(this)
    {
        ui->setupUi(this);

        //Connect select file button
        connect(ui->pushButton, SIGNAL(pressed()), this, SLOT(openFile()));

        //Init file dialog
        fileDialog.setNameFilter(tr("Point Clouds (*.pcd)"));
        fileDialog.setAcceptMode(QFileDialog::AcceptSave);
    }

    SaveDialog::~SaveDialog()
    {
        delete ui;
    }

    void SaveDialog::setPointCloudToSave(CoinPointCloud* cloud)
    {
        this->ui->widget->getDisplay()->getRootNode()->addChild(cloud);
        this->ui->widget->getDisplay()->cameraViewAll();
    }

    void SaveDialog::releaseBuffer()
    {
        this->ui->widget->getDisplay()->getRootNode()->removeAllChildren();
    }

    std::string SaveDialog::getResultFileName()
    {
        return ui->lineEdit->text().toStdString();
    }

    void SaveDialog::openFile()
    {
        if (fileDialog.exec())
        {
            ui->lineEdit->setText(fileDialog.selectedFiles()[0]);
        }
    }
}
