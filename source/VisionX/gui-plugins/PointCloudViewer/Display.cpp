/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "Display.h"

#include <QGridLayout>

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoPickStyle.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoVertexProperty.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>

namespace visionx
{
    DisplayWidget::DisplayWidget(QWidget* parent) : QWidget(parent)

    {
        this->setContentsMargins(1, 1, 1, 1);

        QGridLayout* grid = new QGridLayout(this);
        grid->setContentsMargins(0, 0, 0, 0);
        this->setLayout(grid);
        this->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

        QWidget* view1 = new QWidget(this);
        view1->setMinimumSize(100, 100);
        view1->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

        display = new Display(view1);
        display->show();

        grid->addWidget(view1, 0, 0, 1, 2);
    }

    DisplayWidget::~DisplayWidget()
    {

        delete display;
    }

    Display* DisplayWidget::getDisplay()
    {
        return display;
    }

    Display::Display(QWidget* widget) : SoQtExaminerViewer(widget), sceneRootNode(new SoSeparator), contentRootNode(new SoSeparator), camera(new SoPerspectiveCamera)
    {
        this->setBackgroundColor(SbColor(80 / 255.0f, 80 / 255.0f, 80 / 255.0f));
        this->setAccumulationBuffer(true);
        this->setHeadlight(true);
        this->setViewing(true);
        this->setDecoration(false);

        this->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_BLEND);
        this->setFeedbackVisibility(true);

        //Create scene root node
        sceneRootNode->ref();
        this->setSceneGraph(sceneRootNode);

        //Add camera to scene
        sceneRootNode->addChild(camera);
        this->setCamera(camera);

        //Give camera standard position
        camera->position.setValue(SbVec3f(10, -10, 5));
        camera->pointAt(SbVec3f(0, 0, 0), SbVec3f(0, 0, 1));

        //Add content node
        sceneRootNode->addChild(this->contentRootNode);
    }

    Display::~Display()
    {
        if (sceneRootNode)
        {
            sceneRootNode->unref();
        }
    }

    SoSeparator* Display::getRootNode()
    {
        return this->contentRootNode;
    }

    void Display::cameraViewAll()
    {
        camera->viewAll(this->contentRootNode, SbViewportRegion());
    }

    /*
    //Override the default navigation behaviour of the SoQtExaminerViewer
    SbBool Display::processSoEvent(const SoEvent* const event)
    {
        const SoType type(event->getTypeId());

        //Remapping mouse press events
        if (type.isDerivedFrom(SoMouseButtonEvent::getClassTypeId()))
        {
            SoMouseButtonEvent* const ev = (SoMouseButtonEvent*) event;
            const int button = ev->getButton();
            const SbBool press = ev->getState() == SoButtonEvent::DOWN ? TRUE : FALSE;

            //LEFT MOUSE BUTTON
            if (button == SoMouseButtonEvent::BUTTON1)
            {
                //Enable or disable viewing mode while BUTTON1 pressed
                if (press)
                {
                    if (!this->isViewing())
                    {
                        this->setViewing(true);
                    }
                }
                else
                {
                    if (this->isViewing())
                    {
                        this->setViewing(false);
                    }
                }

                if (this->isViewing())
                {
                    return SoQtExaminerViewer::processSoEvent(ev);
                }
            }

            //MOUSE WHEEL UP AND DOWN
            if (button == SoMouseButtonEvent::BUTTON4 || button == SoMouseButtonEvent::BUTTON5)
            {
                //Zooming is allowed, so just use it. We have to temporarily turn viewing mode
               // on to make SoQtExaminerViewer allow zooming.

                //Swap BUTTON4 and BUTTON5 because zooming out while scrolling up is just retarded
                ev->setButton(button == SoMouseButtonEvent::BUTTON4 ?
                              SoMouseButtonEvent::BUTTON5 : SoMouseButtonEvent::BUTTON4);

                //Zooming is allowed, so just pass it and temporarily set viewing mode on, if it is not already
                //(otherwise coin gives us warning messages...)
                if (!this->isViewing())
                {
                    if (!this->isViewing())
                    {
                        this->setViewing(true);
                    }

                    SoQtExaminerViewer::processSoEvent(ev);

                    if (this->isViewing())
                    {
                        this->setViewing(false);
                    }
                }
                else
                {
                    SoQtExaminerViewer::processSoEvent(ev);
                }

                return TRUE;
            }
        }

        // Keyboard handling
        if (type.isDerivedFrom(SoKeyboardEvent::getClassTypeId()))
        {
            const SoKeyboardEvent* const ev = (const SoKeyboardEvent*) event;

            //The escape key and super key (windows key) is used to switch between
           // viewing modes. We need to disable this behaviour completely.

            //65513 seems to be the super key, which is not available in the enum of keys in coin....
            if (ev->getKey() == SoKeyboardEvent::ESCAPE || ev->getKey() == 65513)
            {
                return TRUE;
            }
            else if (ev->getKey() == SoKeyboardEvent::S && ev->getState() == SoButtonEvent::DOWN)
            {
                if (!this->isSeekMode())
                {
                    if (!this->isViewing())
                    {
                        this->setViewing(true);
                    }

                    SoQtExaminerViewer::processSoEvent(ev);
                    this->setSeekTime(0.5);
                    this->seekToPoint(ev->getPosition());

                    if (this->isViewing())
                    {
                        this->setViewing(false);
                    }
                }
            }
            else if (ev->getKey() == SoKeyboardEvent::R && ev->getState() == SoButtonEvent::DOWN)
            {
                camera->viewAll(this->contentRootNode, SbViewportRegion());
            }
            else
            {
                SoQtExaminerViewer::processSoEvent(ev);
            }

            SoQtExaminerViewer::processSoEvent(ev);
        }


        if (type.isDerivedFrom(SoLocation2Event::getClassTypeId()))
        {
            return SoQtExaminerViewer::processSoEvent(event);
        }

        //YOU SHALL NOT PASS!
        return TRUE;

        return SoQtExaminerViewer::processSoEvent(event);
    }

     */
}
