armarx_set_target("PointCloudViewerGuiPlugin")

# Add more libraries you depend on here, e.g. ${QT_LIBRARIES}.
set(COMPONENT_LIBS
    VisionXInterfaces VisionXCore
    VisionXPointCloud # VisionXPointCloudTools
)

set(SOURCES
    GuiPlugin.cpp
    WidgetController.cpp
    SaveDialog.cpp
    Display.cpp
    CoinPointCloud.cpp
    Manager.cpp
)
set(HEADERS
    GuiPlugin.h
    WidgetController.h
    SaveDialog.h
    Display.h
    CoinPointCloud.h
    Manager.h
)
set(GUI_MOC_HDRS
    GuiPlugin.h
    WidgetController.h
    SaveDialog.h
    Display.h
)
set(GUI_UIS
    Widget.ui
    SaveDialog.ui
)

if(ArmarXGui_FOUND)
        armarx_gui_library(PointCloudViewerGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
