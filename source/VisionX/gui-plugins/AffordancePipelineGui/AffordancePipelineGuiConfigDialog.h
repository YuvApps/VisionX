/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    VisionX
* @author     Peter Kaiser ( peter dot kaiser at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QDialog>
#include <QFileDialog>

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

namespace Ui
{
    class AffordancePipelineGuiConfigDialog;
}

namespace armarx
{
    class AffordancePipelineGuiConfigDialog :
        public QDialog,
        virtual public ManagedIceObject
    {
        Q_OBJECT

    public:
        explicit AffordancePipelineGuiConfigDialog(QWidget* parent = 0);
        ~AffordancePipelineGuiConfigDialog() override;

        // inherited from ManagedIceObject
        std::string getDefaultName() const override
        {
            return "AffordancePipelineGuiConfigDialog" + uuid;
        }

        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        std::string getPlatform();

        std::vector<std::string> getProviderNames();
        std::vector<std::string> getProviderDisplayNames();
        std::vector<std::string> getFilteredProviderNames();
        std::vector<std::string> getPipelineConfigurationFiles();

    public slots:
        void verifyConfig();

    private:
        Ui::AffordancePipelineGuiConfigDialog* ui;
        std::string uuid;

        IceProxyFinderBase* pointCloudSegmenterProxyFinder;
        IceProxyFinderBase* primitiveExtractorProxyFinder;
        IceProxyFinderBase* affordanceExtractionProxyFinder;
        IceProxyFinderBase* pipelineVisualizationProxyFinder;
        IceProxyFinderBase* robotStateComponentProxyFinder;

        friend class AffordancePipelineGuiWidgetController;
    };
}

