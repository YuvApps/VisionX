/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::AffordancePipelineGuiWidgetController
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/gui-plugins/AffordancePipelineGui/ui_AffordancePipelineGuiWidget.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <RobotAPI/interface/core/RobotState.h>

#include <VisionX/interface/core/PointCloudProviderInterface.h>
#include <VisionX/interface/components/PointCloudSegmenter.h>
#include <VisionX/interface/components/PointCloudFilter.h>
#include <VisionX/interface/components/PrimitiveMapper.h>
#include <VisionX/interface/components/AffordanceExtraction.h>
#include <VisionX/interface/components/AffordancePipelineVisualization.h>

#include <QTimer>
#include <QToolBar>

namespace armarx
{
    class AffordancePipelineGuiConfigDialog;

    /**
    \page VisionX-GuiPlugins-AffordancePipelineGui AffordancePipelineGui
    \brief The AffordancePipelineGui provides access to a running affordance extraction pipeline.
           It allows to start/stop the procession of point clouds as well as to specify certain segmentation parameters.

    API Documentation \ref AffordancePipelineGuiWidgetController

    \see AffordancePipelineGuiGuiPlugin
    */

    /**
     * \class AffordancePipelineGuiWidgetController
     * \brief AffordancePipelineGuiWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        AffordancePipelineGuiWidgetController:
        public ArmarXComponentWidgetControllerTemplate<AffordancePipelineGuiWidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit AffordancePipelineGuiWidgetController();

        /**
         * Controller destructor
         */
        virtual ~AffordancePipelineGuiWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override;

        void configured() override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "VisionX.AffordancePipelineGui";
        }

        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent = 0) override;

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;
        void onDisconnectComponent() override;

    public slots:
        void updatePipelineStatus();
        void startPipeline();
        void pausePipeline();
        void stepPipeline();
        void croppingSelected(int index);
        void pointCloudSourceSelected(int index);
        void segmentationSetupSelected(int index);
        void visualizationOptionsChanged(bool prm = false);
        void loadScene();
        void saveScene();

    private:
        QString formatTimestamp(long timestamp);

    private:
        /**
         * Widget Form
         */
        Ui::AffordancePipelineGuiWidget widget;
        QPointer<AffordancePipelineGuiConfigDialog> configDialog;

        QToolBar* customToolbar;

        std::vector<std::string> pointCloudProviderNames;
        std::vector<std::string> filteredPointCloudProviderNames;
        std::vector<std::string> pointCloudProviderDisplayNames;

        std::vector<std::string> pipelineConfigFiles;

        std::string pointCloudSegmenterName;
        std::string primitiveExtractorName;
        std::string affordanceExtractionName;
        std::string pipelineVisualizationName;
        std::string robotStateComponentName;
        std::string platform;

        std::vector<visionx::CapturingPointCloudProviderInterfacePrx> pointCloudProviders;
        std::vector<armarx::PointCloudFilterInterfacePrx> filteredPointCloudProviders;
        unsigned int currentPointCloudProvider;

        visionx::PointCloudSegmenterInterfacePrx pointCloudSegmenter;
        visionx::PrimitiveMapperInterfacePrx primitiveExtractor;
        AffordanceExtractionInterfacePrx affordanceExtraction;
        AffordancePipelineVisualizationInterfacePrx pipelineVisualization;
        RobotStateComponentInterfacePrx robotStateComponent;

        QTimer* updateTimer;
    };
}

