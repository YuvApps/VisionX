armarx_set_target("AffordancePipelineGuiGuiPlugin")

set(SOURCES AffordancePipelineGuiGuiPlugin.cpp  AffordancePipelineGuiWidgetController.cpp AffordancePipelineGuiConfigDialog.cpp)
set(HEADERS AffordancePipelineGuiGuiPlugin.h    AffordancePipelineGuiWidgetController.h   AffordancePipelineGuiConfigDialog.h)
set(GUI_MOC_HDRS AffordancePipelineGuiGuiPlugin.h AffordancePipelineGuiWidgetController.h   AffordancePipelineGuiConfigDialog.h)
set(GUI_UIS AffordancePipelineGuiWidget.ui AffordancePipelineGuiConfigDialog.ui)

# Add more libraries you depend on here, e.g. ${QT_LIBRARIES}.
set(COMPONENT_LIBS
    ArmarXCore
    ArmarXGuiBase
    RobotAPICore
    VisionXInterfaces
    VisionXCore
)

if(ArmarXGui_FOUND)
        armarx_gui_library(AffordancePipelineGuiGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
