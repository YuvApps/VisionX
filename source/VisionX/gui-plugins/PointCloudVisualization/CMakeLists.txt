armarx_set_target("PointCloudVisualizationGuiPlugin")

set(SOURCES PointCloudVisualizationGuiPlugin.cpp PointCloudVisualizationConfigDialog.cpp PointCloudVisualizationWidgetController.cpp)
set(HEADERS PointCloudVisualizationGuiPlugin.h   PointCloudVisualizationConfigDialog.h   PointCloudVisualizationWidgetController.h)

set(GUI_MOC_HDRS PointCloudVisualizationGuiPlugin.h PointCloudVisualizationConfigDialog.h   PointCloudVisualizationWidgetController.h)
set(GUI_UIS PointCloudVisualizationWidget.ui PointCloudVisualizationConfigDialog.ui)
set(GUI_RCS icons.qrc)

# Add more libraries you depend on here, e.g. ${QT_LIBRARIES}.
set(COMPONENT_LIBS VisionXInterfaces VisionXCore)

if(ArmarXGui_FOUND)
    armarx_gui_library(PointCloudVisualizationGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "${GUI_RCS}" "${COMPONENT_LIBS}")
endif()
