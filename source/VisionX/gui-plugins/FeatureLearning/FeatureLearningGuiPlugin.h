/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    VisionX::gui-plugins::FeatureLearning
 * \author     Markus Grotz ( markus dot grotz at kit dot edu )
 * \date       2018
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>


namespace armarx
{
    /**
     * \class FeatureLearningGuiPlugin
     * \ingroup ArmarXGuiPlugins
     * \brief FeatureLearningGuiPlugin brief description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT FeatureLearningGuiPlugin:
        public armarx::ArmarXGuiPlugin
    {
        Q_OBJECT
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")
    public:
        /**
         * All widgets exposed by this plugin are added in the constructor
         * via calls to addWidget()
         */
        FeatureLearningGuiPlugin();
    };
}
