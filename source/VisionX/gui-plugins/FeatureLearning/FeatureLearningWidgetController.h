/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::FeatureLearningWidgetController
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_VisionX_FeatureLearning_WidgetController_H
#define _ARMARX_VisionX_FeatureLearning_WidgetController_H

#include <VisionX/gui-plugins/FeatureLearning/ui_FeatureLearningWidget.h>

#include <VisionX/libraries/SelectableImageViewer/SelectableImageViewer.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <VisionX/core/ImageProcessor.h>

#include <Features/HarrisSIFTFeatures/HarrisSIFTFeatureCalculator.h>
#include <Features/SIFTFeatures/SIFTFeatureEntry.h>
#include <TexturedRecognition/TexturedFeatureSet.h>
#include <Math/LinearAlgebra.h>
#include <Math/FloatMatrix.h>


#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/core/GridFileManager.h>

#include <VisionX/gui-plugins/ImageMonitor/ImageViewerArea.h>

#include <Eigen/Core>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>
#include <ArmarXCore/core/IceManager.h>

#include <QFileDialog>
#include <QMessageBox>

#include <QSettings>
#include <QDir>

#include <math.h>

#include <VisionX/gui-plugins/FeatureLearning/FeatureLearningObjectChooserWidget.h>
#include <VisionX/gui-plugins/FeatureLearning/FeatureLearningSaveToMemoryWidget.h>


namespace armarx
{

    //class FeatureLearningObjectChooserWidget;

    /**
    \page ArmarXGui-GuiPlugins-FeatureLearning FeatureLearning
    \brief The FeatureLearning allows connecting to ArmarX ImageProviders and extract features from their images.

    \image html FeatureLearning.png
    The user can connect and disconnect ImageProviders easily. By clicking on the image, a region can be chosen. SIFT features are calculated within the region. After features have been added to the selection, they can be saved to a file. Additionally, the features can be saved to an object in the PriorMemory, if it is available. Loading from the memory or a file is also supported.

    API Documentation \ref FeatureLearningWidgetController

    \see FeatureLearningGuiPlugin
    */

    /**
     * \class FeatureLearningWidgetController
     * \brief FeatureLearningWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        FeatureLearningWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < FeatureLearningWidgetController >,
        public visionx::ImageProcessor
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit FeatureLearningWidgetController();

        /**
         * Controller destructor
         */
        virtual ~FeatureLearningWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        virtual void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        virtual void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "VisionX.FeatureLearning";
        }

        void reconnect();

        /**
         * @see visionx::ImageProcessor::onInitImageProcessor()
         */
        virtual void onInitImageProcessor() override;

        /**
         * @see visionx::ImageProcessor::onConnectImageProcessor()
         */
        virtual void onConnectImageProcessor() override;

        /**
         * @see visionx::ImageProcessor::onDisconnectImageProcessor()
         */
        virtual void onDisconnectImageProcessor() override;

        /**
         * @see visionx::ImageProcessor::onExitImageProcessor()
         */
        virtual void onExitImageProcessor() override;

        /**
         * @see visionx::ImageProcessor::process()
         */
        virtual void process() override;

    public slots:
        /* QT slot declarations */

        void addPolygonPoint(int, float x, float y);
        void connectButtonClicked(bool toggled);
        void disconnectButtonClicked(bool toggled);
        void loadFeaturesButtonClicked(bool toggled);
        void saveFeaturesButtonClicked(bool toggled);
        void pausePlayButtonClicked(bool toggled);
        void addFeaturesButtonClicked(bool toggled);
        void clearFeaturesButtonClicked(bool toggled);

        void objectChooserButtonClicked(bool toggled);
        void saveToMemoryButtonClicked(bool toggled);

        void objectChooserAccepted();
        void saveToMemoryAccepted();

    signals:
        /* QT signal declarations */
        void imageProviderConnected(bool connected);

    private:
        /**
         * Widget Form
         */
        Ui::FeatureLearningWidget widget;
        QWidget* widgetPointer;

        void loadFeatureSet(QString filePath);
        void saveFeatureSet(QString filePath, float w, float h, float d);
        void updateFeatures();
        int addFeatures();
        void clearFeatures();

        void updateSelection();

        void connectToProvider();
        void disconnectFromProvider();

        void updatePausePlayButtonText();
        void updateImageMonitorUI();
        void updateFeaturesUI();

        bool connectToPriorMemory(memoryx::PersistentObjectClassSegmentBasePrx& classesSegmentPrx, memoryx::GridFileManagerPtr& fileManager);



        static CFloatMatrix* createCuboid(float w, float h, float d);
        static void addRectangle(CFloatMatrix* object, int& offset, float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3, float x4, float y4, float z4, float n1, float n2, float n3);
        static void addTriangle(CFloatMatrix* object, int& offset, float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3, float n1, float n2, float n3);


        // provider
        visionx::ImageProviderInterfacePrx   imageProviderPrx;
        visionx::ImageProviderInfo  imageProviderInfo;


        SelectableImageViewer* imageViewer;

        int numSelectedPoints;

        CByteImage** cameraImages;
        CByteImage* visualizationImage;
        CByteImage* grayImage;

        int numImages;

        bool isPaused;

        std::string imageProviderName;

        CHarrisSIFTFeatureCalculator* featureCalculator;
        CTexturedFeatureSet* objectFeatureSet;
        CTexturedFeatureSet* viewFeatureSet;

        bool undistortImages;
        armarx::RecursiveMutex imageMutex;

        //contains relative position of the mouse clicks on the image
        Eigen::ArrayX2f points;
        Mat3d homography;

        armarx::IceProxyFinder<visionx::ImageProviderInterfacePrx>* proxyFinder;

        bool connected;
        IceUtil::Time timeProvided;

        QSettings mySettings;
        visionx::FeatureLearningObjectChooserWidget* featureLearningObjectChooserWidget;
        visionx::FeatureLearningSaveToMemoryWidget* featureLearningSaveToMemoryWidget;

    };
}

#endif
