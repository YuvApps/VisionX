#include "SemanticGraphVertexItem.h"

#include <ArmarXCore/core/logging/Logging.h>

#include <QGraphicsSceneContextMenuEvent>
#include <QPainter>

namespace armarx
{

    void SemanticGraphVertexItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
    {
        if (selected)
        {
            this->setPen(QPen(borderColor, 4.0f));
            this->setBrush(QBrush(fillColor));
        }
        else
        {
            this->setPen(QPen(borderColor, 2.0f));
            this->setBrush(QBrush(fillColor));
        }
        QGraphicsEllipseItem::paint(painter, option, widget);

        // Add text to the ellipse
        QRectF rect = this->rect();
        painter->setPen(fontColor);

        QFont font;
        if (selected)
        {
            font.setBold(true);
        }
        else
        {
            font.setBold(false);
        }
        painter->setFont(font);
        painter->drawText(rect, Qt::AlignCenter, text);
    }

    void SemanticGraphVertexItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
    {
        ARMARX_VERBOSE << "Pressed node: " << text.toStdString();
        if (event->button() == Qt::LeftButton)
        {
            emit onLeftClick(this);
        }
    }

}
