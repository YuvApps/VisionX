#pragma once

#include <QObject>
#include <QGraphicsItem>

namespace armarx
{

    class SemanticGraphGlobalItem
        : public QObject
        , public QGraphicsRectItem
    {
        Q_OBJECT
    public:
        SemanticGraphGlobalItem();

        // QGraphicsItem interface
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

        void mousePressEvent(QGraphicsSceneMouseEvent* event) override;

        QString text = "Global";
        bool selected = false;

        QColor fillColor = Qt::GlobalColor::white;
        QColor borderColor = Qt::GlobalColor::black;
        QColor fontColor = Qt::GlobalColor::black;


    signals:
        void onLeftClick(SemanticGraphGlobalItem*);
    };

}
