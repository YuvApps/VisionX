/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::gui-plugins::SemanticRelationViewerWidgetController
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <VisionX/gui-plugins/SemanticRelationViewer/ui_SemanticRelationViewerWidget.h>

#include "SemanticGraphVertexItem.h"
#include "SemanticGraphEdgeItem.h"
#include "SemanticGraphGlobalItem.h"

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>
#include <RobotAPI/libraries/widgets/JSONTreeModel.h>
#include <VisionX/interface/components/SemanticRelationAnalyzer.h>
#include <VisionX/interface/libraries/SemanticObjectRelations/GraphStorage.h>

#include <SemanticObjectRelations/RelationGraph/json/AttributedGraph.h>

#include <QTimer>

namespace armarx
{
    struct GraphvizLayoutedGraph;

    /**
    \page VisionX-GuiPlugins-SemanticRelationViewer SemanticRelationViewer
    \brief The SemanticRelationViewer allows visualizing ...

    \image html SemanticRelationViewer.png
    The user can

    API Documentation \ref SemanticRelationViewerWidgetController

    \see SemanticRelationViewerGuiPlugin
    */

    /**
     * \class SemanticRelationViewerWidgetController
     * \brief SemanticRelationViewerWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        SemanticRelationViewerWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < SemanticRelationViewerWidgetController >
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit SemanticRelationViewerWidgetController();

        /**
         * Controller destructor
         */
        virtual ~SemanticRelationViewerWidgetController();

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        QPointer<QDialog> getConfigDialog(QWidget* parent) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "VisionX.SemanticRelationViewer";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;
        void onDisconnectComponent() override;


    public slots:
        /* QT slot declarations */
        void onUpdateGraphs();
        void onAutoUpdateChanged();

        void onLeftClickVertex(SemanticGraphVertexItem* item);
        void onLeftClickEdge(SemanticGraphEdgeItem* item);
        void onLeftClickGlobal(SemanticGraphGlobalItem* item);

    signals:
        /* QT signal declarations */

    private:
        void drawGraph(QGraphicsScene* graphicsScene,
                       GraphvizLayoutedGraph const& layoutedGraph,
                       semrel::AttributedGraph const& graph);

        void setPropertyView(nlohmann::json const& attrs);

        void highlightSelected(QGraphicsItem* selected);

    private:
        Ui::SemanticRelationViewerWidget widget;
        QTimer* timer = nullptr;
        QGraphicsScene graphicsScene;
        QPointer<SimpleConfigDialog> configDialog;

        armarx::semantic::GraphStorageInterfacePrx storage;

        std::map<std::string, semrel::AttributedGraph> graphMap;
        JSONTreeModel propertyModel;
    };
}


