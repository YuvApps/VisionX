#pragma once

#include <QObject>
#include <QGraphicsItem>

#include <cmath>

namespace armarx
{

    class SemanticGraphEdgeItem
        : public QObject
        , public QGraphicsPathItem
    {
        Q_OBJECT
    public:
        // QGraphicsItem interface
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

        void mousePressEvent(QGraphicsSceneMouseEvent* event) override;

        QPainterPath shape() const override;

        int sourceDescriptor = 0;
        int targetDescriptor = 0;

        float openingAngle = M_PI / 8.0f;
        float size = 10.0f;

        bool selected = false;

        QColor color = Qt::GlobalColor::black;

    signals:
        void onLeftClick(SemanticGraphEdgeItem* selected);
    };


}
