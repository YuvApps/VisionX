#include "SemanticGraphGlobalItem.h"

#include <ArmarXCore/core/logging/Logging.h>

#include <QGraphicsSceneContextMenuEvent>
#include <QPainter>

namespace armarx
{

    SemanticGraphGlobalItem::SemanticGraphGlobalItem()
    {
    }

    void SemanticGraphGlobalItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
    {
        if (selected)
        {
            this->setPen(QPen(borderColor, 4.0f));
            this->setBrush(QBrush(fillColor));
        }
        else
        {
            this->setPen(QPen(borderColor, 2.0f));
            this->setBrush(QBrush(fillColor));
        }
        QGraphicsRectItem::paint(painter, option, widget);

        // Add text to the ellipse
        QRectF rect = this->rect();
        painter->setPen(fontColor);

        QFont font;
        if (selected)
        {
            font.setBold(true);
        }
        else
        {
            font.setBold(false);
        }
        painter->setFont(font);
        painter->drawText(rect, Qt::AlignCenter, text);
    }

    void SemanticGraphGlobalItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
    {
        ARMARX_VERBOSE << "Pressed node: " << text.toStdString();
        if (event->button() == Qt::LeftButton)
        {
            emit onLeftClick(this);
        }
    }

}
