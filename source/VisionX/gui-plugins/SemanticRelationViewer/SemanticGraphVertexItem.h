#pragma once

#include <QObject>
#include <QGraphicsItem>

namespace armarx
{

    class SemanticGraphVertexItem
        : public QObject
        , public QGraphicsEllipseItem
    {
        Q_OBJECT
    public:
        // QGraphicsItem interface
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

        void mousePressEvent(QGraphicsSceneMouseEvent* event) override;

        QString text;
        bool selected = false;

        // The descriptor references the vertex in the original graph
        int descriptor = 0;

        QColor fillColor = Qt::GlobalColor::white;
        QColor borderColor = Qt::GlobalColor::black;
        QColor fontColor = Qt::GlobalColor::black;


    signals:
        void onLeftClick(SemanticGraphVertexItem*);
    };

}
