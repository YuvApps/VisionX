/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    VisionX::gui-plugins::SemanticRelationViewerWidgetController
 * \author     Fabian Paus ( fabian dot paus at kit dot edu )
 * \date       2019
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <VirtualRobot/VirtualRobot.h>
#include <SimoxUtility/color/color.h>

#include "SemanticRelationViewerWidgetController.h"

#include <VisionX/gui-plugins/SemanticRelationViewer/GraphvizLayout.h>
#include <VisionX/libraries/SemanticObjectRelations/ice_serialization.h>

#include <ArmarXGui/gui-plugins/StatechartViewerPlugin/view/SplinePath.h>

#include <SemanticObjectRelations/SupportAnalysis/SupportGraph.h>


namespace armarx
{
    namespace
    {

        GraphvizLayoutedGraph layoutGraph(semrel::AttributedGraph const& graph)
        {
            GraphvizLayout layout;
            for (auto vertex : graph.vertices())
            {
                auto desc = vertex.descriptor();
                auto& attrs = vertex.attrib().json;
                std::string name = std::to_string(desc);

                auto object = attrs.find("object");
                if (object != attrs.end())
                {
                    auto nameValue = object->find("name");
                    if (nameValue != object->end() && nameValue->is_string())
                    {
                        name = nameValue->get<std::string>() + std::to_string(object->at("id").get<int>());
                    }
                    nameValue = object->find("instance");
                    if (nameValue != object->end() && nameValue->is_string())
                    {
                        name = nameValue->get<std::string>();
                    }
                }
                layout.addNode(desc, name);
                ARMARX_INFO << "[" << desc << "]: " << name;
            }
            for (auto edge : graph.edges())
            {
                layout.addEdge(edge.sourceDescriptor(), edge.targetDescriptor());
                ARMARX_INFO << edge.sourceDescriptor() << " -> " << edge.targetDescriptor();
            }
            return layout.finish();
        }

    }

    SemanticRelationViewerWidgetController::SemanticRelationViewerWidgetController()
    {
        setlocale(LC_ALL, "en_US.UTF-8");

        widget.setupUi(getWidget());
        timer = new QTimer(this);
        widget.graphView->setScene(&graphicsScene);
        widget.graphView->setRenderHints(QPainter::Antialiasing | QPainter::HighQualityAntialiasing);
        int propWidth = 250;
        int graphWidth = 750;
        widget.splitter->setSizes({graphWidth, propWidth});

        connect(timer, SIGNAL(timeout()), this, SLOT(onUpdateGraphs()));
        connect(widget.updatePushButton, SIGNAL(clicked()), this, SLOT(onUpdateGraphs()));
        connect(widget.autoUpdateCheckBox, SIGNAL(stateChanged(int)), this, SLOT(onAutoUpdateChanged()));
    }


    SemanticRelationViewerWidgetController::~SemanticRelationViewerWidgetController()
    {

    }


    void SemanticRelationViewerWidgetController::loadSettings(QSettings* settings)
    {

    }

    void SemanticRelationViewerWidgetController::saveSettings(QSettings* settings)
    {

    }

    static const std::string STORAGE_NAME_KEY = "SemanticGraphStorage";

    QPointer<QDialog> armarx::SemanticRelationViewerWidgetController::getConfigDialog(QWidget* parent)
    {
        if (!configDialog)
        {
            configDialog = new SimpleConfigDialog(parent);
            configDialog->addProxyFinder<armarx::semantic::GraphStorageInterfacePrx>({STORAGE_NAME_KEY, "SemanticGraphStorage", "SemanticGraphStorage"});
        }
        return qobject_cast<SimpleConfigDialog*>(configDialog);
    }


    void SemanticRelationViewerWidgetController::onInitComponent()
    {
    }

    template <typename T>
    void setProxy(ManagedIceObject* object, T& proxy, std::string const& name)
    {
        object->usingProxy(name);
        proxy = object->getProxy<T>(name);
    }

    void SemanticRelationViewerWidgetController::onConnectComponent()
    {
        std::string storageName = configDialog->getProxyName(STORAGE_NAME_KEY);
        setProxy(this, storage, storageName);
    }

    void SemanticRelationViewerWidgetController::onDisconnectComponent()
    {
        timer->stop();
    }

    void SemanticRelationViewerWidgetController::onUpdateGraphs()
    {
        graphMap.clear();
        semantic::data::GraphMap allGraphs = storage->getAll();
        for (auto& pair : allGraphs)
        {
            std::string const& id = pair.first;
            graphMap[id] = semantic::fromIce(pair.second);
        }

        std::string selectedID = widget.idComboBox->currentText().toStdString();
        widget.idComboBox->clear();
        if (allGraphs.empty())
        {
            return;
        }
        for (auto& pair : allGraphs)
        {
            std::string const& id = pair.first;
            widget.idComboBox->addItem(QString::fromStdString(id));
        }
        int selectedIndex = widget.idComboBox->findText(QString::fromStdString(selectedID));
        if (selectedIndex < 0)
        {
            selectedIndex = 0;
        }
        widget.idComboBox->setCurrentIndex(selectedIndex);

        selectedID = widget.idComboBox->currentText().toStdString();

        if (graphMap.count(selectedID))
        {
            semrel::AttributedGraph const& graph = graphMap[selectedID];

            GraphvizLayoutedGraph layoutedGraph = layoutGraph(graph);
            drawGraph(&graphicsScene, layoutedGraph, graph);

            setPropertyView(nlohmann::json::object());
        }
        else
        {
            std::vector<std::string> ids;
            for (auto& entry : graphMap)
            {
                ids.push_back(entry.first);
            }
            ARMARX_WARNING << "Could not find graph with ID: " << selectedID
                           << "\nExisting IDs: " << ids;
        }
    }

    void SemanticRelationViewerWidgetController::onAutoUpdateChanged()
    {
        if (widget.autoUpdateCheckBox->checkState() == Qt::Checked)
        {
            timer->start(1000);
        }
        else
        {
            timer->stop();
        }
    }

    void SemanticRelationViewerWidgetController::onLeftClickVertex(SemanticGraphVertexItem* selected)
    {
        std::string id = widget.idComboBox->currentText().toStdString();
        if (graphMap.count(id) == 0)
        {
            ARMARX_WARNING << "No graph with ID: " << id;
            return;
        }
        auto& graph = graphMap[id];
        auto vertex = graph.vertex(selected->descriptor);
        ARMARX_VERBOSE << "Vertex[" << selected->descriptor << "]: "
                       << vertex.attrib().json.dump(4);

        setPropertyView(vertex.attrib().json);
        highlightSelected(selected);
    }

    void SemanticRelationViewerWidgetController::onLeftClickEdge(SemanticGraphEdgeItem* selected)
    {
        std::string id = widget.idComboBox->currentText().toStdString();
        if (graphMap.count(id) == 0)
        {
            return;
        }
        auto& graph = graphMap[id];

        auto edge = graph.edge(graph.vertex(selected->sourceDescriptor), graph.vertex(selected->targetDescriptor));
        ARMARX_VERBOSE << "Edge: " << edge.attrib().json.dump(4);

        edge.attrib().json.is_null();
        setPropertyView(edge.attrib().json);
        highlightSelected(selected);
    }

    void SemanticRelationViewerWidgetController::onLeftClickGlobal(SemanticGraphGlobalItem* selected)
    {
        std::string id = widget.idComboBox->currentText().toStdString();
        if (graphMap.count(id) == 0)
        {
            ARMARX_WARNING << "No graph with ID: " << id;
            return;
        }

        auto& graph = graphMap[id];
        setPropertyView(graph.attrib().json);
        highlightSelected(selected);
    }

    void SemanticRelationViewerWidgetController::drawGraph(QGraphicsScene* graphicsScene,
            GraphvizLayoutedGraph const& layoutedGraph,
            semrel::AttributedGraph const& graph)
    {
        graphicsScene->clear();

        float minX = FLT_MAX;
        float minY = FLT_MAX;

        for (auto& pair : layoutedGraph.nodes)
        {
            int id = pair.first;
            GraphvizLayoutedNode const& layoutedNode = pair.second;

            SemanticGraphVertexItem* ellipse = new SemanticGraphVertexItem();
            ellipse->setRect(layoutedNode.posX, layoutedNode.posY, layoutedNode.width, layoutedNode.height);
            ellipse->text = QString::fromStdString(layoutedNode.label);
            ellipse->descriptor = id;

            minX = std::min(layoutedNode.posX, minX);
            minY = std::min(layoutedNode.posY, minY);

            // Get optional style information
            auto vertex = graph.vertex(semrel::ShapeID{id});
            auto& json = vertex.attrib().json;
            if (json.count("style"))
            {
                auto& style = json["style"];
                auto setColor = [&](std::string const & colorName, QColor * output)
                {
                    if (style.count(colorName))
                    {
                        simox::color::Color scolor = style[colorName];
                        QColor qcolor(scolor(0), scolor(1), scolor(2), scolor(3));
                        *output = qcolor;
                    }
                };
                setColor("fill-color", &ellipse->fillColor);
                setColor("border-color", &ellipse->borderColor);
                setColor("font-color", &ellipse->fontColor);
            }

            bool connected = QObject::connect(ellipse, &SemanticGraphVertexItem::onLeftClick,
                                              this, &SemanticRelationViewerWidgetController::onLeftClickVertex);
            if (!connected)
            {
                ARMARX_WARNING << "Failed to connect Qt slots";
            }

            graphicsScene->addItem(ellipse);
        }



        for (auto& pair : layoutedGraph.edges)
        {
            int sourceID = pair.first.first;
            int targetID = pair.first.second;
            GraphvizLayoutedEdge const& layoutedEdge = pair.second;

            QPainterPath path;
            if (layoutedEdge.startPoint)
            {
                path.lineTo(*layoutedEdge.startPoint);
            }
            path.addPath(SplinePath::simplePath(layoutedEdge.controlPoints));
            if (layoutedEdge.endPoint)
            {
                path.lineTo(*layoutedEdge.endPoint);
            }

            for (int i = 0; i <= 10; ++i)
            {
                double t = 0.1 * i;
                QPointF point = path.pointAtPercent(t);

                minX = std::min((float)point.x(), minX);
                minY = std::min((float)point.y(), minY);
            }

            SemanticGraphEdgeItem* item = new SemanticGraphEdgeItem();
            item->sourceDescriptor = sourceID;
            item->targetDescriptor = targetID;
            item->setPath(path);

            // Get optional style information
            auto sourceVertex = graph.vertex(item->sourceDescriptor);
            auto targetVertex = graph.vertex(item->targetDescriptor);
            auto edge = graph.edge(sourceVertex, targetVertex);
            auto& json = edge.attrib().json;
            if (json.count("style"))
            {
                auto& style = json["style"];
                if (style.count("color"))
                {
                    simox::color::Color scolor = style["color"];
                    QColor qcolor(scolor(0), scolor(1), scolor(2), scolor(3));
                    item->color = qcolor;
                }
            }

            bool connected = QObject::connect(item, &SemanticGraphEdgeItem::onLeftClick,
                                              this, &SemanticRelationViewerWidgetController::onLeftClickEdge);
            if (!connected)
            {
                ARMARX_WARNING << "Failed to connect Qt slots";
            }

            graphicsScene->addItem(item);
        }

        SemanticGraphGlobalItem* global = new SemanticGraphGlobalItem();
        global->setRect(minX, minY, 100.0f, 30.0f);
        bool connected = QObject::connect(global, &SemanticGraphGlobalItem::onLeftClick,
                                          this, &SemanticRelationViewerWidgetController::onLeftClickGlobal);
        if (!connected)
        {
            ARMARX_WARNING << "Failed to connect Qt slots";
        }
        graphicsScene->addItem(global);
    }

    void SemanticRelationViewerWidgetController::setPropertyView(nlohmann::json const& attrs)
    {
        propertyModel.setRoot(attrs);

        widget.propertiesView->setModel(&propertyModel);
        widget.propertiesView->expandAll();
        widget.propertiesView->resizeColumnToContents(0);
    }

    void SemanticRelationViewerWidgetController::highlightSelected(QGraphicsItem* selected)
    {
        for (QGraphicsItem* item : graphicsScene.items())
        {
            if (auto* edge = dynamic_cast<SemanticGraphEdgeItem*>(item))
            {
                edge->selected = (edge == selected);
            }
            else if (auto* vertex = dynamic_cast<SemanticGraphVertexItem*>(item))
            {
                vertex->selected = (vertex == selected);
            }
            else if (auto* global = dynamic_cast<SemanticGraphGlobalItem*>(item))
            {
                global->selected = (global == selected);
            }
        }
        graphicsScene.update();
    }
}
