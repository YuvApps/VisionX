
armarx_set_target("UserAssistedSegmenterGuiGuiPlugin")

# most qt components will be linked against in the call armarx_gui_library
#armarx_find_qt(QtCore QtGui QtDesigner)

# ArmarXGui gets included through depends_on_armarx_package(ArmarXGui "OPTIONAL")
# in the toplevel CMakeLists.txt
armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")


find_package(PCL 1.8 QUIET)
armarx_build_if(PCL_FOUND "PCL not available")


set(SOURCES
    UserAssistedSegmenterConfigDialog.cpp
    UserAssistedSegmenterGuiGuiPlugin.cpp 
    UserAssistedSegmenterGuiWidgetController.cpp
    
    widgets/PointCloudSegmentsTable.cpp
    widgets/UserGroupingLineEdit.cpp
)

set(HEADERS
    UserAssistedSegmenterConfigDialog.h
    UserAssistedSegmenterGuiGuiPlugin.h 
    UserAssistedSegmenterGuiWidgetController.h
    
    widgets/PointCloudSegmentsTable.h
    widgets/UserGroupingLineEdit.h
)

set(GUI_MOC_HDRS ${HEADERS})

set(GUI_UIS
    UserAssistedSegmenterConfigDialog.ui
    UserAssistedSegmenterGuiWidget.ui
    
    widgets/PointCloudSegmentsTable.ui
)

# Add more libraries you depend on here, e.g. ${QT_LIBRARIES}.
set(COMPONENT_LIBS 
    RobotAPICore
    VisionXCore
    VisionXPointCloud
    VisionXInterfaces
    
    ${PCL_LIBRARIES}
)

if(ArmarXGui_FOUND)
	armarx_gui_library(UserAssistedSegmenterGuiGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
    
    if(PCL_FOUND)
        target_include_directories(UserAssistedSegmenterGuiGuiPlugin SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})
    endif()
    
endif()
