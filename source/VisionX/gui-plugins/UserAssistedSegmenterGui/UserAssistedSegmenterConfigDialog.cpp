#include "UserAssistedSegmenterConfigDialog.h"
#include <VisionX/gui-plugins/UserAssistedSegmenterGui/ui_UserAssistedSegmenterConfigDialog.h>

#include <QPushButton>
#include <QMessageBox>

#include <IceUtil/UUID.h>


namespace visionx
{

    UserAssistedSegmenterConfigDialog::UserAssistedSegmenterConfigDialog(QWidget* parent) :
        QDialog(parent),
        ui(new Ui::UserAssistedSegmenterConfigDialog),
        uuid(IceUtil::generateUUID())
    {
        ui->setupUi(this);

        connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(verifyConfig()));
        ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);
    }

    UserAssistedSegmenterConfigDialog::~UserAssistedSegmenterConfigDialog()
    {
        delete ui;
    }

    void UserAssistedSegmenterConfigDialog::onInitComponent()
    {
    }

    void UserAssistedSegmenterConfigDialog::onConnectComponent()
    {
    }

    void UserAssistedSegmenterConfigDialog::onExitComponent()
    {
        QObject::disconnect();
    }

    std::string UserAssistedSegmenterConfigDialog::getUserAssistedSegmenterProxyName() const
    {
        return ui->lineEditUserAssistedSegmenterProxy->text().toStdString();
    }

    std::string UserAssistedSegmenterConfigDialog::getUserAssistedSegmenterTopicName() const
    {
        return ui->lineEditUserAssistedSegmenterTopic->text().toStdString();
    }

    void UserAssistedSegmenterConfigDialog::verifyConfig()
    {
        if (ui->lineEditUserAssistedSegmenterTopic->text().isEmpty())
        {
            QMessageBox::critical(this, "Invalid Configuration",
                                  "UserAssistedSegmenterTopicName name must not be empty.");
            return;
        }

        accept();
    }

}
