#include "PointCloudSegmentsTable.h"
#include <VisionX/gui-plugins/UserAssistedSegmenterGui/widgets/ui_PointCloudSegmentsTable.h>

#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/core/visualization/GlasbeyLUT.h>

#include <VisionX/components/pointcloud_core/PCLUtilities.h>


int toByte(float f)
{
    return static_cast<int>(f * 255.f);
}

static QColor qcolor(std::size_t id)
{
    const armarx::DrawColor c = armarx::GlasbeyLUT::at(id);
    return QColor(toByte(c.r), toByte(c.g), toByte(c.b), toByte(c.a));
}


namespace visionx
{

    PointCloudSegmentsTable::PointCloudSegmentsTable(QWidget* parent) :
        QTableWidget(parent),
        ui(new Ui::PointCloudSegmentsTable)
    {
        ui->setupUi(this);

        setColumnCount(3);
        setRowCount(1);

        QStringList header;
        header << "ID" << "#Points" << "C";
        setHorizontalHeaderLabels(header);
        setEditTriggers(QAbstractItemView::NoEditTriggers);
        setColumnWidth(0, 45);
        setColumnWidth(1, 75);
        setColumnWidth(2, 30);
    }

    PointCloudSegmentsTable::~PointCloudSegmentsTable()
    {
        delete ui;
    }


    void PointCloudSegmentsTable::setData(
        const pcl::PointCloud<pcl::PointXYZRGBL>& pointCloud, bool excludeZero)
    {
        setData(visionx::tools::getLabelMap(pointCloud, excludeZero));
    }


    void PointCloudSegmentsTable::setData(
        const std::map<PointCloudSegmentsTable::Label, pcl::PointIndices>& segmentMap)
    {
        // Fill table
        setRowCount(static_cast<int>(segmentMap.size()));

        // Allow sorting by numeric columns: https://stackoverflow.com/a/7852076

        int row = 0;
        for (const auto& [segmentID, indices] : segmentMap)
        {
            std::size_t size = indices.indices.size();

            QTableWidgetItem* itemID = new QTableWidgetItem();
            itemID->setData(Qt::EditRole, segmentID);
            itemID->setTextAlignment(Qt::AlignCenter);
            setItem(row, 0, itemID);

            QTableWidgetItem* itemSize = new QTableWidgetItem();
            itemSize->setData(Qt::EditRole, static_cast<int>(size));
            itemSize->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
            setItem(row, 1, itemSize);

            QTableWidgetItem* itemColor = new QTableWidgetItem(QString(""));
            itemColor->setBackgroundColor(qcolor(segmentID));
            setItem(row, 2, itemColor);

            row++;
        }
        sortItems(1);
    }

}
