/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SemanticGraphStorage
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <VisionX/interface/libraries/SemanticObjectRelations/GraphStorage.h>

#include <map>
#include <mutex>

namespace armarx
{
    /**
     * @class SemanticGraphStoragePropertyDefinitions
     * @brief
     */
    class SemanticGraphStoragePropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        SemanticGraphStoragePropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-SemanticGraphStorage SemanticGraphStorage
     * @ingroup VisionX-Components
     * A description of the component SemanticGraphStorage.
     *
     * @class SemanticGraphStorage
     * @ingroup Component-SemanticGraphStorage
     * @brief Brief description of class SemanticGraphStorage.
     *
     * Detailed description of class SemanticGraphStorage.
     */
    class SemanticGraphStorage
        : virtual public armarx::Component
        , virtual public armarx::semantic::GraphStorageTopicAndInterface
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;

        // GraphStorageInterface interface
        semantic::data::GraphMap getAll(const Ice::Current&) override;
        void remove(const std::string& id, const Ice::Current&) override;
        void removeAll(const Ice::Current&) override;

        // GraphStorageTopic interface
        void reportGraph(std::string const& id, semantic::data::Graph const& graph, const Ice::Current&) override;

    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        virtual void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        virtual void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        virtual void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        virtual void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        std::mutex graphsMutex;
        semantic::data::GraphMap graphs;
    };
}
