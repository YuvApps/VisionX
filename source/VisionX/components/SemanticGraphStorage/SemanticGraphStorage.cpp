/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SemanticGraphStorage
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SemanticGraphStorage.h"


namespace armarx
{
    SemanticGraphStoragePropertyDefinitions::SemanticGraphStoragePropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("SemanticGraphTopicName", "SemanticGraphTopic", "Name of the topic on which attributed graphs are reported");
    }


    std::string SemanticGraphStorage::getDefaultName() const
    {
        return "SemanticGraphStorage";
    }


    void SemanticGraphStorage::onInitComponent()
    {
        usingTopicFromProperty("SemanticGraphTopicName");
    }


    void SemanticGraphStorage::onConnectComponent()
    {
    }


    void SemanticGraphStorage::onDisconnectComponent()
    {

    }


    void SemanticGraphStorage::onExitComponent()
    {

    }

    semantic::data::GraphMap armarx::SemanticGraphStorage::getAll(const Ice::Current&)
    {
        std::unique_lock<std::mutex> lock(graphsMutex);

        return graphs;
    }

    void SemanticGraphStorage::remove(const std::string& id, const Ice::Current&)
    {
        std::unique_lock<std::mutex> lock(graphsMutex);

        graphs.erase(id);
    }

    void SemanticGraphStorage::removeAll(const Ice::Current&)
    {
        std::unique_lock<std::mutex> lock(graphsMutex);

        graphs.clear();
    }

    void SemanticGraphStorage::reportGraph(std::string const& id, semantic::data::Graph const& graph, const Ice::Current&)
    {
        ARMARX_INFO << "Got graph with ID: " << id;

        std::unique_lock<std::mutex> lock(graphsMutex);

        graphs[id] = graph;
    }


    armarx::PropertyDefinitionsPtr SemanticGraphStorage::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new SemanticGraphStoragePropertyDefinitions(
                getConfigIdentifier()));
    }
}


