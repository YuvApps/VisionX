/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RCImageProvider
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <VisionX/interface/components/Calibration.h>
#include <VisionX/core/CapturingImageProvider.h>
#include <RobotAPI/interface/components/RobotHealthInterface.h>
#include <ArmarXCore/core/Component.h>
#include <VisionX/libraries/RoboceptionUtility/RoboceptionUser.h>

#include <Image/ImageProcessor.h>
#include <Calibration/Undistortion.h>


#include <rc_genicam_api/device.h>
#include <rc_genicam_api/stream.h>
#include <memory>

namespace rcg
{

    using DevicePtr = std::shared_ptr<rcg::Device>;
    using StreamPtr = std::shared_ptr<rcg::Stream> ;
}

namespace armarx
{


    /**
     * @class RCImageProviderPropertyDefinitions
     * @brief
     */
    class RCImageProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        RCImageProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {

            defineOptionalProperty<std::string>("DeviceId", "00_14_2d_2c_6e_ed", "");
            defineOptionalProperty<float>("ScaleFactor", 1.0, "Image down scale factor");
            defineOptionalProperty<float>("FrameRate", 25.0f, "Frames per second")
            .setMatchRegex("\\d+(.\\d*)?")
            .setMin(0.0f)
            .setMax(25.0f);
            defineOptionalProperty<bool>("AutoExposure", false, "Enables auto exposure");
            defineOptionalProperty<float>("ExposureTimeMs", 12.0f, "Exposure time in ms if auto exposure is disabled");
            defineOptionalProperty<float>("GainDb", 0.0f, "Gain in db");

            defineOptionalProperty<bool>("EnableColor", true, "Enables colored images");

            defineOptionalProperty<bool>("EnableAutoWhiteBalance", false, "Enable auto white balance");
            defineOptionalProperty<float>("WhiteBalanceRatioRed", 1.2, "Red to green balance ratio.").setMin(0.125).setMax(8.0);
            defineOptionalProperty<float>("WhiteBalanceRatioBlue", 2.4, "Blue to green balance ratio.").setMin(0.125).setMax(8.0);

            defineOptionalProperty<std::string>("ReferenceFrameName", "Roboception_LeftCamera", "Optional reference frame name.");

            defineOptionalProperty<std::string>("RobotHealthTopicName", "RobotHealthTopic", "Name of the RobotHealth topic");
        }
    };

    /**
     * @defgroup Component-RCImageProvider RCImageProvider
     * @ingroup VisionX-Components
     * A description of the component RCImageProvider.
     *
     * @class RCImageProvider
     * @ingroup Component-RCImageProvider
     * @brief Brief description of class RCImageProvider.
     *
     * Detailed description of class RCImageProvider.
     */
    class RCImageProvider :
        virtual public visionx::CapturingImageProvider,
        virtual public visionx::StereoCalibrationCaptureProviderInterface,
        public visionx::RoboceptionUser
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "RCImageProvider";
        }

    protected:
        void onInitCapturingImageProvider() override;

        void onStartCapturingImageProvider() override;

        void onExitCapturingImageProvider() override;

        void onStartCapture(float frameRate) override;

        void onStopCapture() override;

        bool capture(void** ppImageBuffers) override;

        visionx::StereoCalibration getStereoCalibration(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return stereoCalibration;
        }

        bool getImagesAreUndistorted(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }

        std::string getReferenceFrame(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return getProperty<std::string>("ReferenceFrameName").getValue();
        }


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        void updateCameraSettings();

    private:
        RobotHealthInterfacePrx robotHealthTopic;

        float scaleFactor;

    };
}

