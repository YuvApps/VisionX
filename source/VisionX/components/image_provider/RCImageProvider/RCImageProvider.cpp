/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RCImageProvider
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RCImageProvider.h"


using namespace armarx;


#include <rc_genicam_api/system.h>
#include <rc_genicam_api/interface.h>
#include <rc_genicam_api/buffer.h>
#include <rc_genicam_api/image.h>
#include <rc_genicam_api/config.h>

#include <rc_genicam_api/pixel_formats.h>

#include <VisionX/tools/TypeMapping.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>


#include <Calibration/Calibration.h>
#include <Calibration/StereoCalibration.h>
#include <stdlib.h>
using namespace armarx;

namespace
{

    const char* const GC_COMPONENT_ENABLE = "ComponentEnable";
    const char* const GC_COMPONENT_SELECTOR = "ComponentSelector";
    const char* const GC_EXPOSURE_TIME = "ExposureTime";
    const char* const GC_EXPOSURE_AUTO = "ExposureAuto";
    const char* const GC_EXPOSURE_TIME_AUTO_MAX = "ExposureTimeAutoMax";
    const char* const GC_GAIN = "Gain";

}


void RCImageProvider::updateCameraSettings()
{

    bool autoExposure = getProperty<bool>("AutoExposure");
    float exposureTimeMs = getProperty<float>("ExposureTimeMs");
    float gainDb = getProperty<float>("GainDb");

    bool enableColor = getProperty<bool>("EnableColor").getValue();

    std::shared_ptr<GenApi::CNodeMapRef> nodemap = dev->getRemoteNodeMap();
    rcg::setString(nodemap, GC_COMPONENT_SELECTOR, "IntensityCombined", true); // selectz for the following actions

    ARMARX_INFO << "updating camera settings...";

    if (enableColor)
    {
        if (rcg::setEnum(nodemap, "PixelFormat", "YCbCr411_8", false))
        {
            ARMARX_INFO << "Enabling color mode";
        }
        else
        {
            ARMARX_WARNING << "Falling back to monochrome mode";
            enableColor = false;
        }
    }

    if (enableColor)
    {
        if (getProperty<bool>("EnableAutoWhiteBalance").getValue())
        {
            if (rcg::setEnum(nodemap, "BalanceWhiteAuto", "Continuous"))
            {
                ARMARX_INFO << "enabling auto white balance";
            }
            else
            {
                ARMARX_WARNING << "unable to enable auto white balance";
            }
        }
        else
        {
            if (rcg::setEnum(nodemap, "BalanceWhiteAuto", "Off"))
            {
                ARMARX_INFO << "disabling auto white balance";

                rcg::setEnum(nodemap, "BalanceRatioSelector", "Red");
                if (!rcg::setFloat(nodemap, "BalanceRatio", getProperty<float>("WhiteBalanceRatioRed").getValue()))
                {
                    ARMARX_WARNING << "unable to set white balance ratio for red-green";
                }

                rcg::setEnum(nodemap, "BalanceRatioSelector", "Blue");
                if (!rcg::setFloat(nodemap, "BalanceRatio", getProperty<float>("WhiteBalanceRatioBlue").getValue()))
                {
                    ARMARX_WARNING << "unable to set white balance ratio for green-blue";
                }
            }
            else
            {
                ARMARX_WARNING << "unable to disable auto white balance";
            }

        }

        rcg::setEnum(nodemap, "BalanceRatioSelector", "Red");
        float wbRatioRed = rcg::getFloat(nodemap, "BalanceRatio");

        rcg::setEnum(nodemap, "BalanceRatioSelector", "Blue");
        float wbRatioBlue  = rcg::getFloat(nodemap, "BalanceRatio");

        ARMARX_INFO << "white balance ratio is red: " << wbRatioRed << " blue: " << wbRatioBlue;
    }
    else
    {
        if (rcg::setEnum(nodemap, "PixelFormat", "Mono8", false))
        {
            ARMARX_INFO << "Enabling monochrome mode";
        }
        else
        {
            ARMARX_ERROR << "Could not set monochrome mode";
        }
    }

    std::string oldAutoExposure = rcg::getEnum(nodemap, GC_EXPOSURE_AUTO, false);
    ARMARX_INFO << "Setting auto exposure from " << oldAutoExposure << " to " << autoExposure;
    if (!rcg::setEnum(nodemap, GC_EXPOSURE_AUTO, autoExposure ? "Continuous" : "Off", false))
    {
        ARMARX_WARNING << "Could not set auto exposure to: " << autoExposure;
    }
    float oldExposureTime = rcg::getFloat(nodemap, GC_EXPOSURE_TIME) / 1000.0f;
    ARMARX_INFO << "Setting exposure time from " << oldExposureTime << " to " << exposureTimeMs;
    if (!rcg::setFloat(nodemap, GC_EXPOSURE_TIME, exposureTimeMs * 1000.0f, false))
    {
        ARMARX_WARNING << "Could not set exposure time to: " << exposureTimeMs;
    }

    float oldGain = rcg::getFloat(nodemap, GC_GAIN);
    ARMARX_INFO << "Setting gain from " << oldGain << " to " << gainDb;
    if (!rcg::setFloat(nodemap, GC_GAIN, gainDb, false))
    {
        ARMARX_WARNING << "Could not set gain to: " << gainDb;
    }

    ARMARX_INFO << "updating camera settings...done!";
}

void armarx::RCImageProvider::onInitCapturingImageProvider()
{
    //    offeringTopic(getProperty<std::string>("RobotHealthTopicName").getValue());

    if (!initDevice(getProperty<std::string>("DeviceId")))
    {
        getArmarXManager()->asyncShutdown();
        return;
    }

    enableIntensity(false);
    enableIntensityCombined(true);
    enableDisparity(false);
    enableConfidence(false);
    enableError(false);

    rcg::setString(nodemap, GC_COMPONENT_SELECTOR, "IntensityCombined", true);


    updateCameraSettings();

    setupStreamAndCalibration(
        getProperty<float>("ScaleFactor"),
        "/tmp/armar6_rc_color_calibration.txt"
    );

    frameRate = getProperty<float>("FrameRate");
    rcg::setFloat(nodemap, "AcquisitionFrameRate", frameRate);
    ARMARX_INFO << VAROUT(frameRate);

    setNumberImages(numImages);
    setImageFormat(dimensions, visionx::eRgb, visionx::eBayerPatternRg);
    setImageSyncMode(visionx::eCaptureSynchronization);
}

void RCImageProvider::onStartCapturingImageProvider()
{
    //    CapturingImageProvider::onConnectImageProvider();
    // robotHealthTopic = getTopic<RobotHealthInterfacePrx>(getProperty<std::string>("RobotHealthTopicName").getValue());
}

void armarx::RCImageProvider::onExitCapturingImageProvider()
{
    cleanupRC();
}

void armarx::RCImageProvider::onStartCapture(float framesPerSecond)
{
    startRC();
}

void armarx::RCImageProvider::onStopCapture()
{
    stopRC();
}

bool RCImageProvider::capture(void** ppImageBuffers)
{
    visionx::ImageFormatInfo imageFormat = getImageFormat();

    IceUtil::Time timeoutTime = IceUtil::Time::now(IceUtil::Time::Monotonic) + IceUtil::Time::secondsDouble(2);
    while (IceUtil::Time::now(IceUtil::Time::Monotonic) < timeoutTime)
    {
        const rcg::Buffer* buffer = stream->grab((timeoutTime - IceUtil::Time::now(IceUtil::Time::Monotonic)).toMilliSeconds());
        if (!buffer)
        {
            ARMARX_WARNING << deactivateSpam(10) <<  "buffer is NULL - Unable to get image - restarting streaming";
            stream->stopStreaming();
            stream->close();
            ARMARX_WARNING << deactivateSpam(10) <<  "streaming stopped";
            stream->open();
            stream->startStreaming();
            ARMARX_WARNING << deactivateSpam(10) <<  "streaming started again";
            return false;
        }

        if (buffer->getIsIncomplete())
        {
            continue;    // try again
        }
        if (!buffer->getImagePresent(0))
        {
            ARMARX_WARNING << deactivateSpam(10) <<  "Buffer does not contain an image!";
            return false;
        }


        // ARMARX_LOG << "grabbing image " << imageFormat.dimension.width << "x" << imageFormat.dimension.height << " vs " <<  buffer->getWidth() << "x" << buffer->getHeight();

        const unsigned char* p = static_cast<const unsigned char*>(buffer->getBase(0));


        size_t px = buffer->getXPadding(0);
        uint64_t format = buffer->getPixelFormat(0);

        int width = imageFormat.dimension.width;
        int height = imageFormat.dimension.height;
        if (scaleFactor > 1.0)
        {
            width *= scaleFactor;
            height *= scaleFactor;
        }

        const int imageSize = imageFormat.dimension.width * imageFormat.dimension.height * imageFormat.bytesPerPixel;

        for (size_t n = 0; n < numImages; n++)
        {
            auto outPixels = cameraImages[n]->pixels;
            switch (format)
            {
                case Mono8:
                case Confidence8:
                case Error8:
                {
                    unsigned char const* row = p + (n * height) * (width + px);
                    unsigned char* outRow = outPixels;
                    for (int j = 0; j < height; j++)
                    {
                        for (int i = 0; i < width; i++)
                        {
                            const unsigned char c = row[i];
                            outRow[3 * i + 0] = c;
                            outRow[3 * i + 1] = c;
                            outRow[3 * i + 2] = c;
                        }

                        p += px;
                        outRow += width * 3;
                        row += width + px;
                    }
                }
                break;
                case YCbCr411_8:
                {
                    // Compression: 4 pixel are encoded in 6 bytes
                    size_t inStride = width * 6 / 4 + px;
                    unsigned char const* row = p + (n * height) * inStride;
                    unsigned char* outRow = outPixels;
                    uint8_t rgb[3];
                    for (int j = 0; j < height; j++)
                    {
                        for (int i = 0; i < width; i++)
                        {
                            rcg::convYCbCr411toRGB(rgb, row, i);
                            outRow[i * 3 + 0] = rgb[0];
                            outRow[i * 3 + 1] = rgb[1];
                            outRow[i * 3 + 2] = rgb[2];
                        }

                        row += inStride;
                        outRow += width * 3;
                    }
                }
                break;
            }
        }


        armarx::SharedMemoryScopedWriteLockPtr lock(sharedMemoryProvider->getScopedWriteLock());
        updateTimestamp(buffer->getTimestampNS() / 1000, false);
        if (scaleFactor > 1.0)
        {
            ImageProcessor::Resize(cameraImages[0], smallImages[0]);
            ImageProcessor::Resize(cameraImages[1], smallImages[1]);

            for (size_t i = 0; i < numImages; i++)
            {
                memcpy(ppImageBuffers[i], smallImages[i]->pixels, imageSize);
            }
        }
        else
        {

            for (size_t i = 0; i < numImages; i++)
            {
                memcpy(ppImageBuffers[i], cameraImages[i]->pixels, imageSize);
            }
        }

        //        RobotHealthHeartbeatArgs rhha;
        //        rhha.maximumCycleTimeWarningMS = 500; // Robotception is really slow.
        //        rhha.maximumCycleTimeErrorMS = 1000;
        //        robotHealthTopic->heartbeat(getName(), rhha);

        return true;
    }

    ARMARX_INFO << deactivateSpam() << "failed to capture image";
    return false;
}




armarx::PropertyDefinitionsPtr RCImageProvider::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new RCImageProviderPropertyDefinitions(
            getConfigIdentifier()));
}

