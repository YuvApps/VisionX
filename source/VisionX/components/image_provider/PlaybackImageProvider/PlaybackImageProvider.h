/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    visionx::components
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <string>

// IVT
#include <Calibration/StereoCalibration.h>

// VisionX
#include <VisionX/core/CapturingImageProvider.h>
#include <VisionX/libraries/playback.h>
#include <VisionX/interface/components/Calibration.h>


namespace visionx::components
{

    class PlaybackImageProvider :
        virtual public visionx::CapturingImageProvider,
        virtual public StereoCalibrationCaptureProviderInterface
    {

    public:

        static const std::string DEFAULT_NAME;

    protected:

        std::vector<visionx::playback::Playback> m_playbacks;
        double m_current_frame;
        double m_frame_advance;

        bool m_stereo_initialized;
        std::string m_stereo_calibration_file;
        std::string m_stereo_reference_frame;
        CStereoCalibration m_stereo_calibration_ivt;
        visionx::StereoCalibration m_stereo_calibration;

    public:

        PlaybackImageProvider();
        virtual ~PlaybackImageProvider() override;

        virtual void onInitCapturingImageProvider() override;
        virtual void onExitCapturingImageProvider() override;
        virtual void onStartCapture(float) override;
        virtual void onStopCapture() override;
        virtual bool capture(void** imageBuffer) override;

        virtual visionx::StereoCalibration getStereoCalibration(const Ice::Current& c = Ice::emptyCurrent) override;
        virtual bool getImagesAreUndistorted(const Ice::Current& c = Ice::emptyCurrent) override;
        virtual std::string getReferenceFrame(const Ice::Current& c = Ice::emptyCurrent) override;

        virtual std::string getDefaultName() const override;
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:

        void init_stereo_calibration_file();
        void init_stereo_calibration();
        void assert_stereo_initialized();

    };

}
