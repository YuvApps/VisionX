
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore IntelRealSenseProvider)
 
armarx_add_test(IntelRealSenseProviderTest IntelRealSenseProviderTest.cpp "${LIBS}")
