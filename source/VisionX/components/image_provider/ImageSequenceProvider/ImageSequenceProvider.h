/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/core/CapturingImageProvider.h>
#include <VisionX/interface/components/Calibration.h>

#include <string>

// IVT
#include <VideoCapture/BitmapSequenceCapture.h>
#include <Image/ByteImage.h>
#include <Calibration/StereoCalibration.h>

namespace visionx
{
    class ImageSequenceProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ImageSequenceProviderPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("PathLeft", "Filename and path of the left camera images. Enter the path of the first image of the sequence. Filenames need to follow the format: path/*%d.bmp (e.g. path/image_left0000.bmp).");
            defineOptionalProperty<std::string>("PathRight", "", "Filename and path of the right camera images. Enter the path of the first image of the sequence. Filenames need to follow the format: path/*%d.bmp (e.g. path/image_right0000.bmp).");
            defineOptionalProperty<float>("FrameRate", 30.0f, "Frames per second")
            .setMatchRegex("\\d+(.\\d*)?")
            .setMin(0.0f)
            .setMax(60.0f);
            defineOptionalProperty<ImageDimension>("ImageSize", ImageDimension(640,  480), "Target resolution of the images. Loaded images will be converted to this size.")
            .setCaseInsensitive(true)
            .map("320x240",     ImageDimension(320,  240))
            .map("640x480",     ImageDimension(640,  480))
            .map("648x482",     ImageDimension(648,  482))
            .map("800x600",     ImageDimension(800,  600))
            .map("768x576",     ImageDimension(768,  576))
            .map("1024x768",    ImageDimension(1024, 768))
            .map("1024x1024",   ImageDimension(1024, 1024))
            .map("1280x960",    ImageDimension(1280, 960))
            .map("1600x1200",   ImageDimension(1600, 1200))
            .map("none",        ImageDimension(0, 0));

            defineOptionalProperty<std::string>("CalibrationFile", "", "Camera calibration file, will be made available in the SLICE interface");
            defineOptionalProperty<bool>("ImagesAreUndistorted", false, "Sets whether images are provided undistorted.");
            defineOptionalProperty<bool>("LoadNextImageAutomatically", true, "If true, a new image is loaded everytime the 'capture()' function is executed. If false, the same image is provided until 'loadNextImage()' is called from somewhere");
            defineOptionalProperty<int>("RepeatImageCount", 1, "Repeats the images for the specified amount of time.");

            defineOptionalProperty<std::string>("ReferenceFrameName", "EyeLeftCamera", "Optional reference frame name.");
        }
    };

    /**
     * Image sequence provider loads images from a device and broadcasts
     * notifications after loading at a specified frame rate.
     *
     * It supports the following image transmission formats:
     *
     *  - RGB
     *  - Gray Scale
     *
     * \componentproperties
     * \prop VisionX.ImageSequenceProvider.PathLeft: Image directory of the left
     *       (base) camera.
     * \prop VisionX.ImageSequenceProvider.PathRight: Image directory of the
     *       right camera. Leave this empty if stereo is not required.
     */
    class ImageSequenceProvider :
        virtual public CapturingImageProvider,
        virtual public ImageFileSequenceProviderInterface
    {
    public:
        std::string getDefaultName() const override
        {
            return "ImageSequenceProvider";
        }


        /**
         * Returns the StereoCalibration as provided in configuration
         *
         * @return visionx::StereoCalibration
         */
        visionx::StereoCalibration getStereoCalibration(const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * Returns whether images are undistorted
         *
         * @return bool
         */
        bool getImagesAreUndistorted(const Ice::Current& c = Ice::emptyCurrent) override;


        std::string getReferenceFrame(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return getProperty<std::string>("ReferenceFrameName").getValue();
        }

        /**
         * Set the paths for the image files. If there are no stereo images, leave the right path empty
         *
         */
        void setImageFilePath(const std::string& imageFilePathLeft, const std::string& imageFilePathRight = "", const Ice::Current& c = Ice::emptyCurrent) override;

        void loadNextImage(const Ice::Current& c = Ice::emptyCurrent) override;



    protected:
        /**
         * @see visionx::CapturingImageProvider::onInitCapturingImageProvider()
         */
        void onInitCapturingImageProvider() override;

        /**
         * @see visionx::CapturingImageProvider::onExitCapturingImageProvider()
         */
        void onExitCapturingImageProvider() override;

        /**
         * @see visionx::CapturingImageProvider::onStartCapture(float frameRate)
         */
        void onStartCapture(float frameRate) override;

        /**
         * @see visionx::CapturingImageProvider::onStopCapture()
         */
        void onStopCapture() override;

        bool capture(void** ppImageBuffers) override;


        void OpenImageFiles();


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new ImageSequenceProviderPropertyDefinitions(getConfigIdentifier()));
        }

        CByteImage**             images;
        CByteImage**             resizedImages;
        CBitmapSequenceCapture*  bitmapSequenceCapture;
        std::string              filePathLeft;
        std::string              filePathRight;
        boost::mutex             captureMutex;
        bool                     loadNewImageAtNextCapture;

    private:
        /**
         * IVT Stereo Calibration object
         */
        CStereoCalibration ivtStereoCalibration;

        /**
         * VisionX StereoCalibration object
         */
        StereoCalibration stereoCalibration;

        int currentRepeatCount = 0;
        int repeatImageCount;
    };

}

