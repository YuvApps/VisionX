armarx_component_set_name(ImageSequenceProvider)
set(COMPONENT_LIBS VisionXInterfaces VisionXCore VisionXTools ArmarXCore)
set(SOURCES ImageSequenceProvider.cpp)
set(HEADERS ImageSequenceProvider.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

set(EXE_SOURCES main.cpp ImageSequenceProviderApp.h)
armarx_add_component_executable("${EXE_SOURCES}")
