/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ResultImageFuser
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/application/properties/PropertyDefinition.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <VisionX/core/CapturingImageProvider.h>
#include <VisionX/interface/core/ImageProviderInterface.h>
#include <VisionX/tools/ImageUtil.h>

#include <boost/algorithm/string.hpp>

#include <Eigen/Core>


namespace armarx
{

    std::vector<std::string> splitter(std::string propertyValue)
    {
        std::vector<std::string> result;
        boost::split(result, propertyValue, boost::is_any_of(","));
        return result;
    }

    Eigen::Vector3i extractColorValue(std::string propertyValue)
    {
        int number = (int) strtol(&propertyValue.c_str()[1], NULL, 16);

        int r = number >> 16;
        int g = number >> 8 & 0xFF;
        int b = number & 0xFF;

        return Eigen::Vector3i(r, g, b);
    }



    /**
     * @class ResultImageFuserPropertyDefinitions
     * @brief
     */
    class ResultImageFuserPropertyDefinitions:
        public visionx::CapturingImageProviderPropertyDefinitions
    {
    public:
        ResultImageFuserPropertyDefinitions(std::string prefix):
            visionx::CapturingImageProviderPropertyDefinitions(prefix)
        {
            defineOptionalProperty<int>("width", 640, "image width");
            defineOptionalProperty<int>("height", 480, "image height");
            defineOptionalProperty<int>("numImages", 1, "number of images");
            PropertyDefinition<Eigen::Vector3i>::PropertyFactoryFunction f = &extractColorValue;
            defineOptionalProperty<Eigen::Vector3i>("colorMask", extractColorValue("#FFFFFF"),
                                                    "image color that should be used as alpha channel").setFactory(f);
            PropertyDefinition<std::vector<std::string>>::PropertyFactoryFunction g = &splitter;
            defineOptionalProperty<std::vector<std::string>>("imageProviders", splitter("TestImageProvider"),
                    "comma separated list of image providers").setFactory(g);
        }
    };

    /**
     * @class ResultImageFuser
     *
     * @ingroup VisionX-Components
     * @brief A brief description
     *
     *
     * Detailed Description
     */
    class ResultImageFuser :
        virtual public visionx::CapturingImageProvider
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "ResultImageFuser";
        }


        //        void setResultImageProviders(std::string imageProviders, const Ice::Current& c = Ice::emptyCurrent);

    protected:

        /**
         * @see visionx::ImageProviderBase::onInitImageProvider()
         */
        void onInitCapturingImageProvider() override;

        /**
         * @see visionx::ImageProviderBase::onExitImageProvider()
         */
        void onExitCapturingImageProvider() override;

        /**
         * @see visionx::ImageProviderBase::onStartCapture(float frameRate)
         */
        void onStartCapture(float frameRate) override;

        /**
         * @see visionx::ImageProviderBase::onStopCapture()
         */
        void onStopCapture() override;

        /**
         * @see visionx::ImageProviderBase::capture()
         */
        bool capture(void** ppImages) override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:


        void setResultImageProviders(std::vector<std::string> imageProviders);

        void pollImageProviders();

        int bytesPerPixel;
        int width;
        int height;
        int numImages;
        boost::mutex imageMutex;

        Eigen::Vector3i colorMask;
        std::map<std::string, CByteImage**> imageSources;
        std::map<std::string, bool> imageAvailable;


        armarx::PeriodicTask<ResultImageFuser>::pointer_type pollImagesTask;

    };
}

