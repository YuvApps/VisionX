/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <VisionX/core/CapturingImageProvider.h>
#include <VisionX/interface/components/Calibration.h>
#include <opencv2/highgui/highgui.hpp>


namespace visionx
{
    class VideoFileImageProviderPropertyDefinitions : public armarx::ComponentPropertyDefinitions
    {
    public:
        VideoFileImageProviderPropertyDefinitions(std::string prefix) : ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("VideoFilePath", "Path to the video file that should be streamed");
            defineOptionalProperty<bool>("LoopVideo", true, "If true, the video file will be played in a loop");
        }
    };


    class VideoFileImageProvider :
    //        public MonocularCalibrationCapturingProviderInterface,
        public CapturingImageProvider
    {
    public:
        VideoFileImageProvider();

        // ManagedIceObject interface
    protected:
        std::string getDefaultName() const override;

        // CapturingImageProviderInterface interface
    public:
        // CapturingImageProvider interface
    protected:
        void onInitCapturingImageProvider() override;
        void onExitCapturingImageProvider() override;
        bool capture(void** ppImageBuffers) override;

        bool openVideo();
        cv::VideoCapture capturer;
        cv::Mat image;

        // CapturingImageProvider interface
    protected:
        void onStartCapture(float framesPerSecond) override;
        void onStopCapture() override;

        // PropertyUser interface
    public:
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // MonocularCalibrationCapturingProviderInterface interface
    public:
        MonocularCalibration getCalibration(const Ice::Current&);
    protected:
        MonocularCalibration calibration;
    };

} // namespace visionx


