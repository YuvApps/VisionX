/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/interface/components/Calibration.h>

// IEEE1394ImageProvider
#include <VisionX/components/image_provider/IEEE1394ImageProvider/IEEE1394ImageProvider.h>

// IVT
#include <Calibration/StereoCalibration.h>

namespace visionx
{
    class StereoCalibrationProviderPropertyDefinitions:
        public IEEE1394PropertyDefinitions
    {
    public:
        StereoCalibrationProviderPropertyDefinitions(std::string prefix):
            IEEE1394PropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("CalibrationFile", "Camera calibration file");

            defineOptionalProperty<bool>("LeftCameraAsIdentity", true, "Set left camera as identity")
            .setCaseInsensitive(false)
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);

            defineOptionalProperty<std::string>("ReferenceFrameName", "EyeLeftCameras", "Optional reference frame name.");
        }
    };

    /**
     * Stereo camera provider is based on IEEE1394 image provider with the
     * restriction that the number of specified cameras must be two. This
     * provider supplies the processor with a stereo calibration to perform
     * stereo vision.
     *
     * \componentproperties
     * \prop VisionX.StereoCameraProvider.CalibrationFile: The stereo
     *       calibration file of the stereo system.
     */
    class StereoCameraProvider:
        virtual public IEEE1394ImageProvider,
        virtual public StereoCalibrationCaptureProviderInterface
    {
    public:
        /**
         * Part of visionx::StereoCalibrationProvider interface
         *
         * Returns the VisionX StereoCalibration, especially vis ICE
         *
         * @return visionx::StereoCalibration
         */
        visionx::StereoCalibration getStereoCalibration(
            const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * Part of visionx::StereoCalibrationProvider interface
         *
         * Returns whether images are undistorted
         *
         * @return bool
         */
        bool getImagesAreUndistorted(
            const Ice::Current& c = Ice::emptyCurrent) override
        {
            return false;
        }


        std::string getReferenceFrame(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return getProperty<std::string>("ReferenceFrameName").getValue();
        }


        /**
         * @see armarx::Component::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "StereoCameraProvider";
        }

        /**
         * @see ImageProviderBase::onInitImageProvider()
         */
        void onInitCapturingImageProvider() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new StereoCalibrationProviderPropertyDefinitions(
                           getConfigIdentifier()));
        }

    private:
        /**
         * IVT Stereo Calibration object
         */
        CStereoCalibration ivtStereoCalibration;

        /**
         * VisionX StereoCalibration object
         */
        visionx::StereoCalibration stereoCalibration;
    };
}

