/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::StreamDecoderImageProvider
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StreamDecoderImageProvider.h"

#include <Image/ByteImage.h>
using namespace armarx;


void StreamDecoderImageProvider::onInitImageProvider()
{
    pCombinedDecodedImage = NULL;
    usingProxy(getProperty<std::string>("UsedStreamProvider").getValue());
    usingTopic(getProperty<std::string>("imageStreamTopicName").getValue());
    numberImages = 0;

    avcodec_register_all();
    av_init_packet(&m_packet);
    m_decoder = avcodec_find_decoder(AV_CODEC_ID_H264);
    if (!m_decoder)
    {
        ARMARX_ERROR << ("Can't find H264 decoder!");
    }
    m_decoderContext = avcodec_alloc_context3(m_decoder);

    if (m_decoder->capabilities & CODEC_CAP_TRUNCATED)
    {
        m_decoderContext->flags |= CODEC_FLAG_TRUNCATED;
    }


    //we can receive truncated frames
    m_decoderContext->flags2 |= CODEC_FLAG2_CHUNKS;
    m_decoderContext->thread_count = 4;//TODO: random value. May be changing can make decoding faster

    AVDictionary* dictionary = nullptr;
    if (avcodec_open2(m_decoderContext, m_decoder, &dictionary) < 0)
    {
        ARMARX_ERROR << "Could not open decoder";
    }
    ARMARX_INFO << "H264 Decoder successfully opened";

    //m_picture = avcodec_alloc_frame();
    m_picture = av_frame_alloc();
}


void StreamDecoderImageProvider::onConnectComponent()
{
    ScopedLock lock(decodedImageMutex);
    ppDecodedImages = new CByteImage*[numberImages];

    streamProvider = getProxy<Stream::StreamProviderPrx>(getProperty<std::string>("UsedStreamProvider").getValue());
    //    codec = streamProvider->getCodecType();
    numberImages = streamProvider->getNumberOfImages();
    setNumberImages(numberImages);
    int imgWidth, imgHeight, imgType;
    streamProvider->getImageInformation(imgWidth, imgHeight, imgType);
    ARMARX_INFO << "number of Images: " << numberImages;
    for (int i = 0; i < numberImages ; i++)
    {
        ppDecodedImages[i] = new CByteImage(imgWidth, imgHeight, CByteImage::eRGB24);
    }
    pCombinedDecodedImage = new CByteImage(imgWidth, imgHeight * numberImages, CByteImage::eRGB24);
    setImageFormat(visionx::ImageDimension(imgWidth, imgHeight), visionx::eRgb);
    ImageProvider::onConnectComponent();
}


void StreamDecoderImageProvider::onDisconnectImageProvider()
{
    ScopedLock lock(decodedImageMutex);
    delete pCombinedDecodedImage;
    pCombinedDecodedImage = nullptr;
    if (ppDecodedImages)
    {
        for (int i = 0; i < numberImages ; i++)
        {
            delete ppDecodedImages[i];
        }

        delete[] ppDecodedImages;
        ppDecodedImages = nullptr;
    }

}


void StreamDecoderImageProvider::onExitImageProvider()
{

}

armarx::PropertyDefinitionsPtr StreamDecoderImageProvider::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new StreamDecoderImageProviderPropertyDefinitions(
            getConfigIdentifier()));
}

void StreamDecoderImageProvider::reportNewStreamData(const Stream::DataChunk& chunk, Ice::Long imageTimestamp, const Ice::Current&)
{
    ScopedLock lock(streamDecodeMutex);
    m_packet.size = chunk.size();
    m_packet.data = const_cast<Ice::Byte*>(chunk.data());
    //            ARMARX_INFO << "H264Decoder: received encoded frame with framesize " << enc_frame.size();

    while (m_packet.size > 0)
    {
        int got_picture;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
        int len = avcodec_decode_video2(m_decoderContext, m_picture, &got_picture, &m_packet);
#pragma GCC diagnostic pop
        if (len < 0)
        {
            std::string err("Decoding error");
            ARMARX_ERROR << err;
            return;
        }
        if (got_picture)
        {
            ARMARX_DEBUG << deactivateSpam(1) << "H264Decoder: frame decoded!";
            //                    std::vector<unsigned char> result;
            //                    this->storePicture(result);

            if (m_picture->format == AV_PIX_FMT_YUV420P)
            {

                //                    QImage frame_img = QImage(width, height, QImage::Format_RGB888);
                m_swsCtx = sws_getCachedContext(m_swsCtx,  m_picture->width,
                                                m_picture->height, AV_PIX_FMT_YUV420P,
                                                m_picture->width,  m_picture->height,
                                                AV_PIX_FMT_RGB24, SWS_GAUSS,
                                                NULL, NULL, NULL);
                ScopedLock lock(decodedImageMutex);

                uint8_t* dstSlice[] = { pCombinedDecodedImage->pixels };
                int dstStride =  m_picture->width * 3;
                if (sws_scale(m_swsCtx, m_picture->data, m_picture->linesize,
                              0, m_picture->height, dstSlice, &dstStride) != m_picture->height)
                {
                    ARMARX_INFO << "SCALING FAILED!";
                    return;
                }
                for (int i = 0; i < numberImages; ++i)
                {
                    size_t imageByteSize = ppDecodedImages[i]->width * ppDecodedImages[i]->height * ppDecodedImages[i]->bytesPerPixel;
                    //            if(ppInputImages[i]->type != CByteImage::eRGB24)
                    //            {
                    //                ::ImageProcessor::ConvertImage(imageProviderInfo, eRgb,)
                    //            }
                    memcpy(ppDecodedImages[i]->pixels, pCombinedDecodedImage->pixels + i * imageByteSize, imageByteSize);
                }
                updateTimestamp(imageTimestamp);
                provideImages(ppDecodedImages);
                //                ARMARX_INFO << "New decoded image!";
            }
            else
            {
                ARMARX_INFO << "Other format: " << m_picture->format;
            }

            //                        emit newDecodedFrame(frame_img);
            //                    }
            //                    else if (m_picture->format == PIX_FMT_RGB32)
            //                    {
            //                        QImage img = QImage(result.data(), m_picture->width, m_picture->height, QImage::Format_RGB32);
            //                        ARMARX_INFO << "New decoded image!";
            //                        emit newDecodedFrame(img);
            //                    }
            //                    else if (m_picture->format == AV_PIX_FMT_RGB24)
            //                    {
            //                        QImage img = QImage(result.data(), m_picture->width, m_picture->height, QImage::Format_RGB888);
            //                        ARMARX_INFO << "New decoded image!";
            //                        emit newDecodedFrame(img);
            //                    }
            //                    else
            //                    {
            //                        std::string err = std::string( "Unsupported pixel format! Can't create QImage!");
            //                        ARMARX_INFO << err;
            //                        emit criticalError( err );
            //                        return false;
            //                    }
        }
        m_packet.size -= len;
        m_packet.data += len;
    }


}

