/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// VisionXCore
#include <VisionX/core/CapturingImageProvider.h>

// IVT
#include <Image/ByteImage.h>
#include <Image/ImageProcessor.h>
#include <Interfaces/VideoCaptureInterface.h>
//#include <VideoCapture/Linux1394Capture2.h>
#include <VideoCapture/Linux1394CaptureThreaded2.h>

// STL
#include <string>
#include <map>

namespace visionx
{

    struct ColorFormat
    {
        CVideoCaptureInterface::ColorMode colorMode;
        visionx::ImageType imageType;

        ColorFormat()
        {
        }

        ColorFormat(CVideoCaptureInterface::ColorMode colorMode,
                    visionx::ImageType imageType)
            : colorMode(colorMode), imageType(imageType)
        {
        }

        bool operator==(const ColorFormat& colorFormat) const
        {
            if (colorFormat.colorMode != colorMode)
            {
                return false;
            }

            if (colorFormat.imageType != imageType)
            {
                return false;
            }

            return true;
        }
    };

    class IEEE1394PropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        IEEE1394PropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {

            defineOptionalProperty<ImageDimension>("VideoMode", ImageDimension(640,  480), "Image resolution")
            .setCaseInsensitive(true)
            .map("320x240",     ImageDimension(320,  240))
            .map("640x480",     ImageDimension(640,  480))
            .map("800x600",     ImageDimension(800,  600))
            .map("768x576",     ImageDimension(768,  576))
            .map("1024x768",    ImageDimension(1024, 768))
            .map("1280x960",    ImageDimension(1280, 960))
            .map("1600x1200",   ImageDimension(1600, 1200))
            .map("none",        ImageDimension(0, 0));

            defineOptionalProperty<ColorFormat>("ColorMode", ColorFormat(CVideoCaptureInterface::eRGB24, eRgb), "Image color mode")
            .setCaseInsensitive(true)
            .map("gray-scale",          ColorFormat(CVideoCaptureInterface::eGrayScale, eGrayScale))
            .map("rgb",                 ColorFormat(CVideoCaptureInterface::eRGB24, eRgb))
            .map("bayer-pattern",       ColorFormat(CVideoCaptureInterface::eBayerPatternToRGB24, eBayerPattern))
            .map("bayer-pattern-to-rgb", ColorFormat(CVideoCaptureInterface::eBayerPatternToRGB24, eRgb))
            .map("yuv411-to-rgb",       ColorFormat(CVideoCaptureInterface::eYUV411ToRGB24, eRgb));

            defineOptionalProperty<BayerPatternType>("BayerPatternType", eBayerPatternRg, "Raw image color pattern")
            .setCaseInsensitive(true)
            .map("bayer-pattern-bg", eBayerPatternBg)
            .map("bayer-pattern-gb", eBayerPatternGb)
            .map("bayer-pattern-gr", eBayerPatternGr)
            .map("bayer-pattern-rg", eBayerPatternRg);

            defineOptionalProperty<bool>("Format7Mode", false, "Use Format7 mode")
            .setCaseInsensitive(true)
            .map("true",    true)
            .map("yes",     true)
            .map("1",       true)
            .map("false",   false)
            .map("no",      false)
            .map("0",       false);

            defineOptionalProperty<float>("FrameRate", 7.5f, "Frames per second")
            .setMatchRegex("\\d+(.\\d*)?")
            .setMin(0.0f)
            .setMax(60.0f);

            // Camera UIDs
            defineRequiredProperty<std::string>("CameraUIDs", "Camera UIDs seperated by comma, space or tab")
            .setCaseInsensitive(true)
            .setMatchRegex("\\s*[a-zA-Z0-9]{16}\\s*((,|\\s)\\s*[a-zA-Z0-9]{16})*");

            defineOptionalProperty<std::string>("ReferenceFrameName", "EyeLeftCameras", "Optional reference frame name.");
        }
    };


    /**
     * IEEE1394 image provider captures images from one or more cameras and
     * supports the following image transmission formats:
     *
     *  - RGB
     *  - Gray Scale
     *  - Bayer Pattern (Bg, Gb, Gr, Rg)
     *
     * \componentproperties
     * \prop VisionX.IEEE1394ImageProvider.CameraUIDs: Comma or space separated
     *       list of camera UIDs. If not set all cameras will be opened.
     * \prop VisionX.IEEE1394ImageProvider.VideoMode: Defines the camera
     *       resolution.
     *          - 320x240
     *          - 640x480 (default)
     *          - 800x600
     *          - 768x576
     *          - 1024x768
     *          - 1280x960
     *          - 1600x1200
     *          - none
     * \prop VisionX.IEEE1394ImageProvider.ColorMode: Specifies how image format
     *       of camera is interpreted and converted. Cameras may support only
     *       one color mode. Possible values:
     *          - gray-scale
     *          - rgb
     *          - bayer-pattern
     *          - bayer-pattern-to-rgb (default)
     *          - yuv411-to-rgb
     * \prop VisionX.IEEE1394ImageProvider.BayerPatternType: Specifies the bayer
     *       pattern type. Required only if ColorMode uses a bayer pattern
     *       format.
     *       Possible values:
     *          - bayer-pattern-bg
     *          - bayer-pattern-gb
     *          - bayer-pattern-gr
     *          - bayer-pattern.rg (default)
     * \prop VisionX.IEEE1394ImageProvider.FrameRate: Capture frame rate as
     *       float (default: 30fps)
     * \prop VisionX.IEEE1394ImageProvider.Format7Mode: Enable or disable
     *       IEEE1394 format 7 mode (possible values: true, false (default)).
     */
    class IEEE1394ImageProvider :
        virtual public visionx::CapturingImageProvider
    {
    public:
        /**
         * @see visionx::ImageProviderBase::onInitImageProvider()
         */
        void onInitCapturingImageProvider() override;

        /**
         * @see visionx::ImageProviderBase::onExitImageProvider()
         */
        void onExitCapturingImageProvider() override;

        /**
         * @see visionx::ImageProviderBase::onStartCapture()
         */
        void onStartCapture(float frameRate) override;

        /**
         * @see visionx::ImageProviderBase::onStopCapture()
         */
        void onStopCapture() override;

        /**
         * @see visionx::ImageProviderBase::capture()
         */
        bool capture(void** ppImageBuffers) override;

        /**
         * @see armarx::Component::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "IEEE1394ImageProvider";
        }

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new IEEE1394PropertyDefinitions(
                           getConfigIdentifier()));
        }


    protected:
        /**
         * Camera UID list
         */
        std::vector<std::string> uids;

        /**
         * Camera color mode and resulting image time
         */
        ColorFormat colorFormat;

        /**
         * Video dimension data
         */
        visionx::ImageDimension videoDimension;

        /**
         * Specific bayer pattern type, if ColorMode is BayerPattern
         */
        visionx::BayerPatternType bayerPatternType;

        /**
         * Captured images
         */
        CByteImage** ppImages;

        /**
         * Indicate whether using format 7 modes or not
         */
        bool isFormat7Mode;

        /**
         * IEEE1394 Capture
         */
        //CLinux1394Capture2* ieee1394Capturer;
        CLinux1394CaptureThreaded2* ieee1394Capturer;
    };
}

