/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "IEEE1394ImageProvider.h"
#include "Helpers/helpers.h"

// boost
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

// VisionXCore
#include <VisionX/core/exceptions/user/StartingCaptureFailedException.h>
#include <VisionX/core/exceptions/user/FrameRateNotSupportedException.h>

// VisionXTools
#include <VisionX/tools/TypeMapping.h>
#include <VisionX/tools/ImageUtil.h>

// ArmarXCore
#include <ArmarXCore/core/application/properties/Property.h>

using namespace armarx;

namespace visionx
{
    void IEEE1394ImageProvider::onInitCapturingImageProvider()
    {
        using namespace visionx::tools;

        videoDimension      = getProperty<ImageDimension>("VideoMode").getValue();
        colorFormat         = getProperty<ColorFormat>("ColorMode").getValue();
        bayerPatternType    = getProperty<BayerPatternType>("BayerPatternType").getValue();
        isFormat7Mode       = getProperty<bool>("Format7Mode").getValue();
        frameRate           = getProperty<float>("FrameRate").getValue();
        std::string uidStr  = getProperty<std::string>("CameraUIDs").getValue();

        boost::split(uids,
                     uidStr,
                     boost::is_any_of("\t ,"),
                     boost::token_compress_on);

        setNumberImages(uids.size());
        setImageFormat(videoDimension, colorFormat.imageType, bayerPatternType);
        setImageSyncMode(eCaptureSynchronization);

        //        if (isFormat7Mode)
        //        {
        //            ieee1394Capturer =
        //                    new CLinux1394Capture2(
        //                            visionx::tools::convert(videoDimension),
        //                            frameRate,
        //                            0, 0, -1, -1,
        //                            colorFormat.colorMode,
        //                            visionx::tools::convert(bayerPatternType));
        //            ieee1394Capturer->SetCameraUids(uids);
        //        }
        //        else
        //        {
        //            ieee1394Capturer =
        //                    new CLinux1394Capture2(
        //                            visionx::tools::convert(videoDimension),
        //                            colorFormat.colorMode,
        //                            visionx::tools::convert(bayerPatternType),
        //                            visionx::tools::convert(frameRate));
        //            ieee1394Capturer->SetCameraUids(uids);
        //        }

        {
            std::string leftUID, rightUID;
            int numCameras = 0;

            if (uids.size() > 0)
            {
                leftUID = uids.at(0);
                numCameras = 1;
            }

            if (uids.size() > 1)
            {
                rightUID = uids.at(1);
                numCameras = 2;
            }

            ieee1394Capturer = new CLinux1394CaptureThreaded2(visionx::tools::convert(videoDimension), colorFormat.colorMode, visionx::tools::convert(bayerPatternType),
                    visionx::tools::convert(frameRate), numCameras, leftUID.c_str(), rightUID.c_str());
            //ieee1394Capturer = new CLinux1394CaptureThreaded2(-1, CVideoCaptureInterface::e640x480, CVideoCaptureInterface::eRGB24);
            //ieee1394Capturer = new CLinux1394CaptureThreaded2(CVideoCaptureInterface::e640x480, CVideoCaptureInterface::eBayerPatternToRGB24, ImageProcessor::eBayerRG, CVideoCaptureInterface::e15fps, 2, leftUID.c_str(), rightUID.c_str());
            //ieee1394Capturer = new CLinux1394CaptureThreaded2(CVideoCaptureInterface::e640x480, CVideoCaptureInterface::eRGB24, ImageProcessor::eBayerRG, CVideoCaptureInterface::e7_5fps, 2, leftUID.c_str(), rightUID.c_str());

        }

    }


    void IEEE1394ImageProvider::onExitCapturingImageProvider()
    {
        if (ppImages != NULL)
        {
            for (int i = 0; i < getNumberImages(); i++)
            {
                delete ppImages[i];
            }

            delete [] ppImages;

            ppImages = NULL;
        }
    }


    void IEEE1394ImageProvider::onStartCapture(float frameRate)
    {
        ARMARX_LOG << armarx::eINFO << "starting to capture" << flush;

        ppImages = new CByteImage*[2];

        for (int i = 0; i < getNumberImages(); i++)
        {
            ppImages[i] = visionx::tools::createByteImage(getImageFormat(),
                          getImageFormat().type);
        }



        if (!ieee1394Capturer->OpenCamera())
        {
            throw visionx::exceptions::user
            ::StartingCaptureFailedException("Opening cameras failed!");
        }


        ieee1394Capturer->SetGain((unsigned int) - 1);
        ieee1394Capturer->SetExposure((unsigned int) - 1);
        ieee1394Capturer->SetShutter((unsigned int) - 1);
        ieee1394Capturer->SetWhiteBalance(50, 50); // better white balance with 49,36. This 50,50 was the default. All objects trained with 50,50.

    }


    void IEEE1394ImageProvider::onStopCapture()
    {

        ieee1394Capturer->CloseCamera();

        if (ppImages != NULL)
        {
            for (int i = 0; i < getNumberImages(); i++)
            {
                delete ppImages[i];
            }

            delete [] ppImages;

            ppImages = NULL;
        }
    }


    bool IEEE1394ImageProvider::capture(void** ppImageBuffers)
    {
        bool succeeded = false;

        switch (getImageFormat().type)
        {
            case visionx::eBayerPattern:

            //succeeded = ieee1394Capturer->CaptureBayerPatternImage(ppImages);
            //break;

            case visionx::eRgb:
            case visionx::eGrayScale:
                succeeded = ieee1394Capturer->CaptureImage(ppImages);
                break;

            /*
             * Handle image types which are not supported by
             * IEEE1394ImageProvider
             */
            default:
                ARMARX_LOG  << armarx::eERROR
                            << "Image type not supported!"
                            << flush;
                return false;
        }

        if (!succeeded)
        {
            ARMARX_LOG << armarx::eERROR << "Capturing failed!" << flush;
            return false;
        }

        {
            ImageFormatInfo imageFormat = getImageFormat();
            armarx::SharedMemoryScopedWriteLockPtr lock = getScopedWriteLock();


            int imageSize = imageFormat.dimension.width * imageFormat.dimension.height * imageFormat.bytesPerPixel;
            memcpy(ppImageBuffers[0], ppImages[0]->pixels, imageSize);
            memcpy(ppImageBuffers[1], ppImages[1]->pixels, imageSize);
        }

        return true;
    }
}

