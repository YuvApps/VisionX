/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// defines which camera is used
//#define OLP_USE_XB3
//#define OLP_USE_KAHEAD
//#define OLP_USE_DBVISION
#define OLP_USE_ARMAR3
//#define OLP_USE_ARMAR3_ARMAR3_4

//// camera calibration file
//#if defined OLP_USE_XB3
//  #define OLP_CAMERA_CALIBRATION_FILE "C:/Users/Lovov/My Documents/Visual Studio 2008/Projects/SiftFeaturePlaneFinder2/SiftFeaturePlaneFinder/cameras-David7.txt"
//#elif defined OLP_USE_KAHEAD
//  //#define OLP_CAMERA_CALIBRATION_FILE "C:/img/Experiments with Armar/camera_armar3b_640x480_wide-exp8-10.txt"
//  //#define OLP_CAMERA_CALIBRATION_FILE "C:/img/Experiments with Armar/camera_armar3b_640x480_wide-exp1-7.txt"
//  #define OLP_CAMERA_CALIBRATION_FILE "C:/Users/Lovov/My Documents/Visual Studio 2008/Projects/SiftFeaturePlaneFinder2/SiftFeaturePlaneFinder/cameras-Head3.txt"
//  //#define OLP_CAMERA_CALIBRATION_FILE "C:/Users/Lovov/My Documents/Visual Studio 2008/Projects/SiftFeaturePlaneFinder2/SiftFeaturePlaneFinder/cameras-Kyoto1.txt"
//#elif defined OLP_USE_DBVISION
//  #define OLP_CAMERA_CALIBRATION_FILE "params/StereoParametersIVT.cf"
//#elif defined OLP_USE_ARMAR3_ARMAR3_4
//  //#define OLP_CAMERA_CALIBRATION_FILE "/localhome/armar-user/armar-user_test3_mca2.4/david_mca2.4/armar3/robot_interface/files/camera_armar3a_640x480_wide.txt"
//  #define OLP_CAMERA_CALIBRATION_FILE "/localhome/armar-user/armar-user_test3_mca2.4/armar3/robot_interface/files/camera_armar3a_640x480_wide.txt"
//#elif defined OLP_USE_ARMAR3
//    #define OLP_CAMERA_CALIBRATION_FILE "/common/homes/staff/schieben/home/mca2.4/projects/armar3/robot_interface/files/camera_armar3a_640x480_wide.txt"
//    //#define OLP_CAMERA_CALIBRATION_FILE "/home/staff/schieben/mca2.4-3b/projects/armar3b/robot_interface/files/camera_armar3b_640x480_wide.txt"
//#endif

// defines image sizes
#if defined OLP_USE_XB3
#define OLP_IMG_WIDTH_CAM 1280
#define OLP_IMG_HEIGHT_CAM 960
#elif defined OLP_USE_KAHEAD
#define OLP_IMG_WIDTH_CAM 640
#define OLP_IMG_HEIGHT_CAM 480
#elif defined OLP_USE_DBVISION
#define OLP_IMG_WIDTH_CAM 640   // <- enter the resolution of the camera images here
#define OLP_IMG_HEIGHT_CAM 480
#elif defined OLP_USE_ARMAR3
#define OLP_IMG_WIDTH_CAM 640
#define OLP_IMG_HEIGHT_CAM 480
#else
#define OLP_IMG_WIDTH_CAM 640
#define OLP_IMG_HEIGHT_CAM 480
#endif

#define OLP_IMG_WIDTH 640
#define OLP_IMG_HEIGHT 480


// define if a human executes the pushing
//#define OLP_HUMAN_PUSHES
// defines which robot is used for pushing
//#define OLP_USE_KUKA_ARMS
//#define OLP_USE_ATR_ROBOT

// operating system
//#define WIN32
#define LINUX

// influences the computational effort, e.g. the number of iterations of RANSAC
#if defined _DEBUG
#define OLP_EFFORT_MODIFICATOR 0.5f
#else
#define OLP_EFFORT_MODIFICATOR 1.0f
#endif


// modifies the tolerance for points lying on planes and cylinders, can be reduced
// for good or increased for bad stereo calibrations
#if defined OLP_USE_XB3
#define OLP_TOLERANCE_MODIFICATOR 0.9f
#elif defined OLP_USE_KAHEAD
#define OLP_TOLERANCE_MODIFICATOR 1.5f
#elif defined OLP_USE_DBVISION
#define OLP_TOLERANCE_MODIFICATOR 1.5f
#elif defined OLP_USE_ARMAR3
#define OLP_TOLERANCE_MODIFICATOR 1.5f
#else
#define OLP_TOLERANCE_MODIFICATOR 1.0f
#endif


// the maximal allowed distance of an object from the camera, in mm
#define OLP_MAX_OBJECT_DISTANCE 1500.0f     // 1200

// minimal number of feature points for a hypothesis
#define OLP_MIN_NUM_FEATURES 30     // 25

// maximal number of initial hypotheses that will be generated
#define OLP_MAX_NUM_HYPOTHESES 80    // 120

// influences how many clusters are created when applying x-means to the generated initial hypotheses
// a bigger factor leads to more clusters
#define OLP_CLUSTERING_FACTOR_PLANES 2.5f
// maximal number of parts into which the object may be divided by clustering
#define OLP_MAX_NUM_CLUSTERING_PARTS 6  // 6


// number of parallel threads if OpenMP is used. If undefined, the number of threads will be set to the number of CPUs
#ifdef OLP_USE_ARMAR3_ARMAR3_4
#define OMP_NUM_THREADS 2
#else
//#define OMP_NUM_THREADS 1
#endif


// decide if classes of known objects will be loaded
//#define OLP_LOAD_KNOWN_OBJECTS false


// decide if a new visual vocabulary will be learned (may take around 1 hour!)
//#define OLP_LEARN_NEW_VISUAL_VOCABULARY false

// for the point/komma problem
#define OLP_KOMMA

// directories and files for object recognition
#if defined OLP_USE_ARMAR3_ARMAR3_4
#define OLP_OBJECT_LEARNING_DIR "/localhome/armar-user/armar-user_test3_mca2.4/david_mca2.4/OLP/data/"
#define OLP_HISTOGRAM_DIR "/localhome/armar-user/armar-user_test3_mca2.4/david_mca2.4/OLP/data/Histograms-punkt/"
#define OLP_OBJECT_NAMES_FILE "/localhome/armar-user/armar-user_test3_mca2.4/david_mca2.4/OLP/data/Histograms-punkt/ObjectNames.txt"
#define OLP_VOCABULARY_FILE "/localhome/armar-user/armar-user_test3_mca2.4/david_mca2.4/OLP/data/VisualVocabulary1000d.txt"
#define OLP_ADDITIONAL_OBJECT_IMAGES_DIR "/localhome/armar-user/armar-user_test3_mca2.4/david_mca2.4/OLP/data/objects/"
#define OLP_NEW_VOCABULARY_FILE "/localhome/armar-user/armar-user_test3_mca2.4/david_mca2.4/OLP/data/VisualVocabulary1000e.txt"
#define OLP_NEW_VOCABULARY_TRAINING_IMAGES "/localhome/armar-user/armar-user_test3_mca2.4/david_mca2.4/OLP/data/objects/MoreObjects000.bmp"
#elif defined OLP_USE_ARMAR3
#ifdef OLP_KOMMA
//#define OLP_HISTOGRAM_DIR "/common/homes/staff/schieben/home/OLP/data/Histograms-komma/"
//#define OLP_OBJECT_NAMES_FILE "/common/homes/staff/schieben/home/OLP/data/Histograms-komma/ObjectNames.txt"
//#define OLP_VOCABULARY_FILE "/common/homes/staff/schieben/home/OLP/data/VisualVocabulary1000d-komma.txt"
//#define OLP_VOCABULARY_FILE "/home/mobiletamer/OLP/data/VisualVocabulary1000d-komma.txt"
#else
#define OLP_HISTOGRAM_DIR "/common/homes/staff/schieben/home/OLP/data/Histograms-punkt/"
#define OLP_OBJECT_NAMES_FILE "/common/homes/staff/schieben/home/OLP/data/Histograms-punkt/ObjectNames.txt"
#define OLP_VOCABULARY_FILE "/common/homes/staff/schieben/home/OLP/data/VisualVocabulary1000d-punkt.txt"
//#define OLP_VOCABULARY_FILE "/home/mobiletamer/OLP/data/VisualVocabulary1000d.txt"
#endif
//#define OLP_OBJECT_LEARNING_DIR "/home/staff/schieben/OLP/data/LearnedObjects/"
#define OLP_OBJECT_LEARNING_DIR "/common/homes/staff/schieben/home/datalog/LearnedObjects/temp/"
//#define OLP_ADDITIONAL_OBJECT_IMAGES_DIR "/common/homes/staff/schieben/home/OLP/data/objects/"
//#define OLP_NEW_VOCABULARY_FILE "/common/homes/staff/schieben/home/OLP/data/VisualVocabulary1000e.txt"
//#define OLP_NEW_VOCABULARY_TRAINING_IMAGES "/common/homes/staff/schieben/home/OLP/data/objects/MoreObjects000.bmp"
#elif defined OLP_USE_DBVISION
#define OLP_OBJECT_LEARNING_DIR "params/ObjectLearning/"
#define OLP_HISTOGRAM_DIR "params/ObjectLearning/HistogramsE/"
#define OLP_OBJECT_NAMES_FILE "params/ObjectLearning/HistogramsE/ObjectNames.txt"
#define OLP_VOCABULARY_FILE "params/ObjectLearning/VisualVocabulary1000d.txt"
#define OLP_ADDITIONAL_OBJECT_IMAGES_DIR "params/ObjectLearning/objects/"
#define OLP_NEW_VOCABULARY_FILE "params/ObjectLearning/objects/VisualVocabulary1000e.txt"
#define OLP_NEW_VOCABULARY_TRAINING_IMAGES "params/ObjectLearning/objects/MoreObjects000.bmp"   // <- names have to end on 000, 001, 002 etc.
#else
#define OLP_OBJECT_LEARNING_DIR "c:/img/"
#define OLP_HISTOGRAM_DIR "c:/img/HistogramsE/"
#define OLP_OBJECT_NAMES_FILE "c:/img/HistogramsE/ObjectNames.txt"
#define OLP_VOCABULARY_FILE "c:/img/VisualVocabulary1000d.txt"
#define OLP_ADDITIONAL_OBJECT_IMAGES_DIR "c:/img/objects/"
#define OLP_NEW_VOCABULARY_FILE "c:/img/VisualVocabulary1000e.txt"
#define OLP_NEW_VOCABULARY_TRAINING_IMAGES "c:/img/objects/MoreObjects000.bmp"  // <- names have to end on 000, 001, 002 etc.
#endif
//#define OLP_NEW_VOCABULARY_NUMBER_OF_TRAINING_IMAGES 20


// define if images should be shown in OpenCV windows
#ifndef OLP_USE_ARMAR3_ARMAR3_4
#define OLP_SHOW_RESULT_IMAGES
#endif
// waiting time for the visualization (in ms)
#define OLP_WAITING_TIME_VISUALISATION 400

// screenshots
#ifdef OLP_USE_ARMAR3_ARMAR3_4
#define OLP_MAKE_RESULT_SCREENSHOTS false
#define OLP_MAKE_INTERMEDIATE_SCREENSHOTS false
#define OLP_SAVE_COMPLETE_POINTCLOUD false
#define OLP_SAVE_CONFIRMED_OBJECT false
#else
#define OLP_MAKE_RESULT_SCREENSHOTS true
#define OLP_MAKE_INTERMEDIATE_SCREENSHOTS true
#define OLP_SAVE_COMPLETE_POINTCLOUD true
#define OLP_SAVE_CONFIRMED_OBJECT true
#endif
#define OLP_SCREENSHOT_PATH OLP_OBJECT_LEARNING_DIR
//#define OLP_SCREENSHOT_PATH "/homes/staff/schieben/datalog/YCB/Head/temp/"


// scaling factor for the image before being fouriertransformed
#define OLP_FOURIER_TRANSFORM_SCALING_FACTOR 0.5



// size of the MSER color histogram
#define OLP_SIZE_MSER_HISTOGRAM 64
// number of SIFT descriptors per point
//#define OLP_NUM_DESCRIPTORS_PER_POINT 9
// size of the hue histogram for object recognition
#define OLP_SIZE_OBJECT_HUE_HISTOGRAM 64


// tolerance for the validation if a point moved concurrently (in mm)
#define OLP_TOLERANCE_CONCURRENT_MOTION 80.0f   // 70
// minimal necessary motion for a hypothesis to be validated (in mm)
#define OLP_MINIMAL_MOTION_MEASURE 30.0f    // 50
// when using RGBD: maximal distance (in px) from next confirmed point for being added as a new candidate (if point is also in changed image area)
#define OLP_MAX_DISTANCE_FOR_ADDING_FOREGROUND_CANDIDATE_2D 40
// when using RGBD: maximal distance (in mm) from next confirmed point for being added as a new candidate (if point is also in changed image area)
#define OLP_MAX_DISTANCE_FOR_ADDING_FOREGROUND_CANDIDATE_3D 70
// generate a segmentation image region from the confirmed points, and add all scene points in that region to the confirmed points
#define OLP_ADD_POINTS_FROM_SEGMENTED_REGION
// threshold to decide whether a pixel changed or not
#define OLP_FOREGROUND_THRESHOLD 50 //50


// for color ICP: weight of the color distance to the cartesian distance (maximal color distance = x mm)
#define OLP_ICP_COLOR_DISTANCE_WEIGHT 40.0f
// for color ICP: point correspondences with a bigger distance than this will not be used
#define OLP_ICP_CUTOFF_DISTANCE 3*OLP_TOLERANCE_CONCURRENT_MOTION // FLT_MAX



// set minimal distance between Harris Interest Points that will be detected (in pixels)
#define OLP_HARRIS_POINT_DISTANCE 3.0f
// set minimal relative saliency of Harris Interest Points (smaller -> more points)
#define OLP_HARRIS_POINT_QUALITY 0.0001f // 0.001

// define to calculate and use MSERs
#define OLP_USE_MSERS


// define to use points from the depth map
#define OLP_USE_DEPTH_MAP
// distance between depth map pixels that will be used
#define OLP_DEPTH_MAP_PIXEL_DISTANCE 3  // 3
// effort for relocalizing a hypothesis after pushing
#ifdef OLP_USE_ARMAR3_ARMAR3_4
#define OLP_EFFORT_POINTCLOUD_MATCHING 1
#else
#define OLP_EFFORT_POINTCLOUD_MATCHING 3  // 3
#endif
// factor for clustering the changed points after the first push to create foreground hypotheses (bigger->more hypotheses)
#define OLP_CLUSTERING_FACTOR_FOREGROUND_HYPOTHESES 10.0f
// clustering of promising regions for object re-localization (bigger->more promising positions will be tested)
#define OLP_CLUSTERING_FACTOR_OBJECT_LOCALIZATION 2.5f  // 1.5

#ifdef OLP_USE_DEPTH_MAP
// create hypotheses from large color MSERs
#define OLP_FIND_UNICOLORED_HYPOTHESES                              // <--
// minimal size for an MSER to be used for creating a hypothesis (in pixels)
#define OLP_MSER_HYPOTHESIS_MIN_SIZE 225
// maximal size for an MSER to be used for creating a hypothesis (in pixels)
#define OLP_MSER_HYPOTHESIS_MAX_SIZE (OLP_IMG_WIDTH*OLP_IMG_HEIGHT/100)
// create planar hypotheses
#define OLP_FIND_PLANES                                             // <--
// create cylindrical hypotheses
//#define OLP_FIND_CYLINDERS
// create spherical hypotheses
//#define OLP_FIND_SPHERES
// create hypotheses from clusters of points that just lie close to each other, but not on a plane/cylinder/sphere
//#define OLP_FIND_IRREGULAR_CLUSTERS
// generate hypotheses from not-yet-covered salient regions
#define OLP_FIND_SALIENCY_HYPOTHESES                                    // <--
// use the LCCP segmentation to generate hypotheses
//#define OLP_USE_LCCP
#define OLP_MIN_SIZE_LCCP_SEGMENT OLP_MIN_NUM_FEATURES
#define OLP_MAX_SIZE_LCCP_SEGMENT (OLP_IMG_WIDTH*OLP_IMG_HEIGHT/100)
//#define OLP_MAKE_LCCP_SEG_IMAGES
#else
// create planar hypotheses
//#define OLP_FIND_PLANES
// create cylindrical hypotheses
//#define OLP_FIND_CYLINDERS
// create spherical hypotheses
//#define OLP_FIND_SPHERES
// create hypotheses from clusters of points that just lie close to each other, but not on a plane/cylinder/sphere
//#define OLP_FIND_IRREGULAR_CLUSTERS
#endif


// define this to keep only those initial hypotheses that are local maxima (with relation to their height)
#ifdef OLP_USE_DEPTH_MAP
#define OLP_FILTER_INITIAL_HYPOTHESES_WITH_MAXIMUMNESS
#endif

// no SIFT points with x coordinate in image smaller than this value will be used, so that the
// resulting hypotheses can get a correct maximumness rating
#ifdef OLP_FILTER_INITIAL_HYPOTHESES_WITH_MAXIMUMNESS
#define OLP_MIN_X_VALUE_SIFT_POINTS 120
#else
#define OLP_MIN_X_VALUE_SIFT_POINTS 0
#endif


// a central position in front of the robot. The hypothesis closest to this position is chosen for pushing,
// and the object is always pushed towards this point
#define OLP_CENTRAL_POSITION_FOR_PUSHING_X 0
#define OLP_CENTRAL_POSITION_FOR_PUSHING_Y 500
#define OLP_CENTRAL_POSITION_FOR_PUSHING_Z 1100


// define if using OpenCV version >= 2.3
#define OLP_USE_NEW_OPENCV

