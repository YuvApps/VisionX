/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MSERCalculation.h"


// IVT
#include <Image/ByteImage.h>
#include <Image/ImageProcessor.h>
#include <Image/IplImageAdaptor.h>
#include <Calibration/StereoCalibration.h>
#include <Calibration/Calibration.h>

// system
#include <ctime>
#include <cmath>


//#include <ArmarXCore/core/logging/Logging.h>




CMSERCalculation::CMSERCalculation(void)
{
}



CMSERCalculation::~CMSERCalculation(void)
{
}




void CMSERCalculation::FindMSERs2D(const CByteImage* pRGBImage, const CByteImage* pHSVImage, std::vector<CMSERDescriptor*>& aMSERDescriptors)
{
#if (CV_MAJOR_VERSION == 2)
#if (CV_MINOR_VERSION < 4)
    //timeval tStart, tEnd;
    //long tTimeDiff;
    //gettimeofday(&tStart, 0);

    //    int delta; //! delta, in the code, it compares (size_{i}-size_{i-delta})/size_{i-delta}
    //    int maxArea; //! prune the area which bigger than maxArea
    //    int minArea; //! prune the area which smaller than minArea
    //    float maxVariation; //! prune the area have simliar size to its children
    //    float minDiversity; //! trace back to cut off mser with diversity < min_diversity
    //    The next few params for MSER of color image:
    //    int maxEvolution; //! for color image, the evolution steps
    //    double areaThreshold; //! the area threshold to cause re-initialize
    //    double minMargin; //! ignore too small margin
    //    int edgeBlurSize; //! the aperture size for edge blur

    cv::MSER pMSERDetector = cv::MSER();

    //    ARMARX_VERBOSE_S << "delta: %d  maxArea: %d minArea: %d  maxVariation: %.3f  minDiversity: %.3f  maxEvolution: %d  areaThreshold: %.4f  minMargin: %.4f  edgeBlurSize: %d\n",
    //           pMSERDetector.delta, pMSERDetector.maxArea, pMSERDetector.minArea, pMSERDetector.maxVariation, pMSERDetector.minDiversity,
    //           pMSERDetector.maxEvolution, pMSERDetector.areaThreshold, pMSERDetector.minMargin, pMSERDetector.edgeBlurSize);

    pMSERDetector.maxArea *= 10;
    pMSERDetector.maxVariation *= 3;
    pMSERDetector.maxEvolution *= 3;
    pMSERDetector.minMargin *= 0.33;

    // convert image
    IplImage* pIplImage;
    pIplImage = IplImageAdaptor::Adapt(pRGBImage);

    std::vector<std::vector<cv::Point> > aContoursCVLeft;
    std::vector<std::vector<Vec2d> > aContoursLeft;

    //pMSERDetector(pIplImageLeft, aContoursCV, cv::Mat());

    cv::Seq<CvSeq*> contoursLeft;
    cv::MemStorage storageLeft(cvCreateMemStorage(0));
    cvExtractMSER(pIplImage, NULL, &contoursLeft.seq, storageLeft, pMSERDetector);

    cv::SeqIterator<CvSeq*> itLeft = contoursLeft.begin();
    size_t i, ncontours = contoursLeft.size();
    aContoursCVLeft.resize(ncontours);

    for (i = 0; i < ncontours; i++, ++itLeft)
    {
        while (cv::Seq<cv::Point>(*itLeft).size() > 0)
        {
            aContoursCVLeft[i].push_back(cv::Seq<cv::Point>(*itLeft).front());
            cv::Seq<cv::Point>(*itLeft).pop_front();
        }
    }

    //storage.release();

    //std::vector<CMSERDescriptor*> aMSERDescriptorsLeft;
    for (int i = 0; i < (int)aContoursCVLeft.size(); i++)
    {
        std::vector<Vec2d> aPoints;
        aContoursLeft.push_back(aPoints);

        for (int j = 0; j < (int)aContoursCVLeft.at(i).size(); j++)
        {
            Vec2d vTemp = {(float)aContoursCVLeft.at(i).at(j).x, (float)aContoursCVLeft.at(i).at(j).y};
            aContoursLeft.at(i).push_back(vTemp);
        }
    }

    //gettimeofday(&tEnd, 0);
    //tTimeDiff = (1000*tEnd.tv_sec+tEnd.tv_usec/1000) - (1000*tStart.tv_sec+tStart.tv_usec/1000);
    //ARMARX_VERBOSE_S << "\nTime for 2D MSER detection: %d ms\n\n", tTimeDiff);
    //
    //gettimeofday(&tStart, 0);

    for (int i = 0; i < (int)aContoursLeft.size(); i++)
    {
        CMSERDescriptor* pDescriptor = CreateMSERDescriptor(&aContoursLeft.at(i), pHSVImage);
        aMSERDescriptors.push_back(pDescriptor);
        //ARMARX_VERBOSE_S << "\nCenter: (%.1f,%.1f)  \nEW1: %.1f  EV1: (%.3f, %.3f)  \nEW2: %.1f  EV2: (%.3f, %.3f) \n\n",
        //  pDescriptor->vMean.x, pDescriptor->vMean.y,
        //  sqrtf(pDescriptor->fEigenvalue1), pDescriptor->vEigenvector1.x, pDescriptor->vEigenvector1.y,
        //  sqrtf(pDescriptor->fEigenvalue2), pDescriptor->vEigenvector2.x, pDescriptor->vEigenvector2.y);
    }

    //gettimeofday(&tEnd, 0);
    //tTimeDiff = (1000*tEnd.tv_sec+tEnd.tv_usec/1000) - (1000*tStart.tv_sec+tStart.tv_usec/1000);
    //ARMARX_VERBOSE_S << "\nTime for MSER descriptor creation: %d ms\n\n", tTimeDiff);

    cvReleaseImageHeader(&pIplImage);
    storageLeft.release();

#elif (CV_MINOR_VERSION == 4)
    cv::MSER pMSERDetector = cv::MSER();
    IplImage* pIplImage;
    pIplImage = IplImageAdaptor::Adapt(pRGBImage);
    std::vector<std::vector<cv::Point> > aContoursCVLeft;

    pMSERDetector(pIplImage, aContoursCVLeft, cv::Mat());

    std::vector<std::vector<Vec2d> > aContoursLeft;

    for (int i = 0; i < (int)aContoursCVLeft.size(); i++)
    {
        std::vector<Vec2d> aPoints;
        aContoursLeft.push_back(aPoints);

        for (int j = 0; j < (int)aContoursCVLeft.at(i).size(); j++)
        {
            Vec2d vTemp = {(float)aContoursCVLeft.at(i).at(j).x, (float)aContoursCVLeft.at(i).at(j).y};
            aContoursLeft.at(i).push_back(vTemp);
        }
    }

    for (int i = 0; i < (int)aContoursLeft.size(); i++)
    {
        CMSERDescriptor* pDescriptor = CreateMSERDescriptor(&aContoursLeft.at(i), pHSVImage);
        aMSERDescriptors.push_back(pDescriptor);
    }

    cvReleaseImageHeader(&pIplImage);
#else
    ARMARX_WARNING_S << "This code is not implemented for OpenCV Version " << CV_VERSION << " (CV_MINOR_VERSION = " << CV_MINOR_VERSION << ")";
#endif
#else
    ARMARX_WARNING_S << "This code is not implemented for OpenCV Version " << CV_VERSION << " (CV_MAJOR_VERSION = " << CV_MAJOR_VERSION << ")";
#endif
}





void CMSERCalculation::StereoMatchMSERs(const std::vector<CMSERDescriptor*>& aMSERDescriptorsLeft, const std::vector<CMSERDescriptor*>& aMSERDescriptorsRight,
                                        CStereoCalibration* pStereoCalibration, const float fToleranceFactor, std::vector<CMSERDescriptor3D*>& aRegions3D)
{
    // find stereo correspondences
    const float fRatioThresholdHigh = 1.0f + fToleranceFactor * 0.25f; // 1.25
    const float fRatioThresholdLow = 1.0f / fRatioThresholdHigh;
    const float fRatioThresholdHigh2 = fRatioThresholdHigh * fRatioThresholdHigh;
    const float fRatioThresholdLow2 = fRatioThresholdLow * fRatioThresholdLow;

    for (int i = 0; i < (int)aMSERDescriptorsLeft.size(); i++)
    {
        float fMinEpipolarDistance = fToleranceFactor * 2.0f;   // 2
        int nCorrespondenceIndex = -1;
        const float fOwnEVRatio = aMSERDescriptorsLeft.at(i)->fEigenvalue2 / aMSERDescriptorsLeft.at(i)->fEigenvalue1;

        for (int j = 0; j < (int)aMSERDescriptorsRight.size(); j++)
        {
            const float fSizeRatio = (float)aMSERDescriptorsLeft.at(i)->nSize / (float)aMSERDescriptorsRight.at(j)->nSize;

            if ((fRatioThresholdLow < fSizeRatio) && (fSizeRatio < fRatioThresholdHigh))
            {
                const float fEV1Ratio = aMSERDescriptorsLeft.at(i)->fEigenvalue1 / aMSERDescriptorsRight.at(j)->fEigenvalue1;
                const float fEV2Ratio = aMSERDescriptorsLeft.at(i)->fEigenvalue2 / aMSERDescriptorsRight.at(j)->fEigenvalue2;
                const float fRatioRatio = (aMSERDescriptorsRight.at(j)->fEigenvalue2 / aMSERDescriptorsRight.at(j)->fEigenvalue1) / fOwnEVRatio;

                if ((fRatioThresholdLow2 < fEV1Ratio) && (fEV1Ratio < fRatioThresholdHigh2) && (fRatioThresholdLow2 < fEV2Ratio)
                    && (fEV2Ratio < fRatioThresholdHigh2) && (fRatioThresholdLow2 < fRatioRatio) && (fRatioRatio < fRatioThresholdHigh2))
                {
                    const float fEpipolarDistance = fabsf(pStereoCalibration->CalculateEpipolarLineInLeftImageDistance(aMSERDescriptorsLeft.at(i)->vMean, aMSERDescriptorsRight.at(j)->vMean));

                    if (fEpipolarDistance < fMinEpipolarDistance)
                    {
                        Vec3d vPoint3D;
                        pStereoCalibration->Calculate3DPoint(aMSERDescriptorsLeft.at(i)->vMean, aMSERDescriptorsRight.at(j)->vMean, vPoint3D, false, false);

                        if (vPoint3D.z < OLP_MAX_OBJECT_DISTANCE)
                        {
                            fMinEpipolarDistance = fEpipolarDistance;
                            nCorrespondenceIndex = j;
                        }
                    }
                }
            }
        }

        if (nCorrespondenceIndex != -1)
        {
            CMSERDescriptor3D* pDescr3D = CreateMSERDescriptor3D(aMSERDescriptorsLeft.at(i), aMSERDescriptorsRight.at(nCorrespondenceIndex), pStereoCalibration);
            aRegions3D.push_back(pDescr3D);
        }
    }

}







void CMSERCalculation::FindMSERs3D(const CByteImage* pByteImageLeft, const CByteImage* pByteImageRight, CStereoCalibration* pStereoCalibration,
                                   const float fToleranceFactor, std::vector<CMSERDescriptor3D*>& aRegions3D)
{
    //CByteImage* pByteImageLeftGrey = new CByteImage(pByteImageLeft->width, pByteImageLeft->height, CByteImage::eGrayScale);
    //ImageProcessor::ConvertImage(pByteImageLeft, pByteImageLeftGrey);

    // create HSV images
    CByteImage* pHSVImageLeft = new CByteImage(OLP_IMG_WIDTH, OLP_IMG_HEIGHT, CByteImage::eRGB24);
    CByteImage* pHSVImageRight = new CByteImage(OLP_IMG_WIDTH, OLP_IMG_HEIGHT, CByteImage::eRGB24);
    ImageProcessor::CalculateHSVImage(pByteImageLeft, pHSVImageLeft);
    ImageProcessor::CalculateHSVImage(pByteImageRight, pHSVImageRight);

    //timeval tStart, tEnd;
    //long tTimeDiff;
    //gettimeofday(&tStart, 0);

    // find MSERs in left and right image
    std::vector<CMSERDescriptor*> aMSERDescriptorsLeft;
    std::vector<CMSERDescriptor*> aMSERDescriptorsRight;
    FindMSERs2D(pByteImageLeft, pHSVImageLeft, aMSERDescriptorsLeft);
    FindMSERs2D(pByteImageRight, pHSVImageRight, aMSERDescriptorsRight);


    // find stereo correspondences
    StereoMatchMSERs(aMSERDescriptorsLeft, aMSERDescriptorsRight, pStereoCalibration, fToleranceFactor, aRegions3D);


    //// debug
    //for (int i=0; i<(int)aRegions3D.size() && i<5; i++)
    //{
    //  ARMARX_VERBOSE_S << "Region %d: histogram distance left-right %.4f\n", i, HistogramDistanceL2(aRegions3D.at(i)->pRegionLeft, aRegions3D.at(i)->pRegionRight));
    //  ARMARX_VERBOSE_S << "\nHistogram region %d inner points:\n", i);
    //  for (int j=0; j<OLP_SIZE_MSER_HISTOGRAM; j++)
    //  {
    //      ARMARX_VERBOSE_S << "%d\t", (int)(aRegions3D.at(i)->pRegionLeft->pHueHistogramInnerPoints[j] * 1000));
    //  }
    //  ARMARX_VERBOSE_S << "\nHistogram region %d surrounding region:\n", i);
    //  for (int j=0; j<OLP_SIZE_MSER_HISTOGRAM; j++)
    //  {
    //      ARMARX_VERBOSE_S << "%d\t", (int)(aRegions3D.at(i)->pRegionLeft->pHueHistogramSurroundingRegion[j] * 1000));
    //  }
    //  ARMARX_VERBOSE_S << "\n\n");
    //}


    //gettimeofday(&tEnd, 0);
    //tTimeDiff = (1000*tEnd.tv_sec+tEnd.tv_usec/1000) - (1000*tStart.tv_sec+tStart.tv_usec/1000);
    //ARMARX_VERBOSE_S << "\nTime for MSER detection and stereo matching: %d ms\n\n", tTimeDiff);

    for (size_t i = 0; i < aMSERDescriptorsLeft.size(); i++)
    {
        aMSERDescriptorsLeft.at(i)->pPoints2D->clear();
        delete aMSERDescriptorsLeft.at(i)->pPoints2D;
        delete aMSERDescriptorsLeft.at(i);
    }

    for (size_t i = 0; i < aMSERDescriptorsRight.size(); i++)
    {
        aMSERDescriptorsRight.at(i)->pPoints2D->clear();
        delete aMSERDescriptorsRight.at(i)->pPoints2D;
        delete aMSERDescriptorsRight.at(i);
    }


    delete pHSVImageLeft;
    delete pHSVImageRight;

    //ARMARX_VERBOSE_S << "\n%d 3D-MSERs found.\n\n", aRegions3D.size());
}






void CMSERCalculation::GetMeanAndCovariance2D(std::vector<Vec2d>& aPoints2D, Vec2d& vMean, Mat2d& mCovariance)
{
    mCovariance.r1 = 0;
    mCovariance.r2 = 0;
    mCovariance.r3 = 0;
    mCovariance.r4 = 0;

    if (aPoints2D.size() < 2)
    {
        return;
    }

    vMean.x = 0;
    vMean.y = 0;

    for (int i = 0; i < (int)aPoints2D.size(); i++)
    {
        vMean.x += (float)aPoints2D.at(i).x;
        vMean.y += (float)aPoints2D.at(i).y;
    }

    vMean.x /= aPoints2D.size();
    vMean.y /= aPoints2D.size();

    for (int i = 0; i < (int)aPoints2D.size(); i++)
    {
        mCovariance.r1 += (vMean.x - (float)aPoints2D.at(i).x) * (vMean.x - (float)aPoints2D.at(i).x);
        mCovariance.r2 += (vMean.x - (float)aPoints2D.at(i).x) * (vMean.y - (float)aPoints2D.at(i).y);
        mCovariance.r4 += (vMean.y - (float)aPoints2D.at(i).y) * (vMean.y - (float)aPoints2D.at(i).y);
    }

    mCovariance.r1 /= aPoints2D.size() - 1;
    mCovariance.r2 /= aPoints2D.size() - 1;
    mCovariance.r3 = mCovariance.r2;
    mCovariance.r4 /= aPoints2D.size() - 1;
}



void CMSERCalculation::GetEigenvaluesAndEigenvectors2D(Mat2d mMatrix2D, float& fLambda1, float& fLambda2, Vec2d& vE1, Vec2d& vE2)
{
    float fTemp1 = 0.5f * (mMatrix2D.r1 + mMatrix2D.r4);
    float fTemp2 = sqrtf(fTemp1 * fTemp1 - mMatrix2D.r1 * mMatrix2D.r4 + mMatrix2D.r2 * mMatrix2D.r3);

    fLambda1 = fTemp1 + fTemp2;
    fLambda2 = fTemp1 - fTemp2;

    vE1.x = fLambda1 - mMatrix2D.r4;
    vE1.y = mMatrix2D.r3;
    Math2d::NormalizeVec(vE1);

    vE2.x = fLambda2 - mMatrix2D.r4;
    vE2.y = mMatrix2D.r3;
    Math2d::NormalizeVec(vE2);

}




CMSERDescriptor* CMSERCalculation::CreateMSERDescriptor(std::vector<Vec2d>* aPoints2D, const CByteImage* pHSVImage)
{
    if (aPoints2D->size() < 2)
    {
        return NULL;
    }

    CMSERDescriptor* pDescr = new CMSERDescriptor();

    pDescr->nSize = (int)aPoints2D->size();

    pDescr->pPoints2D = new std::vector<Vec2d>;

    for (int i = 0; i < (int)aPoints2D->size(); i++)
    {
        pDescr->pPoints2D->push_back(aPoints2D->at(i));
    }


    Mat2d mCovariance;
    GetMeanAndCovariance2D(*aPoints2D, pDescr->vMean, mCovariance);

    GetEigenvaluesAndEigenvectors2D(mCovariance, pDescr->fEigenvalue1, pDescr->fEigenvalue2, pDescr->vEigenvector1, pDescr->vEigenvector2);

    CreateHSVHistograms(pDescr, pHSVImage);

    return pDescr;
}




CMSERDescriptor3D* CMSERCalculation::CreateMSERDescriptor3D(CMSERDescriptor* pMSERLeft, CMSERDescriptor* pMSERRight, CStereoCalibration* pStereoCalibration)
{
    CMSERDescriptor3D* pDescr3D = new CMSERDescriptor3D();


    pDescr3D->pRegionLeft = pMSERLeft->GetCopy();
    //pDescr3D->pRegionLeft = new CMSERDescriptor();
    //*(pDescr3D->pRegionLeft) = *pMSERLeft;
    //std::vector<Vec2d>* pPoints2D = new std::vector<Vec2d>();
    //for (int k=0; k<(int)pMSERLeft->pPoints2D->size(); k++)
    //{
    //  pPoints2D->push_back(pMSERLeft->pPoints2D->at(k));
    //}
    //pDescr3D->pRegionLeft->pPoints2D = pPoints2D;

    pDescr3D->pRegionRight = pMSERRight->GetCopy();
    //pDescr3D->pRegionRight = new CMSERDescriptor();
    //*(pDescr3D->pRegionRight)= *pMSERRight;
    //pPoints2D = new std::vector<Vec2d>();
    //for (int k=0; k<(int)pMSERRight->pPoints2D->size(); k++)
    //{
    //  pPoints2D->push_back(pMSERRight->pPoints2D->at(k));
    //}
    //pDescr3D->pRegionRight->pPoints2D = pPoints2D;


    pStereoCalibration->Calculate3DPoint(pMSERLeft->vMean, pMSERRight->vMean, pDescr3D->vPosition, false, false);


    Vec2d vSigmaPointLeft, vSigmaPointRight, vTemp;

    const float fSigmaPointFactor = 1.0f;

    Math2d::MulVecScalar(pMSERLeft->vEigenvector1, -fSigmaPointFactor * sqrtf(pMSERLeft->fEigenvalue1), vTemp);
    Math2d::AddVecVec(pMSERLeft->vMean, vTemp, vSigmaPointLeft);
    Math2d::MulVecScalar(pMSERRight->vEigenvector1, -fSigmaPointFactor * sqrtf(pMSERRight->fEigenvalue1), vTemp);
    Math2d::AddVecVec(pMSERRight->vMean, vTemp, vSigmaPointRight);
    pStereoCalibration->Calculate3DPoint(vSigmaPointLeft, vSigmaPointRight, pDescr3D->vSigmaPoint1a, false, false);

    Math2d::MulVecScalar(pMSERLeft->vEigenvector1, fSigmaPointFactor * sqrtf(pMSERLeft->fEigenvalue1), vTemp);
    Math2d::AddVecVec(pMSERLeft->vMean, vTemp, vSigmaPointLeft);
    Math2d::MulVecScalar(pMSERRight->vEigenvector1, fSigmaPointFactor * sqrtf(pMSERRight->fEigenvalue1), vTemp);
    Math2d::AddVecVec(pMSERRight->vMean, vTemp, vSigmaPointRight);
    pStereoCalibration->Calculate3DPoint(vSigmaPointLeft, vSigmaPointRight, pDescr3D->vSigmaPoint1b, false, false);

    Math2d::MulVecScalar(pMSERLeft->vEigenvector2, -fSigmaPointFactor * sqrtf(pMSERLeft->fEigenvalue2), vTemp);
    Math2d::AddVecVec(pMSERLeft->vMean, vTemp, vSigmaPointLeft);
    Math2d::MulVecScalar(pMSERRight->vEigenvector2, -fSigmaPointFactor * sqrtf(pMSERRight->fEigenvalue2), vTemp);
    Math2d::AddVecVec(pMSERRight->vMean, vTemp, vSigmaPointRight);
    pStereoCalibration->Calculate3DPoint(vSigmaPointLeft, vSigmaPointRight, pDescr3D->vSigmaPoint2a, false, false);

    Math2d::MulVecScalar(pMSERLeft->vEigenvector2, fSigmaPointFactor * sqrtf(pMSERLeft->fEigenvalue2), vTemp);
    Math2d::AddVecVec(pMSERLeft->vMean, vTemp, vSigmaPointLeft);
    Math2d::MulVecScalar(pMSERRight->vEigenvector2, fSigmaPointFactor * sqrtf(pMSERRight->fEigenvalue2), vTemp);
    Math2d::AddVecVec(pMSERRight->vMean, vTemp, vSigmaPointRight);
    pStereoCalibration->Calculate3DPoint(vSigmaPointLeft, vSigmaPointRight, pDescr3D->vSigmaPoint2b, false, false);


    return pDescr3D;
}




inline void CMSERCalculation::CreateHSVHistograms(CMSERDescriptor* pDescriptor, const CByteImage* pHSVImage)
{
    for (int i = 0; i < OLP_SIZE_MSER_HISTOGRAM; i++)
    {
        pDescriptor->pHueHistogramInnerPoints[i] = 0;
        pDescriptor->pHueHistogramSurroundingRegion[i] = 0;
    }

    //**************************************************************************************************************
    // histogram of the inner points of the region
    //**************************************************************************************************************

    const int nBucketSize = (int)(ceil(256.0f / (float)OLP_SIZE_MSER_HISTOGRAM));
    int nPixelIndex, nSaturationValue, nHistogramIndex;
    float fWeight;
    int nSaturationSum = 0;

    for (int i = 0; i < (int)pDescriptor->pPoints2D->size(); i++)
    {
        nPixelIndex = 3 * (int)(pDescriptor->pPoints2D->at(i).y * OLP_IMG_WIDTH + pDescriptor->pPoints2D->at(i).x);
        nSaturationValue = pHSVImage->pixels[nPixelIndex + 1];
        nSaturationSum += nSaturationValue;
        nHistogramIndex = pHSVImage->pixels[nPixelIndex] / nBucketSize;
        fWeight = 1.0f - 1.0f / (1.0f + 0.0025f * (nSaturationValue * nSaturationValue));
        pDescriptor->pHueHistogramInnerPoints[nHistogramIndex] += 0.4f * fWeight;
        pDescriptor->pHueHistogramInnerPoints[(nHistogramIndex + OLP_SIZE_MSER_HISTOGRAM + 1) % OLP_SIZE_MSER_HISTOGRAM] += 0.2f * fWeight;
        pDescriptor->pHueHistogramInnerPoints[(nHistogramIndex + OLP_SIZE_MSER_HISTOGRAM - 1) % OLP_SIZE_MSER_HISTOGRAM] += 0.2f * fWeight;
        pDescriptor->pHueHistogramInnerPoints[(nHistogramIndex + OLP_SIZE_MSER_HISTOGRAM + 2) % OLP_SIZE_MSER_HISTOGRAM] += 0.1f * fWeight;
        pDescriptor->pHueHistogramInnerPoints[(nHistogramIndex + OLP_SIZE_MSER_HISTOGRAM - 2) % OLP_SIZE_MSER_HISTOGRAM] += 0.1f * fWeight;
    }

    // normalize
    float fSum = 0;

    for (int i = 0; i < OLP_SIZE_MSER_HISTOGRAM; i++)
    {
        fSum += pDescriptor->pHueHistogramInnerPoints[i];
    }

    if (fSum != 0)
    {
        const float fOneBySum = 1.0f / fSum;

        for (int i = 0; i < OLP_SIZE_MSER_HISTOGRAM; i++)
        {
            pDescriptor->pHueHistogramInnerPoints[i] *= fOneBySum;
        }
    }
    else    // set to equally distributed values
    {
        const float fDummy = 1.0f / (float)OLP_SIZE_MSER_HISTOGRAM;

        for (int i = 0; i < OLP_SIZE_MSER_HISTOGRAM; i++)
        {
            pDescriptor->pHueHistogramInnerPoints[i] = fDummy;
        }
    }

    pDescriptor->fAverageSaturationInnerPoints = ((float)nSaturationSum) / ((float)(255 * pDescriptor->pPoints2D->size()));


    //**************************************************************************************************************
    // histogram of the rectangular region spanned by the eigenvectors of the MSER
    //**************************************************************************************************************

    Vec2d pCorners[4];
    Vec2d vTemp1, vTemp2;
    const float fStdDevFactor = 2.5f;
    const float fAdditionalPixels = 2.0f;
    const float fRegionSize = (fStdDevFactor * sqrtf(pDescriptor->fEigenvalue1) + fAdditionalPixels) * (fStdDevFactor * sqrtf(pDescriptor->fEigenvalue2) + fAdditionalPixels);

    //// rhombus / Raute
    //Math2d::MulVecScalar(pDescriptor->vEigenvector1, (fStdDevFactor*sqrtf(pDescriptor->fEigenvalue1)+fAdditionalPixels), vTemp);
    //Math2d::SubtractVecVec(pDescriptor->vMean, vTemp, pCorners[0]);
    //Math2d::AddVecVec(pDescriptor->vMean, vTemp, pCorners[1]);
    //Math2d::MulVecScalar(pDescriptor->vEigenvector2, (fStdDevFactor*sqrtf(pDescriptor->fEigenvalue2)+fAdditionalPixels), vTemp);
    //Math2d::SubtractVecVec(pDescriptor->vMean, vTemp, pCorners[2]);
    //Math2d::AddVecVec(pDescriptor->vMean, vTemp, pCorners[3]);

    // parallelogramm
    Math2d::MulVecScalar(pDescriptor->vEigenvector1, (fStdDevFactor * sqrtf(pDescriptor->fEigenvalue1) + fAdditionalPixels), vTemp1);
    Math2d::MulVecScalar(pDescriptor->vEigenvector2, (fStdDevFactor * sqrtf(pDescriptor->fEigenvalue2) + fAdditionalPixels), vTemp2);
    Math2d::SubtractVecVec(pDescriptor->vMean, vTemp1, pCorners[0]);
    Math2d::SubtractVecVec(pCorners[0], vTemp2, pCorners[0]);
    Math2d::AddVecVec(pDescriptor->vMean, vTemp1, pCorners[1]);
    Math2d::SubtractVecVec(pCorners[1], vTemp2, pCorners[1]);
    Math2d::SubtractVecVec(pDescriptor->vMean, vTemp1, pCorners[2]);
    Math2d::AddVecVec(pCorners[2], vTemp2, pCorners[2]);
    Math2d::AddVecVec(pDescriptor->vMean, vTemp1, pCorners[3]);
    Math2d::AddVecVec(pCorners[3], vTemp2, pCorners[3]);


    // determine which corner is up, low, left, right
    Vec2d vUp, vLow, vLeft, vRight;
    SortQuadrangleCorners(pCorners, vUp, vLow, vLeft, vRight);

    CreateWeightedHueHistogramWithinQuadrangle(vUp, vLow, vLeft, vRight, pHSVImage, pDescriptor->pHueHistogramSurroundingRegion, OLP_SIZE_MSER_HISTOGRAM, nSaturationSum);

    pDescriptor->fAverageSaturationSurroundingRegion = ((float)nSaturationSum) / fRegionSize;

}




float CMSERCalculation::HistogramDistanceL2(CMSERDescriptor* pDescriptor1, CMSERDescriptor* pDescriptor2)
{
    float fSum = 0;

    for (int i = 0; i < OLP_SIZE_MSER_HISTOGRAM; i++)
    {
        fSum += (pDescriptor1->pHueHistogramInnerPoints[i] - pDescriptor2->pHueHistogramInnerPoints[i])
                * (pDescriptor1->pHueHistogramInnerPoints[i] - pDescriptor2->pHueHistogramInnerPoints[i])
                + (pDescriptor1->pHueHistogramSurroundingRegion[i] - pDescriptor2->pHueHistogramSurroundingRegion[i])
                * (pDescriptor1->pHueHistogramSurroundingRegion[i] - pDescriptor2->pHueHistogramSurroundingRegion[i]);
    }

    float fRet = sqrtf(fSum / (float)(2 * OLP_SIZE_MSER_HISTOGRAM));

    //if ( (fRet!=fRet) || _isnan(fRet) || (fRet>100) )
    //{
    //  for (int i=0; i<OLP_SIZE_MSER_HISTOGRAM; i++)
    //  {
    //      ARMARX_VERBOSE_S << "%.2f\t%.2f\n", pDescriptor1->pHueHistogramInnerPoints[i], pDescriptor2->pHueHistogramInnerPoints[i]);
    //  }
    //}

    return fRet;
}



float CMSERCalculation::HistogramDistanceChi2(CMSERDescriptor* pDescriptor1, CMSERDescriptor* pDescriptor2)
{
    float fSum = 0;

    for (int i = 0; i < OLP_SIZE_MSER_HISTOGRAM; i++)
    {
        fSum += (pDescriptor1->pHueHistogramInnerPoints[i] - pDescriptor2->pHueHistogramInnerPoints[i])
                * (pDescriptor1->pHueHistogramInnerPoints[i] - pDescriptor2->pHueHistogramInnerPoints[i])
                / (pDescriptor1->pHueHistogramInnerPoints[i] + pDescriptor2->pHueHistogramInnerPoints[i] + 0.00001f)
                + (pDescriptor1->pHueHistogramSurroundingRegion[i] - pDescriptor2->pHueHistogramSurroundingRegion[i])
                * (pDescriptor1->pHueHistogramSurroundingRegion[i] - pDescriptor2->pHueHistogramSurroundingRegion[i])
                / (pDescriptor1->pHueHistogramSurroundingRegion[i] + pDescriptor2->pHueHistogramSurroundingRegion[i] + 0.00001f);
    }

    float fRet = sqrtf(fSum / (float)(2 * OLP_SIZE_MSER_HISTOGRAM));

    return fRet;
}





// determine upper, lower, left and right corner. up = min y value
void CMSERCalculation::SortQuadrangleCorners(Vec2d* pCorners, Vec2d& vUp, Vec2d& vLow, Vec2d& vLeft, Vec2d& vRight)
{
    int nUp1, nUp2, nLow1, nLow2, nTemp;

    if (pCorners[0].y < pCorners[1].y)
    {
        nUp1 = 0;
        nLow1 = 1;
    }
    else
    {
        nUp1 = 1;
        nLow1 = 0;
    }

    if (pCorners[2].y < pCorners[3].y)
    {
        nUp2 = 2;
        nLow2 = 3;
    }
    else
    {
        nUp2 = 3;
        nLow2 = 2;
    }

    if (pCorners[nUp1].y > pCorners[nUp2].y)
    {
        nTemp = nUp1;
        nUp1 = nUp2;
        nUp2 = nTemp;
    }

    if (pCorners[nLow1].y < pCorners[nLow2].y)
    {
        nTemp = nLow1;
        nLow1 = nLow2;
        nLow2 = nTemp;
    }

    Math2d::SetVec(vUp, pCorners[nUp1]);
    Math2d::SetVec(vLow, pCorners[nLow1]);

    if (pCorners[nUp2].x < pCorners[nLow2].x)
    {
        Math2d::SetVec(vLeft, pCorners[nUp2]);
        Math2d::SetVec(vRight, pCorners[nLow2]);
    }
    else
    {
        Math2d::SetVec(vLeft, pCorners[nLow2]);
        Math2d::SetVec(vRight, pCorners[nUp2]);
    }

}





inline void CMSERCalculation::CreateWeightedHueHistogramWithinQuadrangle(Vec2d vUp, Vec2d vLow, Vec2d vLeft, Vec2d vRight, const CByteImage* pHSVImage,
        float* pHistogram, const int nHistogramSize, int& nSaturationSum)
{

    const int nBucketSize = (int)(ceil(256.0f / (float)nHistogramSize));
    int nPixelIndex, nSaturationValue, nHistogramIndex;
    float fWeight;

    // stay within the image
    if (vUp.x < 0)
    {
        vUp.x = 0;
    }
    else if (vUp.x >= OLP_IMG_WIDTH)
    {
        vUp.x = OLP_IMG_WIDTH - 1;
    }

    if (vUp.y < 0)
    {
        vUp.y = 0;
    }
    else if (vUp.y >= OLP_IMG_HEIGHT)
    {
        vUp.y = OLP_IMG_HEIGHT - 1;
    }

    if (vLow.x < 0)
    {
        vLow.x = 0;
    }
    else if (vLow.x >= OLP_IMG_WIDTH)
    {
        vLow.x = OLP_IMG_WIDTH - 1;
    }

    if (vLow.y < 0)
    {
        vLow.y = 0;
    }
    else if (vLow.y >= OLP_IMG_HEIGHT)
    {
        vLow.y = OLP_IMG_HEIGHT - 1;
    }

    if (vLeft.x < 0)
    {
        vLeft.x = 0;
    }
    else if (vLeft.x >= OLP_IMG_WIDTH)
    {
        vLeft.x = OLP_IMG_WIDTH - 1;
    }

    if (vLeft.y < 0)
    {
        vLeft.y = 0;
    }
    else if (vLeft.y >= OLP_IMG_HEIGHT)
    {
        vLeft.y = OLP_IMG_HEIGHT - 1;
    }

    if (vRight.x < 0)
    {
        vRight.x = 0;
    }
    else if (vRight.x >= OLP_IMG_WIDTH)
    {
        vRight.x = OLP_IMG_WIDTH - 1;
    }

    if (vRight.y < 0)
    {
        vRight.y = 0;
    }
    else if (vRight.y >= OLP_IMG_HEIGHT)
    {
        vRight.y = OLP_IMG_HEIGHT - 1;
    }


    nSaturationSum = 0;
    float fDeltaLeft, fDeltaRight;
    int nLeft, nRight;
    const int nUp = (int)vUp.y;
    const int nLow = (int)vLow.y;

    if (vLeft.y < vRight.y)
    {
        const int nMiddle1 = (int)vLeft.y;
        const int nMiddle2 = (int)vRight.y;
        fDeltaLeft = (vLeft.x - vUp.x) / (vLeft.y - vUp.y);
        fDeltaRight = (vRight.x - vUp.x) / (vRight.y - vUp.y);

        for (int i = nUp; i < nMiddle1; i++)
        {
            nLeft = (int)(vUp.x + (i - nUp) * fDeltaLeft);
            nRight = (int)(vUp.x + (i - nUp) * fDeltaRight);

            for (int j = nLeft; j <= nRight; j++)
            {
                nPixelIndex = 3 * (i * OLP_IMG_WIDTH + j);
                nSaturationValue = pHSVImage->pixels[nPixelIndex + 1];
                nSaturationSum += nSaturationValue;
                nHistogramIndex = pHSVImage->pixels[nPixelIndex] / nBucketSize;
                fWeight = 1.0f - 1.0f / (1.0f + 0.0025f * (nSaturationValue * nSaturationValue));
                pHistogram[nHistogramIndex] += 0.4f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize + 1) % nHistogramSize] += 0.2f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize - 1) % nHistogramSize] += 0.2f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize + 2) % nHistogramSize] += 0.1f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize - 2) % nHistogramSize] += 0.1f * fWeight;
            }
        }

        fDeltaLeft = (vLow.x - vLeft.x) / (vLow.y - vLeft.y);

        for (int i = nMiddle1; i < nMiddle2; i++)
        {
            nLeft = (int)(vLeft.x + (i - nMiddle1) * fDeltaLeft);
            nRight = (int)(vUp.x + (i - nUp) * fDeltaRight);

            for (int j = nLeft; j <= nRight; j++)
            {
                nPixelIndex = 3 * (i * OLP_IMG_WIDTH + j);
                nSaturationValue = pHSVImage->pixels[nPixelIndex + 1];
                nSaturationSum += nSaturationValue;
                nHistogramIndex = pHSVImage->pixels[nPixelIndex] / nBucketSize;
                fWeight = 1.0f - 1.0f / (1.0f + 0.0025f * (nSaturationValue * nSaturationValue));
                pHistogram[nHistogramIndex] += 0.4f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize + 1) % nHistogramSize] += 0.2f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize - 1) % nHistogramSize] += 0.2f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize + 2) % nHistogramSize] += 0.1f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize - 2) % nHistogramSize] += 0.1f * fWeight;
            }
        }

        fDeltaRight = (vLow.x - vRight.x) / (vLow.y - vRight.y);

        for (int i = nMiddle2; i < nLow; i++)
        {
            nLeft = (int)(vLeft.x + (i - nMiddle1) * fDeltaLeft);
            nRight = (int)(vRight.x + (i - nMiddle2) * fDeltaRight);

            for (int j = nLeft; j <= nRight; j++)
            {
                nPixelIndex = 3 * (i * OLP_IMG_WIDTH + j);
                nSaturationValue = pHSVImage->pixels[nPixelIndex + 1];
                nSaturationSum += nSaturationValue;
                nHistogramIndex = pHSVImage->pixels[nPixelIndex] / nBucketSize;
                fWeight = 1.0f - 1.0f / (1.0f + 0.0025f * (nSaturationValue * nSaturationValue));
                pHistogram[nHistogramIndex] += 0.4f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize + 1) % nHistogramSize] += 0.2f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize - 1) % nHistogramSize] += 0.2f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize + 2) % nHistogramSize] += 0.1f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize - 2) % nHistogramSize] += 0.1f * fWeight;
            }
        }
    }
    else
    {
        const int nMiddle1 = (int)vRight.y;
        const int nMiddle2 = (int)vLeft.y;
        fDeltaLeft = (vLeft.x - vUp.x) / (vLeft.y - vUp.y);
        fDeltaRight = (vRight.x - vUp.x) / (vRight.y - vUp.y);

        for (int i = nUp; i < nMiddle1; i++)
        {
            nLeft = (int)(vUp.x + (i - nUp) * fDeltaLeft);
            nRight = (int)(vUp.x + (i - nUp) * fDeltaRight);

            for (int j = nLeft; j <= nRight; j++)
            {
                nPixelIndex = 3 * (i * OLP_IMG_WIDTH + j);
                nSaturationValue = pHSVImage->pixels[nPixelIndex + 1];
                nSaturationSum += nSaturationValue;
                nHistogramIndex = pHSVImage->pixels[nPixelIndex] / nBucketSize;
                fWeight = 1.0f - 1.0f / (1.0f + 0.0025f * (nSaturationValue * nSaturationValue));
                pHistogram[nHistogramIndex] += 0.4f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize + 1) % nHistogramSize] += 0.2f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize - 1) % nHistogramSize] += 0.2f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize + 2) % nHistogramSize] += 0.1f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize - 2) % nHistogramSize] += 0.1f * fWeight;
            }
        }

        fDeltaRight = (vLow.x - vRight.x) / (vLow.y - vRight.y);

        for (int i = nMiddle1; i < nMiddle2; i++)
        {
            nLeft = (int)(vUp.x + (i - nUp) * fDeltaLeft);
            nRight = (int)(vRight.x + (i - nMiddle1) * fDeltaRight);

            for (int j = nLeft; j <= nRight; j++)
            {
                nPixelIndex = 3 * (i * OLP_IMG_WIDTH + j);
                nSaturationValue = pHSVImage->pixels[nPixelIndex + 1];
                nSaturationSum += nSaturationValue;
                nHistogramIndex = pHSVImage->pixels[nPixelIndex] / nBucketSize;
                fWeight = 1.0f - 1.0f / (1.0f + 0.0025f * (nSaturationValue * nSaturationValue));
                pHistogram[nHistogramIndex] += 0.4f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize + 1) % nHistogramSize] += 0.2f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize - 1) % nHistogramSize] += 0.2f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize + 2) % nHistogramSize] += 0.1f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize - 2) % nHistogramSize] += 0.1f * fWeight;
            }
        }

        fDeltaLeft = (vLow.x - vLeft.x) / (vLow.y - vLeft.y);

        for (int i = nMiddle2; i < nLow; i++)
        {
            nLeft = (int)(vLeft.x + (i - nMiddle2) * fDeltaLeft);
            nRight = (int)(vRight.x + (i - nMiddle1) * fDeltaRight);

            for (int j = nLeft; j <= nRight; j++)
            {
                nPixelIndex = 3 * (i * OLP_IMG_WIDTH + j);
                nSaturationValue = pHSVImage->pixels[nPixelIndex + 1];
                nSaturationSum += nSaturationValue;
                nHistogramIndex = pHSVImage->pixels[nPixelIndex] / nBucketSize;
                fWeight = 1.0f - 1.0f / (1.0f + 0.0025f * (nSaturationValue * nSaturationValue));
                pHistogram[nHistogramIndex] += 0.4f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize + 1) % nHistogramSize] += 0.2f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize - 1) % nHistogramSize] += 0.2f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize + 2) % nHistogramSize] += 0.1f * fWeight;
                pHistogram[(nHistogramIndex + nHistogramSize - 2) % nHistogramSize] += 0.1f * fWeight;
            }
        }
    }


    // normalize
    float fSum = 0;

    for (int i = 0; i < nHistogramSize; i++)
    {
        fSum += pHistogram[i];
    }

    if (fSum != 0)
    {
        const float fOneBySum = 1.0f / fSum;

        for (int i = 0; i < nHistogramSize; i++)
        {
            pHistogram[i] *= fOneBySum;
        }
    }
    else    // set to equally distributed values
    {
        const float fDummy = 1.0f / (float)nHistogramSize;

        for (int i = 0; i < nHistogramSize; i++)
        {
            pHistogram[i] = fDummy;
        }
    }

}

