/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "ObjectHypothesis.h"

#include <Math/Math2d.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>


// forward declarations
class CSIFTFeatureCalculator;
class CStereoCalibration;
class CCalibration;
class CByteImage;
class CStereoMatcher;


class CFeatureCalculation
{
public:
    CFeatureCalculation();
    ~CFeatureCalculation();

    void GetAllFeaturePoints(const CByteImage* pImageLeftColor, const CByteImage* pImageRightColor, const CByteImage* pImageLeftGrey, const CByteImage* pImageRightGrey, const int nDisparityPointDistance, CStereoCalibration* pStereoCalibration,
                             CSIFTFeatureArray& aAllSIFTPoints, std::vector<CMSERDescriptor3D*>& aAllMSERs, std::vector<CHypothesisPoint*>& aPointsFromDepthImage, CByteImage* pDisparityImage, std::vector<Vec3d>* pAll3DPoints = NULL);

#ifdef OLP_USE_DEPTH_MAP

    void GetAllFeaturePoints(const CByteImage* pImageLeftColor, const CByteImage* pImageLeftGrey, const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointcloud, const int nDisparityPointDistance,
                             CSIFTFeatureArray& aAllSIFTPoints, std::vector<CMSERDescriptor3D*>& aAllMSERs, std::vector<CHypothesisPoint*>& aPointsFromDepthImage, const CCalibration* calibration);
#endif

private:

    void FilterForegroundPoints(const std::vector<CHypothesisPoint*>& aAllPoints, const CByteImage* pForegroundImage, std::vector<CHypothesisPoint*>& aForegroundPoints);

    void CalculateForegroundRatioOfHypothesis(const CObjectHypothesis* pHypothesis, const CByteImage* pForegroundImage, float& fForegroundRatio, int& nNumForegroundPixels);
    static inline void GetEnclosingRectangle(const Vec2d* pPoints, const int nNumPoints, Vec2d& p1, Vec2d& p2, Vec2d& p3, Vec2d& p4, bool& bRotated);
    static inline int CountForegroundPixelsInRectangle(const Vec2d vMiddleMin, const Vec2d vMinMiddle, const Vec2d vMaxMiddle, const Vec2d vMiddleMax, const CByteImage* pForegroundImage);

    void GetPointsFromDisparity(const CByteImage* pImageLeftColor, const CByteImage* pImageRightColor, const CByteImage* pImageLeftGrey, const CByteImage* pImageRightGrey, CStereoCalibration* pStereoCalibration,
                                const int nDisparityPointDistance, std::vector<CHypothesisPoint*>& aPointsFromDisparity, CByteImage* pDisparityImage, std::vector<Vec3d>* pAll3DPoints);

    void CalculateSmoothedDisparityImage(float* pInputDisparity, float* pSmoothedDisparity, const int nRadius = 1);

#ifdef OLP_USE_DEPTH_MAP
    static Vec3d GetCorresponding3DPoint(const Vec2d point2D, const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointcloud);
#endif

    static const int m_nMaxNumInterestPoints = 4000;
    Vec2d* m_pInterestPoints;

    CSIFTFeatureCalculator* m_pSIFTFeatureCalculator;
};

