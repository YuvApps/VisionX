/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "ObjectHypothesis.h"

// IVT
#include <Math/Math3d.h>

// stdlib
#include <cmath>
#include <string>

class CCalibration;
class CByteImage;


namespace COLPTools
{
    void InterpolateRotation(const Mat3d m1, const Mat3d m2, const float fWeight, Mat3d& mResult);
    void InterpolateTransformation(const Mat3d m1, const Vec3d v1, const Mat3d m2, const Vec3d v2, const float fWeight, Mat3d& mResult, Vec3d& vResult);

    void ClusterXMeans(const std::vector<CHypothesisPoint*>& aPoints, const int nMinNumClusters, const int nMaxNumClusters, const float fBICFactor, std::vector<std::vector<CHypothesisPoint*> >& aaPointClusters);
    void ClusterXMeans(const std::vector<Vec3d>& aPoints, const int nMinNumClusters, const int nMaxNumClusters, const float fBICFactor, std::vector<std::vector<Vec3d> >& aaPointClusters, std::vector<std::vector<int> >& aaOldIndizes);

    void FilterForegroundPoints(const std::vector<CHypothesisPoint*>& aAllPoints, const CByteImage* pForegroundImage, const CCalibration* calibration, std::vector<CHypothesisPoint*>& aForegroundPoints);
    bool PointIsInForeground(const CHypothesisPoint* pPoint, const CByteImage* pForegroundImage, const CCalibration* calibration);
    bool PointIsInForeground(const Vec3d vPoint, const CByteImage* pForegroundImage, const CCalibration* calibration);
    void CalculateForegroundRatioOfHypothesis(const CObjectHypothesis* pHypothesis, const CByteImage* pForegroundImage, const CCalibration* calibration, float& fForegroundRatio, int& nNumForegroundPixels);
    void GetEnclosingRectangle(const Vec2d* pPoints, const int nNumPoints, const bool bUseSecondMaxPoints, Vec2d& p1, Vec2d& p2, Vec2d& p3, Vec2d& p4, bool& bRotated);
    int CountForegroundPixelsInRectangle(const Vec2d vMiddleMin, const Vec2d vMinMiddle, const Vec2d vMaxMiddle, const Vec2d vMiddleMax, const CByteImage* pForegroundImage);

    void GetMeanAndVariance(const std::vector<CHypothesisPoint*>& pHypothesisPoints, Vec3d& vMean, float& fVariance);
    void GetMeanAndVariance(const std::vector<Vec3d>& aPoints, Vec3d& vMean, float& fVariance);
    void RemoveOutliers(std::vector<Vec3d>& aPoints, const float fStdDevFactor, std::vector<int>* pOldIndices);
    void RemoveOutliers(std::vector<CHypothesisPoint*>& aPoints, const float fStdDevFactor);

    bool CheckIfPointIsAlreadyInHypothesis(const Vec3d vPoint, const CObjectHypothesis* pHypothesis);

    void ConvertRGB2HSV(const unsigned char r, const unsigned char g, const unsigned char b, unsigned char& h, unsigned char& s, unsigned char& v);
    void CreateHueAndSaturationHistogram(const CObjectHypothesis* pHypothesis, std::vector<float>& aHueHistogram, std::vector<float>& aSaturationHistogram);
    void CreateHueAndSaturationHistogramInWindow(const CByteImage* pHSVImage, const int nMinX, const int nMinY, const int nMaxX, const int nMaxY, std::vector<float>& aHueHistogram, std::vector<float>& aSaturationHistogram);
    void SmoothHistogram(std::vector<float>& aHistogram);
    void NormalizeHistogram(std::vector<float>& aHistogram);

    float GetHistogramDistanceL1(const std::vector<float>& aHistogram1, const std::vector<float>& aHistogram2);
    float GetHistogramDistanceL2(const std::vector<float>& aHistogram1, const std::vector<float>& aHistogram2);
    float GetHistogramDistanceX2(const std::vector<float>& aHistogram1, const std::vector<float>& aHistogram2);

    void DrawCross(CByteImage* pGreyImage, int x, int y, int nBrightness);
    void DrawCross(CByteImage* pColorImage, int x, int y, int r, int g, int b);
    void DrawFatCross(CByteImage* pColorImage, int x, int y, int r, int g, int b);

    void CreateObjectSegmentationMask(const CObjectHypothesis* pHypothesis, const CCalibration* calibration, CByteImage*& pForegroundImage);
    void CreateSegmentationProbabilityMap(const CObjectHypothesis* pHypothesis, const CCalibration* calibration, CByteImage*& pProbabilityImage);

    void SetNumberInFileName(std::string& sFileName, int nNumber, int nNumDigits = 4);

    float GetHypothesesIntersectionRatio(const CObjectHypothesis* pHypothesis1, const CObjectHypothesis* pHypothesis2, const CCalibration* calibration);

    void DrawHypothesis(CObjectHypothesis* pHypothesis, const CCalibration* calibration, CByteImage* pImage, const int nNumFusedHypotheses = 1);
    void FillHolesRGB(const CByteImage* pInputImage, CByteImage* pOutputImage, const int nRadius = 1);
    void FillHolesGray(const CByteImage* pInputImage, CByteImage* pOutputImage, const int nRadius = 1);

    // Quicksort
    void SortByPositionZ(std::vector<CHypothesisPoint*>& pHypothesisPoints);
    void SortByPositionZ(std::vector<CHypothesisPoint*>& pHypothesisPoints, const int nLeftLimit, const int nRightLimit);

    void CalculateEdgeImageFromImageAndDisparity(CByteImage* pImageGray, CByteImage* pDisparity, CByteImage* pEdgeImage);
}

