#!/bin/bash

# By Christian Dreher 2019-04-10; edited by Mirko Wächter April 2019; tested on OpenPose 1.4.

#############################################
# Config ####################################
#############################################

# Target OpenPose version (must be a valid Git tag).
openpose_version="v1.4.0"
# Version revision number (to be able to update package even when the lib did not change).
h2t_package_revision="1"


if [ "$#" -ne 1 ]; then
  echo "Usage: $0 INSTALL-DIRECTORY" >&2
  exit 1
fi

scriptDir="$(readlink -f "$(dirname "$0")")"

#############################################
# Clone repo ################################
#############################################

if [ ! -d "./openpose" ]; then
    git clone https://github.com/CMU-Perceptual-Computing-Lab/openpose.git
    cd openpose
    git checkout v1.4.0
else
    cd openpose
    # Warn user if the target Git tag is not checked out, but don't fail.
    if [ "$(git tag --points-at HEAD)" != "${openpose_version}" ]; then
        echo "Warning! Not on desired Git tag '${openpose_version}'. This is untested and will probably fail. If it succeeds, still don't use the deb."
    fi
fi

#############################################
# Apply needed git patches ##################
#############################################

if [ ! -f "cmake_patch_applied" ]; then
    git apply $scriptDir/openpose_cmake.patch && touch "cmake_patch_applied"
fi
if [ ! -f "cmake_patch_applied" ]; then
    echo "Couldn't apply CMake patch! Integrate the patch manually if needed."
    exit
fi

#############################################
# Change to build dir now ###################
#############################################

mkdir build &> /dev/null || true
cd build

#############################################
# CMake #####################################
#############################################

cmakeargs=""
cmakeargs+="-DCaffe_INCLUDE_DIRS=/usr/include/caffe/ "
cmakeargs+="-DCaffe_LIBS=/usr/lib/x86_64-linux-gnu/libcaffe.so "
cmakeargs+="-DBUILD_CAFFE=OFF "
cmakeargs+="-DCMAKE_C_COMPILER=$(which gcc-6) "
cmakeargs+="-DCMAKE_CXX_COMPILER=$(which g++-6) "
cmakeargs+="-DCUDA_HOST_COMPILER=$(which g++-6) "
cmakeargs+="-DCMAKE_BUILD_TYPE=Release "
cmakeargs+="-DDOWNLOAD_BODY_25_MODEL=ON "
cmakeargs+="-DDOWNLOAD_BODY_COCO_MODEL=ON "
cmakeargs+="-DDOWNLOAD_BODY_MPI_MODEL=ON "
cmakeargs+="-DDOWNLOAD_FACE_MODEL=ON "
cmakeargs+="-DDOWNLOAD_HAND_MODEL=ON "
cmakeargs+="-DBUILD_PYTHON=ON "
cmakeargs+="-DWITH_OPENCV_WITH_OPENGL=OFF "
cmakeargs+="-DOPENPOSE_H2T_REVISION=${h2t_package_revision} "
cmakeargs+="-DCMAKE_INSTALL_PREFIX=$1 "
cmake .. ${cmakeargs}

#############################################
# Make ######################################
#############################################

make -j`nproc`
make install
echo "You need to cmake VisionX now with: cmake -DOpenPose_DIR=$1/lib/OpenPose .."
#armarx-dev exec --no-deps VisionX "cd build && cmake -DOpenPose_DIR=$1/lib/OpenPose ."

