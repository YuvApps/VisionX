/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::BlurrinessMetric
 * @author     David Sippel ( uddoe at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/tools/ImageUtil.h>
#include <VisionX/tools/FPSCounter.h>

#include <opencv2/opencv.hpp>

#include <Image/IplImageAdaptor.h>

#include <Eigen/Core>
#include <Eigen/Geometry>


#include <ArmarXCore/observers/DebugObserver.h>
namespace armarx
{
    /**
     * @class BlurrinessMetricPropertyDefinitions
     * @brief
     */
    class BlurrinessMetricPropertyDefinitions:
        public visionx::ImageProcessorPropertyDefinitions
    {
    public:
        BlurrinessMetricPropertyDefinitions(std::string prefix):
            visionx::ImageProcessorPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
            defineOptionalProperty<float>("Framerate", 30.0, "the framerate");
            defineOptionalProperty<float>("ThresholdLaplaceBlur", 250.0, "Threshold value below which the image is considered blurry");
            defineOptionalProperty<float>("ThresholdPerceptualBlur", 0.97, "Threshold value below which the image is considered blurry");
            defineOptionalProperty<std::string>("providerName", "Armar3ImageProvider", "ImageProvider name");

            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
        }
    };

    /**
     * @defgroup Component-BlurrinessMetric BlurrinessMetric
     * @ingroup VisionX-Components
     * A description of the component BlurrinessMetric.
     *
     * @class BlurrinessMetric
     * @ingroup Component-BlurrinessMetric
     * @brief Brief description of class BlurrinessMetric.
     *
     * Detailed description of class BlurrinessMetric.
     */
    class BlurrinessMetric :
        virtual public visionx::ImageProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "BlurrinessMetric";
        }

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // ImageProcessor interface
    protected:
        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onExitImageProcessor() override;
        void process() override;

    private:
        CByteImage** cameraImages;
        std::string providerName;
        boost::mutex imageMutex;

        visionx::ImageProviderInterfacePrx imageProviderPrx;

        DebugObserverInterfacePrx debugObserver;

        visionx::FPSCounter fpsCounter;
        float frameRate, thresholdLaplace, thresholdPerceptual;

        size_t seq;


        double laplaceVarianceBlurrinessMetric(cv::Mat& in);
        double blurringPerceptual(const cv::Mat& src);
        double blurringMarziliano(const cv::Mat& src);
    };
}

