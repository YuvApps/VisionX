/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::BlurrinessMetric
 * @author     David Sippel ( uddoe at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BlurrinessMetric.h"


#include <ArmarXCore/observers/ObserverObjectFactories.h>

using namespace armarx;


armarx::PropertyDefinitionsPtr BlurrinessMetric::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new BlurrinessMetricPropertyDefinitions(
            getConfigIdentifier()));
}


void armarx::BlurrinessMetric::onInitImageProcessor()
{
    providerName = getProperty<std::string>("providerName").getValue();
    usingImageProvider(providerName);

    offeringTopic(getProperty<std::string>("DebugObserverName").getValue());
    frameRate = getProperty<float>("Framerate").getValue();
    thresholdLaplace = getProperty<float>("ThresholdLaplaceBlur").getValue();
    thresholdPerceptual = getProperty<float>("ThresholdPerceptualBlur").getValue();
}

void armarx::BlurrinessMetric::onConnectImageProcessor()
{
    boost::mutex::scoped_lock lock(imageMutex);

    visionx::ImageProviderInfo imageProviderInfo = getImageProvider(providerName);
    imageProviderPrx = getProxy<visionx::ImageProviderInterfacePrx>(providerName);

    cameraImages = new CByteImage*[2];
    cameraImages[0] = visionx::tools::createByteImage(imageProviderInfo);
    cameraImages[1] = visionx::tools::createByteImage(imageProviderInfo);


    debugObserver = getTopic<DebugObserverInterfacePrx>(getProperty<std::string>("DebugObserverName").getValue());

    //enableResultImages(0, imageProviderPrx->getImageFormat().dimension, imageProviderPrx->getImageFormat().type);

    seq = 0;

}

void armarx::BlurrinessMetric::onExitImageProcessor()
{
}

void armarx::BlurrinessMetric::process()
{
    boost::mutex::scoped_lock lock(imageMutex);

    if (!waitForImages(getProperty<std::string>("providerName").getValue(), 1000))
    {
        ARMARX_WARNING << "Timeout while waiting for camera images (>1000ms)";
        return;
    }

    int numImages = getImages(cameraImages);

    if (numImages == 0)
    {
        ARMARX_WARNING << "Didn't receive one image! Aborting!";
        return;
    }



    IplImage* ppIplImages[1] = { IplImageAdaptor::Adapt(cameraImages[0]) };

    cv::Mat image = cv::cvarrToMat(ppIplImages[0]);

    cv::Mat gray_image;
    cv::cvtColor(image, gray_image, CV_BGR2GRAY);

    double blurrinessLaplaceVariance = laplaceVarianceBlurrinessMetric(gray_image);
    double blurrinessPerceptualBlur = blurringPerceptual(gray_image);
    double blurrinessMarziliano = blurringMarziliano(gray_image);

    ARMARX_LOG << deactivateSpam(1) << "blurrinessLaplaceVariance: " << ((blurrinessLaplaceVariance < thresholdLaplace) ? "Blurry" : "Not blurry") << "   Value: " << blurrinessLaplaceVariance;
    ARMARX_LOG << deactivateSpam(1) << "blurrinessPerceptialBlur: " << ((blurrinessPerceptualBlur > thresholdPerceptual) ? "Blurry" : "Not blurry") << "   Value: " << blurrinessPerceptualBlur;
    ARMARX_LOG << deactivateSpam(1) << "blurrinessMarziliano: " << blurrinessMarziliano;

    StringVariantBaseMap debugValues;
    debugValues["BlurrinessValueLaplace"] = new Variant(blurrinessLaplaceVariance);
    debugValues["BlurryLaplace"] = new Variant((blurrinessLaplaceVariance < thresholdLaplace));

    debugValues["BlurrinessValuePerceptual"] = new Variant(blurrinessPerceptualBlur);
    debugValues["BlurryPerceptual"] = new Variant((blurrinessPerceptualBlur > thresholdPerceptual));

    debugValues["BlurrinessValueMarziliano"] = new Variant(blurrinessMarziliano);

    debugValues["seq"] = new Variant((int) seq);
    seq++;

    debugObserver->setDebugChannel("BlurrinessMetric", debugValues);

    CByteImage* resultImages[1] = { IplImageAdaptor::Adapt(ppIplImages[0]) };
    provideResultImages(resultImages);

    if (frameRate > 0.0)
    {
        fpsCounter.assureFPS(frameRate);
    }

}

/**
 * @brief armarx::BlurrinessMetric::laplaceVarianceBlurrinessMetric
 * 'LAPV' algorithm from Pech et al. (2000)
 * decsai.ugr.es/vip/files/conferences/Autofocusing2000.pdf
 * @param in Input image as matrix
 * @return blurriness value
 */
double armarx::BlurrinessMetric::laplaceVarianceBlurrinessMetric(cv::Mat& in)
{
    cv::Mat out;
    cv::Laplacian(in, out, CV_64F);

    cv::Scalar mean, variance;
    cv::meanStdDev(out, mean, variance);

    return (variance.val[0] * variance.val[0]);
}

/**
 * @brief blurringPerceptual
 * @inproceedings{crete2007blur,
      title={The blur effect: Perception and estimation with a new no-reference perceptual blur metric},
      author={Cr{\'e}t{\'e}-Roffet, Fr{\'e}d{\'e}rique and Dolmiere, Thierry and Ladret, Patricia and Nicolas, Marina},
      booktitle={SPIE Electronic Imaging Symposium Conf Human Vision and Electronic Imaging},
      volume={12},
      pages={EI--6492},
      year={2007}
    }
 * @param src Input image as matrix
 * @return blurriness value
 */
double armarx::BlurrinessMetric::blurringPerceptual(const cv::Mat& src)
{
    double blur_index = 0;

    cv::Mat smoothV(src.rows, src.cols, CV_8UC1);
    cv::Mat smoothH(src.rows, src.cols, CV_8UC1);
    cv::blur(src, smoothV, cv::Size(1, 9));
    cv::blur(src, smoothH, cv::Size(9, 1));


    double difS_V = 0, difS_H = 0, difB_V = 0, difB_H = 0;
    double somaV = 0, somaH = 0, varV = 0, varH = 0;

    for (int i = 0; i < src.rows; ++i)
    {

        for (int j = 0; j < src.cols; ++j)
        {

            if (i >= 1)
            {
                difS_V = abs(src.at<uchar>(i, j) - src.at<uchar>(i - 1, j));
                difB_V = abs(smoothV.at<uchar>(i, j) - smoothV.at<uchar>(i - 1, j));
            }

            if (j >= 1)
            {
                difS_H = abs(src.at<uchar>(i, j) - src.at<uchar>(i, j - 1));
                difB_H = abs(smoothH.at<uchar>(i, j) - smoothH.at<uchar>(i, j - 1));
            }

            varV += cv::max(0.0, difS_V - difB_V);
            varH += cv::max(0.0, difS_H - difB_H);
            somaV += difS_V;
            somaH += difS_H;
        }
    }

    blur_index = cv::max((somaV - varV) / somaV, (somaH - varH) / somaH);

    return blur_index;
}


/**
 * @brief blurringMarziliano
 * @ARTICLE{Marziliano04perceptualblur,
    author = {Pina Marziliano and Frederic Dufaux and Stefan Winkler and Touradj Ebrahimi},
    title = {Perceptual blur and ringing metrics: Application to JPEG2000,” Signal Process},
    journal = {Image Commun},
    year = {2004},
    pages = {163--172} }
 * @param src Input image as matrix
 * @return blurriness value
 */
double  armarx::BlurrinessMetric::blurringMarziliano(const cv::Mat& src)
{
    double blur_index = 0;

    cv::Mat edges(src.rows, src.cols, CV_8UC1);
    cv::Mat element = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));

    cv::GaussianBlur(src, edges, cv::Size(3, 3), 0);
    cv::morphologyEx(edges, edges, cv::MORPH_OPEN, element);
    cv::morphologyEx(edges, edges, cv::MORPH_CLOSE, element);
    cv::Sobel(edges, edges, CV_8UC1, 1, 0);

    unsigned int edge_counter = 0;
    int c_start;
    int c_end;
    int k;

    uchar  max = 0;
    uchar  min = 255;

    int length = 0;

    for (int i = 0; i < src.rows; ++i)
    {
        for (int j = 0; j < src.cols; ++j)
        {

            c_start = -1;
            c_end   = -1;

            if (edges.at<uchar>(i, j) > 0)
            {
                edge_counter++;

                /** Left side of the border */
                if (j == 0)
                {
                    c_start = 0;
                }
                else
                {
                    /** Check the first derivate */
                    if ((src.at<uchar>(i, j - 1) - src.at<uchar>(i, j)) > 0)
                    {
                        k   = j;
                        max = src.at<uchar>(i, k);
                        while ((max <= src.at<uchar>(i, k - 1)) && (k > 1))
                        {
                            k--;
                            max = src.at<uchar>(i, k);
                            c_start  = k;
                        }
                    }
                    else /* (src.at<uchar>(i,j-1) - src.at<uchar>(i,j)) < 0 */
                    {
                        k   = j;
                        min = src.at<uchar>(i, k);
                        while ((min >= src.at<uchar>(i, k - 1)) && (k > 1))
                        {
                            k--;
                            min = src.at<uchar>(i, k);
                            c_start  = k;
                        }
                    }
                }

                /** Right side of the border */
                if (j == (src.cols - 1))
                {
                    c_end = src.cols - 1;
                }
                else
                {
                    /** Check the first derivate */
                    if ((src.at<uchar>(i, j + 1) - src.at<uchar>(i, j)) > 0)
                    {
                        k   = j;
                        max = src.at<uchar>(i, k);
                        while (max <= src.at<uchar>(i, k + 1))
                        {
                            k++;
                            max = src.at<uchar>(i, k);
                            c_end  = k;
                            if (k == src.cols - 1)
                            {
                                break;
                            }
                        }
                    }
                    else /* (src.at<uchar>(i,j+1) - src.at<uchar>(i,j)) <= 0 */
                    {
                        k   = j;
                        min = src.at<uchar>(i, k);
                        while (min >= src.at<uchar>(i, k + 1))
                        {
                            k++;
                            min = src.at<uchar>(i, k);
                            c_end  = k;
                            if (k == src.cols - 1)
                            {
                                break;
                            }
                        }
                    }
                }

                assert((c_end - c_start) >= 0);
                length += (c_end - c_start);

            } /* if(edges.at<uchar>(i,j) > 0) */
        }
    }

    if (edge_counter != 0)
    {
        blur_index  = ((double) length) / edge_counter;
    }
    else
    {
        blur_index  = 0;
    }

    return blur_index;
}
