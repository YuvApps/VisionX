armarx_set_target("KinectAndCameraCalibration Lib")
set(LIB_NAME       VisionXKinectAndCameraCalibration)

set(LIBS    VisionXInterfaces
            VisionXCore
            VisionXTools
            VisionXCalibrationCreator
            ArmarXCoreInterfaces
            ArmarXCore
            RobotAPICore
            ArmarXCoreObservers
            RobotAPIRobotStateComponent
)

set(LIB_FILES
    KinectAndCameraCalibration.cpp
    KinectAndCameraCalibrationObserver.cpp
)

set(LIB_HEADERS
    KinectAndCameraCalibration.h
    KinectAndCameraCalibrationObserver.h
)

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

