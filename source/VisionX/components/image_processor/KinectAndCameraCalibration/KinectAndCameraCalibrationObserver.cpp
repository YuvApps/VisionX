/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "KinectAndCameraCalibrationObserver.h"
#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>

namespace visionx
{
    void KinectAndCameraCalibrationObserver::onInitObserver()
    {
        usingTopic("KinectAndCameraCalibration");
    }

    void KinectAndCameraCalibrationObserver::onConnectObserver()
    {
        offerChannel("calibrationResult", "Gives information of KinectAndCameraCalibration process.");
        //offerDataField("calibrationResult", "calibrationFinished", armarx::VariantType::Bool, "Is true when the calibration successfully finished, false if the calibration failed.");
        armarx::Variant defaultValue(0);
        offerDataFieldWithDefault("calibrationResult", "calibrationFinished", defaultValue, "Is 1 when the calibration successfully finished, -1 if the calibration failed.");
        offerConditionCheck("equals", new armarx::ConditionCheckEquals());
    }

    void KinectAndCameraCalibrationObserver::reportCalibrationFinished(bool result, const Ice::Current&)
    {
        if (result)
        {
            ARMARX_IMPORTANT << "Calibration between the kinect and camera successfully finished.";
            setDataField("calibrationResult", "calibrationFinished", 1);
        }
        else
        {
            ARMARX_IMPORTANT << "Calibration between the kinect and camera failed.";
            setDataField("calibrationResult", "calibrationFinished", -1);
        }

        updateChannel("calibrationResult");
    }

}
