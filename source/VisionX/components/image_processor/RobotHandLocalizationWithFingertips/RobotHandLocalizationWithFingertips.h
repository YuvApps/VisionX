/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     David Schiebener (schiebener at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <map>
#include <string>
#include <set>

// VisionX
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/interface/components/HandLocalization.h>

// Core
#include <RobotAPI/libraries/core/LinkedPose.h>

// IVT
#include <Image/ByteImage.h>

class CStereoCalibration;

namespace visionx
{
    // forward declarations
    class CHandLocalisation;
    class CHandModelVisualizer;


    class RobotHandLocalizationWithFingertipsPropertyDefinitions:
        public visionx::ImageProcessorPropertyDefinitions
    {
    public:
        RobotHandLocalizationWithFingertipsPropertyDefinitions(std::string prefix):
            visionx::ImageProcessorPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ImageProviderAdapterName", "ImageProvider", "Ice Adapter name of the image provider");
            defineOptionalProperty<std::string>("RobotStateProxyName", "RobotState", "Ice Adapter name of the robot state proxy");
            defineOptionalProperty<std::string>("HandFrameName", "TCP R", "Name of the robot state frame of the hand that will be localized");
            defineOptionalProperty<std::string>("CameraFrameName", "Eye_Left", "Name of the robot state frame of the primary camera");
        }
    };

    /**
     * RobotHandLocalizationWithFingertips localizes the robot hand using the marker ball and the finger tips.
     *
     * \componentproperties
     * \prop VisionX.RobotHandLocalizationWithFingertips.ImageProviderAdapterName: Name of the
     *       image provider that delivers the camera images.
     * \prop VisionX.RobotHandLocalizationWithFingertips.RobotStateProxyName: Name of the robot state
     *       proxy used to obtain the current robot state.
     * \prop VisionX.RobotHandLocalizationWithFingertips.HandFrameName: Name of the frame in the robot
     *       model that corresponds to the localized hand.
     * \prop VisionX.RobotHandLocalizationWithFingertips.CameraFrameName: Name of the robot state frame of the primary camera
     */
    class RobotHandLocalizationWithFingertips :
        virtual public visionx::ImageProcessor,
        virtual public visionx::HandLocalizationWithFingertipsInterface
    {
    public:
        /**
        * @see Component::getDefaultName()
        */
        std::string getDefaultName() const override
        {
            return "RobotHandLocalizationWithFingertips";
        }

    protected:
        // inherited from VisionComponent
        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onExitImageProcessor() override;

        void process() override;

        /**
        * Returns the hand pose.
        */
        armarx::FramedPoseBasePtr getHandPose(const Ice::Current& c = Ice::emptyCurrent) override;

        /**
        * Returns the positions of the fingertips in this order: thumb, index, middle, ring, pinky.
        */
        visionx::FramedPositionBaseList getFingertipPositions(const Ice::Current& c = Ice::emptyCurrent) override;


        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(
                       new RobotHandLocalizationWithFingertipsPropertyDefinitions(
                           getConfigIdentifier()));
        }

    private:
        std::string providerName;
        ImageProviderInterfacePrx imageProviderPrx;
        ImageFormatInfo imageFormat;
        std::string robotStateProxyName;
        armarx::RobotStateComponentInterfacePrx robotStateProxy;
        std::string handFrameName, cameraFrameName;

        CStereoCalibration* stereoCalibration;
        CByteImage** cameraImages;
        CByteImage** resultImages;
        static const int numResultImages = 4;

        CHandLocalisation* handLocalization;
        CHandModelVisualizer* handModelVisualizer;
        static const bool drawComplexHandModelInResultImage = true;
    };

}

