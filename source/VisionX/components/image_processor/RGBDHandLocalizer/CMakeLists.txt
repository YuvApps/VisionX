armarx_component_set_name("RGBDHandLocalizer")
set(COMPONENT_LIBS
    MemoryXCore
    RobotAPICore
    VisionXPointCloud
    VisionXCore
    ${PCL_FILTERS_LIBRARY}

)

set(SOURCES RGBDHandLocalizer.cpp bloblabeler.cpp)
set(HEADERS RGBDHandLocalizer.h   bloblabeler.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)
