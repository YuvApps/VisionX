/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::TrackingError
 * @author     David Sippel ( uddoe at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/tools/ImageUtil.h>
#include <VisionX/tools/FPSCounter.h>

#include <VisionX/interface/components/TrackingErrorInterface.h>

#include <opencv2/opencv.hpp>

#include <Image/IplImageAdaptor.h>

#include <Eigen/Core>
#include <Eigen/Geometry>


#include <ArmarXCore/observers/DebugObserver.h>
namespace armarx
{
    /**
     * @class TrackingErrorPropertyDefinitions
     * @brief
     */
    class TrackingErrorPropertyDefinitions:
        public visionx::ImageProcessorPropertyDefinitions
    {
    public:
        TrackingErrorPropertyDefinitions(std::string prefix):
            visionx::ImageProcessorPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
            defineOptionalProperty<float>("Framerate", 0.0, "the framerate");
            defineOptionalProperty<std::string>("providerName", "Armar3ImageProvider", "ImageProvider name");
            defineOptionalProperty<bool>("UseChessBoard", true, "use chessboard for feature tracking");
            defineOptionalProperty<std::string>("TrackingErrorTopicName", "TrackingErrorTopic", "TrackingErrorTopicName name");
            defineOptionalProperty<int>("Chessboard.Width", 7, "Chessboard width");
            defineOptionalProperty<int>("Chessboard.Height", 5, "Chessboard height");

            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
        }
    };

    /**
     * @defgroup Component-TrackingError TrackingError
     * @ingroup VisionX-Components
     * A description of the component TrackingError.
     *
     * @class TrackingError
     * @ingroup Component-TrackingError
     * @brief Brief description of class TrackingError.
     *
     * Detailed description of class TrackingError.
     */
    class TrackingError :
        virtual public visionx::ImageProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "TrackingError";
        }

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // ImageProcessor interface
    protected:
        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onExitImageProcessor() override;
        void process() override;

    private:
        CByteImage** cameraImages;
        std::string providerName;
        boost::mutex imageMutex;
        cv::Mat previousImage, old2;
        std::vector< cv::Point2f > previousFeatures, features2_prev;
        bool useChessboard;

        int chessboardWidth, chessboardHeight, imageWidth, imageHeight;

        visionx::ImageProviderInterfacePrx imageProviderPrx;
        TrackingErrorListenerPrx prx;

        DebugObserverInterfacePrx debugObserver;

        visionx::FPSCounter fpsCounter;
        float frameRate;
    };
}

