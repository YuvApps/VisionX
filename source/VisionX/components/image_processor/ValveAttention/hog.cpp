#include "hog.h"

#include "choughcirclesdemo.h"


#include <filesystem>
#include <boost/range/iterator_range.hpp>
#include <boost/algorithm/string/predicate.hpp>

HoG::HoG(int numOfBing, int sampleTime, int numOfHistory, int minRandomWindow, int randomWindowScaleFloor, int randomWindowScaleCeil)
{
    std::cout << "a HoG class constructed" << std::endl;
    numberOfBins = numOfBing;
    sampleTimes = sampleTime;
    numberOfHistory = numOfHistory;
    minimumRandomWindow = minRandomWindow;
    randomWindowScaleRateFloor = randomWindowScaleFloor;
    randomWindowScaleRateCeil = randomWindowScaleCeil;
}

void HoG::loadTrainingData(std::string path)
{

    doLoadTrainingData(path + "t_training/", trainingHistTable, 6);
    doLoadTrainingData(path + "n_training/", trainingHistTableNegative, 12);
}


void HoG::doLoadTrainingData(std::string path, std::vector<std::vector<float> >& histTable, int scaling)
{
    int width, height;
    CByteImage* origin, *greyImage;
    CFloatImage* hogImg, *amplitude;
    std::vector<float> trainHist(numberOfBins, 0);
    std::vector<float> trainHistNorm(numberOfBins, 0);

    std::vector<std::string> fileNames;

    for (std::filesystem::recursive_directory_iterator end, dir(path);
         dir != end; ++dir)
    {
        if (dir->path().extension() == ".bmp")
        {
            fileNames.push_back(dir->path().string());
        }
    }


    for (std::string filename : fileNames)
    {
        CBitmapCapture capture(filename.c_str());

        if (capture.OpenCamera() == false)
        {
            std::cout << "error: could not open camera. Positive examples. " << filename << std::endl;
        }

        origin = new CByteImage(capture.GetWidth(), capture.GetHeight(), CByteImage::eRGB24);
        capture.CaptureImage(&origin);

        for (int j = 2; j < scaling; j++)
        {
            width = capture.GetWidth() / j;
            height = capture.GetHeight() / j;

            CByteImage* image = new CByteImage(width, height, CByteImage::eRGB24);
            greyImage = new CByteImage(width, height, CByteImage::eGrayScale);
            hogImg = new CFloatImage(width, height, 1);
            amplitude = new CFloatImage(width, height, 1);

            ImageProcessor::Resize(origin, image);
            HoG::preprocess(image, greyImage);
            HoG::hogDescriptor(greyImage, hogImg, amplitude);
            HoG::calculateHist(360, numberOfBins, hogImg, &trainHist, amplitude);
            HoG::histogramNorm(&trainHist, &trainHistNorm);
            //          HoG::histogramRotationInvariant(&trainHistNorm, &trainHistNorm);
            histTable.push_back(trainHistNorm);
            delete image;
            delete greyImage;
            delete hogImg;
            delete amplitude;

        }

        delete origin;
        //        std::cout << trainingHistTable.size() << std::endl;
    }
}





void HoG::findSalientRegions(const CByteImage* origin, CFloatImage* saliencyImage)
{
    int width = origin->width;
    int height = origin->height;

    CByteImage tempSaliencyImage(width, height, CByteImage::eGrayScale);
    CByteImage* saliencyImageOrigin = new CByteImage(width, height, CByteImage::eGrayScale);
    CFloatImage* hogImg = new CFloatImage(width, height, 1);
    CFloatImage* amplitude = new CFloatImage(width, height, 1);
    CByteImage* greyImage = new CByteImage(width, height, CByteImage::eGrayScale);

    HoG::preprocess(origin, greyImage);
    HoG::hogDescriptor(greyImage, hogImg, amplitude);
    ImageProcessor::Zero(saliencyImageOrigin);

    //    std::cout << "compair greyImage, hogImg, amplitude" << greyImage->width << " " << greyImage->height << std::endl;
    //    std::cout << hogImg->width << " " << hogImg->height << std::endl;
    //    std::cout << amplitude->width << " " << amplitude->height << std::endl;


    //    std::cout << "step into findSalienRegions Function!" << std::endl;
    for (int i = 0; i < numberOfHistory; i++)
    {
        for (int i = 0; i < saliencyImageOrigin->width * saliencyImageOrigin->height; i++)
        {
            saliencyImageOrigin->pixels[i] /= 3;
        }

        #pragma omp parallel for
        for (int i = 0; i < sampleTimes; i++)
        {
            int sampleWindowSize, windowCenterX, windowCenterY;
            sampleWindowSize = minimumRandomWindow + rand() % (std::min(width, height) - minimumRandomWindow);
            //sampleWindowSize = 200;
            //sampleWindowSize = 250;
            windowCenterX = sampleWindowSize / 2 + rand() % (width - sampleWindowSize);
            windowCenterY = sampleWindowSize / 2 + rand() % (height - sampleWindowSize);
            //Important: adjust fenste windows' size according to the training Images.
            for (int j = randomWindowScaleRateFloor; j < randomWindowScaleRateCeil; j++)
                //for (int j=2; j<3; j++)
            {
                sampleWindowSize /= j;
                CByteImage* sampleWindow = new CByteImage(sampleWindowSize, sampleWindowSize, CByteImage::eGrayScale);
                CFloatImage* sampleHogImg = new CFloatImage(sampleWindow->width, sampleWindow->height, 1);
                CFloatImage* sampleAmplitude = new CFloatImage(sampleWindow->width, sampleWindow->height, 1);
                for (int wIn = windowCenterX - (sampleWindowSize / 2), wOut = 0; wOut < sampleWindowSize; wIn++, wOut++)
                {
                    for (int hIn = windowCenterY - (sampleWindowSize / 2), hOut = 0; hOut < sampleWindowSize; hIn++, hOut++)
                    {
                        sampleWindow->pixels[hOut * sampleWindowSize + wOut] = greyImage->pixels[hIn * width + wIn];
                        sampleHogImg->pixels[hOut * sampleWindowSize + wOut] = hogImg->pixels[hIn * width + wIn];
                        sampleAmplitude->pixels[hOut * sampleWindowSize + wOut] = amplitude->pixels[hIn * width + wIn];
                    }
                }

                std::vector<float> sampleHist(numberOfBins, 0);
                std::vector<float> sampleHistNorm(numberOfBins, 0);

                HoG::calculateHist(360, numberOfBins, sampleHogImg, &sampleHist, sampleAmplitude);
                HoG::histogramNorm(&sampleHist, &sampleHistNorm);
                //                HoG::histogramRotationInvariant(&sampleHistNorm, &sampleHistNorm);


                if (useHoGDetector)
                {
                    bool predict = knn(trainingHistTable, trainingHistTableNegative, sampleHistNorm);
                    if (predict)
                    {
                        // set saliencyImage
                        for (int j = -1; j <= 1; j++)
                        {
                            for (int k = -1; k <= 1; k++)
                            {
                                saliencyImageOrigin->pixels[width * (windowCenterY + j) + (windowCenterX + k)] = 255;
                            }
                        }
                    }
                }

                if (useHoughDetector)
                {

                    CHoughCircles hough;
                    //hough.HoughSaliency(sampleWindow, saliencyImageOrigin, sampleWindowSize, width, height, windowCenterX, windowCenterY);
                    hough.openCVHoughSaliency(sampleWindow, saliencyImageOrigin, sampleWindowSize, width, height, windowCenterX, windowCenterY);

                    //using hough transformation to detect circle
                }


                delete sampleHogImg;
                delete sampleAmplitude;
                delete sampleWindow;
            }
        }

        for (int j = 0; j < 3; j++)
        {
            //ImageProcessor::HistogramStretching(&saliencyImageOrigin, &tempSaliencyImage, 0.0, 1.0);
            ImageProcessor::GaussianSmooth5x5(saliencyImageOrigin, &tempSaliencyImage);
            ImageProcessor::GaussianSmooth5x5(&tempSaliencyImage, saliencyImageOrigin);
        }
        ImageProcessor::HistogramStretching(saliencyImageOrigin, &tempSaliencyImage, 0.0, 1.0);
        ImageProcessor::CopyImage(&tempSaliencyImage, saliencyImageOrigin);

        for (int i = 0; i < saliencyImage->width * saliencyImage->height; i++)
        {
            saliencyImage->pixels[i] = (float)saliencyImageOrigin->pixels[i] / 255;
            //std::cout << "pixel is " << saliencyImage->pixels[i] << std::endl;
        }
    }

    delete hogImg;
    delete amplitude;
    delete greyImage;
    delete saliencyImageOrigin;
}

bool HoG::hogDescriptor(CByteImage* origin, CFloatImage* outImage, CFloatImage* amplitude)
{
    int width, height;
    width = origin->width;
    height = origin->height;
    CShortImage sobelXImage(width, height);
    CShortImage sobelYImage(width, height);
    // CFloatImage hogImage(width, height, 1);

    ImageProcessor::SobelX(origin, &sobelXImage, false);
    ImageProcessor::SobelY(origin, &sobelYImage, false);

    for (int i = 0; i < width * height; i++)
    {
        //std::cout << (int)pImage.pixels[i] << std::endl;

        outImage->pixels[i] = atan2((float)sobelXImage.pixels[i], (float)sobelYImage.pixels[i]);
        outImage->pixels[i] = (outImage->pixels[i] * 180) / M_PI;
        if (outImage->pixels[i] < 0)
        {
            outImage->pixels[i] += 360;
        }
        amplitude->pixels[i] = sqrt((float)sobelXImage.pixels[i] * (float)sobelXImage.pixels[i] + (float)sobelYImage.pixels[i] * (float)sobelYImage.pixels[i]);
    }
    return true;
}

void HoG::calculateHist(int range, int bin, CFloatImage* inputImg, std::vector<float>* output, CFloatImage* amplitude)
{
    //    std::cout << "step into the calculateHist function!" << std::endl;
    int index, binRange;
    std::vector<float>::iterator it;
    binRange = ceil((float)range / (float)bin);

    for (it = output->begin(); it < output->end(); it++)
    {
        *it = 0;
    }

    for (int i = 0; i < inputImg->height * inputImg->width; i++)
    {
        index = inputImg->pixels[i] / binRange;
        output->at(index) = output->at(index) + amplitude->pixels[i];
    }
}

void HoG::histogramNorm(std::vector<float>* input, std::vector<float>* normOutput)
{
    int sum = 0;
    for (size_t i = 0; i < normOutput->size(); i++)
    {
        normOutput->at(i) = 0;
    }
    for (size_t i = 0; i < input->size(); i++)
    {
        sum = sum + input->at(i);
    }
    for (size_t i = 0; i < input->size(); i++)
    {
        normOutput->at(i) = input->at(i) / sum;
    }
}

bool HoG::knn(std::vector<std::vector<float> > trainingHistTable, std::vector<std::vector<float> > trainingHistTableNegative, std::vector<float> testWindow)
{
    float nearstDistance, secondDistance, thirdDistance;
    int sum;
    //    std::pair <std::vector<float>, int> nearstNeighbor, secondNeighbor, thirdNeighbor;
    //    nearstNeighbor.second = -1;
    //    secondNeighbor.second = -1;
    //    thirdNeighbor.second = -1;
    nearstDistance = std::numeric_limits<int>::max();
    secondDistance = std::numeric_limits<int>::max();
    thirdDistance = std::numeric_limits<int>::max();

    std::pair <std::vector<float>*, int> nearstNeighborPtr, secondNeighborPtr, thirdNeighborPtr;
    nearstNeighborPtr.second = -1;
    secondNeighborPtr.second = -1;
    thirdNeighborPtr.second = -1;

    //    findThreeNearstNeighbors(testWindow, trainingHistTable, 1, &nearstNeighbor, &secondNeighbor, &thirdNeighbor, &nearstDistance, &secondDistance, &thirdDistance);
    //    findThreeNearstNeighbors(testWindow, trainingHistTableNegative, 0, &nearstNeighbor, &secondNeighbor, &thirdNeighbor, &nearstDistance, &secondDistance, &thirdDistance);

    findThreeNearstNeighbors(testWindow, trainingHistTable, 1, &nearstNeighborPtr, &secondNeighborPtr, &thirdNeighborPtr, &nearstDistance, &secondDistance, &thirdDistance);
    findThreeNearstNeighbors(testWindow, trainingHistTableNegative, 0, &nearstNeighborPtr, &secondNeighborPtr, &thirdNeighborPtr, &nearstDistance, &secondDistance, &thirdDistance);

    //   sum = thirdNeighbor.second + secondNeighbor.second + nearstNeighbor.second;
    sum = thirdNeighborPtr.second + secondNeighborPtr.second + nearstNeighborPtr.second;

    if (sum >= 2)
    {
        //if (nearstNeighbor.second == 1 && secondNeighbor.second == 1 && nearstDistance < 0.15)
        if (nearstNeighborPtr.second == 1 && secondNeighborPtr.second == 1 && nearstDistance < 0.15)
        {
            //std::cout << "The window includes a valve!" << std::endl;
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
}


void HoG::preprocess(const CByteImage* origin, CByteImage* outImage)
{
    ImageProcessor::ConvertImage(origin, outImage);
    for (int i = 0; i < 4; i++)
    {
        ImageProcessor::GaussianSmooth5x5(outImage, outImage);

    }
    //    if(!ImageProcessor::HistogramStretching(outImage, outImage))
    //    {
    //        return false;
    //    }
}


float HoG::distanceBetweenPoints(std::vector<float> firstPoint, std::vector<float> secondPoint, int metrics)
{
    float distance = 0;
    float manhattanDistance = 0;
    float multiplicationSumme = 0;
    float squareSummeFirst = 0;
    float squareSummeSecond = 0;
    float result = 0;
    float chiSquare = 0;
    if (firstPoint.size() != secondPoint.size())
    {
        //std::cout << "Two points have different dimensions." << std::endl;
        return -1;
    }
    for (size_t i = 0; i < firstPoint.size(); i++)
    {
        distance += pow(firstPoint[i] - secondPoint[i], 2);
        manhattanDistance += fabs(firstPoint[i] - secondPoint[i]);
        multiplicationSumme += firstPoint[i] * secondPoint[i];
        squareSummeFirst += pow(firstPoint[i], 2);
        squareSummeSecond += pow(secondPoint[i], 2);
        if ((firstPoint[i] + secondPoint[i]) != 0)
        {
            chiSquare += (firstPoint[i] - secondPoint[i]) * (firstPoint[i] - secondPoint[i]) / (firstPoint[i] + secondPoint[i]);
        }
    }

    //std::cout << sqrt(distance) << std::endl;
    switch (metrics)
    {
        case 1:
            //Euclidean Distance
            result = sqrt(distance);
            break;
        case 2:
            //Mean Squared Error
            result = distance;
            break;
        case 3:
            //Manhattan Distance
            result = manhattanDistance;
            break;
        case 4:
            //Cosine Distance
            result = (1 - multiplicationSumme / (sqrt(squareSummeFirst) * sqrt(squareSummeSecond)));
            break;
        case 5:
            result = chiSquare;
            break;
        default:
            result = sqrt(distance);
            break;
    }
    //std::cout << "The distance between two points is: " << result << std::endl;
    return result;
}

//void HoG::findThreeNearstNeighbors(std::vector<float> testWindow, std::vector<std::vector<float> > trainingHistTable, int lable, std::pair <std::vector<float>, int>* nearstNeighbor, std::pair <std::vector<float>, int>* secondNeighbor, std::pair <std::vector<float>, int>* thirdNeighbor, float* nearstDistance, float* secondDistance, float* thirdDistance)
void HoG::findThreeNearstNeighbors(std::vector<float> testWindow, std::vector<std::vector<float> > trainingHistTable, int lable, std::pair <std::vector<float>*, int>* nearstNeighbor, std::pair <std::vector<float>*, int>* secondNeighbor, std::pair <std::vector<float>*, int>* thirdNeighbor, float* nearstDistance, float* secondDistance, float* thirdDistance)
{
    float distance;
    std::vector<std::vector<float> >::iterator hisIt;
    for (hisIt = trainingHistTable.begin(); hisIt < trainingHistTable.end(); hisIt++)
    {
        distance = distanceBetweenPoints(*hisIt, testWindow, 4);
        //std::cout << "The distance between two points is " << distance << std::endl;
        if (distance > *thirdDistance) {}
        else if (distance < *nearstDistance)
        {
            *thirdNeighbor = *secondNeighbor;
            *thirdDistance = *secondDistance;
            *secondNeighbor = *nearstNeighbor;
            *secondDistance = *nearstDistance;
            //nearstNeighbor->first = *hisIt;
            nearstNeighbor->first = &(*hisIt);
            nearstNeighbor->second = lable;
            *nearstDistance = distance;

        }
        else if (distance < *secondDistance)
        {
            *thirdNeighbor = *secondNeighbor;
            *thirdDistance = *secondDistance;
            // secondNeighbor->first = *hisIt;
            secondNeighbor->first = &(*hisIt);
            secondNeighbor->second = lable;
            *secondDistance = distance;
        }
        else
        {
            //thirdNeighbor->first = *hisIt;
            thirdNeighbor->first = &(*hisIt);
            thirdNeighbor->second = lable;
            *thirdDistance = distance;
        }
    }

}

void HoG::histogramRotationInvariant(std::vector<float>* inputHist, std::vector<float>* outputHist)
{
    std::vector<float> newHistogram;
    float max = 0, second = 0;
    for (size_t i = 0; i < inputHist->size(); i++)
    {
        if (inputHist->at(i) > inputHist->at(max))
        {
            max = i;
        }
        else if (inputHist->at(i) > inputHist->at(second))
        {
            second = i;
        }
        //       std::cout << inputHist->at(i) << std::endl;
    }
    //    std::cout << "the index of the maximum is: " << max << " , the value is: " << inputHist->at(max) << std::endl;
    //    std::cout << "the index of the second maximum is: " << second << " , the value is: " << inputHist->at(second) << std::endl;
    //    std::cout << "The difference between the first two max is: " << inputHist->at(max) - inputHist->at(second) << std::endl;

    for (size_t i = max; i < inputHist->size(); i++)
    {
        newHistogram.push_back(inputHist->at(i));
    }
    for (int i = 0; i < max; i++)
    {
        newHistogram.push_back(inputHist->at(i));
    }

    for (size_t i = 0; i < newHistogram.size(); i++)
    {
        outputHist->at(i) = newHistogram.at(i);
    }
    //std::cout << "The number of bin in newHistogram is: " << newHistogram.size() << std::endl;

}

void HoG::setParameters(bool useHough, bool useHoG)
{
    useHoughDetector = useHough;
    useHoGDetector = useHoG;

}
