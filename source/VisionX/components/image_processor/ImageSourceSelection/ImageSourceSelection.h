/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ImageSourceSelection
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <VisionX/interface/components/ImageSourceSelectionInterface.h>
#include <VisionX/interface/components/Calibration.h>


#include <VisionX/core/ImageProcessor.h>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

#include <mutex>

namespace armarx
{

    class StereoResultImageProvider;

    static std::vector<std::string> stringToVector(std::string propertyValue)
    {
        std::vector<std::string> result;
        boost::split(result, propertyValue, boost::is_any_of("\t ,"),  boost::token_compress_on);
        return result;
    }



    /**
     * @class ImageSourceSelectionPropertyDefinitions
     * @brief
     */
    class ImageSourceSelectionPropertyDefinitions:
        public visionx::ImageProcessorPropertyDefinitions
    {
    public:
        ImageSourceSelectionPropertyDefinitions(std::string prefix):
            visionx::ImageProcessorPropertyDefinitions(prefix)
        {
            defineOptionalProperty<visionx::ImageDimension>("TargetDimension", visionx::ImageDimension(640, 480), "")
            .setCaseInsensitive(true)
            .map("320x240",     visionx::ImageDimension(320,  240))
            .map("640x480",     visionx::ImageDimension(640,  480))
            .map("800x600",     visionx::ImageDimension(800,  600))
            .map("768x576",     visionx::ImageDimension(768,  576))
            .map("1024x768",    visionx::ImageDimension(1024, 768))
            .map("1280x720",    visionx::ImageDimension(1280, 960))
            .map("1280x960",    visionx::ImageDimension(1280, 960))
            .map("1280x1024",   visionx::ImageDimension(1280, 960))
            .map("1600x1200",   visionx::ImageDimension(1600, 1200))
            .map("1920x1080",   visionx::ImageDimension(1920, 1080))
            .map("1920x1200",   visionx::ImageDimension(1920, 1200));
            defineOptionalProperty<int>("NumberOfImages", 0, "If set to >0, this number of images will be reported. Otherwise, the number of images of the first image provider will be used.");

            defineOptionalProperty<std::string>("resultProviderName",  "Armar3ImageProvider", "Names of the result image provider");
            defineOptionalProperty<std::string>("defaultProviderName",  "RCImageProvider", "Name of the image provider to be used when no provider is set.");
            PropertyDefinition<std::vector<std::string>>::PropertyFactoryFunction f = &stringToVector;
            //            defineOptionalProperty<std::vector<std::string>>("providerNames", {"Armar3WideImageProvider", "Armar3FovealImageProvider"}, "Names of the image providers").setFactory(f);

        }
    };

    /**
     * @defgroup Component-ImageSourceSelection ImageSourceSelection
     * @ingroup VisionX-Components
     * A description of the component ImageSourceSelection.
     *
     * @class ImageSourceSelection
     * @ingroup Component-ImageSourceSelection
     * @brief Brief description of class ImageSourceSelection.
     *
     * Detailed description of class ImageSourceSelection.
     */
    class ImageSourceSelection :
        virtual public visionx::ImageProcessor,
        virtual public ImageSourceSelectionInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "ImageSourceSelection";
        }

        void setImageSource(const std::string& imageSource, int relativeTimeoutMs, const Ice::Current& c) override;

        std::string getCurrentImageSource(const Ice::Current& c = Ice::emptyCurrent) override
        {
            ScopedRecursiveLock lock(imageSourceMutex);
            return providerTimeouts.empty() ? defaultProvider : providerTimeouts.begin()->first;
        }

    protected:
        void setImageSource(const std::string& imageSource);
        void removeImageSource(const std::string& imageSource);
        /**
         * enable stereo result images
         */
        void enableStereoResultImages(visionx::ImageDimension imageDimension, visionx::ImageType imageType);

        /**
         * @see visionx::ImageProcessor::onInitImageProcessor()
         */
        void onInitImageProcessor() override;

        /**
         * @see visionx::ImageProcessor::onConnectImageProcessor()
         */
        void onConnectImageProcessor() override;

        /**
         * @see visionx::ImageProcessor::onExitImageProcessor()
         */
        void onExitImageProcessor() override;

        /**
         * @see visionx::ImageProcessor::process()
         */
        void process() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        bool addImageProvider(const std::string& providerName)
        {
            return addImageProvider(providerName, imageDisplayType, imageProviderInfo);
        }
        bool addImageProvider(const std::string& providerName, visionx::ImageType imageDisplayType, visionx::ImageProviderInfo& imageProviderInfo);
    private:

        visionx::ImageDimension targetDimension;
        float scaleFactorX, scaleFactorY;

        RecursiveMutex mutex, imageSourceMutex;

        std::map<std::string, visionx::CByteImageUPtrVec > cameraImages;
        visionx::CByteImageUPtrVec resultCameraImages;
        int numImages;

        //        std::vector<std::string> providers;
        std::string defaultProvider;
        //        std::string activeProvider;
        visionx::ImageType imageDisplayType;
        visionx::ImageProviderInfo imageProviderInfo;

        std::list<std::pair<std::string, IceUtil::Time>> providerTimeouts;
        void deleteTimedOutProviders();
    };



    class StereoResultImageProvider :
        virtual public visionx::ResultImageProvider,
        virtual public visionx::StereoCalibrationProviderInterface
    {
        friend class ImageSourceSelection;

    public:

        StereoResultImageProvider()
            : visionx::ResultImageProvider()
        {

        }

        visionx::StereoCalibration getStereoCalibration(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return stereoCalibration;
        }

        bool getImagesAreUndistorted(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return imagesAreUndistorted;
        }

        std::string getReferenceFrame(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return referenceFrame;
        }

        void setStereoCalibration(visionx::StereoCalibration stereoCalibration, bool imagesAreUndistorted, const std::string& referenceFrame)
        {
            this->stereoCalibration = stereoCalibration;
            this->imagesAreUndistorted = imagesAreUndistorted;
            this->referenceFrame = referenceFrame;
            calibrationPrx->reportStereoCalibrationChanged(this->stereoCalibration, this->imagesAreUndistorted, this->referenceFrame);
        }

    protected:

        void onInitImageProvider() override
        {
            visionx::ResultImageProvider::onInitImageProvider();
            offeringTopic("StereoCalibrationInterface");
        }

        void onConnectImageProvider() override
        {
            visionx::ResultImageProvider::onConnectImageProvider();
            calibrationPrx = getTopic<StereoCalibrationUpdateInterfacePrx>("StereoCalibrationInterface");
        }

    private:
        visionx::StereoCalibration stereoCalibration;
        bool imagesAreUndistorted;
        StereoCalibrationUpdateInterfacePrx calibrationPrx;
        std::string referenceFrame;
    };


    using StereoResultImageProviderPtr = IceInternal::Handle<StereoResultImageProvider>;

}

