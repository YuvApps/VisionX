/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::OLPEvaluation
 * @author     David ( david dot schiebener at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "OLPEvaluation.h"

#include <boost/thread/detail/thread.hpp>

#include <ArmarXCore/observers/ObserverObjectFactories.h>


namespace visionx
{

    void OLPEvaluation::onInitComponent()
    {
        usingProxy(getProperty<std::string>("OLPName").getValue());
        usingProxy(getProperty<std::string>("OLPObserverName").getValue());
        usingProxy(getProperty<std::string>("ImageProviderName").getValue());
        usingProxy(getProperty<std::string>("KinematicUnitName").getValue());
    }


    void evaluateOLP(OLPEvaluation* p, bool testRecognition)
    {
        usleep(500000);

        armarx::NameControlModeMap controlMode;
        controlMode["Neck_1_Pitch"] = armarx::ePositionControl;
        controlMode["Cameras"] = armarx::ePositionControl;
        p->kinematicUnitProxy->switchControlMode(controlMode);
        armarx::NameValueMap jointValues;
        jointValues["Neck_1_Pitch"] = 0.3;
        jointValues["Cameras"] = -0.3;
        p->kinematicUnitProxy->setJointAngles(jointValues);

        std::vector<std::string> imageFilePathsLeft, imageFilePathsRight;

        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/01-Cheez-It/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/01-Cheez-It/snapshot_right_0000.bmp");
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/06-Master-Chef/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/06-Master-Chef/snapshot_right_0000.bmp");
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/08a-Pringles-standing/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/08a-Pringles-standing/snapshot_right_0000.bmp");
        imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/09-Frenchs-Mustard/snapshot_left_0000.bmp");
        imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/09-Frenchs-Mustard/snapshot_right_0000.bmp");
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/11-Banana/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/11-Banana/snapshot_right_0000.bmp");

        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/14-Lemon/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/14-Lemon/snapshot_right_0000.bmp");
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/17-Orange/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/17-Orange/snapshot_right_0000.bmp");
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/22-Pitcher/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/22-Pitcher/snapshot_right_0000.bmp");
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/25-Bowl/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/25-Bowl/snapshot_right_0000.bmp");
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/31-Mug/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/31-Mug/snapshot_right_0000.bmp");

        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/36-Marker/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/36-Marker/snapshot_right_0000.bmp");
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/39-Padlock/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/39-Padlock/snapshot_right_0000.bmp");
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/42-Screwdriver/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/42-Screwdriver/snapshot_right_0000.bmp");
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/46-Clamp/snapshot_left_0000.bmp");                  //<-----
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/46-Clamp/snapshot_right_0000.bmp");                //<-----
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/53-Mini-Soccer-Ball/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/53-Mini-Soccer-Ball/snapshot_right_0000.bmp");

        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/55-Tennis-Ball/snapshot_left_0000.bmp");            //<-----
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/55-Tennis-Ball/snapshot_right_0000.bmp");          //<-----
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/58-Marbles/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/58-Marbles/snapshot_right_0000.bmp");
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/62-Cup-Lila/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/62-Cup-Lila/snapshot_right_0000.bmp");
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/66-Rope/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/66-Rope/snapshot_right_0000.bmp");
        //    imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/70-Colored-Wood-Blocks/snapshot_left_0000.bmp");
        //    imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/70-Colored-Wood-Blocks/snapshot_right_0000.bmp");

        //imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/76-Timer/snapshot_left_0000.bmp");
        //imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/76-Timer/snapshot_right_0000.bmp");



        if (testRecognition)
        {
            ARMARX_IMPORTANT_S << "Starting recognition evaluation";

            imageFilePathsLeft.clear();
            imageFilePathsLeft.push_back("/homes/staff/schieben/datalog/YCB/Head/reco-test/img_pos/snapshot_left_0000.bmp");
            imageFilePathsRight.push_back("/homes/staff/schieben/datalog/YCB/Head/reco-test/img_pos/snapshot_right_0000.bmp");

            std::vector<std::string> objectNames;
            objectNames.push_back("Orange");

            for (size_t objectNumber = 0; objectNumber < imageFilePathsLeft.size(); objectNumber++)
            {
                p->imageProviderProxy->setImageFilePath(imageFilePathsLeft.at(objectNumber), imageFilePathsRight.at(objectNumber));

                usleep(5000000);

                for (int i = 0; i < 6; i++)
                {
                    p->olpProxy->recognizeObject(objectNames.at(objectNumber));

                    usleep(1000000);
                    ARMARX_INFO_S << "Loading image number " << i + 1;
                    p->imageProviderProxy->loadNextImage();
                    usleep(3000000);
                }
            }

            ARMARX_IMPORTANT_S << "Finished recognition evaluation";
        }
        else
        {
            ARMARX_IMPORTANT_S << "Starting segmentation evaluation";

            for (size_t objectNumber = 0; objectNumber < imageFilePathsLeft.size(); objectNumber++)
            {
                p->imageProviderProxy->setImageFilePath(imageFilePathsLeft.at(objectNumber), imageFilePathsRight.at(objectNumber));

                usleep(5000000);

                int olpStatus = 0;

                // create initial hypotheses

                p->olpProxy->CreateInitialObjectHypotheses();

                usleep(1000000);
                p->imageProviderProxy->loadNextImage();
                usleep(3000000);

                do
                {
                    usleep(100000);
                    olpStatus = p->olpObserverProxy->getDataField(new armarx::DataFieldIdentifierBase("ObjectLearningByPushingObserver", "objectHypotheses", "initialHypothesesCreated"))->getInt();
                }
                while (olpStatus == 0);

                if (olpStatus == -1)
                {
                    ARMARX_WARNING_S << "Creation of initial hypotheses failed, skipping this object";
                    continue;
                }

                // only for LCCP evaluation
                //ARMARX_IMPORTANT_S << "Skipping and continuing with next scene for LCCP evaluation";
                //continue;


                // validate initial hypotheses

                p->olpProxy->ValidateInitialObjectHypotheses();

                usleep(1000000);
                p->imageProviderProxy->loadNextImage();
                usleep(3000000);

                do
                {
                    usleep(100000);
                    olpStatus = p->olpObserverProxy->getDataField(new armarx::DataFieldIdentifierBase("ObjectLearningByPushingObserver", "objectHypotheses", "hypothesesValidated"))->getInt();
                }
                while (olpStatus == 0);

                if (olpStatus == -1)
                {
                    ARMARX_WARNING_S << "Validation of initial hypotheses failed, skipping this object";
                    continue;
                }


                // revalidate hypotheses

                for (int i = 2; i <= 5; i++)
                {
                    ARMARX_INFO_S << "Loading image number " << i;

                    p->olpProxy->RevalidateConfirmedObjectHypotheses();

                    usleep(1000000);
                    p->imageProviderProxy->loadNextImage();
                    usleep(3000000);

                    do
                    {
                        usleep(100000);
                        olpStatus = p->olpObserverProxy->getDataField(new armarx::DataFieldIdentifierBase("ObjectLearningByPushingObserver", "objectHypotheses", "hypothesesValidated"))->getInt();
                    }
                    while (olpStatus == 0);

                    if (olpStatus == -1)
                    {
                        ARMARX_WARNING_S << "Validation of hypotheses failed, skipping this object";
                        break;
                    }
                }

                ARMARX_IMPORTANT_S << "Finished segmentation of object " << objectNumber + 1 << " of " << imageFilePathsLeft.size();
            }

            ARMARX_IMPORTANT_S << "Finished segmenting all objects";
        }
    }


    void OLPEvaluation::onConnectComponent()
    {
        olpProxy = getProxy<ObjectLearningByPushingInterfacePrx>(getProperty<std::string>("OLPName").getValue());
        olpObserverProxy = getProxy<ObjectLearningByPushingListenerPrx>(getProperty<std::string>("OLPObserverName").getValue());
        imageProviderProxy = getProxy<ImageFileSequenceProviderInterfacePrx>(getProperty<std::string>("ImageProviderName").getValue());
        kinematicUnitProxy = getProxy<armarx::KinematicUnitInterfacePrx>(getProperty<std::string>("KinematicUnitName").getValue());
        bool testRecognition = getProperty<bool>("TestRecognition").getValue();

        boost::thread t(evaluateOLP, this, testRecognition);
    }


    void OLPEvaluation::onDisconnectComponent()
    {

    }


    void OLPEvaluation::onExitComponent()
    {

    }


    armarx::PropertyDefinitionsPtr OLPEvaluation::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new OLPEvaluationPropertyDefinitions(getConfigIdentifier()));
    }
}
