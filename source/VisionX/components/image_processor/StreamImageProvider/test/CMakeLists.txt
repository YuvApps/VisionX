
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore StreamImageProvider)
 
armarx_add_test(StreamImageProviderTest StreamImageProviderTest.cpp "${LIBS}")