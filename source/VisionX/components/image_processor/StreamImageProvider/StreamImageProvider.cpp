/**
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    TabletTeleoperation::ArmarXObjects::StreamImageProvider
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "StreamImageProvider.h"

// VisionX
#include <VisionX/tools/TypeMapping.h>

using namespace armarx;





void StreamImageProvider::setReceiver(StreamReceiverPtr receiver)
{
    this->streamReceiver = receiver;
}

void StreamImageProvider::onConnectComponent()
{

    setNumberImages(streamReceiver->getNumberOfImages());

    int imgWidth = 0;
    int imgHeight = 0;
    int imgType = 0;

    streamReceiver->getImageInformation(imgWidth, imgHeight, imgType);

    // ARMARX_WARNING << "image imformation : w: " << imgWidth << " h: " << imgHeight << " t: " << imgType ;

    visionx::ImageType colorType = visionx::ImageType(imgType);
    CByteImage::ImageType byteType =  visionx::tools::convert(colorType);


    //ARMARX_WARNING << "expected : x: " << visionx::eRgb << " y: " << CByteImage::eRGB24;
    //ARMARX_WARNING << "colorType : " << colorType << " byteType " << byteType ;

    setImageFormat(visionx::ImageDimension(imgWidth, imgHeight), colorType);
    ARMARX_INFO << "Images: " << getNumberImages();
    for (int i = 0; i < getNumberImages(); ++i)
    {
        images.push_back(new CByteImage(imgWidth, imgHeight,  byteType));
    }


    ImageProvider::onConnectComponent();
}

void armarx::StreamImageProvider::onInitImageProvider()
{
    // set imageprovider properties
    imagePullTask = new PeriodicTask<StreamImageProvider>(this, &StreamImageProvider::pullImages, 30, true, "PullImages");
    usingProxy("StreamReceiver");
}

void armarx::StreamImageProvider::onConnectImageProvider()
{
    imagePullTask->start();
}

void armarx::StreamImageProvider::onExitImageProvider()
{
    imagePullTask->stop();

    for (auto& i : images)
    {
        delete i;
    }

}

void StreamImageProvider::pullImages()
{
    ARMARX_INFO << deactivateSpam(1)  << "Pulling images";
    streamReceiver->getImages(images);
    provideImages(&images[0]);

}
