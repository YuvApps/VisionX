/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     David Schiebener (schiebener at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// VisionX
#include <VisionX/components/object_perception/ObjectLocalizerProcessor.h>
#include <VisionX/interface/components/ArMarkerLocalizerInterface.h>

// ArUco
#include <aruco/aruco.h>


// forward declarations
class CByteImage;

namespace visionx
{
    class ArMarkerLocalizerPropertyDefinitions:
        public ImageProcessorPropertyDefinitions
    {
    public:
        ArMarkerLocalizerPropertyDefinitions(std::string prefix):
            ImageProcessorPropertyDefinitions(prefix)
        {
            defineOptionalProperty<float>("MarkerSize", 40.0, "The side length of the marker(s)");
            defineOptionalProperty<std::string>("ReferenceFrameName", "DepthCamera", "Name of the ReferenceFrameName");
            defineOptionalProperty<std::string>("AgentName", "Armar6", "name of the agent");
            defineOptionalProperty<std::string>("ImageProviderName", "ImageProvider", "name of the image provider to use");
        }
    };


    /**
     * ArMarkerLocalizer uses CTexturedRecognition of IVTRecognition in order to recognize and localize objects.
     * The object data is read from PriorKnowledge and CommonStorage via MemoryX.
     * The object localization is invoked automatically by the working memory if the object has been requested there.
     */
    class ArMarkerLocalizer:
        virtual public ArMarkerLocalizerInterface,
        virtual public ImageProcessor
    {
    public:
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return armarx::PropertyDefinitionsPtr(new ArMarkerLocalizerPropertyDefinitions(getConfigIdentifier()));
        }

        /**
        * @see Component::getDefaultName()
        */
        std::string getDefaultName() const override
        {
            return "ArMarkerLocalizer";
        }

    protected:

        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onDisconnectImageProcessor() override {}
        void onExitImageProcessor() override {}


        void process() override;

        ///**
        // * Add object class to textured object recognition.
        // *
        // * @param objectClassEntity entity containing all information available for the object class
        // * @param fileManager GridFileManager required to read files associated to prior knowledge from the database.
        // *
        // * @return success of adding this entity to the TexturedObjectDatabase
        // */
        //bool addObjectClass(const memoryx::EntityPtr& objectClassEntity, const memoryx::GridFileManagerPtr& fileManager) override;

        ///**
        // * localizes textured object instances
        // *
        // * @param objectClassNames names of the class to localize
        // * @param cameraImages the two input images
        // * @param resultImages the two result images. are provided if result images are enabled.
        // *
        // * @return list of object instances
        // */
        //memoryx::ObjectLocalizationResultList localizeObjectClasses(const std::vector<std::string>& objectClassNames, CByteImage** cameraImages, armarx::MetaInfoSizeBasePtr imageMetaInfo, CByteImage** resultImages) override;


    private:
        IceUtil::Time startingTime;
        aruco::CameraParameters arucoCameraParameters;
        aruco::MarkerDetector markerDetector;
        float markerSize;
        std::map<std::string, int> markerIDs;
        std::map<std::string, float> markerSizes;

        CByteImage** cameraImages;

        std::string imageProviderName;
        bool gotAnyImages = false;

        visionx::ArMarkerLocalizationResultList lastLocalizationResult;
        armarx::Mutex resultMutex;

        visionx::ArMarkerLocalizationResultList localizeAllMarkersInternal();

    public:
        // ArMarkerLocalizerInterface interface
        visionx::ArMarkerLocalizationResultList LocalizeAllMarkersNow(const Ice::Current&) override;
        visionx::ArMarkerLocalizationResultList GetLatestLocalizationResult(const Ice::Current&) override;

    };
}

