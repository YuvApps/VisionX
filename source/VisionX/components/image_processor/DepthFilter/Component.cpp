/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::DepthFilter
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <VisionX/components/image_processor/DepthFilter/Component.h>


// STD/STL
#include <exception>
#include <string>

// Boost
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

// IVT
#include <Image/ByteImage.h>

// Ice
#include <IceUtil/Time.h>
using namespace IceUtil;

// ArmarX
#include <VisionX/tools/ImageUtil.h>
using namespace armarx;
using namespace visionx;
using namespace visionx::depthfilter;


namespace
{
    DrawColor24Bit parse_color(std::string color)
    {
        std::vector<std::string> rgb;
        std::vector<unsigned char> rgb_vals;
        boost::split(rgb, color, boost::is_any_of(","));

        for (const std::string& val : rgb)
        {
            const long long int val_check = boost::lexical_cast<long long int>(val);

            if (0 <= val_check and val_check <= 255)
            {
                rgb_vals.push_back(static_cast<unsigned char>(val_check));
            }
        }

        if (rgb_vals.size() != 3)
        {
            throw std::invalid_argument("Format for color must be `R,G,B`, where each of R, G and B "
                                        "is a value between 0 and 255");
        }

        return DrawColor24Bit{rgb_vals[0], rgb_vals[1], rgb_vals[2]};
    }
}


const std::string
depthfilter::Component::default_name = "DepthFilter";


depthfilter::Component::~Component()
{
    // pass
}


void
depthfilter::Component::onInitImageProcessor()
{
    // Input image provider.
    m_ipc_in = getProperty<std::string>("ipc.ImageProviderIn");

    // Output image provider.
    m_ipc_out = getProperty<std::string>("ipc.ImageProviderOut");

    // Threshold and mode.
    {
        const int val = getProperty<int>("conf.DepthThreshold");

        if (val < 0)
        {
            m_mode = replace_mode::undercut_threshold;
            m_threshold = static_cast<unsigned int>(-val);
        }
        else
        {
            m_mode = replace_mode::exceed_threshold;
            m_threshold = static_cast<unsigned int>(val);
        }
    }

    // Replacement colors.
    m_color_depth = parse_color(getProperty<std::string>("conf.ColorDepth"));
    m_color_invalid = parse_color(getProperty<std::string>("conf.ColorInvalid"));
}


void
depthfilter::Component::onConnectImageProcessor()
{
    usingImageProvider(m_ipc_in);
    const bool wait_for_proxy = true;
    ImageProviderInfo provider_info = getImageProvider(m_ipc_in, wait_for_proxy);

    unsigned int num_images = 1;

    if (not getProperty<bool>("conf.RGBOnly"))
    {
        num_images = static_cast<unsigned int>(provider_info.numberImages);
    }

    enableResultImages(
        static_cast<int>(num_images),
        provider_info.imageFormat.dimension,
        provider_info.destinationImageType,
        m_ipc_out
    );

    m_buffer = new CByteImage*[2];
    m_buffer[0] = visionx::tools::createByteImage(provider_info);
    m_buffer[1] = visionx::tools::createByteImage(provider_info);
    m_buffer_single = new CByteImage*[1];
}


void
depthfilter::Component::onDisconnectImageProcessor()
{
    delete m_buffer[0];
    delete m_buffer[1];
    delete[] m_buffer;
    delete[] m_buffer_single;
}


void
depthfilter::Component::onExitImageProcessor()
{
    // pass
}


std::string
depthfilter::Component::getDefaultName() const
{
    return depthfilter::Component::default_name;
}


void
depthfilter::Component::process()
{
    {
        const Time timeout = Time::seconds(1);
        if (not waitForImages(m_ipc_in, timeout))
        {
            ARMARX_WARNING << "Timeout while waiting for images (>" << timeout << ")";
            return;
        }
    }

    // Get images.
    MetaInfoSizeBasePtr info;
    getImages(m_ipc_in, m_buffer, info);

    // Apply depth filter.
    applyDepthFilter(*m_buffer[1], m_threshold, m_mode, m_color_depth, m_color_invalid,
                     *m_buffer[0]);

    // Provide result images.
    if (getProperty<bool>("conf.RGBOnly"))
    {
        m_buffer_single[0] = m_buffer[0];
        provideResultImages(m_buffer_single, info);
    }
    else
    {
        provideResultImages(m_buffer, info);
    }
}


void
depthfilter::Component::applyDepthFilter(
    const CByteImage& depth,
    const unsigned int threshold,
    const replace_mode mode,
    const armarx::DrawColor24Bit& color_depth,
    const armarx::DrawColor24Bit& color_invalid,
    CByteImage& rgb)
{
    const bool noise_resistant = false;
    const unsigned int pixels_size = static_cast<unsigned int>(rgb.width * rgb.height) * 3;

    // Loop over each pixel.
    for (unsigned int pixel_pos = 0; pixel_pos < pixels_size; pixel_pos += 3)
    {
        const unsigned int z_value = static_cast<unsigned int>(visionx::tools::rgbToDepthValue(
                                         depth.pixels[pixel_pos + 0],
                                         depth.pixels[pixel_pos + 1],
                                         depth.pixels[pixel_pos + 2],
                                         noise_resistant
                                     ));

        if (z_value == 0)
        {
            rgb.pixels[pixel_pos + 0] = color_invalid.r;
            rgb.pixels[pixel_pos + 1] = color_invalid.g;
            rgb.pixels[pixel_pos + 2] = color_invalid.b;
        }
        else if ((mode == replace_mode::exceed_threshold and z_value > threshold)
                 or (mode == replace_mode::undercut_threshold and z_value < threshold))
        {
            rgb.pixels[pixel_pos + 0] = color_depth.r;
            rgb.pixels[pixel_pos + 1] = color_depth.g;
            rgb.pixels[pixel_pos + 2] = color_depth.b;
        }
    }
}


PropertyDefinitionsPtr
depthfilter::Component::createPropertyDefinitions()
{
    PropertyDefinitionsPtr defs{new ComponentPropertyDefinitions{getConfigIdentifier()}};

    // Options for inter process communication.
    defs->defineOptionalProperty<std::string>(
        "ipc.ImageProviderIn",
        "ImageProvider",
        "Name of the input image provider (before filter)."
    );
    defs->defineOptionalProperty<std::string>(
        "ipc.ImageProviderOut",
        "ImageProvider2",
        "Name of the output image provider (after filter)."
    );

    // Depth filter configuration.
    defs->defineOptionalProperty<int>(
        "conf.DepthThreshold",
        1000,
        "Depth threshold in [mm].  Positive values: Set all RGB values to `conf.Color` if actual "
        "depth exceeds the depth threshold.  Negative values: Set all RGB values to `conf.Color` "
        "if actual depth undercuts the depth threshold."
    );
    defs->defineOptionalProperty<std::string>(
        "conf.ColorDepth",
        "255,255,255",
        "Color to replace for depth. \nFormat: R,G,B \nColor values: 0-255"
    );
    defs->defineOptionalProperty<std::string>(
        "conf.ColorInvalid",
        "0,0,0",
        "Color to replace for invalid patches. \nFormat: R,G,B \nColor values: 0-255"
    );
    defs->defineOptionalProperty<bool>(
        "conf.RGBOnly",
        false,
        "Provide filtered RGB image only instead of RGB-D image."
    );

    return defs;
}
