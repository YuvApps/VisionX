/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::OpenCVImageStabilizer
 * @author     David Sippel ( uddoe at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/tools/ImageUtil.h>

#include <VisionX/interface/components/OpenCVImageStabilizerInterface.h>

#include <Image/IplImageAdaptor.h>

#include <opencv2/opencv.hpp>

#include <queue>

namespace armarx
{
    /**
     * @class OpenCVImageStabilizerPropertyDefinitions
     * @brief
     */
    class OpenCVImageStabilizerPropertyDefinitions:
        public visionx::ImageProcessorPropertyDefinitions
    {
    public:
        OpenCVImageStabilizerPropertyDefinitions(std::string prefix):
            visionx::ImageProcessorPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");

            defineOptionalProperty<float>("Framerate", 30.0, "the framerate");
            defineOptionalProperty<std::string>("providerName", "Armar3WideImageProvider", "ImageProvider name");
            defineOptionalProperty<bool>("drawFeatures", false, "Draw the detected features");
        }
    };

    /**
     * @defgroup Component-OpenCVImageStabilizer OpenCVImageStabilizer
     * @ingroup VisionX-Components
     * A description of the component OpenCVImageStabilizer.
     *
     * @class OpenCVImageStabilizer
     * @ingroup Component-OpenCVImageStabilizer
     * @brief Brief description of class OpenCVImageStabilizer.
     *
     * Detailed description of class OpenCVImageStabilizer.
     */
    class OpenCVImageStabilizer :
        virtual public visionx::ImageProcessor,
        virtual public OpenCVImageStabilizerInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "OpenCVImageStabilizer";
        }

    protected:
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // ImageProcessor interface
    protected:
        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onExitImageProcessor() override;
        void process() override;

    private:
        visionx::ImageProviderInterfacePrx imageProviderPrx;
        CByteImage** cameraImages;

        cv::Mat prev, prevGrey, prevTransform;
        std::vector< cv::Point2f > prevFeatures;
        std::vector< cv::Vec3d > deltaTransformations, smoothedTrajectory;
        std::queue< cv::Mat > oldImages;

        boost::mutex imageMutex;

        std::string providerName;
        float frameRate;
        bool drawFeatures;

        // OpenCVImageStabilizerInterface interface
    public:
        void resetOpenCVStabilization(const Ice::Current&) override;
    };
}

