
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore UserAssistedSegmenter)
 
armarx_add_test(UserAssistedSegmenterTest UserAssistedSegmenterTest.cpp "${LIBS}")