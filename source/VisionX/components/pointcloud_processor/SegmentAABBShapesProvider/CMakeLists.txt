armarx_component_set_name("SegmentAABBShapesProvider")


find_package(Simox QUIET)
armarx_build_if(Simox_FOUND "Simox not available")

#find_package(PCL 1.8 QUIET)
#armarx_build_if(PCL_FOUND "PCL not available")

find_package(SemanticObjectRelations QUIET)
armarx_build_if(SemanticObjectRelations_FOUND "SemanticObjectRelations not available")


set(COMPONENT_LIBS
    VirtualRobot
    SemanticObjectRelations
#    ${PCL_COMMON_LIBRARY}

    RobotAPICore
    VisionXInterfaces VisionXCore VisionXPointCloud VisionXSemanticObjectRelations
)

set(SOURCES
    SegmentAABBShapesProvider.cpp
)
set(HEADERS
    SegmentAABBShapesProvider.h
)


armarx_add_component("${SOURCES}" "${HEADERS}")


#find_package(MyLib QUIET)
#armarx_build_if(MyLib_FOUND "MyLib not available")
# all target_include_directories must be guarded by if(Xyz_FOUND)
# for multiple libraries write: if(X_FOUND AND Y_FOUND)....
#if(MyLib_FOUND)
#    target_include_directories(SegmentAABBShapesProvider PUBLIC ${MyLib_INCLUDE_DIRS})
#endif()

#if(PCL_FOUND)
#    target_include_directories(SegmentAABBShapesProvider SYSTEM PUBLIC ${PCL_INCLUDE_DIRS})
#    remove_empty_elements(PCL_DEFINITIONS)
#    target_compile_options(SegmentAABBShapesProvider PUBLIC ${PCL_DEFINITIONS})
#endif()

armarx_component_set_name("SegmentAABBShapesProviderApp")
set(COMPONENT_LIBS SegmentAABBShapesProvider)
armarx_add_component_executable(main.cpp)
