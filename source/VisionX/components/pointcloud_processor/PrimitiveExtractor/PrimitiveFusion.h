/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <pcl/common/angles.h>

#include <MemoryX/libraries/memorytypes/segment/EnvironmentalPrimitiveSegment.h>

namespace armarx
{
    /**
     * @class PrimitiveFusion
     * @brief A brief description
     *
     * Detailed Description
     */
    class PrimitiveFusion
    {
    public:
        /**
         * PrimitiveFusion Constructor
         */
        PrimitiveFusion();

        /**
         * PrimitiveFusion Destructor
         */
        ~PrimitiveFusion();

        bool testBoxIntersection(Eigen::Matrix4f leftPose, Eigen::Matrix4f rightPose, Eigen::Vector3f leftExtent, Eigen::Vector3f rightExtent);

        void getIntersectingPrimitives(memoryx::EnvironmentalPrimitiveBaseList& primitives, memoryx::EnvironmentalPrimitiveBasePtr primitive, memoryx::EnvironmentalPrimitiveBaseList& intersectingPrimitives, float eps = 0.0f);

        void getIntersectingSimilarPrimitives(memoryx::EnvironmentalPrimitiveBaseList& primitives, memoryx::EnvironmentalPrimitiveBasePtr primitive, memoryx::EnvironmentalPrimitiveBaseList& intersectingPrimitives, float eps = 0.0f);

        bool isSimilar(memoryx::EnvironmentalPrimitiveBasePtr leftPrimitive, memoryx::EnvironmentalPrimitiveBasePtr rightPrimitive);

        void findBoxPrimitives(memoryx::EnvironmentalPrimitiveBaseList& primitives, std::vector<memoryx::EntityBasePtr>& boxes,  memoryx::EnvironmentalPrimitiveSegmentBasePrx environmentalPrimitiveSegment);

    private:


        Eigen::Vector3f projectVector(Eigen::Vector3f axis, Eigen::Vector3f u);

        bool testPlane(memoryx::EnvironmentalPrimitiveBasePtr leftPrimitive, memoryx::EnvironmentalPrimitiveBasePtr rightPrimitive);
        bool testCylinder(memoryx::EnvironmentalPrimitiveBasePtr leftPrimitive, memoryx::EnvironmentalPrimitiveBasePtr rightPrimitive);

    };
}
