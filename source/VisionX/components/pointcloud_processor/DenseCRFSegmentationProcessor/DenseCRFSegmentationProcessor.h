/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ROBDEKON::ArmarXObjects::DenseCRFSegmentationProcessor
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_ROBDEKON_DenseCRFSegmentationProcessor_H
#define _ARMARX_COMPONENT_ROBDEKON_DenseCRFSegmentationProcessor_H


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

// RemoteGUI
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <VisionX/libraries/PointCloudGraph/voxel_grid_graph_builder.h>
#include <VisionX/libraries/PointCloudGraph/point_cloud_graph.h>
#include <VisionX/libraries/PointCloudGraph/PointXYZRGBLNormal.h>
#include <VisionX/libraries/PointCloudGraph/common.h>

#include <pcl/point_types.h>

#include <boost/graph/filtered_graph.hpp>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include "DenseGraphCRF.hpp"
#include "Common.h"


namespace armarx
{


    /**
     * @class DenseCRFSegmentationProcessorPropertyDefinitions
     * @brief
     */
    class DenseCRFSegmentationProcessorPropertyDefinitions :
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        DenseCRFSegmentationProcessorPropertyDefinitions(std::string prefix) :
            visionx::PointCloudProcessorPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("ProviderName", "PointCloudProvider",
                                                "Name of the point cloud provider.");
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver",
                                                "Name of the topic the DebugObserver listens on");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates",
                                                "Name of the DebugDrawerTopic");
            defineOptionalProperty<std::string>("RemoteGuiName", "RemoteGuiProvider", "Name of the remote gui");
            defineOptionalProperty<int>("NumClasses", 20, "Number of maximal object classes in an image");
            defineOptionalProperty<float>("GroundTruthProbability", 0.5,
                                          "Probability with which the previous labels are correct");
            //            defineOptionalProperty<bool>("IsPreSegmented", false,
            //                                         "States whether the incoming PointCloud is already segmented. "
            //                                         "If false, it will be segmented using random walkers.");
            defineOptionalProperty<bool>("UseVertexOnlyGraph", false,
                                         "If true, the graph is not computed via a voxel grid and"
                                         " therefore has no edges between the vertices. This also means the"
                                         " curvature is not signed in this case.");
            defineOptionalProperty<bool>("UseApproximateVoxels", false,
                                         "If true & UseVertexOnlyGraph is true, the downsampling"
                                         " uses the ApproximateVoxelGrid for downsampling instead of the normal"
                                         "VoxelGrid");
            defineOptionalProperty<bool>("ProvideDebugGraphs", false,
                                         "If true a pointcloud for each timestep and for the"
                                         " segmentation is provided as result cloud");
        }
    };

    /**
     * @defgroup Component-DenseCRFSegmentationProcessor DenseCRFSegmentationProcessor
     * @ingroup ROBDEKON-Components
     * A description of the component DenseCRFSegmentationProcessor.
     *
     * @class DenseCRFSegmentationProcessor
     * @ingroup Component-DenseCRFSegmentationProcessor
     * @brief Brief description of class DenseCRFSegmentationProcessor.
     *
     * Detailed description of class DenseCRFSegmentationProcessor.
     */

    class DenseCRFSegmentationProcessor :
        virtual public visionx::PointCloudProcessor
    {
    public:
        typedef boost::graph_traits<Graph>::vertex_iterator VertexIterator;

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "DenseCRFSegmentationProcessor";
        }

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        void onInitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        void onConnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        void onDisconnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        void onExitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        template<typename TimestampMap>
        struct vertex_timestamp_unequal_filter
        {
            vertex_timestamp_unequal_filter() = default;

            vertex_timestamp_unequal_filter(TimestampMap tsm, double ts) : m_tsm(tsm), m_ts(ts) {}

            template<typename Vertex>
            bool operator()(const Vertex& v) const
            {
                // keep all verticers that are not the one supplied to the constructor
                double current_ts = get(m_tsm, v);
                return abs(m_ts - current_ts) > 0.00001;
            }

            TimestampMap m_tsm;
            double m_ts{};
        };

        template<typename TimestampMap>
        struct vertex_timestamp_equal_filter
        {
            vertex_timestamp_equal_filter() = default;

            vertex_timestamp_equal_filter(TimestampMap tsm, double ts) : m_tsm(tsm), m_ts(ts) {}

            template<typename Vertex>
            bool operator()(const Vertex& v) const
            {
                // keep all verticers that are not the one supplied to the constructor
                double current_ts = get(m_tsm, v);
                return abs(m_ts - current_ts) < 0.00001;
            }

            TimestampMap m_tsm;
            double m_ts;
        };




        double initial_time_;
        GraphPtr current_graph_ptr_;
        GraphWithTimestampPtr persistent_graph_ptr_;

        std::deque<Graph> graph_queue_;
        std::deque<double> timestamp_queue_;

        std::string remote_gui_name_;
        RemoteGuiInterfacePrx remote_gui_;
        SimplePeriodicTask<>::pointer_type gui_task_;
        RemoteGui::TabProxy gui_tab_;

        // gui values
        // use_xxx for the computation of the graph edges
        bool use_xyz_,
             use_normals_,
             use_curvature_,
             use_rgb_,
             use_combined_,
             use_time_;
        // weighting factor of xxx in the calculation of the graph edges
        float xyz_influence_,
              normals_influence_,
              curvature_influence_,
              rgb_influence_,
              time_influence_;
        // resolution of the voxel grid during calculation of the graph using supervoxels
        float voxel_resolution_;
        // number of nearest neighbors for the calculation of the graph using nearest neighbors
        int map_iterations_;
        int num_classes_;
        float gt_prob_;
        bool pre_segmented_, vertex_only_, approximate_voxels_, provide_debug_graphs_;
        armarx::Mutex _graphMutex;

        void computeGraphUsingVoxelGrid(const PointCloudT::Ptr inputCloudPtr);

        void computeVertexOnlyGraph(pcl::PointCloud<PointT>::Ptr input_cloud_ptr);

        void generateRandomClassLabels();

        //        void computeRandomWalkerSegmentation();

        //        void computeEdgeWeights();

        void updatePersistentGraph();

        void addGraphToPersistentGraph(Graph& graph);

        void addCurrentGraphToPersistentGraph();

        void removeGraphFromPersistentGraph(Graph& graph);

        void removeCurrentGraphFromPersistentGraph();

        void removeTimestampFromPersistentGraph(double ts);

        static void copyRGBLToRGBLNormal(PointCloudT& input_cloud, PointCloudWithNormalT& output_cloud);
        static void copyRGBLNormalToRGBL(PointCloudWithNormalT& input_cloud, PointCloudT& output_cloud);

        GraphWithTimestamp retrieveGraphFromPersistentGraph(double ts);

        GraphWithTimestamp retrieveCurrentGraphFromPersistentGraph();

        template<typename FilterT>
        GraphWithTimestamp filterAndCopyPersistentGraph(TimestampMap tsm, FilterT filter)
        {
            ARMARX_TRACE;
            typedef boost::filtered_graph<GraphWithTimestamp, boost::keep_all, FilterT> FilteredGraph;
            typedef typename boost::graph_traits<FilteredGraph>::vertex_iterator FilteredVertexIterator;
            FilteredGraph filtered_graph = boost::make_filtered_graph(*persistent_graph_ptr_, boost::keep_all(),
                                           filter);
            FilteredVertexIterator vi, v_end;
            GraphWithTimestamp temp_graph;
            TimestampMap temp_tsm = boost::get(boost::vertex_timestamp_t(), temp_graph.m_graph);
            ConfidenceMap cm = boost::get(boost::vertex_confidence_t(), persistent_graph_ptr_->m_graph);
            ConfidenceMap temp_cm = boost::get(boost::vertex_confidence_t(), temp_graph.m_graph);
            int num_vertices = 0;
            for (boost::tie(vi, v_end) = boost::vertices(filtered_graph); vi != v_end; ++vi)
            {
                num_vertices++;
                VertexWTsId new_vertex = boost::add_vertex(temp_graph);
                boost::put(temp_tsm, new_vertex, get(tsm, *vi));
                boost::put(temp_cm, new_vertex, get(cm, *vi));
                PointWithNormalT point = filtered_graph.m_g.m_graph.m_point_cloud->points[*vi];
                temp_graph[new_vertex] = point;
            }
            //            ARMARX_INFO << "Filtered Graph has " << num_vertices << " num_vertices and "
            //                        << static_cast<int>(boost::num_vertices(temp_graph)) << " Vertices";
            return temp_graph;
        }

        pcl::PointCloud<pcl::PointXYZL>::ConstPtr selectRandomSeeds();

        Eigen::MatrixXf computeRandomUnaryEnergy(int num_points);

        void provideAllGraphs();


    };
}

#endif
