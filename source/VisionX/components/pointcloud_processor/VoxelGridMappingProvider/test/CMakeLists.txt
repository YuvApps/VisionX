
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore VoxelGridMappingProvider)
 
armarx_add_test(VoxelGridMappingProviderTest VoxelGridMappingProviderTest.cpp "${LIBS}")