/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "Feature.hpp"
#include "Memoizer.hpp"
#include "../gdiam.h"


gdiam_bbox calculateBB(const std::vector<Eigen::Vector3f>& points)
{
    double eps = 0.005;
    //Convert to gdiam format
    int size = points.size();
    gdiam_real* tmpVals = new gdiam_real[size * 3];

    for (int i = 0; i < size * 3; i += 3)
    {
        tmpVals[i + 0] = points[i / 3][0];
        tmpVals[i + 1] = points[i / 3][1];
        tmpVals[i + 2] = points[i / 3][2];
    }

    gdiam_point* tmpPoints = gdiam_convert(tmpVals, size);
    gdiam_bbox bbox = gdiam_approx_mvbb(tmpPoints, size, eps);

    delete[] tmpVals;
    delete[] tmpPoints;

    return bbox;
}

class BBFeature : public Feature
{
public:
    BBFeature() {}
    BBFeature(const std::vector<Eigen::Vector3f>& points)
        : m_bbox(calculateBB(points))
    {
        calculateProperties();
    }
    BBFeature(const std::pair<std::string, std::vector<Eigen::Vector3f>>& points)
        : m_bbox(memoized(calculateBB)(points))
    {
        calculateProperties();
    }
    virtual double compare(const Feature&) const = 0;
protected:
    gdiam_bbox m_bbox;
    double m_len1;
    double m_len2;
    double m_len3;

    double m_longSide;
    double m_shortSide;
    double m_mediumSide;

    double m_area;
    double m_volume;
private:
    void calculateProperties()
    {
        m_len1 = (m_bbox.high_1 - m_bbox.low_1);
        m_len2 = (m_bbox.high_2 - m_bbox.low_2);
        m_len3 = (m_bbox.high_3 - m_bbox.low_3);
        m_area = m_len1 * m_len2 * m_len3;
        m_volume = m_bbox.volume();

        m_longSide = std::max(m_len1, std::max(m_len2, m_len3));
        m_shortSide = std::min(m_len1, std::min(m_len2, m_len3));

        m_mediumSide = m_len1 * m_len2 * m_len3 / (m_longSide * m_shortSide);
    }
};
