/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <map>
#include <string>
#include <vector>
#include <functional>
#include <memory>
#include <utility>

#include "Feature.hpp"

class FeatureCalculator
{
public:
    using Points = std::vector<Eigen::Vector3f>;
    template<class F>
    void addFeature()
    {
        features.push_back(FeaturePtr(new F()));
    }

    // Due to bug in GCC, this won't work in GCC versions smaller than 4.9. See GCC bug 41933

    // template<class F, class... Args>
    // void addFeature(Args... args) {
    //  featureCtors.push_back([&](std::vector<Eigen::Vector3f> &points) {
    //          return F(points, args...);
    //      });
    // }

    std::map<std::string, FeaturePtr> getFeatures(const Points& points)
    {
        std::map<std::string, FeaturePtr> fs;

        for (auto& f : features)
        {
            FeaturePtr feature = f->calculate(points);
            fs[feature->name()] = feature;
        }

        return fs;
    }

    std::map<std::string, FeaturePtr> getFeatures(const std::pair<std::string, Points>& points)
    {
        std::map<std::string, FeaturePtr> fs;

        for (auto& f : features)
        {
            FeaturePtr feature = f->calculate(points);
            fs[feature->name()] = feature;
        }

        return fs;
    }

private:
    std::vector<FeaturePtr> features;
};

