/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <vector>
#include <limits>
#include <math.h>

#include "BBFeature.hpp"

class BBVolume : public BBFeature
{
public:
    BBVolume() {}
    BBVolume(const std::vector<Eigen::Vector3f>& points)
        : BBFeature(points)
    {
        m_name = "BBVolume";
    }

    BBVolume(const std::pair<std::string, std::vector<Eigen::Vector3f>>& points)
        : BBFeature(points)
    {
        m_name = "BBVolume";
    }

    double volume() const
    {
        return m_volume;
    }

    FeaturePtr calculate(const Points& points)
    {
        return FeaturePtr(new BBVolume(points));
    }
    FeaturePtr calculate(const TaggedPoints& points)
    {
        return FeaturePtr(new BBVolume(points));
    }


    double compare(const Feature& other) const
    {
        const BBVolume* casted = dynamic_cast<const BBVolume*>(&other);

        if (casted)
        {
            return std::pow(m_volume - casted->volume(), 2.0);
        }
        else
        {
            return std::numeric_limits<double>::max();
        }
    }

    virtual void serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        AbstractObjectSerializerPtr featureObj;

        //Check if object already has a "features" field, else create a new one
        if (obj->hasElement(FEATURE_FIELD_NAME))
        {
            featureObj = obj->getElement(FEATURE_FIELD_NAME);
            featureObj->setDouble(m_name, m_volume);
        }
        else
        {
            featureObj = obj->createElement();
            featureObj->setDouble(m_name, m_volume);
            obj->setElement(FEATURE_FIELD_NAME, featureObj);
        }
    }

    virtual void deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
        AbstractObjectSerializerPtr featureObj = obj->getElement(FEATURE_FIELD_NAME);
        //Now copy the result array from the DB
        m_volume = featureObj->getDouble(m_name);
    }

    virtual std::ostream& output(std::ostream& out) const
    {
        return out << m_volume;
    }
};

