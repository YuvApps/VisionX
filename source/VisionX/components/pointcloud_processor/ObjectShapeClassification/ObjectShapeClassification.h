/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <map>
#include <string>
#include <set>
#include <vector>

// RobotAPI
#include <RobotAPI/libraries/core/LinkedPose.h>

// Core
#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>

// VisionX
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/interface/components/ObjectShapeClassification.h>

// MemoryX
#include <MemoryX/interface/components/PriorKnowledgeInterface.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/core/GridFileManager.h>

// Boost headers
#include <boost/cstdint.hpp>

//Eigen
#include <Eigen/Core>

#include "Features/FeatureCalculator.hpp"

namespace visionx
{


    class ObjectShapeClassificationPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ObjectShapeClassificationPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("PriorKnowledgeMemoryProxyName", "PriorKnowledge", "Proxy name for the prior knowledge memory component");
        }
    };

    using Point = Eigen::Vector3f;
    using Points = std::vector<Point>;
    using TaggedPoints = std::pair<std::string, Points>;

    class ObjectShapeClassification:
        virtual public visionx::ObjectShapeClassificationInterface,
        virtual public armarx::Component
    {
    public:

        std::string getDefaultName() const
        {
            return "ObjectShapeClassification";
        }

        void onInitComponent();
        void onConnectComponent();


        std::string FindSimilarKnownObject(const ::visionx::types::PointList& segmentedObjectPoints, const ::Ice::Current&);

    protected:
        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions()
        {
            return armarx::PropertyDefinitionsPtr(new ObjectShapeClassificationPropertyDefinitions(getConfigIdentifier()));
        }

    private:
        FeatureCalculator featureCalculator;

        void refetchObjects();
        void checkFeatures();
        Points getPointsFromIV(memoryx::ObjectClassPtr obj);
        void matchToFoundPointCloud(const Points& newPoints, const Points& foundPointCloud);
        TaggedPoints compareToDB(Points& points);

        boost::recursive_mutex mutexEntities;

        memoryx::PriorKnowledgeInterfacePrx memoryPrx;
        memoryx::CommonStorageInterfacePrx databasePrx;
        memoryx::PersistentObjectClassSegmentBasePrx classesSegmentPrx;

        memoryx::GridFileManagerPtr fileManager;

        std::vector<memoryx::ObjectClassPtr> dbObjects;

        std::string settings_priorMemory;

        bool connected;
    };

}



