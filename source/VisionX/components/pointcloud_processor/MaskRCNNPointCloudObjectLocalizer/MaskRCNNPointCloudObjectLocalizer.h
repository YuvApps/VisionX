/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::MaskRCNNPointCloudObjectLocalizer
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <Eigen/Core>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/interface/observers/RequestableService.h>


#include <RobotAPI/interface/core/PoseBase.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <MemoryX/interface/core/ProbabilityMeasures.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <VisionX/components/pointcloud_core/PCLUtilities.h>
#include <VisionX/interface/components/SimpleLocation.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
//#include <pcl/point_types.h>

//#include <pcl/common/transforms.h>

//#include <pcl/io/pcd_io.h>

//#include <pcl/filters/filter.h>
//#include <pcl/filters/passthrough.h>
//#include <pcl/filters/approximate_voxel_grid.h>

//#include <pcl/features/normal_3d.h>
//#include <pcl/features/normal_3d_omp.h>
//#include <pcl/features/fpfh_omp.h>
//#include <pcl/features/shot_omp.h>
//#include <pcl/search/kdtree.h>
//#include <pcl/kdtree/kdtree_flann.h>

//#include <pcl/registration/icp.h>
//#include <pcl/registration/gicp6d.h>

//#include <pcl/correspondence.h>
#include <pcl/recognition/cg/geometric_consistency.h>
#pragma GCC diagnostic pop

#include <limits>

namespace visionx
{


    typedef pcl::PointXYZRGBA PointT;
    typedef pcl::PointXYZRGBL PointL;
    typedef pcl::FPFHSignature33 PointD;
    //typedef pcl::SHOT352 PointD;

    /**
     * @class MaskRCNNPointCloudObjectLocalizerPropertyDefinitions
     * @brief
     */
    class MaskRCNNPointCloudObjectLocalizerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        MaskRCNNPointCloudObjectLocalizerPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("agentName", "Armar6", "Name of the agent for which the sensor values are provided");
            defineOptionalProperty<std::string>("providerName", "MaskRCNNPointCloudProvider", "name of the point cloud provider");
            defineOptionalProperty<std::string>("sourceNodeName", "DepthCamera", "the robot node to use as source coordinate system for the captured point clouds");
            defineOptionalProperty<std::string>("ObjectNameIdMap", "spraybottle:1;screwdriver:2;torch:3;cloth:4;cutter:5;pliers:6;brush:7", "map between object names and mask rcnn names", armarx::PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<uint32_t>("BackgroundLabelId", 0, "Label in the pointcloud for the plane or background.", armarx::PropertyDefinitionBase::eModifiable);

            defineOptionalProperty<std::string>("DebugDrawerTopic", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");

        }
    };

    /**

      http://pointclouds.org/documentation/tutorials/global_hypothesis_verification.php
     * @class MaskRCNNPointCloudObjectLocalizer
     *
     * @ingroup VisionX-Components
     * @brief A brief description
     *
     *
     * Detailed Description
     */
    class MaskRCNNPointCloudObjectLocalizer :
        virtual public armarx::SimpleLocationInterface,
        virtual public PointCloudProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "MaskRCNNPointCloudObjectLocalizer";
        }


        void getLocation(armarx::FramedOrientationBasePtr& orientation, armarx::FramedPositionBasePtr& position, const Ice::Current& c = ::Ice::Current()) override;

        memoryx::ObjectLocalizationResultList localizeObjectClasses(const memoryx::ObjectClassNameList& objectClassNames, const Ice::Current& c = ::Ice::Current()) override;

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        void onInitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        void onConnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        void onDisconnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        void onExitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        boost::mutex pointCloudMutex;


        std::string agentName;

        std::string providerName;
        std::string sourceNodeName;


        boost::mutex localizeMutex;


        armarx::DebugDrawerInterfacePrx debugDrawerPrx;
        armarx::RequestableServiceListenerInterfacePrx serviceTopic;
    };
}

