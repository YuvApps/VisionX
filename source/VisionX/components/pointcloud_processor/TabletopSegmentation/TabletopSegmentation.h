/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ActiveVision::ArmarXObjects::TabletopSegmentation
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_ActiveVision_TabletopSegmentation_H
#define _ARMARX_COMPONENT_ActiveVision_TabletopSegmentation_H




#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/interface/observers/RequestableService.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <VisionX/interface/components/TabletopSegmentationInterface.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>

#include <pcl/point_types.h>

#include <pcl/ModelCoefficients.h>


#include <pcl/common/angles.h>

#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>

#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/surface/convex_hull.h>



namespace armarx
{

    class ServiceProvider : public armarx::RequestableServiceListenerInterface
    {
    public:
        ServiceProvider(const std::string& serviceName, const std::function<void(int)>& callback);

        // RequestableServiceListenerInterface interface
        void requestService(const std::string&, Ice::Int relativeTimeoutMs, const Ice::Current&);
        void setServiceConfig(const std::string&, const StringVariantBaseMap&, const Ice::Current&) {}
    private:
        std::string serviceName;
        std::function<void(int)> callback;
    };
    using ServiceProviderPtr = IceUtil::Handle<ServiceProvider>;

    typedef pcl::PointXYZRGBA PointT;
    typedef pcl::PointXYZRGBL PointL;

    /**
     * @class TabletopSegmentationPropertyDefinitions
     * @brief
     */
    class TabletopSegmentationPropertyDefinitions:
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        TabletopSegmentationPropertyDefinitions(std::string prefix):
            visionx::PointCloudProcessorPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");
            defineOptionalProperty<std::string>("RemoteGuiName", "RemoteGuiProvider", "Name of the remote gui provider");
            defineOptionalProperty<bool>("AlwaysActive", true, "If enabled the component runs always. Otherwise only when requested via the requestable service interface.", armarx::PropertyDefinitionBase::eModifiable);

            defineOptionalProperty<double>("DistanceThreshold", 20.0, "The distance threshold for the plane.");
            defineOptionalProperty<double>("EpsAngle", pcl::deg2rad(10.0), "The eps angle (in rad) for the plane perpendicular to the z-axis. If zero the perpendicular constraint is removed.");

            defineOptionalProperty<bool>("IncludeTabletopPlane", false, "If enabled the points from the tabletop plane will be added as a separate cluster.");
            defineOptionalProperty<bool>("CalculatePlane", true, "Calculate plane");

            defineOptionalProperty<double>("ClusterTolerance", 20.0, "the tolerance used for euclidean cluster extraction.");
            defineOptionalProperty<int>("MinClusterSize", 100, "The minimum size of a cluster.");
            defineOptionalProperty<int>("MaxClusterSize", 25000, "The maximum size of a cluster.");

            defineOptionalProperty<double>("HeightMin", 0.0, "The minimum distance to the plane.");
            defineOptionalProperty<double>("HeightMax", 500.0, "The maximum distance to the plane.");
        }
    };

    /**
     * @defgroup Component-TabletopSegmentation TabletopSegmentation
     * @ingroup VisionX-Components
     * A description of the component TabletopSegmentation.
     *
     * @class TabletopSegmentation
     * @ingroup Component-TabletopSegmentation
     * @brief Brief description of class TabletopSegmentation.
     *
     * Detailed description of class TabletopSegmentation.
     */

    class TabletopSegmentation
        : virtual public visionx::TabletopSegmentationInterface
        , virtual public visionx::PointCloudProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "TabletopSegmentation";
        }

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        void onInitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        void onConnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        void onDisconnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        void onExitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // TabletopSegmentationInterface interface
    public:
        visionx::TabletopSegmentationPlanePtr getTablePlane(const Ice::Current&) override;

    private:
        void extractEuclideanClusters(const pcl::PointCloud<PointT>::ConstPtr& inputCloud, const pcl::PointCloud<PointL>::Ptr& resultCloud);

        void guiTask();
        void guiCreate();
        void guiUpdate(armarx::RemoteGui::TabProxy& tab);

    private:
        DebugDrawerInterfacePrx debugDrawer;
        DebugObserverInterfacePrx debugObserver;
        RemoteGuiInterfacePrx remoteGui;
        RunningTask<TabletopSegmentation>::pointer_type remoteGuiTask;

        pcl::SACSegmentation<PointT> seg;

        bool includeTabletopPlane;
        pcl::EuclideanClusterExtraction<PointT> ec;

        pcl::ExtractIndices<PointT> extract;

        pcl::ExtractPolygonalPrismData<PointT> prism;
        pcl::ConvexHull<PointT> hull;

        ServiceProviderPtr serviceProvider;
        IceUtil::Time nextTimeoutAbs;

        Mutex tablePlaneMutex;
        visionx::TabletopSegmentationPlanePtr tablePlane;

    private: // properties
        std::mutex prop_mutex;

        float prop_DistanceThreshold = 20.0;
        float prop_EpsAngle = pcl::deg2rad(10.0);

        float prop_ClusterTolerance = 20.0;
        int prop_MinClusterSize = 100;
        int prop_MaxClusterSize = 25000;

        float prop_HeightMin = 0.0;
        float prop_HeightMax = 500.0;

        bool prop_IncludeTabletopPlane = false;
        bool prop_CalculatePlane = true;
    };
}

#endif
