/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SegmentRansacShapeExtractor
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <pcl/point_types.h>

#include <SemanticObjectRelations/ShapeExtraction/ShapeExtraction.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>


#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>

#include <VisionX/interface/libraries/SemanticObjectRelations/ShapesTopic.h>
#include <VisionX/interface/components/SegmentRansacShapeExtractor.h>
#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>


namespace visionx
{

    /**
     * @class SegmentRansacShapeExtractorPropertyDefinitions
     * @brief Property definitions of `SegmentRansacShapeExtractor`.
     */
    class SegmentRansacShapeExtractorPropertyDefinitions :
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        SegmentRansacShapeExtractorPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-SegmentRansacShapeExtractor SegmentRansacShapeExtractor
     * @ingroup VisionX-Components
     * A description of the component SegmentRansacShapeExtractor.
     *
     * @class SegmentRansacShapeExtractor
     * @ingroup Component-SegmentRansacShapeExtractor
     * @brief Brief description of class SegmentRansacShapeExtractor.
     *
     * Detailed description of class SegmentRansacShapeExtractor.
     */
    class SegmentRansacShapeExtractor :
        virtual public visionx::PointCloudProcessor,
        virtual public visionx::SegmentRansacShapeExtractorInterface
    {
        /// The used point type.
        using PointT = pcl::PointXYZRGBL;

    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // SegmentRansacShapeExtractorInterface interface
        armarx::semantic::data::ShapeList extractShapes(const Ice::Current& = Ice::emptyCurrent) override;
        armarx::semantic::data::ShapeList getExtractedShapes(const Ice::Current& = Ice::emptyCurrent) override;


    protected:

        /// @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        void onInitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        void onConnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        void onDisconnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        void onExitPointCloudProcessor() override;


        /// @see visionx::PointCloudProcessor::process()
        void process() override;


        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:


        // INPUT

        std::mutex inputPointCloudMutex;

        /// The latest received point cloud.
        pcl::PointCloud<pcl::PointXYZRGBL>::Ptr inputPointCloud;


        // PROCESSING

        /// Locked while process is running.
        std::mutex extractionMutex;

        /// The shape extraction.
        semrel::ShapeExtraction shapeExtraction;

        armarx::SimpleRunningTask<>::pointer_type shapeExtractionTask;


        // RESULT

        struct ExtractedShapes
        {

            /// Protects `extractedShapes` and `extractedShapesIce`.
            std::mutex mutex;

            /// The result of the latest processed point cloud.
            semrel::ShapeList shapes;

            /// Serialized version of `shapes`. Is kept up to date.
            armarx::semantic::data::ShapeList shapesIce;
        };

        ExtractedShapes extractedShapes;


        // ICE

        armarx::DebugObserverInterfacePrx debugObserver;
        armarx::DebugDrawerTopic debugDrawer;

        std::string shapesName;
        armarx::semantic::ShapesTopicPrx shapesTopic;

    };
}

