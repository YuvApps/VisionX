/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::SegmentSpatialRelations
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <pcl/point_types.h>

#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>

#include <VisionX/interface/libraries/SemanticObjectRelations/GraphStorage.h>
#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>


namespace visionx
{

    /**
     * @class SegmentSpatialRelationsPropertyDefinitions
     * @brief Property definitions of `SegmentSpatialRelations`.
     */
    class SegmentSpatialRelationsPropertyDefinitions :
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        SegmentSpatialRelationsPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-SegmentSpatialRelations SegmentSpatialRelations
     * @ingroup VisionX-Components
     * A description of the component SegmentSpatialRelations.
     *
     * @class SegmentSpatialRelations
     * @ingroup Component-SegmentSpatialRelations
     * @brief Brief description of class SegmentSpatialRelations.
     *
     * Detailed description of class SegmentSpatialRelations.
     */
    class SegmentSpatialRelations :
        virtual public visionx::PointCloudProcessor
    {
        /// The used point type.
        using PointT = pcl::PointXYZRGBL;

    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        void onInitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        void onConnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        void onDisconnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        void onExitPointCloudProcessor() override;


        /// @see visionx::PointCloudProcessor::process()
        void process() override;


        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        armarx::DebugObserverInterfacePrx debugObserver;
        armarx::DebugDrawerTopic debugDrawer;

        armarx::semantic::GraphStorageTopicPrx graphStorage;

    };
}

