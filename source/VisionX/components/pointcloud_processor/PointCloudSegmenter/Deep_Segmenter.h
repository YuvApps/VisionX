/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Eren Aksoy ( eren dot aksoy at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <iostream>
#include <vector>
#include <pcl/point_types.h>
#include <pcl/search/search.h>

#include <Eigen/Core>

#include <ArmarXCore/core/Component.h>
#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>

class DeepSegClass
{

private:
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr labeledCloud;
    armarx::Blob rgbImage;

public:
    DeepSegClass();

    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr& GetLabeledPointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& CloudPtr, armarx::Blob segmentImage);

    armarx::Blob GetImageFromPointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& CloudPtr);
};

