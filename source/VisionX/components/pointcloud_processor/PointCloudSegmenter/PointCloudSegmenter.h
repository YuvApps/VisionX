/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Eren Aksoy ( eren dot aksoy at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <VisionX/interface/components/DeepSegmenterInterface.h>

#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>
#include <VisionX/interface/components/PointCloudSegmenter.h>

#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>
#include <pcl/common/transforms.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/fpfh_omp.h>
#include <pcl/search/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/correspondence.h>
#include <pcl/recognition/cg/geometric_consistency.h>

#include "LCCP_Segmenter.h"
#include "EuclideanBasedClustering.h"
#include "RG_Segmenter.h"
#include "Deep_Segmenter.h"

// boost
#include <boost/thread.hpp>
#include <boost/circular_buffer.hpp>


#include <Image/ByteImage.h>

namespace visionx
{
    using PointO = pcl::PointXYZRGBA;
    using PointOPtr = pcl::PointCloud<PointO>::Ptr;
    using PointL = pcl::PointXYZL;
    using PointLPtr = pcl::PointCloud<PointL>::Ptr;
    using PointRGBL = pcl::PointXYZRGBL;
    using PointRGBLPtr = pcl::PointCloud<PointRGBL>::Ptr;

    /**
     * @class PointCloudSegmenterPropertyDefinitions
     * @brief
    */
    class PointCloudSegmenterPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PointCloudSegmenterPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            // General parameters
            defineOptionalProperty<std::string>("providerName", "FakePointCloudProvider", "Name of the point cloud provider");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the debug drawer topic");
            defineOptionalProperty<std::string>("DeepSegmenterTopicName", "DeepSegmenterPythonUpdates", "Name of the deep segmenter topic");
            defineOptionalProperty<std::string>("segmentationMethod", "EC", "Name of the offered segmentation method (EC, LCCP or RG)");
            defineOptionalProperty<bool>("StartEnabled", true, "Enable or disable the segmentation by default");
            defineOptionalProperty<bool>("SingleRun", false, "Segment only one scene after each enable call");

            // LCCP parameters
            defineOptionalProperty<int>("lccpMinimumSegmentSize",  10, "Minimum segment size required for the lccp segmentation");
            defineOptionalProperty<float>("lccpVoxelResolution",  25, "Supervoxel resolution required for the lccp segmentation");
            defineOptionalProperty<float>("lccpSeedResolution",  80, "Seed resolution required for the lccp segmentation");
            defineOptionalProperty<float>("lccpColorImportance",  0.0, "Color importance required for the lccp segmentation");
            defineOptionalProperty<float>("lccpSpatialImportance",  1.0, "Spatial importance required for the lccp segmentation");
            defineOptionalProperty<float>("lccpNormalImportance",  4.0, "Normal importance required for the lccp segmentation");
            defineOptionalProperty<float>("lccpConcavityThreshold",  10.0, "Concavity threshold required for the lccp segmentation");
            defineOptionalProperty<float>("lccpSmoothnessThreshold",  0.1, "Smoothness threshold required for the lccp segmentation");

            // RG parameters
            defineOptionalProperty<float>("rgSmoothnessThreshold",  6.0, "Smoothness threshold required for the region growing segmentation");
            defineOptionalProperty<float>("rgCurvatureThreshold",  50.0, "Curvature threshold required for the region growing segmentation");

            // Debug parameters
            defineOptionalProperty<bool>("VisualizeCropBox", false, "Visualize crop box via debug drawer");
        }
    };

    /**
    * @class PointCloudSegmenter
    *
    * @ingroup VisionX-Components
    * @brief A brief description
    *
    *
    * Detailed Description
    */
    class PointCloudSegmenter :
        virtual public PointCloudSegmenterInterface,
        virtual public PointCloudProcessor
    {
    public:
        PointCloudSegmenter();

        /**
        * @see armarx::ManagedIceObject::getDefaultName()
        */
        std::string getDefaultName() const override
        {
            return "PointCloudSegmenter";
        }

        void changePointCloudProvider(const std::string& providerName, const Ice::Current& c = Ice::emptyCurrent) override;

    protected:
        /**
        * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        */
        void onInitPointCloudProcessor() override;

        /**
        * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        */
        void onConnectPointCloudProcessor() override;

        /**
        * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        */
        void onDisconnectPointCloudProcessor() override;

        /**
        * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        */
        void onExitPointCloudProcessor() override;

        /**
        * @see visionx::PointCloudProcessor::process()
        */
        void process() override;

        /**
        * @see PropertyUser::createPropertyDefinitions()
        */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void enablePipelineStep(const Ice::Current& c = Ice::emptyCurrent) override;
        void disablePipelineStep(const Ice::Current& c = Ice::emptyCurrent) override;
        bool isPipelineStepEnabled(const Ice::Current& c = Ice::emptyCurrent) override;
        armarx::TimestampBasePtr getLastProcessedTimestamp(const Ice::Current& c = Ice::emptyCurrent) override;

        void setLccpParameters(const LccpParameters& parameters, const Ice::Current& c = Ice::emptyCurrent) override;
        void loadLccpParametersFromFile(const std::string& filename, const Ice::Current& c = Ice::emptyCurrent) override;

        LccpParameters getLccpParameters(const Ice::Current& c = Ice::emptyCurrent) override;

        void setRgParameters(float smoothnessThreshold, float curvatureThreshold, const Ice::Current& c = Ice::emptyCurrent) override;
        void loadRgParametersFromFile(const std::string& filename, const Ice::Current& c = Ice::emptyCurrent) override;

    private:
        void convertFromXYZRGBAtoXYZL(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& sourceCloudPtr, pcl::PointCloud<pcl::PointXYZL>::Ptr& targetCloudPtr);
        void pushColoredLabeledCloudtoBuffer();

    private:
        LCCPSegClass* pLCCP;
        RGSegClass* pRGSegmenter;
        EuclideanBasedClustering* pECSegmenter;
        DeepSegClass* pDeepSegmenter;

        PointOPtr inputCloudPtr;
        PointRGBLPtr labeledColoredCloudPtr;

        armarx::Mutex timestampMutex;
        long lastProcessedTimestamp;

        armarx::Mutex enableMutex;
        bool enabled;

        armarx::Mutex providerMutex;
        std::string providerName;
        std::string segmentationMethod;

        // LCCP segmentation parameters
        uint32_t minSegSize;
        float voxelRes;
        float seedRes;
        float colorImp;
        float spatialImp;
        float normalImp;
        float concavityThres;
        float smoothnessThes;

        // RG segmentation parameters
        float rgSmoothnessThes;
        float rgCurvatureThres;

        void colorizeLabeledCloud();

        armarx::DebugDrawerInterfacePrx debugDrawerTopic;
        armarx::DeepSegmenterInterfacePrx deepSegmenterTopic;
        armarx::Blob rgbImage;
    };
}

