/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Eren Aksoy ( eren dot aksoy at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RG_Segmenter.h"

RGSegClass::RGSegClass()
{
    /// Create variables needed for preparations
    input_cloud_ptr.reset(new pcl::PointCloud<PointT>);
    output_colored_cloud.reset(new pcl::PointCloud<PointT>);
    output_labeled_cloud.reset(new pcl::PointCloud<PointL>);

    SmoothnessThreshold = 30.0f;
    CurvatureThreshold = 10.0f;
}
//---------------------------------------------------------------------------------------------------------------------//

pcl::PointCloud< pcl::PointXYZRGBA >::Ptr&  RGSegClass::ApplyRegionGrowingSegmentation(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& CloudPtr)
{
    // copy original points to the cloud
    pcl::copyPointCloud(*CloudPtr, *input_cloud_ptr);

    RegionGrowingSegmenter();

    return output_colored_cloud;
}
//---------------------------------------------------------------------------------------------------------------------//

void RGSegClass::RegionGrowingSegmenter()
{
    pcl::search::Search<PointT>::Ptr tree = boost::shared_ptr<pcl::search::Search<PointT> > (new pcl::search::KdTree<PointT>);
    pcl::PointCloud <pcl::Normal>::Ptr normals(new pcl::PointCloud <pcl::Normal>);
    pcl::NormalEstimation<PointT, pcl::Normal> normal_estimator;
    normal_estimator.setSearchMethod(tree);
    normal_estimator.setInputCloud(input_cloud_ptr);
    normal_estimator.setKSearch(50);
    normal_estimator.compute(*normals);

    /*
    pcl::IndicesPtr indices (new std::vector <int>);
    pcl::PassThrough<PointT> pass;
    pass.setInputCloud (input_cloud_ptr);
    pass.setFilterFieldName ("y");
    pass.setFilterLimits (0.0, 5000.0);
    pass.filter (*indices);
    */

    pcl::RegionGrowing<PointT, pcl::Normal> reg;
    reg.setMinClusterSize(50);
    reg.setMaxClusterSize(1000000);
    reg.setSearchMethod(tree);
    reg.setNumberOfNeighbours(30);
    reg.setInputCloud(input_cloud_ptr);
    //reg.setIndices (indices);
    reg.setInputNormals(normals);
    reg.setSmoothnessThreshold(SmoothnessThreshold / 180.0 * M_PI);
    reg.setCurvatureThreshold(CurvatureThreshold);

    std::vector <pcl::PointIndices> clusters;
    reg.extract(clusters);

    std::cout << "=RG Segmenter: Number of clusters is  " << clusters.size() << std::endl;

    output_colored_cloud = reg.getColoredCloudRGBA();

}
//---------------------------------------------------------------------------------------------------------------------//

void RGSegClass::UpdateRegionGrowingSegmentationParameters(float SmoothnessThres, float CurvatureThres)
{
    SmoothnessThreshold = SmoothnessThres;
    CurvatureThreshold = CurvatureThres;

    std::cout << "=RG Segmenter: Parameters updated! >>   SmoothnessThreshold : " << SmoothnessThreshold   << "  CurvatureThreshold: " << CurvatureThreshold << std::endl;
}
//---------------------------------------------------------------------------------------------------------------------//

void RGSegClass::ConvertFromXYZRGBAtoXYZL()
{
    // to be done!...



}
//---------------------------------------------------------------------------------------------------------------------//

