armarx_component_set_name(PointCloudSegmenter)


set(COMPONENT_LIBS
    VisionXPointCloud
    RobotAPICore
    ${PCL_SEARCH_LIBRARY}
    ${PCL_FILTERS_LIBRARY}
    ${PCL_FEATURES_LIBRARY}
    ${PCL_SEGMENTATION_LIBRARY}
)

set(SOURCES PointCloudSegmenter.cpp LCCP_Segmenter.cpp RG_Segmenter.cpp EuclideanBasedClustering.cpp Deep_Segmenter.cpp)
set(HEADERS PointCloudSegmenter.h   LCCP_Segmenter.h   RG_Segmenter.h   EuclideanBasedClustering.h   Deep_Segmenter.h)

armarx_add_component("${SOURCES}" "${HEADERS}")
