/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Eren Aksoy ( eren dot aksoy at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// Stdlib
#include <stdlib.h>
#include <cmath>
#include <limits.h>

#include <boost/format.hpp>


// PCL input/output
#include <pcl/console/parse.h>
#include <pcl/io/pcd_io.h>

//PCL other
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/supervoxel_clustering.h>

// The segmentation class this example is for
#include <pcl/segmentation/lccp_segmentation.h>


namespace visionx
{
    class PointCloudSegmenter;
}


class LCCPSegClass
{

    friend class visionx::PointCloudSegmenter;

private:

    using PointT = pcl::PointXYZRGBA;  // The point type used for input
    using SuperVoxelAdjacencyList = pcl::LCCPSegmentation<PointT>::SupervoxelAdjacencyList;


    pcl::PointCloud<pcl::PointXYZL>::Ptr sv_labeled_cloud;
    pcl::PointCloud<pcl::PointXYZL>::Ptr lccp_labeled_cloud;

    pcl::PointCloud<PointT>::Ptr input_cloud_ptr;
    pcl::PointCloud<pcl::Normal>::Ptr input_normals_ptr;

    float normals_scale;
    bool has_normals;

    ///  Default values of parameters before parsing
    // Supervoxel Stuff
    float voxel_resolution;
    float seed_resolution;
    float color_importance;
    float spatial_importance;
    float normal_importance;
    bool use_single_cam_transform;
    bool use_supervoxel_refinement;

    // LCCPSegmentation Stuff
    float concavity_tolerance_threshold;
    float smoothness_threshold;
    uint32_t min_segment_size;
    bool use_extended_convexity;
    bool use_sanity_criterion;

    // Segmentation Stuff
    uint k_factor;

    bool SegmentPointCloud();

    std::map<uint32_t, pcl::Supervoxel<PointT>::Ptr> supervoxel_clusters;

public:

    LCCPSegClass();

    pcl::PointCloud<pcl::PointXYZL>::Ptr& GetLabeledPointCloud(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& CloudPtr);
    size_t GetSuperVoxelClusterSize()
    {
        return supervoxel_clusters.size();
    };

    void UpdateParameters(float voxelRes, float seedRes, float colorImp, float spatialImp, float normalImp, float concavityThres, float smoothnessThes, uint32_t minSegSize);


    ~LCCPSegClass();
};

