/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudFilter
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <pcl/point_types.h>

#include <pcl/common/transforms.h>

#include <pcl/filters/filter.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/filters/crop_hull.h>

#include <Eigen/Core>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

#include <RobotAPI/interface/core/PoseBase.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>

#include <VisionX/interface/components/PointCloudFilter.h>
#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>


namespace visionx
{
    class CoordinateFrame;
    template <class PointT>
    class FramedPointCloud;


    /// @class PointCloudFilterPropertyDefinitions
    class PointCloudFilterPropertyDefinitions :
        public visionx::PointCloudProcessorPropertyDefinitions
    {
    public:
        PointCloudFilterPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-PointCloudFilter PointCloudFilter
     * @ingroup VisionX-Components
     * A description of the component PointCloudFilter.
     *
     * @class PointCloudFilter
     * @ingroup Component-PointCloudFilter
     * @brief Brief description of class PointCloudFilter.
     *
     * Detailed description of class PointCloudFilter.
     */
    class PointCloudFilter :
        virtual public armarx::PointCloudFilterInterface,
        virtual public visionx::PointCloudProcessor
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;

        virtual std::string getReferenceFrame(const Ice::Current& = Ice::emptyCurrent) override;

        virtual void setCroppingParameters(const armarx::Vector3BasePtr& min, const armarx::Vector3BasePtr& max,
                                           const std::string& frame, const Ice::Current& = Ice::emptyCurrent) override;

    protected:

        /// @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
        virtual void onInitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
        virtual void onConnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
        virtual void onDisconnectPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
        virtual void onExitPointCloudProcessor() override;

        /// @see visionx::PointCloudProcessor::process()
        virtual void process() override;

        template <typename PointType> void processPointCloud();

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        /// Draw the given frame with given role.
        void drawFrame(const armarx::FramedPose& frame, const std::string& role);

        /// Change the point cloud to given frame.
        /// Also logs transformation and draws target frame.
        template <typename PointT>
        void changePointCloudFrame(FramedPointCloud<PointT>& pointCloud,
                                   const std::string& role, const std::string& frame);

        /// Create the remote gui. May only be called if `remoteGui` is not nullptr.
        void remoteGuiCreate();
        /// Create the remote gui. May only be called if `remoteGui` is not nullptr.
        void remoteGuiRun();

        void remoteGuiUpdate(armarx::RemoteGui::TabProxy& tab);



    private:

        // Robot
        armarx::RobotStateComponentInterfacePrx robotStateComponent;
        VirtualRobot::RobotPtr localRobot;

        // Point cloud / provider.
        std::string providerName;
        std::string pointCloudFormat;
        std::string providerSourceFrameName;  ///< Point cloud frame specified by provider.

        struct Parameters
        {
            // Frames
            std::string sourceFrameName;
            std::string targetFrameName;

            // Cropping
            bool croppingEnabled;
            Eigen::Vector3f croppingMinPoint;
            Eigen::Vector3f croppingMaxPoint;
            Eigen::Vector3f croppingRPY;
            std::string croppingFrameName;

            // Downsampling
            bool downsamplingEnabled;
            float gridLeafSize;

            // Collision model filter
            bool applyCollisionModelFilter;

            bool reportCloudOutsideOfCroppingArea = false;

            Eigen::Matrix4f providerFrameTransformation = Eigen::Matrix4f::Identity();
        };

        armarx::Mutex parametersMutex;
        Parameters parameters;


        // Remote GUI
        /// The remote GUI proxy. Only fetched when property `RemoteGuiEnabled` is true.
        armarx::RemoteGuiInterfacePrx remoteGui;
        /// The remote GUI tab.
        armarx::RemoteGui::TabProxy remoteGuiTab;
        /// The remote GUI main thread.
        armarx::SimpleRunningTask<>::pointer_type remoteGuiTask;


        // Debug Drawer
        armarx::DebugDrawerTopic debugDrawer { getDefaultName() };

    };
}

