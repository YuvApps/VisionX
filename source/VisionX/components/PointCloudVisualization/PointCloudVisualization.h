/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudVisualization
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/system/Synchronization.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <VisionX/interface/core/PointCloudProviderInterface.h>
#include <VisionX/interface/core/DataTypes.h>
#include <VisionX/interface/components/PointCloudVisualization.h>
#include <VisionX/components/pointcloud_core/PointCloudProcessor.h>

#include <memory>

namespace visionx
{
    class PointCloudVisualization;

    // Wrapper object used to monitor one provider and call a parameterized callback in the main component
    class PointCloudVisualizationHandler
    {
    public:
        PointCloudVisualizationHandler(PointCloudVisualization* component, const std::string& providerName);
        ~PointCloudVisualizationHandler();

        void processPointCloudProvider();
        void stop();

    protected:
        std::string providerName;
        PointCloudVisualization* component;
        armarx::RunningTask<PointCloudVisualizationHandler>::pointer_type task;
    };

    using PointCloudVisualizationHandlerPtr = std::shared_ptr<PointCloudVisualizationHandler>;


    /**
     * @class PointCloudVisualizationPropertyDefinitions
     * @brief
     */
    class PointCloudVisualizationPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PointCloudVisualizationPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("PointCloudProviders", "", "Comma-separated list of point cloud provider names");
            defineOptionalProperty<std::string>("InitiallyDisabledVisualizations", "", "Comma-separated list of point cloud provider names, which should not be visualized by default");
            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the debug drawer topic");
            defineOptionalProperty<std::string>("DefaultVisualizationType", "Plain", "Default point cloud visualization type.");
        }
    };

    using DebugDrawerPointCloudPtr = std::shared_ptr<armarx::DebugDrawer24BitColoredPointCloud>;
    using PointCloudVisualizationCollection = std::map<visionx::PointContentType, DebugDrawerPointCloudPtr>;

    /**
     * @defgroup Component-PointCloudVisualization PointCloudVisualization
     * @ingroup VisionX-Components
     * A description of the component PointCloudVisualization.
     *
     * @class PointCloudVisualization
     * @ingroup Component-PointCloudVisualization
     * @brief Brief description of class PointCloudVisualization.
     *
     * Detailed description of class PointCloudVisualization.
     */
    class PointCloudVisualization :
        virtual public PointCloudVisualizationInterface,
        virtual public PointCloudProcessor
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "PointCloudVisualization";
        }

        void processPointCloudProvider(const std::string& providerName);

        template<typename PointT>
        void processPointCloud(const std::string& providerName);

        bool waitForNewPointCloud(const std::string& providerName);

        PointCloudProviderVisualizationInfoList getAvailableProviders(const Ice::Current& c = Ice::emptyCurrent) override;
        void enablePointCloudVisualization(const std::string& providerName, visionx::PointContentType type, bool enable, const Ice::Current& c = Ice::emptyCurrent) override;
        PointCloudVisualizationInfo getCurrentPointCloudVisualizationInfo(const std::string& providerName, const Ice::Current& c = Ice::emptyCurrent) override;

    protected:
        /**
         * @see visionx::PointCloudProcessor::onInitPointCloudProcessor()
         */
        void onInitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onConnectPointCloudProcessor()
         */
        void onConnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onDisconnectPointCloudProcessor()
         */
        void onDisconnectPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::onExitPointCloudProcessor()
         */
        void onExitPointCloudProcessor() override;

        /**
         * @see visionx::PointCloudProcessor::process()
         */
        void process() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        void updateVisualization(const std::string& providerName);
        void clearAllLayers();
        std::string pointCloudTypeToText(visionx::PointContentType type);
        visionx::PointContentType pointCloudTextToType(std::string text);

    protected:
        armarx::DebugDrawerInterfacePrx debugDrawerTopicPrx;

        std::vector<std::string> providerNames;
        std::map<std::string, visionx::MetaPointCloudFormatPtr> pointCloudFormats;
        std::map<std::string, PointCloudVisualizationHandlerPtr> providerHandlers;
        std::map<std::string, visionx::PointContentType> visualizationTypes;
        std::map<std::string, bool> enabledFlags;

        std::map<std::string, PointCloudVisualizationCollection> visualizations;

        std::map<std::string, int> debugDrawerVisualizationIteration;

        armarx::Mutex pointCloudMutex;
    };
}

