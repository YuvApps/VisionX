/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PointCloudConversions.h"
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/colors.h>

namespace visionx::tools
{

    template <> visionx::PointContentType getPointContentType<pcl::PointXYZ>()
    {
        return visionx::ePoints;
    }
    template <> visionx::PointContentType getPointContentType<pcl::PointXYZL>()
    {
        return visionx::eLabeledPoints;
    }
    template <> visionx::PointContentType getPointContentType<pcl::PointXYZI>()
    {
        return visionx::eIntensity;
    }
    template<>
    PointContentType getPointContentType<pcl::PointNormal>()
    {
        return visionx::eOrientedPoints;
    }
    template <> visionx::PointContentType getPointContentType<pcl::PointXYZRGBA>()
    {
        return visionx::eColoredPoints;
    }
    template <> visionx::PointContentType getPointContentType<pcl::PointXYZRGBL>()
    {
        return visionx::eColoredLabeledPoints;
    }
    template <> visionx::PointContentType getPointContentType<pcl::PointXYZRGBNormal>()
    {
        return visionx::eColoredOrientedPoints;
    }


    size_t getBytesPerPoint(visionx::PointContentType pointContent)
    {
        switch (pointContent)
        {
            case visionx::ePoints:
                return sizeof(visionx::Point3D);

            case visionx::eColoredPoints:
                return sizeof(visionx::ColoredPoint3D);

            case visionx::eOrientedPoints:
                return sizeof(visionx::OrientedPoint3D);

            case visionx::eColoredOrientedPoints:
                return sizeof(visionx::ColoredOrientedPoint3D);

            case visionx::eLabeledPoints:
                return sizeof(visionx::LabeledPoint3D);

            case visionx::eColoredLabeledPoints:
                return sizeof(visionx::ColoredLabeledPoint3D);

            case visionx::eIntensity:
                return sizeof(visionx::IntensityPoint3D);

            default:
                return 0;
                //ARMARX_FATAL_S << "invalid point cloud content " << pointContent;
        }
    }

    bool isColored(PointContentType type)
    {
        switch (type)
        {
            case visionx::eColoredPoints:
            case visionx::eColoredOrientedPoints:
            case visionx::eColoredLabeledPoints:
                return true;
            default:
                return false;
        }
    }

    bool isLabeled(PointContentType type)
    {
        switch (type)
        {
            case visionx::eLabeledPoints:
            case visionx::eColoredLabeledPoints:
                return true;
            default:
                return false;
        }
    }


    void convertFromPCL(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr sourcePtr, void** target)
    {
        const unsigned int Area = sourcePtr->width * sourcePtr->height;
        visionx::ColoredPoint3D* pBuffer = new visionx::ColoredPoint3D[Area];

        size_t size = sourcePtr->points.size();
        auto& points = sourcePtr->points;
        for (size_t i = 0; i < size; i++)
        {
            auto& p = points[i];
            auto& pB = pBuffer[i];
            pB.point.x = p.x;
            pB.point.y = p.y;
            pB.point.z = p.z;
            pB.color.r = p.r;
            pB.color.g = p.g;
            pB.color.b = p.b;
            pB.color.a = p.a;
        }

        memcpy(target[0], pBuffer, Area * sizeof(visionx::ColoredPoint3D));
        delete[] pBuffer;
    }


    void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr targetPtr)
    {
        if (pointCloudFormat->type == visionx::eColoredPoints)
        {
            targetPtr->width = static_cast<uint32_t>(pointCloudFormat->width);
            targetPtr->height = static_cast<uint32_t>(pointCloudFormat->height);
            targetPtr->points.resize(pointCloudFormat->size / getBytesPerPoint(pointCloudFormat->type));
            visionx::ColoredPoint3D* pBuffer = reinterpret_cast<visionx::ColoredPoint3D*>(*source);
            size_t size = targetPtr->points.size();
            auto& points = targetPtr->points;
            for (size_t i = 0; i < size; i++)
            {
                auto& p = points[i];
                auto& pB = pBuffer[i];
                p.x = pB.point.x;
                p.y = pB.point.y;
                p.z = pB.point.z;
                p.r = pB.color.r;
                p.g = pB.color.g;
                p.b = pB.color.b;
                p.a = pB.color.a;
            }

        }
        else if (pointCloudFormat->type == visionx::eColoredLabeledPoints)
        {

            pcl::PointCloud<pcl::PointXYZRGBL>::Ptr temp(new pcl::PointCloud<pcl::PointXYZRGBL>());
            convertToPCL(source, pointCloudFormat, temp);
            pcl::copyPointCloud(*temp, *targetPtr);
        }
    }

    void convertFromPCL(pcl::PointCloud<pcl::PointXYZL>::Ptr sourcePtr, void** target)
    {
        const unsigned int Area = sourcePtr->width * sourcePtr->height;
        visionx::LabeledPoint3D* pBuffer = new visionx::LabeledPoint3D[Area];

        for (size_t i = 0; i < sourcePtr->points.size(); i++)
        {
            pBuffer[i].point.x = sourcePtr->points[i].x;
            pBuffer[i].point.y = sourcePtr->points[i].y;
            pBuffer[i].point.z = sourcePtr->points[i].z;
            pBuffer[i].label = static_cast<int>(sourcePtr->points[i].label);
        }

        memcpy(target[0], pBuffer, Area * sizeof(visionx::LabeledPoint3D));
        delete[] pBuffer;
    }


    void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZL>::Ptr targetPtr)
    {
        targetPtr->width = static_cast<uint32_t>(pointCloudFormat->width);
        targetPtr->height = static_cast<uint32_t>(pointCloudFormat->height);
        targetPtr->points.resize(pointCloudFormat->size / getBytesPerPoint(pointCloudFormat->type));
        visionx::LabeledPoint3D* pBuffer = reinterpret_cast<visionx::LabeledPoint3D*>(*source);

        for (size_t i = 0; i < targetPtr->points.size(); i++)
        {
            targetPtr->points[i].x = pBuffer[i].point.x;
            targetPtr->points[i].y = pBuffer[i].point.y;
            targetPtr->points[i].z = pBuffer[i].point.z;
            targetPtr->points[i].label = static_cast<uint32_t>(pBuffer[i].label);
        }
    }



    void convertFromPCL(pcl::PointCloud<pcl::PointXYZI>::Ptr sourcePtr, void** target)
    {
        const unsigned int Area = sourcePtr->width * sourcePtr->height;
        visionx::IntensityPoint3D* pBuffer = new visionx::IntensityPoint3D[Area];

        for (size_t i = 0; i < sourcePtr->points.size(); i++)
        {
            pBuffer[i].point.x = sourcePtr->points[i].x;
            pBuffer[i].point.y = sourcePtr->points[i].y;
            pBuffer[i].point.z = sourcePtr->points[i].z;
            pBuffer[i].intensity = sourcePtr->points[i].intensity;
        }

        memcpy(target[0], pBuffer, Area * sizeof(visionx::IntensityPoint3D));
        delete[] pBuffer;
    }


    void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZI>::Ptr targetPtr)
    {
        targetPtr->width = static_cast<uint32_t>(pointCloudFormat->width);
        targetPtr->height = static_cast<uint32_t>(pointCloudFormat->height);
        targetPtr->points.resize(pointCloudFormat->size / getBytesPerPoint(pointCloudFormat->type));
        visionx::IntensityPoint3D* pBuffer = reinterpret_cast<visionx::IntensityPoint3D*>(*source);

        for (size_t i = 0; i < targetPtr->points.size(); i++)
        {
            targetPtr->points[i].x = pBuffer[i].point.x;
            targetPtr->points[i].y = pBuffer[i].point.y;
            targetPtr->points[i].z = pBuffer[i].point.z;
            targetPtr->points[i].intensity = pBuffer[i].intensity;
        }
    }



    void convertFromPCL(pcl::PointCloud<pcl::PointXYZ>::Ptr sourcePtr, void** target)
    {
        const unsigned int Area = sourcePtr->width * sourcePtr->height;
        visionx::Point3D* pBuffer = new visionx::Point3D[Area];

        for (size_t i = 0; i < sourcePtr->points.size(); i++)
        {
            pBuffer[i].x = sourcePtr->points[i].x;
            pBuffer[i].y = sourcePtr->points[i].y;
            pBuffer[i].z = sourcePtr->points[i].z;
        }

        memcpy(target[0], pBuffer, Area * sizeof(visionx::Point3D));
        delete[] pBuffer;

    }

    void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZ>::Ptr targetPtr)
    {
        targetPtr->width = static_cast<uint32_t>(pointCloudFormat->width);
        targetPtr->height = static_cast<uint32_t>(pointCloudFormat->height);
        targetPtr->points.resize(pointCloudFormat->size / getBytesPerPoint(pointCloudFormat->type));
        visionx::Point3D* pBuffer = reinterpret_cast<visionx::Point3D*>(*source);

        for (size_t i = 0; i < targetPtr->points.size(); i++)
        {
            targetPtr->points[i].x = pBuffer[i].x;
            targetPtr->points[i].y = pBuffer[i].y;
            targetPtr->points[i].z = pBuffer[i].z;
        }
    }

    void convertToPCL(const ColoredPointCloud& source, pcl::PointCloud<pcl::PointXYZRGBA>::Ptr targetPtr)
    {
        targetPtr->points.resize(targetPtr->size());

        for (size_t i = 0; i < targetPtr->points.size(); i++)
        {
            targetPtr->points[i].x = source.at(i).point.x;
            targetPtr->points[i].y = source.at(i).point.y;
            targetPtr->points[i].z = source.at(i).point.z;
            targetPtr->points[i].r = source.at(i).color.r;
            targetPtr->points[i].g = source.at(i).color.g;
            targetPtr->points[i].b = source.at(i).color.b;
            targetPtr->points[i].a = source.at(i).color.a;
        }
    }

    void convertFromPCL(const  pcl::PointCloud<pcl::PointXYZRGBA>::Ptr sourcePtr, ColoredPointCloud& target)
    {
        target.resize(sourcePtr->size());

        for (size_t i = 0; i < target.size(); i++)
        {
            target.at(i).point.x = sourcePtr->points[i].x;
            target.at(i).point.y = sourcePtr->points[i].y;
            target.at(i).point.z = sourcePtr->points[i].z;
            target.at(i).color.r = sourcePtr->points[i].r;
            target.at(i).color.g = sourcePtr->points[i].g;
            target.at(i).color.b = sourcePtr->points[i].b;
            target.at(i).color.a = sourcePtr->points[i].a;
        }
    }


    void convertFromPCL(pcl::PointCloud<pcl::PointXYZRGBL>::Ptr sourcePtr, void** target)
    {
        const unsigned int Area = sourcePtr->width * sourcePtr->height;
        visionx::ColoredLabeledPoint3D* pBuffer = new visionx::ColoredLabeledPoint3D[Area];

        for (size_t i = 0; i < sourcePtr->points.size(); i++)
        {
            pBuffer[i].point.x = sourcePtr->points[i].x;
            pBuffer[i].point.y = sourcePtr->points[i].y;
            pBuffer[i].point.z = sourcePtr->points[i].z;
            pBuffer[i].color.r = sourcePtr->points[i].r;
            pBuffer[i].color.g = sourcePtr->points[i].g;
            pBuffer[i].color.b = sourcePtr->points[i].b;
            pBuffer[i].color.a = sourcePtr->points[i].a;
            pBuffer[i].label = static_cast<int>(sourcePtr->points[i].label);
        }

        memcpy(target[0], pBuffer, Area * sizeof(visionx::ColoredLabeledPoint3D));
        delete[] pBuffer;
    }

    void convertFromPCL(const pcl::PointCloud<pcl::PointXYZRGBL>& source,
                        ColoredLabeledPointCloud& target)
    {
        target.resize(source.size());

        for (std::size_t i = 0; i < source.size(); ++i)
        {
            const pcl::PointXYZRGBL& p = source.points[i];
            target[i].point = visionx::Point3D { p.x, p.y, p.z };
            target[i].color = visionx::RGBA { p.r, p.g, p.b, p.a };
            target[i].label = static_cast<int>(p.label);
        }
    }

    void convertToPCL(const ColoredLabeledPointCloud& source,
                      pcl::PointCloud<pcl::PointXYZRGBL>& target)
    {
        target.resize(source.size());

        for (std::size_t i = 0; i < source.size(); ++i)
        {
            const visionx::ColoredLabeledPoint3D& p = source[i];

            target[i].x = p.point.x;
            target[i].y = p.point.y;
            target[i].z = p.point.z;
            target[i].r = p.color.r;
            target[i].g = p.color.g;
            target[i].b = p.color.b;
            target[i].a = p.color.a;
            target[i].label = static_cast<uint32_t>(p.label);
        }
    }


    void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZRGBL>::Ptr targetPtr)
    {

        if (pointCloudFormat->type == visionx::eColoredLabeledPoints)
        {
            targetPtr->width = static_cast<uint32_t>(pointCloudFormat->width);
            targetPtr->height = static_cast<uint32_t>(pointCloudFormat->height);
            targetPtr->points.resize(pointCloudFormat->size / getBytesPerPoint(pointCloudFormat->type));

            visionx::ColoredLabeledPoint3D* pBuffer = reinterpret_cast<visionx::ColoredLabeledPoint3D*>(*source);

            for (size_t i = 0; i < targetPtr->points.size(); i++)
            {
                targetPtr->points[i].x = pBuffer[i].point.x;
                targetPtr->points[i].y = pBuffer[i].point.y;
                targetPtr->points[i].z = pBuffer[i].point.z;
                targetPtr->points[i].r = pBuffer[i].color.r;
                targetPtr->points[i].g = pBuffer[i].color.g;
                targetPtr->points[i].b = pBuffer[i].color.b;
                targetPtr->points[i].a = pBuffer[i].color.a;
                targetPtr->points[i].label = static_cast<uint32_t>(pBuffer[i].label);
            }
        }
        else if (pointCloudFormat->type == visionx::eColoredPoints)
        {

            pcl::PointCloud<pcl::PointXYZRGBA>::Ptr temp(new pcl::PointCloud<pcl::PointXYZRGBA>());
            convertToPCL(source, pointCloudFormat, temp);
            pcl::copyPointCloud(*temp, *targetPtr);

        }
        else if (pointCloudFormat->type == visionx::eLabeledPoints)
        {

            pcl::PointCloud<pcl::PointXYZL>::Ptr temp(new pcl::PointCloud<pcl::PointXYZL>());
            convertToPCL(source, pointCloudFormat, temp);
            pcl::copyPointCloud(*temp, *targetPtr);
        }
    }

    void convertFromPCL(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr& sourcePtr, void** target)
    {
        const unsigned int area = sourcePtr->width * sourcePtr->height;
        std::vector<visionx::ColoredOrientedPoint3D> buffer(area);

        for (size_t i = 0; i < sourcePtr->points.size(); i++)
        {
            buffer[i].point.x = sourcePtr->points[i].x;
            buffer[i].point.y = sourcePtr->points[i].y;
            buffer[i].point.z = sourcePtr->points[i].z;
            buffer[i].color.r = sourcePtr->points[i].r;
            buffer[i].color.g = sourcePtr->points[i].g;
            buffer[i].color.b = sourcePtr->points[i].b;
            buffer[i].normal.nx = sourcePtr->points[i].normal_x;
            buffer[i].normal.ny = sourcePtr->points[i].normal_y;
            buffer[i].normal.nz = sourcePtr->points[i].normal_z;
        }

        copy(buffer.begin(), buffer.end(), static_cast<visionx::ColoredOrientedPoint3D*>(target[0]));
    }

    void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr targetPtr)
    {
        targetPtr->width = static_cast<uint32_t>(pointCloudFormat->width);
        targetPtr->height = static_cast<uint32_t>(pointCloudFormat->height);
        targetPtr->points.resize(pointCloudFormat->size / getBytesPerPoint(pointCloudFormat->type));
        visionx::ColoredOrientedPoint3D* pBuffer = static_cast<visionx::ColoredOrientedPoint3D*>(*source);

        for (size_t i = 0; i < targetPtr->points.size(); i++)
        {
            targetPtr->points[i].x = pBuffer[i].point.x;
            targetPtr->points[i].y = pBuffer[i].point.y;
            targetPtr->points[i].z = pBuffer[i].point.z;
            targetPtr->points[i].r = pBuffer[i].color.r;
            targetPtr->points[i].g = pBuffer[i].color.g;
            targetPtr->points[i].b = pBuffer[i].color.b;
            targetPtr->points[i].normal_x = pBuffer[i].normal.nx;
            targetPtr->points[i].normal_y = pBuffer[i].normal.ny;
            targetPtr->points[i].normal_z = pBuffer[i].normal.nz;
        }
    }

    void convertFromPCL(pcl::PointCloud<pcl::PointXYZRGB>::Ptr sourcePtr, void** target)
    {
        ARMARX_CHECK_EXPRESSION(target);
        ARMARX_CHECK_EXPRESSION(sourcePtr);
        visionx::ColoredPoint3D* pBuffer = reinterpret_cast<ColoredPoint3D*>(target);

        size_t size = sourcePtr->points.size();
        auto& points = sourcePtr->points;
        for (size_t i = 0; i < size; i++)
        {
            auto& p = points[i];
            auto& pB = pBuffer[i];
            pB.point.x = p.x;
            pB.point.y = p.y;
            pB.point.z = p.z;
            pB.color.r = p.r;
            pB.color.g = p.g;
            pB.color.b = p.b;
            pB.color.a = 255;
        }


    }


    void convertToPCL(void** source, visionx::MetaPointCloudFormatPtr pointCloudFormat, pcl::PointCloud<pcl::PointXYZRGB>::Ptr targetPtr)
    {
        if (pointCloudFormat->type == visionx::eColoredPoints)
        {


            targetPtr->width = pointCloudFormat->width;
            targetPtr->height = pointCloudFormat->height;
            targetPtr->points.resize(pointCloudFormat->size / getBytesPerPoint(pointCloudFormat->type));
            visionx::ColoredPoint3D* pBuffer = reinterpret_cast<visionx::ColoredPoint3D*>(*source);
            size_t size = targetPtr->points.size();
            auto& points = targetPtr->points;
            for (size_t i = 0; i < size; i++)
            {
                auto& p = points[i];
                auto& pB = pBuffer[i];
                p.x = pB.point.x;
                p.y = pB.point.y;
                p.z = pB.point.z;
                p.r = pB.color.r;
                p.g = pB.color.g;
                p.b = pB.color.b;
            }

        }
        else if (pointCloudFormat->type == visionx::eColoredLabeledPoints)
        {

            pcl::PointCloud<pcl::PointXYZRGBL>::Ptr temp(new pcl::PointCloud<pcl::PointXYZRGBL>());
            convertToPCL(source, pointCloudFormat, temp);
            pcl::copyPointCloud(*temp, *targetPtr);
        }
    }

    void convertFromPCLToDebugDrawer(const pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& sourcePtr, armarx::DebugDrawer24BitColoredPointCloud& target, PCLToDebugDrawerConversionMode mode, float pointSize)
    {
        target.pointSize = pointSize;

        std::size_t numPoints = sourcePtr->width * sourcePtr->height;
        target.points.reserve(numPoints);

        armarx::DebugDrawer24BitColoredPointCloudElement e;
        for (const auto& p : sourcePtr->points)
        {
            if (std::isfinite(p.x) && std::isfinite(p.y) && std::isfinite(p.z))
            {
                e.x = p.x;
                e.y = p.y;
                e.z = p.z;

                if (mode == eConvertAsColors)
                {
                    e.color.r = p.r;
                    e.color.g = p.g;
                    e.color.b = p.b;
                }
                else
                {
                    // Simple rainbow coloring
                    float value = (static_cast<int>(e.z) / 10) % 256;

                    e.color.r = static_cast<Ice::Byte>((value > 128) ? (value - 128) * 2 : 0);
                    e.color.g = static_cast<Ice::Byte>((value < 128) ? 2 * value : 255 - ((value - 128) * 2));
                    e.color.b = static_cast<Ice::Byte>((value < 128) ? 255 - (2 * value) : 0);
                }

                target.points.push_back(e);
            }
        }
    }

    void convertFromPCLToDebugDrawer(const pcl::PointCloud<pcl::PointXYZL>::Ptr& sourcePtr, armarx::DebugDrawer24BitColoredPointCloud& target, PCLToDebugDrawerConversionMode mode, float pointSize)
    {
        target.pointSize = pointSize;

        std::size_t numPoints = sourcePtr->width * sourcePtr->height;
        target.points.reserve(numPoints);
        std::map<uint32_t, pcl::RGB> colorMap;

        armarx::DebugDrawer24BitColoredPointCloudElement e;
        for (const auto& p : sourcePtr->points)
        {
            if (std::isfinite(p.x) && std::isfinite(p.y) && std::isfinite(p.z))
            {
                e.x = p.x;
                e.y = p.y;
                e.z = p.z;

                if (mode == eConvertAsLabels)
                {
                    if (!colorMap.count(p.label))
                    {
                        colorMap[p.label] = pcl::GlasbeyLUT::at(p.label % pcl::GlasbeyLUT::size());
                    }
                    const auto c = colorMap[p.label];

                    e.color.r = c.r;
                    e.color.g = c.g;
                    e.color.b = c.b;
                }
                else
                {
                    // Simple rainbow coloring
                    float value = (static_cast<int>(e.z) / 10) % 256;

                    e.color.r = static_cast<Ice::Byte>((value > 128) ? (value - 128) * 2 : 0);
                    e.color.g = static_cast<Ice::Byte>((value < 128) ? 2 * value : 255 - ((value - 128) * 2));
                    e.color.b = static_cast<Ice::Byte>((value < 128) ? 255 - (2 * value) : 0);
                }

                target.points.push_back(e);
            }
        }
    }

    void convertFromPCLToDebugDrawer(
        const pcl::PointCloud<pcl::PointXYZ>::Ptr& sourcePtr,
        armarx::DebugDrawer24BitColoredPointCloud& target,
        PCLToDebugDrawerConversionMode mode, float pointSize)
    {
        (void) mode; // unused

        target.pointSize = pointSize;

        std::size_t numPoints = sourcePtr->width * sourcePtr->height;
        target.points.reserve(numPoints);

        armarx::DebugDrawer24BitColoredPointCloudElement e;
        for (const auto& p : sourcePtr->points)
        {
            if (std::isfinite(p.x) && std::isfinite(p.y) && std::isfinite(p.z))
            {
                e.x = p.x;
                e.y = p.y;
                e.z = p.z;

                // Simple rainbow coloring
                float value = (static_cast<int>(e.z) / 10) % 256;

                e.color.r = static_cast<Ice::Byte>((value > 128) ? (value - 128) * 2 : 0);
                e.color.g = static_cast<Ice::Byte>((value < 128) ? 2 * value : 255 - ((value - 128) * 2));
                e.color.b = static_cast<Ice::Byte>((value < 128) ? 255 - (2 * value) : 0);

                target.points.push_back(e);
            }
        }
    }

    void convertFromPCLToDebugDrawer(const pcl::PointCloud<pcl::PointXYZRGBL>::Ptr& sourcePtr, armarx::DebugDrawer24BitColoredPointCloud& target, PCLToDebugDrawerConversionMode mode, float pointSize)
    {
        target.pointSize = pointSize;

        std::size_t numPoints = sourcePtr->width * sourcePtr->height;
        target.points.reserve(numPoints);
        std::map<uint32_t, pcl::RGB> colorMap;

        armarx::DebugDrawer24BitColoredPointCloudElement e;
        for (const auto& p : sourcePtr->points)
        {
            if (std::isfinite(p.x) && std::isfinite(p.y) && std::isfinite(p.z))
            {
                e.x = p.x;
                e.y = p.y;
                e.z = p.z;

                if (mode == eConvertAsColors)
                {
                    e.color.r = p.r;
                    e.color.g = p.g;
                    e.color.b = p.b;
                }
                else if (mode == eConvertAsLabels)
                {
                    if (!colorMap.count(p.label))
                    {
                        colorMap[p.label] = pcl::GlasbeyLUT::at(p.label % pcl::GlasbeyLUT::size());
                    }
                    const auto c = colorMap[p.label];

                    e.color.r = c.r;
                    e.color.g = c.g;
                    e.color.b = c.b;
                }
                else
                {
                    // Simple rainbow coloring
                    float value = (static_cast<int>(e.z) / 10) % 256;

                    e.color.r = static_cast<Ice::Byte>((value > 128) ? (value - 128) * 2 : 0);
                    e.color.g = static_cast<Ice::Byte>((value < 128) ? 2 * value : 255 - ((value - 128) * 2));
                    e.color.b = static_cast<Ice::Byte>((value < 128) ? 255 - (2 * value) : 0);
                }

                target.points.push_back(e);
            }
        }
    }


    // pcl::PointXYZ <-> visionx::Point3D
    template <>
    void convertPointFromPCL(const pcl::PointXYZ& source, visionx::Point3D& target)
    {
        detail::convertXYZ(source, target);
    }
    template <>
    void convertPointToPCL(const visionx::Point3D& source, pcl::PointXYZ& target)
    {
        detail::convertXYZ(source, target);
    }

    // pcl::PointXYZRGBA <-> visionx::ColoredPoint3D
    template <>
    void convertPointFromPCL(const pcl::PointXYZRGBA& source, visionx::ColoredPoint3D& target)
    {
        detail::convertXYZ(source, target.point);
        detail::convertRGBA(source, target.color);
    }
    template <>
    void convertPointToPCL(const visionx::ColoredPoint3D& source, pcl::PointXYZRGBA& target)
    {
        detail::convertXYZ(source.point, target);
        detail::convertRGBA(source.color, target);
    }

    // pcl::PointXYZL <-> visionx::LabeledPoint3D
    template <>
    void convertPointFromPCL(const pcl::PointXYZL& source, visionx::LabeledPoint3D& target)
    {
        detail::convertXYZ(source, target.point);
        detail::convertLabel(source, target);
    }
    template <>
    void convertPointToPCL(const visionx::LabeledPoint3D& source, pcl::PointXYZL& target)
    {
        detail::convertXYZ(source.point, target);
        detail::convertLabel(source, target);
    }

    // pcl::PointXYZRGBL <-> visionx::ColoredLabeledPoint3D
    template <>
    void convertPointFromPCL(const pcl::PointXYZRGBL& source, visionx::ColoredLabeledPoint3D& target)
    {
        detail::convertXYZ(source, target.point);
        detail::convertRGBA(source, target.color);
        detail::convertLabel(source, target);
    }

    template <>
    void convertPointToPCL(const visionx::ColoredLabeledPoint3D& source, pcl::PointXYZRGBL& target)
    {
        detail::convertXYZ(source.point, target);
        detail::convertRGBA(source.color, target);
        detail::convertLabel(source, target);
    }

    static std::vector<std::string> makePointContentTypeNames()
    {
        std::vector<std::string> names(7);
        names.at(ePoints) = "ePoints";
        names.at(eColoredPoints) = "eColoredPoints";
        names.at(eOrientedPoints) = "eOrientedPoints";
        names.at(eColoredOrientedPoints) = "eColoredOrientedPoints";
        names.at(eLabeledPoints) = "eLabeledPoints";
        names.at(eColoredLabeledPoints) = "eColoredLabeledPoints";
        names.at(eIntensity) = "eIntensity";
        return names;
    }

    std::string toString(PointContentType pointContent)
    {
        static const std::vector<std::string> names = makePointContentTypeNames();
        return names.at(pointContent);
    }

}

