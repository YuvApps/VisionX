#pragma once

#include <ostream>

#include <VisionX/interface/core/DataTypes.h>


namespace visionx
{
    bool operator!=(const Point3D& lhs, const Point3D& rhs);
    std::ostream& operator<<(std::ostream& os, const Point3D& rhs);

    // bool operator!=(const RGBA& lhs, const RGBA& rhs);  // Already defined.
    std::ostream& operator<<(std::ostream& os, const RGBA& rhs);

    bool operator!=(const ColoredPoint3D& lhs, const ColoredPoint3D& rhs);
    std::ostream& operator<<(std::ostream& os, const ColoredPoint3D& rhs);

    bool operator!=(const LabeledPoint3D& lhs, const LabeledPoint3D& rhs);
    std::ostream& operator<<(std::ostream& os, const LabeledPoint3D& rhs);
}
