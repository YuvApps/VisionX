#pragma once

#include <ostream>

#include <pcl/point_types.h>


namespace pcl
{
    bool operator!=(const PointXYZ& lhs, const PointXYZ& rhs);
    std::ostream& operator<<(std::ostream& os, const PointXYZ& rhs);

    bool operator!=(const PointXYZRGBA& lhs, const PointXYZRGBA& rhs);
    std::ostream& operator<<(std::ostream& os, const PointXYZRGBA& rhs);

    bool operator!=(const PointXYZL& lhs, const PointXYZL& rhs);
    std::ostream& operator<<(std::ostream& os, const PointXYZL& rhs);
}

