
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore VisionXPointCloud)
 
armarx_add_test(PointCloudConversionsTest   PointCloudConversionsTest.cpp   "${LIBS}")
armarx_add_test(PCLUtilitiesTest            PCLUtilitiesTest.cpp            "${LIBS}")
