/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PointCloudFilter
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE VisionX::ArmarXObjects::PointCloudCore

#define ARMARX_BOOST_TEST

#include <VisionX/Test.h>
#include <VisionX/components/pointcloud_core/PointCloudConversions.h>
#include <VisionX/components/pointcloud_core/pcl_point_operators.h>
#include <VisionX/components/pointcloud_core/ice_point_operators.h>

#include <iostream>

namespace vtools = visionx::tools;


BOOST_AUTO_TEST_CASE(test_getPointContentType)
{
    BOOST_CHECK_EQUAL(vtools::getPointContentType<pcl::PointXYZ>(), visionx::ePoints);
    BOOST_CHECK_EQUAL(vtools::getPointContentType<pcl::PointXYZL>(), visionx::eLabeledPoints);
    BOOST_CHECK_EQUAL(vtools::getPointContentType<pcl::PointXYZI>(), visionx::eIntensity);
    BOOST_CHECK_EQUAL(vtools::getPointContentType<pcl::PointXYZRGBA>(), visionx::eColoredPoints);
    BOOST_CHECK_EQUAL(vtools::getPointContentType<pcl::PointXYZRGBL>(), visionx::eColoredLabeledPoints);
    BOOST_CHECK_EQUAL(vtools::getPointContentType<pcl::PointXYZRGBNormal>(), visionx::eColoredOrientedPoints);
}



BOOST_AUTO_TEST_SUITE(Test_convertFromToPCL)

constexpr uint32_t POINT_CLOUD_SIZE = 64;


template <typename PointT>
void setPointXYZ(std::size_t i, PointT& point)
{
    point.x = i;
    point.y = 2 * i + 1;
    point.z = - 0.5f * (i * i);
}

template <typename PointT>
void setPointRGBA(std::size_t i, PointT& point)
{
    point.r = i % 2;
    point.g = i % 10;
    point.b = i % 42;
    point.a = i % 255;
}

template <typename PointT>
void setPointLabel(std::size_t i, PointT& point)
{
    point.label = i % 1000000;
}

template <typename PointCloudT>
void fillXYZ(PointCloudT& pointCloud)
{
    for (std::size_t i = 0; i < pointCloud.size(); ++i)
    {
        setPointXYZ(i, pointCloud[i]);
    }
}

template <typename PointCloudT>
void fillXYZPoint(PointCloudT& pointCloud)
{
    for (std::size_t i = 0; i < pointCloud.size(); ++i)
    {
        setPointXYZ(i, pointCloud[i].point);
    }
}

template <typename PointCloudT>
void fillRGBA(PointCloudT& pointCloud)
{
    for (std::size_t i = 0; i < pointCloud.size(); ++i)
    {
        setPointRGBA(i, pointCloud[i]);
    }
}

template <typename PointCloudT>
void fillRGBAColor(PointCloudT& pointCloud)
{
    for (std::size_t i = 0; i < pointCloud.size(); ++i)
    {
        setPointRGBA(i, pointCloud[i].color);
    }
}

template <typename PointCloudT>
void fillLabel(PointCloudT& pointCloud)
{
    for (std::size_t i = 0; i < pointCloud.size(); ++i)
    {
        setPointLabel(i, pointCloud[i]);
    }
}


BOOST_AUTO_TEST_CASE(test_convertFromToPCL_PointXYZ_to_Point3D)
{
    pcl::PointCloud<pcl::PointXYZ> inPcl(POINT_CLOUD_SIZE, 1), outPcl;
    visionx::Point3DList in(POINT_CLOUD_SIZE), out;

    fillXYZ(inPcl);
    fillXYZ(in);

    vtools::convertFromPCL(inPcl, out);
    BOOST_CHECK_EQUAL_COLLECTIONS(in.begin(), in.end(), out.begin(), out.end());

    out = vtools::convertFromPCL<visionx::Point3D>(inPcl);
    out = vtools::convertFromPCL<visionx::Point3DList::value_type>(inPcl);
    BOOST_CHECK_EQUAL_COLLECTIONS(in.begin(), in.end(), out.begin(), out.end());

    vtools::convertToPCL(in, outPcl);
    BOOST_CHECK_EQUAL_COLLECTIONS(inPcl.begin(), inPcl.end(), outPcl.begin(), outPcl.end());

    outPcl = *vtools::convertToPCL<pcl::PointXYZ>(in);
    outPcl = *vtools::convertToPCL<pcl::PointCloud<pcl::PointXYZ>::PointType>(in);
    BOOST_CHECK_EQUAL_COLLECTIONS(in.begin(), in.end(), out.begin(), out.end());
}


BOOST_AUTO_TEST_CASE(test_convertFromPCL_PointXYZRGBA_to_ColoredPoint3D)
{
    pcl::PointCloud<pcl::PointXYZRGBA> inPcl(POINT_CLOUD_SIZE, 1), outPcl;
    visionx::ColoredPointCloud in(POINT_CLOUD_SIZE), out;

    fillXYZ(inPcl);
    fillXYZPoint(in);

    fillRGBA(inPcl);
    fillRGBAColor(in);

    vtools::convertFromPCL(inPcl, out);
    BOOST_CHECK_EQUAL_COLLECTIONS(in.begin(), in.end(), out.begin(), out.end());

    out = vtools::convertFromPCL<visionx::ColoredPoint3D>(inPcl);
    out = vtools::convertFromPCL<visionx::ColoredPointCloud::value_type>(inPcl);
    BOOST_CHECK_EQUAL_COLLECTIONS(in.begin(), in.end(), out.begin(), out.end());

    vtools::convertToPCL(in, outPcl);
    BOOST_CHECK_EQUAL_COLLECTIONS(inPcl.begin(), inPcl.end(), outPcl.begin(), outPcl.end());

    outPcl = *vtools::convertToPCL<pcl::PointXYZRGBA>(in);
    outPcl = *vtools::convertToPCL<pcl::PointCloud<pcl::PointXYZRGBA>::PointType>(in);
    BOOST_CHECK_EQUAL_COLLECTIONS(in.begin(), in.end(), out.begin(), out.end());
}


BOOST_AUTO_TEST_CASE(test_convertFromPCL_PointXYZL_to_LabeledPoint3D)
{
    pcl::PointCloud<pcl::PointXYZL> inPcl(POINT_CLOUD_SIZE, 1), outPcl;
    visionx::LabeledCloud in(POINT_CLOUD_SIZE), out;

    fillXYZ(inPcl);
    fillXYZPoint(in);

    fillLabel(inPcl);
    fillLabel(in);

    vtools::convertFromPCL(inPcl, out);
    BOOST_CHECK_EQUAL_COLLECTIONS(in.begin(), in.end(), out.begin(), out.end());

    out = vtools::convertFromPCL<visionx::LabeledPoint3D>(inPcl);
    out = vtools::convertFromPCL<visionx::LabeledCloud::value_type>(inPcl);
    BOOST_CHECK_EQUAL_COLLECTIONS(in.begin(), in.end(), out.begin(), out.end());

    vtools::convertToPCL(in, outPcl);
    BOOST_CHECK_EQUAL_COLLECTIONS(inPcl.begin(), inPcl.end(), outPcl.begin(), outPcl.end());

    outPcl = *vtools::convertToPCL<pcl::PointXYZL>(in);
    outPcl = *vtools::convertToPCL<pcl::PointCloud<pcl::PointXYZL>::PointType>(in);
    BOOST_CHECK_EQUAL_COLLECTIONS(in.begin(), in.end(), out.begin(), out.end());
}


BOOST_AUTO_TEST_SUITE_END()
