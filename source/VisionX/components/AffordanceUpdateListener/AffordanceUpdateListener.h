/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <VisionX/interface/components/AffordanceUpdateListener.h>

namespace armarx
{
    /**
     * @class AffordanceUpdateListenerPropertyDefinitions
     * @brief
     */
    class AffordanceUpdateListenerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        AffordanceUpdateListenerPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @class AffordanceUpdateListener
     * @ingroup VisionX-Components
     * @brief A brief description
     *
     *
     * Detailed Description
     */
    class AffordanceUpdateListener :
        virtual public armarx::Component,
        virtual public armarx::AffordanceUpdateListenerInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "AffordanceUpdateListener";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void reportNewAffordances(const Ice::Current& c = Ice::emptyCurrent) override;
        bool newAffordancesAvailable(const Ice::Current& c = Ice::emptyCurrent) override;

    protected:
        boost::mutex affordancesMutex;
        bool news;
    };
}

