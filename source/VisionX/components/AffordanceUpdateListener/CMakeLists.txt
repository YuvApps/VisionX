armarx_component_set_name("AffordanceUpdateListener")

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore VisionXInterfaces)

set(SOURCES AffordanceUpdateListener.cpp)
set(HEADERS AffordanceUpdateListener.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)
