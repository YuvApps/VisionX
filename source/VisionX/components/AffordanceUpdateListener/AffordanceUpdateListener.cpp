/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "AffordanceUpdateListener.h"


using namespace armarx;


void AffordanceUpdateListener::onInitComponent()
{
    usingTopic("ExtractedAffordances");
}


void AffordanceUpdateListener::onConnectComponent()
{
    news = false;
}


void AffordanceUpdateListener::onDisconnectComponent()
{
}


void AffordanceUpdateListener::onExitComponent()
{
}

PropertyDefinitionsPtr AffordanceUpdateListener::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new AffordanceUpdateListenerPropertyDefinitions(
                                      getConfigIdentifier()));
}

void AffordanceUpdateListener::reportNewAffordances(const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(affordancesMutex);

    ARMARX_INFO << "New affordances received";
    news = true;
}

bool AffordanceUpdateListener::newAffordancesAvailable(const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(affordancesMutex);

    ARMARX_INFO << "Asked for new affordances: " << news;
    bool res = news;
    news = false;
    return res;
}

