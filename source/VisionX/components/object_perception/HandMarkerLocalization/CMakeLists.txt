armarx_component_set_name(HandMarkerLocalization)

set(COMPONENT_LIBS VisionXInterfaces
                   VisionXCore
                   VisionXTools
                   ArmarXCore
                   RobotAPICore
                   ArmarXCoreObservers
                   MemoryXInterfaces
                   MemoryXCore
                   MemoryXObjectRecognitionHelpers
                   MemoryXEarlyVisionHelpers
)

set(SOURCES HandMarkerLocalization.cpp)
set(HEADERS HandMarkerLocalization.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

set(EXE_SOURCES main.cpp HandMarkerLocalizationApp.h)
armarx_add_component_executable("${EXE_SOURCES}")
