/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     David Schiebener (schiebener at kit dot edu)
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "HandMarkerLocalization.h"

// Core
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

// MemoryX
#include <MemoryX/core/entity/Entity.h>
#include <MemoryX/core/entity/ProbabilityMeasures.h>
#include <MemoryX/libraries/helpers/ObjectRecognitionHelpers/ObjectRecognitionWrapper.h>

// IVT
#include <Calibration/Calibration.h>
#include <Image/ImageProcessor.h>
#include <Color/ColorParameterSet.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>

// IVTRecognition
#include <Locators/VisualTargetLocator.h>
namespace visionx
{



    HandMarkerLocalization::HandMarkerLocalization()
    {
        numberOfResultImages = 3;
    }


    void HandMarkerLocalization::onInitObjectLocalizerProcessor()
    {
        usingProxy(getProperty<std::string>("RobotStateComponentName").getValue());
        tcpNameLeft = getProperty<std::string>("TCPNameLeft").getValue();
        tcpNameRight = getProperty<std::string>("TCPNameRight").getValue();
        markerNameLeft = getProperty<std::string>("MarkerNameLeft").getValue();
        markerNameRight = getProperty<std::string>("MarkerNameRight").getValue();
        useVision = getProperty<bool>("UseVision").getValue();
    }


    void HandMarkerLocalization::onConnectObjectLocalizerProcessor()
    {
        robotStateComponent = getProxy<armarx::RobotStateComponentInterfacePrx>(getProperty<std::string>("RobotStateComponentName").getValue());
        localRobot = armarx::RemoteRobot::createLocalClone(robotStateComponent);

    }



    bool HandMarkerLocalization::initRecognizer()
    {
        if (useVision)
        {
            std::string colorParameterFilename = getProperty<std::string>("ColorParameterFile").getValue();

            if (!armarx::ArmarXDataPath::getAbsolutePath(colorParameterFilename, colorParameterFilename))
            {
                ARMARX_ERROR << "Could not find color parameter file in ArmarXDataPath: " << colorParameterFilename;
                return false;
            }

            ARMARX_VERBOSE << "Color parameter file name: " << colorParameterFilename;

            visualTargetLocator = new CVisualTargetLocator();

            if (!visualTargetLocator->Init(colorParameterFilename.c_str(), getStereoCalibration()))
            {
                return false;
            }

            colorParameterSet = new CColorParameterSet();
            colorParameterSet->LoadFromFile(colorParameterFilename.c_str());
        }

        showSegmentedResultImages = getProperty<bool>("ShowSegmentedResultImages").getValue();

        if (!showSegmentedResultImages)
        {
            ::ImageProcessor::Zero(resultImages[2]);
        }

        return true;
    }



    bool HandMarkerLocalization::addObjectClass(const memoryx::EntityPtr& objectClassEntity, const memoryx::GridFileManagerPtr& fileManager)
    {
        ARMARX_VERBOSE << "Adding object class " << objectClassEntity->getName();

        memoryx::EntityWrappers::HandMarkerBallWrapperPtr recognitionWrapper = objectClassEntity->addWrapper(new memoryx::EntityWrappers::HandMarkerBallWrapper(fileManager));
        markerColors[objectClassEntity->getName()] = recognitionWrapper->getObjectColor();

        return true;
    }


    memoryx::ObjectLocalizationResultList HandMarkerLocalization::localizeObjectClasses(const std::vector<std::string>& objectClassNames, CByteImage** cameraImages, armarx::MetaInfoSizeBasePtr imageMetaInfo, CByteImage** resultImages)
    {
        // there will be only a valid timestamp in the metainfo if vision is used
        auto timestamp = useVision ? imageMetaInfo->timeProvided : armarx::TimeUtil::GetTime().toMicroSeconds();

        const float recognitionCertainty = 0.8f;
        const float uncertaintyWhenReturningKinematicPosition = 10000;
        const float maxDistToFwdKinPose = getProperty<float>("MaximalAcceptedDistanceFromForwardKinematics").getValue();
        if (!armarx::RemoteRobot::synchronizeLocalCloneToTimestamp(localRobot, robotStateComponent, timestamp))
        {
            ARMARX_WARNING << deactivateSpam(1) << "Failed to synchronize local robot - results are probably wrong";
        }
        const auto agentName = localRobot->getName();
        std::string firstObjectName = objectClassNames.at(0);
        bool firstEntryIsLeft = firstObjectName.find("left") != std::string::npos || firstObjectName.find("Left") != std::string::npos;
        // get poses of hands and markers
        Eigen::Matrix4f leftHandPose = localRobot->getRobotNode(tcpNameLeft)->getPoseInRootFrame();
        Eigen::Matrix4f rightHandPose = localRobot->getRobotNode(tcpNameRight)->getPoseInRootFrame();
        Eigen::Matrix4f leftMarkerPose = localRobot->getRobotNode(markerNameLeft)->getPoseInRootFrame();
        Eigen::Matrix4f rightMarkerPose = localRobot->getRobotNode(markerNameRight)->getPoseInRootFrame();
        // get orientation
        armarx::FramedOrientationPtr orientationLeftHand = new armarx::FramedOrientation(leftHandPose, localRobot->getRootNode()->getName(), agentName);
        armarx::FramedOrientationPtr orientationRightHand = new armarx::FramedOrientation(rightHandPose, localRobot->getRootNode()->getName(), agentName);

        // convert all poses to camera coordinates
        VirtualRobot::RobotNodePtr rnCamera = localRobot->getRobotNode(referenceFrameName);
        ARMARX_CHECK_EXPRESSION_W_HINT(rnCamera, "Reference frame not present in remote robot!");
        Eigen::Matrix4f cameraPose = rnCamera->getPoseInRootFrame();
        Eigen::Matrix4f cameraPoseInv = cameraPose.inverse();
        leftHandPose = cameraPoseInv * leftHandPose;
        rightHandPose = cameraPoseInv * rightHandPose;
        leftMarkerPose = cameraPoseInv * leftMarkerPose;
        rightMarkerPose = cameraPoseInv * rightMarkerPose;
        // marker offsets to the TCP
        Eigen::Vector3f markerOffsetToTCPLeft = leftHandPose.block<3, 1>(0, 3) - leftMarkerPose.block<3, 1>(0, 3);
        Eigen::Vector3f markerOffsetToTCPRight = rightHandPose.block<3, 1>(0, 3) - rightMarkerPose.block<3, 1>(0, 3);
        // hand orientation from the kinematics will be returned because the localization does not give an orientation

        memoryx::ObjectLocalizationResultList resultList;

        if (useVision)
        {
            // localize marker(s)
            Vec3d handMarkerPositionLeft, handMarkerPositionRight;
            int numFoundMarkers = 0;

            if (getResultImagesEnabled())
            {
                numFoundMarkers = visualTargetLocator->LocateOneOrTwoTargets(cameraImages, resultImages, handMarkerPositionLeft, handMarkerPositionRight, -1, getImagesAreUndistorted(), markerColors[firstObjectName]);
                // old version of the method
                //visualTargetLocator->Locate(cameraImages, resultImages, handMarkerPositionLeft, -1, getImagesAreUndistorted(), markerColors[firstObjectName]);
            }
            else
            {
                numFoundMarkers = visualTargetLocator->LocateOneOrTwoTargets(cameraImages, NULL, handMarkerPositionLeft, handMarkerPositionRight, -1, getImagesAreUndistorted(), markerColors[firstObjectName]);
            }

            ARMARX_VERBOSE << "Found " << numFoundMarkers << " hand markers.";

            if (numFoundMarkers == 0)
            {
                ARMARX_INFO << "Marker not found, color: " << markerColors[firstObjectName];
            }

            // return found marker(s)
            if (numFoundMarkers > 0)
            {
                // assemble result
                memoryx::ObjectLocalizationResult result;
                result.recognitionCertainty = recognitionCertainty;
                //            result.localizationType = eHandMarkerLocalizer;
                Eigen::Vector3f position;

                if (numFoundMarkers == 1)
                {
                    if (objectClassNames.size() == 1)
                    {
                        result.objectClassName = objectClassNames.at(0);
                        position << handMarkerPositionLeft.x, handMarkerPositionLeft.y, handMarkerPositionLeft.z;

                        float distToFwdKinPos;
                        if (firstEntryIsLeft)
                        {
                            distToFwdKinPos = (leftMarkerPose.block<3, 1>(0, 3) - position).norm();
                            position = position + markerOffsetToTCPLeft;
                            result.orientation = orientationLeftHand;
                        }
                        else
                        {
                            distToFwdKinPos = (rightMarkerPose.block<3, 1>(0, 3) - position).norm();
                            position = position + markerOffsetToTCPRight;
                            result.orientation = orientationRightHand;
                        }

                        result.position = new armarx::FramedPosition(position, referenceFrameName, agentName);
                        result.positionNoise = calculateLocalizationUncertainty(position);
                        result.timeStamp = new armarx::TimestampVariant(imageMetaInfo->timeProvided);
                        armarx::FramedPositionPtr pos = armarx::FramedPositionPtr::dynamicCast(result.position);
                        if (firstEntryIsLeft)
                        {
                            ARMARX_VERBOSE << "Position of left hand: " << pos->toRootFrame(localRobot)->output();
                        }
                        else
                        {
                            ARMARX_VERBOSE << "Position of right hand: " << pos->toRootFrame(localRobot)->output();
                        }
                        ARMARX_DEBUG << "Distance to position from forward kinematics: " << distToFwdKinPos << "(max: " << maxDistToFwdKinPose << ")";

                        if (distToFwdKinPos < maxDistToFwdKinPose)
                        {
                            resultList.push_back(result);

                            // mark TCP pose with a cross
                            if (getResultImagesEnabled())
                            {
                                drawCrossInImage(resultImages[0], position, true);
                                drawCrossInImage(resultImages[1], position, false);
                            }
                        }
                        else
                        {
                            ARMARX_VERBOSE << "Refusing found marker because it is too far from the position from the kinematic model (" << distToFwdKinPos << ", max: " << maxDistToFwdKinPose << ")";
                        }
                    }
                    else if (objectClassNames.size() == 2)
                    {
                        ARMARX_IMPORTANT << "Found only 1 hand marker, but 2 were searched for. Returning the localized marker position as a localization result for the closer hand from forward kinematics." ;

                        position << handMarkerPositionLeft.x, handMarkerPositionLeft.y, handMarkerPositionLeft.z;
                        float distToFwdKinPosLeft = (leftMarkerPose.block<3, 1>(0, 3) - position).norm();
                        float distToFwdKinPosRight = (rightMarkerPose.block<3, 1>(0, 3) - position).norm();

                        std::string nameLeft, nameRight;

                        if (firstEntryIsLeft)
                        {
                            nameLeft = objectClassNames.at(0);
                            nameRight = objectClassNames.at(1);
                        }
                        else
                        {
                            nameLeft = objectClassNames.at(1);
                            nameRight = objectClassNames.at(0);
                        }

                        ARMARX_VERBOSE << "Distance to position from forward kinematics for " << nameLeft << ": " << distToFwdKinPosLeft << "(max: " << maxDistToFwdKinPose << ")";
                        ARMARX_VERBOSE << "Distance to position from forward kinematics for " << nameRight << ": " << distToFwdKinPosRight << "(max: " << maxDistToFwdKinPose << ")";


                        if (distToFwdKinPosLeft < maxDistToFwdKinPose && distToFwdKinPosLeft < distToFwdKinPosRight)
                        {
                            result.objectClassName = nameLeft;
                            position = position + markerOffsetToTCPLeft;
                            result.position = new armarx::FramedPosition(position, referenceFrameName, agentName);
                            result.positionNoise = calculateLocalizationUncertainty(position);
                            result.orientation = orientationLeftHand;
                            resultList.push_back(result);
                        }

                        if (distToFwdKinPosRight < maxDistToFwdKinPose && distToFwdKinPosRight < distToFwdKinPosLeft)
                        {
                            result.objectClassName = nameRight;
                            position = position + markerOffsetToTCPRight;
                            result.position = new armarx::FramedPosition(position, referenceFrameName, agentName);
                            result.positionNoise = calculateLocalizationUncertainty(position);
                            result.orientation = orientationRightHand;
                            resultList.push_back(result);
                        }
                    }
                }
                else if (numFoundMarkers == 2)
                {
                    if (objectClassNames.size() == 1)
                    {
                        ARMARX_IMPORTANT << "Found 2 hand markers, but only 1 was searched for." ;

                        result.objectClassName = objectClassNames.at(0);

                        position << handMarkerPositionLeft.x, handMarkerPositionLeft.y, handMarkerPositionLeft.z;

                        float distToFwdKinPosLeft = (leftMarkerPose.block<3, 1>(0, 3) - position).norm();
                        float distToFwdKinPosRight = (rightMarkerPose.block<3, 1>(0, 3) - position).norm();

                        if (firstEntryIsLeft)
                        {
                            ARMARX_VERBOSE << "Distance to position from forward kinematics: " << distToFwdKinPosLeft << "(max: " << maxDistToFwdKinPose << ")";

                            position = position + markerOffsetToTCPLeft;
                            result.position = new armarx::FramedPosition(position, referenceFrameName, agentName);
                            result.positionNoise = calculateLocalizationUncertainty(position);
                            result.orientation = orientationLeftHand;

                            if (distToFwdKinPosLeft < maxDistToFwdKinPose && distToFwdKinPosLeft < distToFwdKinPosRight)
                            {
                                resultList.push_back(result);
                            }
                        }
                        else
                        {
                            ARMARX_VERBOSE << "Distance to position from forward kinematics: " << distToFwdKinPosRight << "(max: " << maxDistToFwdKinPose << ")";

                            position = position + markerOffsetToTCPRight;
                            result.position = new armarx::FramedPosition(position, referenceFrameName, agentName);
                            result.positionNoise = calculateLocalizationUncertainty(position);
                            result.orientation = orientationRightHand;

                            if (distToFwdKinPosRight < maxDistToFwdKinPose && distToFwdKinPosRight < distToFwdKinPosLeft)
                            {
                                resultList.push_back(result);
                            }
                        }



                        position << handMarkerPositionRight.x, handMarkerPositionRight.y, handMarkerPositionRight.z;

                        distToFwdKinPosLeft = (leftMarkerPose.block<3, 1>(0, 3) - position).norm();
                        distToFwdKinPosRight = (rightMarkerPose.block<3, 1>(0, 3) - position).norm();

                        if (firstEntryIsLeft)
                        {
                            ARMARX_VERBOSE << "Distance to position from forward kinematics: " << distToFwdKinPosLeft << "(max: " << maxDistToFwdKinPose << ")";

                            position = position + markerOffsetToTCPLeft;
                            result.position = new armarx::FramedPosition(position, referenceFrameName, agentName);
                            result.positionNoise = calculateLocalizationUncertainty(position);
                            result.orientation = orientationLeftHand;

                            if (distToFwdKinPosLeft < maxDistToFwdKinPose && distToFwdKinPosLeft < distToFwdKinPosRight)
                            {
                                resultList.push_back(result);
                            }
                        }
                        else
                        {
                            ARMARX_VERBOSE << "Distance to position from forward kinematics: " << distToFwdKinPosRight << "(max: " << maxDistToFwdKinPose << ")";

                            position = position + markerOffsetToTCPRight;
                            result.position = new armarx::FramedPosition(position, referenceFrameName, agentName);
                            result.positionNoise = calculateLocalizationUncertainty(position);
                            result.orientation = orientationRightHand;

                            if (distToFwdKinPosRight < maxDistToFwdKinPose && distToFwdKinPosRight < distToFwdKinPosLeft)
                            {
                                resultList.push_back(result);
                            }
                        }
                    }
                    else if (objectClassNames.size() == 2)
                    {
                        std::string nameLeft, nameRight;

                        if (firstEntryIsLeft)
                        {
                            nameLeft = objectClassNames.at(0);
                            nameRight = objectClassNames.at(1);
                        }
                        else
                        {
                            nameLeft = objectClassNames.at(1);
                            nameRight = objectClassNames.at(0);
                        }

                        result.objectClassName = nameLeft;
                        position << handMarkerPositionLeft.x, handMarkerPositionLeft.y, handMarkerPositionLeft.z;
                        position = position + markerOffsetToTCPLeft;
                        float distToFwdKinPosLeft = (leftMarkerPose.block<3, 1>(0, 3) - position).norm();
                        float distToFwdKinPosRight = (rightMarkerPose.block<3, 1>(0, 3) - position).norm();
                        result.position = new armarx::FramedPosition(position, referenceFrameName, agentName);
                        result.orientation = orientationLeftHand;
                        result.positionNoise = calculateLocalizationUncertainty(position);
                        ARMARX_VERBOSE << "Distance to position from forward kinematics (left): " << distToFwdKinPosLeft << "(max: " << maxDistToFwdKinPose << ")";

                        if (distToFwdKinPosLeft < maxDistToFwdKinPose && distToFwdKinPosLeft < distToFwdKinPosRight)
                        {
                            resultList.push_back(result);
                        }

                        result.objectClassName = nameRight;
                        position << handMarkerPositionRight.x, handMarkerPositionRight.y, handMarkerPositionRight.z;
                        position = position + markerOffsetToTCPRight;
                        distToFwdKinPosLeft = (leftMarkerPose.block<3, 1>(0, 3) - position).norm();
                        distToFwdKinPosRight = (rightMarkerPose.block<3, 1>(0, 3) - position).norm();
                        result.position = new armarx::FramedPosition(position, referenceFrameName, agentName);
                        result.orientation = orientationRightHand;
                        result.positionNoise = calculateLocalizationUncertainty(position);
                        ARMARX_VERBOSE << "Distance to position from forward kinematics (right): " << distToFwdKinPosRight << "(max: " << maxDistToFwdKinPose << ")";

                        if (distToFwdKinPosRight < maxDistToFwdKinPose && distToFwdKinPosRight < distToFwdKinPosLeft)
                        {
                            resultList.push_back(result);
                        }
                    }
                }
            }

            // draw color segmented image
            if (showSegmentedResultImages)
            {
                CByteImage* pSegmentedImage = new CByteImage(cameraImages[0]->width, cameraImages[0]->height, CByteImage::eGrayScale);
                ::ImageProcessor::FilterColor(cameraImages[0], pSegmentedImage, markerColors[firstObjectName], colorParameterSet, false);
                ::ImageProcessor::ConvertImage(pSegmentedImage, resultImages[2]);
                delete pSegmentedImage;
            }
        }

        // localization failed or vision is turned off
        if (resultList.size() < objectClassNames.size())
        {
            // return poses from forward kinematics
            memoryx::ObjectLocalizationResult result;
            result.recognitionCertainty = recognitionCertainty;
            //        result.localizationType = eForwardKinematics;
            armarx::FramedPositionPtr positionLeftHand = new armarx::FramedPosition(localRobot->getRobotNode(tcpNameLeft)->getPoseInRootFrame(), localRobot->getRootNode()->getName(), agentName);
            armarx::FramedPositionPtr positionRightHand = new armarx::FramedPosition(localRobot->getRobotNode(tcpNameRight)->getPoseInRootFrame(), localRobot->getRootNode()->getName(), agentName);
            Eigen::Matrix3f variances;
            float var = uncertaintyWhenReturningKinematicPosition * uncertaintyWhenReturningKinematicPosition;
            variances <<   var, 0, 0,
                      0, var, 0,
                      0, 0, var;

            if (objectClassNames.size() == 1)
            {
                ARMARX_INFO << "Returning hand pose from forward kinematics for one hand: " << objectClassNames.at(0);

                result.objectClassName = objectClassNames.at(0);

                if (firstEntryIsLeft)
                {
                    ARMARX_INFO << "left hand position: " << positionLeftHand->output();
                    result.position = positionLeftHand;
                    result.orientation = orientationLeftHand;
                    Eigen::Vector3f mean = positionLeftHand->toEigen();
                    result.positionNoise = new memoryx::MultivariateNormalDistribution(mean, variances);
                }
                else
                {
                    ARMARX_INFO << "right hand position: " << positionRightHand->output();
                    result.position = positionRightHand;
                    result.orientation = orientationRightHand;
                    Eigen::Vector3f mean = positionRightHand->toEigen();
                    result.positionNoise = new memoryx::MultivariateNormalDistribution(mean, variances);
                }

                resultList.push_back(result);
            }
            else if (objectClassNames.size() == 2)
            {
                std::string leftName = firstEntryIsLeft ? objectClassNames.at(0) : objectClassNames.at(1);
                std::string rightName = firstEntryIsLeft ? objectClassNames.at(1) : objectClassNames.at(0);

                if (resultList.size() == 0)
                {
                    ARMARX_INFO << "Returning hand pose from forward kinematics for two hands: " << objectClassNames.at(0) << ", " << objectClassNames.at(1);

                    result.objectClassName = leftName;
                    result.position = positionLeftHand;
                    result.orientation = orientationLeftHand;
                    Eigen::Vector3f mean = positionLeftHand->toEigen();
                    result.positionNoise = new memoryx::MultivariateNormalDistribution(mean, variances);
                    resultList.push_back(result);

                    result.objectClassName = rightName;
                    result.position = positionRightHand;
                    result.orientation = orientationRightHand;
                    mean = positionRightHand->toEigen();
                    result.positionNoise = new memoryx::MultivariateNormalDistribution(mean, variances);
                    resultList.push_back(result);
                }
                else
                {
                    if (resultList.at(0).objectClassName == leftName)
                    {
                        ARMARX_INFO << "Returning hand pose from recognition for " << leftName << " and from forward kinematics for " << rightName;
                        result.objectClassName = rightName;
                        result.position = positionRightHand;
                        result.orientation = orientationRightHand;
                        Eigen::Vector3f mean = positionRightHand->toEigen();
                        result.positionNoise = new memoryx::MultivariateNormalDistribution(mean, variances);
                        resultList.push_back(result);
                    }
                    else
                    {
                        ARMARX_INFO << "Returning hand pose from recognition for " << rightName << " and from forward kinematics for " << leftName;
                        result.objectClassName = leftName;
                        result.position = positionLeftHand;
                        result.orientation = orientationLeftHand;
                        Eigen::Vector3f mean = positionLeftHand->toEigen();
                        result.positionNoise = new memoryx::MultivariateNormalDistribution(mean, variances);
                        resultList.push_back(result);
                    }
                }
            }
            else
            {
                ARMARX_WARNING << "More than 2 hands requested - this case is not handled here";
            }
        }
        for (auto& result : resultList)
        {
            result.timeStamp = new armarx::TimestampVariant(timestamp);
        }

        ARMARX_DEBUG << "Finished localization, returning.";

        return resultList;
    }



    void HandMarkerLocalization::drawCrossInImage(CByteImage* image, Eigen::Vector3f point, bool leftCamera)
    {
        Vec3d positionIVT = {point(0), point(1), point(2)};
        Vec2d projectedPoint;

        if (leftCamera)
        {
            getStereoCalibration()->GetLeftCalibration()->WorldToImageCoordinates(positionIVT, projectedPoint, getImagesAreUndistorted());
        }
        else
        {
            getStereoCalibration()->GetRightCalibration()->WorldToImageCoordinates(positionIVT, projectedPoint, getImagesAreUndistorted());
        }

        const int size = 3;

        if (size < projectedPoint.x && size < projectedPoint.y && projectedPoint.x < image->width - size - 1 && projectedPoint.y < image->height - size - 3)
        {
            for (int i = -size; i <= size; i++)
            {
                int indexHorizontalLine = 3 * ((int)projectedPoint.y * image->width + (int)projectedPoint.x + i);
                int indexVerticalLine = 3 * (((int)projectedPoint.y + i) * image->width + (int)projectedPoint.x);
                image->pixels[indexHorizontalLine] = 255;
                image->pixels[indexHorizontalLine + 1] = 255;
                image->pixels[indexHorizontalLine + 2] = 255;
                image->pixels[indexVerticalLine] = 255;
                image->pixels[indexVerticalLine + 1] = 255;
                image->pixels[indexVerticalLine + 2] = 255;
            }
        }
    }
}
