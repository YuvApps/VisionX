/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     Kai Welke (welke at kit dot edu)
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ObjectLocalizerProcessorJob.h"
#include "ObjectLocalizerProcessor.h"

// IVT
#include <Image/ImageProcessor.h>


using namespace visionx;

ObjectLocalizerProcessorJob::ObjectLocalizerProcessorJob(ObjectLocalizerProcessor* processor)
    : processor(processor)
    , jobPending(false)
    , jobFinished(false)
    , abortRequested(false)
{
}

void ObjectLocalizerProcessorJob::start(std::vector<std::string> objectClassNames)
{
    // set data and notify processing thread
    armarx::ScopedLock lock(jobMutex);
    this->objectClassNames = objectClassNames;
    jobPending = true;
    jobFinished = false;
}

void ObjectLocalizerProcessorJob::reset()
{
    abortRequested = false;
}

memoryx::ObjectLocalizationResultList ObjectLocalizerProcessorJob::waitForResult()
{
    while (!jobFinished)
    {
        usleep(1000);
    }
    jobFinished = false;
    armarx::ScopedLock lock(jobMutex);
    return result;
}

void ObjectLocalizerProcessorJob::exit()
{
    abortRequested = true;
}

void ObjectLocalizerProcessorJob::process()
{
    if (!processor)
    {
        ARMARX_ERROR << "No processor set";
        return;
    }

    // allow initialization of recognition methods
    processor->initRecognizer();

    // fill object database with objects from prior knowledge
    processor->initObjectClasses();

    // main loop
    memoryx::ObjectLocalizationResultList localResult;

    while (!abortRequested)
    {
        try
        {
            // wait for new processing job
            while (!jobPending && !abortRequested)
            {
                usleep(1000);
            }
            jobPending = false;
            ARMARX_VERBOSE << "Job received";

            if (abortRequested)
            {
                ARMARX_INFO << "ObjectLocalizerProcessorJob::process(): exit requested";
                continue;
            }


            localResult.clear();

            ARMARX_DEBUG << "ObjectLocalizerProcessorJob::process(): starting localization";
            int numImages = processor->getImages(processor->imageProviderName, processor->cameraImages, processor->imageMetaInfo);
            if (numImages > 0)
            {
                // recognize
                if (processor->getResultImagesEnabled())
                {
                    if (processor->isResultImageMaskEnabled())
                    {
                        Eigen::Vector3i colorMask = processor->getColorMask();
                        for (int n = 0; n < numImages; n++)
                        {
                            for (int j = 0; j < processor->resultImages[n]->height; j++)
                            {
                                for (int i = 0; i < processor->resultImages[n]->width; i++)
                                {
                                    processor->resultImages[n]->pixels[3 * (j * processor->resultImages[n]->width + i) + 0] = colorMask(0);
                                    processor->resultImages[n]->pixels[3 * (j * processor->resultImages[n]->width + i) + 1] = colorMask(1);
                                    processor->resultImages[n]->pixels[3 * (j * processor->resultImages[n]->width + i) + 2] = colorMask(2);
                                }
                            }
                        }
                    }
                    else
                    {
                        ::ImageProcessor::CopyImage(processor->cameraImages[0], processor->resultImages[0]);
                        if (numImages > 1)
                        {
                            ::ImageProcessor::CopyImage(processor->cameraImages[1], processor->resultImages[1]);
                        }
                    }
                }
                ARMARX_DEBUG << "starting localization algo";
                localResult = processor->localizeObjectClasses(objectClassNames, processor->cameraImages, processor->imageMetaInfo, processor->resultImages);

                // visualize
                if (processor->getResultImagesEnabled())
                {
                    processor->provideResultImages(processor->resultImages, processor->imageMetaInfo);
                }
                ARMARX_DEBUG << "finished localization algo";
            }
            else
            {
                ARMARX_WARNING << deactivateSpam(1) << "Could not get images";
            }
        }
        catch (std::exception& e)
        {
            ARMARX_ERROR << "Localization failed: " << processor->getName();
            armarx::handleExceptions();
        }

        // set result and notify
        armarx::ScopedLock lock(jobMutex);
        result = localResult;
        jobFinished = true;
    }
}
