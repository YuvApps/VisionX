armarx_component_set_name(BigBowlLocalization)

set(COMPONENT_LIBS VisionXInterfaces
                   VisionXCore
                   VisionXTools
                   ArmarXCore
                   RobotAPICore
                   ArmarXCoreObservers
                   MemoryXInterfaces
                   MemoryXCore
                   MemoryXObjectRecognitionHelpers
                   MemoryXEarlyVisionHelpers
                   VisionXObjectPerception
)

set(SOURCES BigBowlLocalization.cpp)
set(HEADERS BigBowlLocalization.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

set(EXE_SOURCES main.cpp BigBowlLocalizationApp.h)
armarx_add_component_executable("${EXE_SOURCES}")
