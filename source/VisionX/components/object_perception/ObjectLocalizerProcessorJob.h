/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Component
 * @author     Kai Welke (welke at kit dot edu)
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VisionX/interface/components/ObjectLocalizerInterfaces.h>
#include <ArmarXCore/core/system/Synchronization.h>

#include <atomic>

namespace visionx
{
    class ObjectLocalizerProcessor;

    /**
     * ObjectLocalizerProcessorJob encapsules the object localization job.
     * This helper class is used internally by ObjectLocalizerProcessor
     * in order to assure execution of all relevant interface methods in
     * a single thread (as e.g. required for methods using OpenGL as
     * SegmentableObjectRecognition)
     */
    class ObjectLocalizerProcessorJob
    {
    public:
        /**
         * Init the job by providing the ObjectLocalizerProcessor that implements the required functionality.
         *
         * @param processor pointer to the processor. Called by ObjectLocalizerProcessor.
         */
        ObjectLocalizerProcessorJob(ObjectLocalizerProcessor* processor);

        /**
         * Processing method. Schedules initialization of the recognizer, object database and localization
         * requests in a single thread. Called by ObjectLocalizerProcessor.
         */
        void process();

        /**
         * Requests exit to the job. Assures the process method retruns as soon as possible. Non-blocking and
         * called by ObjectLocalizerProcessor.
         */
        void exit();

        /**
         * Start a localization job for the given objectClassName
         *
         * @param objectClassName name of the object class to localize
         */
        void start(std::vector<std::string> objectClassNames);
        void reset();

        /**
         * Wait for the localization result. Will return an empty result on exit
         *
         * @return Localization result list
         */
        memoryx::ObjectLocalizationResultList waitForResult();


    private:
        ObjectLocalizerProcessor* processor;

        std::vector<std::string> objectClassNames;
        memoryx::ObjectLocalizationResultList result;

        armarx::Mutex jobMutex;
        std::atomic<bool> jobPending;
        std::atomic<bool> jobFinished;
        std::atomic<bool> abortRequested;
    };
}

