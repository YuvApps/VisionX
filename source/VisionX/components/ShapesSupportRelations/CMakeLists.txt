armarx_component_set_name("ShapesSupportRelations")

find_package(SemanticObjectRelations)
armarx_build_if(SemanticObjectRelations_FOUND "SemanticObjectRelations is required")


set(COMPONENT_LIBS
    ArmarXCore ArmarXCoreInterfaces  # for DebugObserverInterface
    # RobotAPICore  # for DebugDrawerTopic

    ${PROJECT_NAME}Interfaces
    VisionXSemanticObjectRelations
)


set(SOURCES
    ShapesSupportRelations.cpp
)
set(HEADERS
    ShapesSupportRelations.h
)


armarx_add_component("${SOURCES}" "${HEADERS}")

if (SemanticObjectRelations_FOUND)
    # target_include_directories(ShapesSupportRelations PUBLIC ${SemanticObjectRelations_INCLUDE_DIRS})
    target_link_libraries(ShapesSupportRelations PUBLIC SemanticObjectRelations)
endif()


# add unit tests
add_subdirectory(test)


armarx_component_set_name("ShapesSupportRelationsApp")
set(COMPONENT_LIBS ShapesSupportRelations)
armarx_add_component_executable(main.cpp)
