/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::ShapesSupportRelations
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <Eigen/Core>

#include <SemanticObjectRelations/SupportAnalysis/SupportAnalysis.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <VisionX/interface/components/ShapesSupportRelations.h>
#include <VisionX/interface/libraries/SemanticObjectRelations/ShapesTopic.h>
#include <VisionX/interface/libraries/SemanticObjectRelations/GraphStorage.h>


namespace armarx::semantic
{
    /**
     * @class ShapesSupportRelationsPropertyDefinitions
     * @brief Property definitions of `ShapesSupportRelations`.
     */
    class ShapesSupportRelationsPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ShapesSupportRelationsPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-ShapesSupportRelations ShapesSupportRelations
     * @ingroup VisionX-Components
     * A description of the component ShapesSupportRelations.
     *
     * @class ShapesSupportRelations
     * @ingroup Component-ShapesSupportRelations
     * @brief Brief description of class ShapesSupportRelations.
     *
     * Detailed description of class ShapesSupportRelations.
     */
    class ShapesSupportRelations :
        virtual public armarx::Component,
        virtual public ShapesSupportRelationsInterface
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // ShapesTopic interface
        /// Topic/pipeline: Extract the support graph for `objects` and publish it to the graph topic.
        void reportShapes(const std::string& name, const data::ShapeList& objects,
                          const Ice::Current&) override;

        // SupportRelationsFromShapesInterface interface
        /// Service: Extract the support graph for `objects` and return it.
        data::Graph extractSupportGraph(const data::ShapeList& objects, const Ice::LongSeq& safeObjectIDs,
                                        const Ice::Current& = Ice::emptyCurrent) override;

        data::Graph extractSupportGraph(const semrel::ShapeMap& objects,
                                        const std::set<semrel::ShapeID>& safeObjectIDs);


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        const Eigen::Vector3f gravityInGlobal = - Eigen::Vector3f::UnitZ();

        Eigen::Vector3f getGravityFromRobotStateComponent();


    private:

        // ICE

        /// The graph listener proxy.
        GraphStorageTopicPrx graphTopic;
        std::string graphName;

        /// The robot state component.
        RobotStateComponentInterfacePrx robotStateComponent;

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;
        /// Debug drawer. Used for 3D visualization.
        armarx::DebugDrawerTopic debugDrawer;


        // PROCESSING

        /// The support analysis.
        semrel::SupportAnalysis supportAnalysis;


        // OPTIONS

        std::string pointCloudFrameName;

    };
}
