/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::RCPointCloudProvider
 * @author     Fabian Paus ( paus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/util/CPPUtility/TripleBuffer.h>
#include <ArmarXCore/core/Component.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/RemoteGuiComponentPlugin.h>

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/components/pointcloud_core/CapturingPointCloudProvider.h>
#include <VisionX/interface/components/RGBDImageProvider.h>
#include <VisionX/libraries/RoboceptionUtility/RoboceptionUser.h>

#include <Image/ImageProcessor.h>
#include <Calibration/Undistortion.h>


#include <rc_genicam_api/device.h>
#include <rc_genicam_api/stream.h>
#include <rc_genicam_api/imagelist.h>
#include <memory>

namespace rcg
{

    using DevicePtr = std::shared_ptr<rcg::Device>;
    using StreamPtr = std::shared_ptr<rcg::Stream> ;
}

namespace visionx
{


    /**
     * @class RCPointCloudProviderPropertyDefinitions
     * @brief
     */
    class RCPointCloudProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        RCPointCloudProviderPropertyDefinitions(std::string prefix);
    };

    /**
     * @defgroup Component-RCPointCloudProvider RCPointCloudProvider
     * @ingroup VisionX-Components
     * A description of the component RCPointCloudProvider.
     *
     * @class RCPointCloudProvider
     * @ingroup Component-RCPointCloudProvider
     * @brief Brief description of class RCPointCloudProvider.
     *
     * Detailed description of class RCPointCloudProvider.
     */
    class RCPointCloudProvider :
        virtual public RGBDPointCloudProviderInterface,
        virtual public CapturingPointCloudProvider,
        virtual public ImageProvider,
        public RoboceptionUser,
        virtual public armarx::RemoteGuiComponentPluginUser
    {
    public:
        RCPointCloudProvider();

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "RCPointCloudProvider";
        }

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

    protected:
        void onInitCapturingPointCloudProvider() override;

        void onExitCapturingPointCloudProvider() override;

        void onStartCapture(float frameRate) override;

        void onStopCapture() override;

        bool doCapture() override;

        bool hasSharedMemorySupport(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }


        visionx::StereoCalibration getStereoCalibration(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return stereoCalibration;
        }

        bool getImagesAreUndistorted(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }

        std::string getReferenceFrame(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return getProperty<std::string>("ReferenceFrameName").getValue();
        }

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


        void startCaptureForNumFrames(Ice::Int, const Ice::Current&) override;

        // ImageProvider interface
    protected:
        void onInitImageProvider() override;
        void onExitImageProvider() override;

    private:
        armarx::RemoteGui::WidgetPtr buildGui();
        void processGui(armarx::RemoteGui::TabProxy& prx);

        void updateFinalCloudTransform(
            float sx, float sy, float sz,
            float rx, float ry, float rz,
            float tx, float ty, float tz);

        Eigen::Matrix4f getFinalCloudTransform();

    private:
        double scan3dCoordinateScale = 1.0;

        rcg::ImageList intensityBuffer;
        rcg::ImageList disparityBuffer;

        armarx::WriteBufferedTripleBuffer<Eigen::Matrix<float, 9, 1>> finalCloudTransform;
    };
}

