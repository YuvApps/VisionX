/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::KinectV2PointCloudProvider
 * @author     Christoph Pohl (christoph dot pohl at kit dot edu)
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#ifndef VISIONX_KINECTHELPERS_H
#define VISIONX_KINECTHELPERS_H

#include <libfreenect2/libfreenect2.hpp>
#include <Eigen/Geometry>
#include <pcl/point_cloud.h>
#include <opencv2/opencv.hpp>
#include <pcl/point_types.h>
#include <VisionX/tools/ImageUtil.h>

template<typename PointT>
class KinectToPCLHelper
{
public:
    KinectToPCLHelper(const libfreenect2::Freenect2Device::IrCameraParams& depth_p, bool mirror) : mirror_(mirror),
        qnan_(std::numeric_limits<float>::quiet_NaN())
    {
        const int w = 512;
        const int h = 424;
        float* pm1 = colmap.data();
        float* pm2 = rowmap.data();
        for (int i = 0; i < w; i++)
        {
            *pm1++ = (i - depth_p.cx + 0.5) / depth_p.fx;
        }
        for (int i = 0; i < h; i++)
        {
            *pm2++ = (i - depth_p.cy + 0.5) / depth_p.fy;
        }
    }

    typename pcl::PointCloud<PointT>::Ptr
    calculatePointCloud(libfreenect2::Frame& undistorted, libfreenect2::Frame& registered,
                        typename pcl::PointCloud<PointT>::Ptr cloud)
    {
        const std::size_t w = undistorted.width;
        const std::size_t h = undistorted.height;
        cloud->resize(w * h);
        cv::Mat tmp_itD0(undistorted.height, undistorted.width, CV_32FC1, undistorted.data);
        cv::Mat tmp_itRGB0(registered.height, registered.width, CV_8UC4, registered.data);

        if (mirror_)
        {

            cv::flip(tmp_itD0, tmp_itD0, 1);
            cv::flip(tmp_itRGB0, tmp_itRGB0, 1);

        }

        const float* itD0 = (float*) tmp_itD0.ptr();
        const char* itRGB0 = (char*) tmp_itRGB0.ptr();

        PointT* itP = &cloud->points[0];
        bool is_dense = true;

        for (std::size_t y = 0; y < h; ++y)
        {

            const unsigned int offset = y * w;
            const float* itD = itD0 + offset;
            const char* itRGB = itRGB0 + offset * 4;
            const float dy = rowmap(y);
            bool color = pcl::traits::has_color<PointT>::value;
            //            bool label = pcl::traits::has_label<PointT>::value;
            //            bool normal = pcl::traits::has_normal<PointT>::value;
            for (std::size_t x = 0; x < w; ++x, ++itP, ++itD, itRGB += 4)
            {
                const float depth_value = *itD / 1000.0;
                //                auto rgb = tmp_itRGB0.at<cv::Vec4b>(y, x);

                if (!std::isnan(depth_value) && (std::abs(depth_value) >= 0.0001))
                {

                    const float rx = colmap(x) * depth_value;
                    const float ry = dy * depth_value;
                    itP->z = depth_value;
                    itP->x = rx;
                    itP->y = ry;

                    if (color)
                    {
                        itP->b = itRGB[0];
                        itP->g = itRGB[1];
                        itP->r = itRGB[2];
                    }
                }
                else
                {
                    itP->z = qnan_;
                    itP->x = qnan_;
                    itP->y = qnan_;
                    is_dense = false;
                }
            }
        }
        cloud->is_dense = is_dense;
        return cloud;
    };

private:
    Eigen::Matrix<float, 512, 1> colmap;
    Eigen::Matrix<float, 424, 1> rowmap;
    bool mirror_;
    float qnan_;

};

struct KinectToIVTHelper
{
    explicit KinectToIVTHelper(bool mirror) : mirror_(mirror)
    {
    };

    void calculateImages(libfreenect2::Frame& undistorted, libfreenect2::Frame& registered, CByteImage** rgbImages)
    {
        cv::Mat tmp_itD0(undistorted.height, undistorted.width, CV_32FC1, undistorted.data);
        cv::Mat tmp_itRGB0(registered.height, registered.width, CV_8UC4, registered.data);
        calculateImages(tmp_itD0, tmp_itRGB0, rgbImages);
    }

    void calculateImages(cv::Mat& undistorted, cv::Mat& registered, CByteImage** rgbImages)
    {
        if (mirror_)
        {
            cv::flip(undistorted, undistorted, 1);
            cv::flip(registered, registered, 1);
        }
        for (int j = 0; j < undistorted.rows; j++)
        {
            int index = 3 * (j * undistorted.cols);
            for (int i = 0; i < undistorted.cols; i++)
            {
                float value = undistorted.at<float>(j, i);

                visionx::tools::depthValueToRGB((int)value, rgbImages[1]->pixels[index + 0], rgbImages[1]->pixels[index + 1],
                                                rgbImages[1]->pixels[index + 2], false);

                auto v = registered.at<cv::Vec4b>(j, i);
                for (int k = 0; k < 3; k++)
                {
                    rgbImages[0]->pixels[index + k] = v[2 - k];
                }
                index += 3;
            }
        }
    }

private:
    bool mirror_;
};

#endif //VISIONX_KINECTHELPERS_H
