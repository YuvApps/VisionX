armarx_component_set_name("RTABMapRegistration")

find_package(RTABMap 0.16.3 QUIET)
armarx_build_if(RTABMap_FOUND "RTABMap not available")
if(RTABMap_FOUND)
    #since rtabmap does not set rpath.
    # this line is required
    # if rtabmap links against realsense
    find_package(realsense2 QUIET)
endif()

set(COMPONENT_LIBS
    VisionXPointCloud
    VisionXCore
    MemoryXMemoryTypes
    RobotAPICore
    ${RTABMap_LIBRARIES}    
    ${PCL_FILTERS_LIBRARY}
)

set(SOURCES RTABMapRegistration.cpp)
set(HEADERS RTABMapRegistration.h)

armarx_add_component("${SOURCES}" "${HEADERS}")
if(RTABMap_FOUND)
    target_include_directories(RTABMapRegistration SYSTEM PUBLIC ${RTABMap_INCLUDE_DIRS})
endif()
