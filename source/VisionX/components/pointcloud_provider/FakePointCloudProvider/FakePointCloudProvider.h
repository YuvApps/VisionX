/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::FakePointCloudProvider
 * @author     Markus Grotz ( markus-grotz at web dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <cmath>
#include <filesystem>
#include <mutex>
#include <vector>

#include <pcl/common/transforms.h>
#include <pcl/filters/filter.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <RobotAPI/interface/core/RobotState.h>

#include <VisionX/interface/components/FakePointCloudProviderInterface.h>
#include <VisionX/components/pointcloud_core/CapturingPointCloudProvider.h>
#include <VisionX/core/ImageProvider.h>


class CByteImage;

namespace visionx
{
    /// @class FakePointCloudProviderPropertyDefinitions
    class FakePointCloudProviderPropertyDefinitions :
        public CapturingPointCloudProviderPropertyDefinitions
    {
    public:
        FakePointCloudProviderPropertyDefinitions(std::string prefix);
    };


    /**
      * @class FakePointCloudProvider
      * @ingroup VisionX-Components
      * @brief A brief description
      *
      * Detailed Description
      */
    class FakePointCloudProvider :
        virtual public FakePointCloudProviderInterface,
        virtual public CapturingPointCloudProvider,
        virtual public ImageProvider
    {
    public:

        ///  @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

    protected:

        /// @see visionx::PointCloudProviderBase::onInitPointCloudProvider()
        void onInitCapturingPointCloudProvider() override;

        /// @see visionx::PointCloudProviderBase::onExitPointCloudProvider()
        void onExitCapturingPointCloudProvider() override;

        /// @see visionx::PointCloudProviderBase::onStartCapture()
        void onStartCapture(float frameRate) override;

        /// @see visionx::PointCloudProviderBase::onStopCapture()
        void onStopCapture() override;

        /// @see visionx::PointCloudProviderBase::doCapture()
        bool doCapture() override;

        MetaPointCloudFormatPtr getDefaultPointCloudFormat() override;

        /// @see visionx::CapturingImageProvider::onInitImageProvider()
        void onInitImageProvider() override;

        /// @see visionx::CapturingImageProvider::onExitImageProvider()
        void onExitImageProvider() override;

        StereoCalibration getStereoCalibration(const Ice::Current& = Ice::emptyCurrent) override;

        bool getImagesAreUndistorted(const Ice::Current& = Ice::emptyCurrent) override;

        std::string getReferenceFrame(const Ice::Current& = Ice::emptyCurrent) override;


        // mixed inherited stuff
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void setPointCloudFilename(const std::string& filename, const ::Ice::Current& = Ice::emptyCurrent) override;

        bool hasSharedMemorySupport(const Ice::Current& = Ice::emptyCurrent) override;


    private:

        bool loadPointCloud(const std::string& fileName);
        bool loadPointCloudDirectory(const std::string& directoryName);
        bool loadCalibrationFile(std::string fileName, visionx::CameraParameters& calibration);

        template<typename PointT>
        bool processAndProvide(const pcl::PCLPointCloud2Ptr& pointCloud);
        bool processRGBImage(const pcl::PCLPointCloud2Ptr& pointCloud);


    private:

        std::string pointCloudFileName;
        /// Assume point cloud was stored in this frame.
        std::string sourceFrameName;
        /// Frame in which point cloud is provided. (armarx::GlobalFrame by default.)
        std::string targetFrameName;
        std::string providedPointCloudFormat;
        std::string robotStateComponentName;

        std::mutex pointCloudMutex;
        std::vector<pcl::PCLPointCloud2Ptr> pointClouds;

        unsigned int counter = 0;

        bool rewind = false;
        bool removeNAN = false;
        float scaleFactor = 0;

        StereoCalibration calibration;
        CByteImage** rgbImages = nullptr;

        armarx::RobotStateComponentInterfacePrx robotStateComponent;
        VirtualRobot::RobotPtr localRobot;
    };
}
