armarx_component_set_name("KinectV1PointCloudProvider")

find_package(Freenect QUIET)
armarx_build_if(Freenect_FOUND "Libfreenect not available")

find_package(libusb-1.0 QUIET)
armarx_build_if(LIBUSB_1_FOUND "libusb-1.0 not available")

set(COMPONENT_LIBS
    VisionXPointCloud
    VisionXCore
    ${Freenect_LIBRARIES}
    ${LIBUSB_1_LIBRARIES}
)
set(SOURCES KinectV1PointCloudProvider.cpp)
set(HEADERS KinectV1PointCloudProvider.h KinectV1Device.hpp)

armarx_add_component("${SOURCES}" "${HEADERS}")
message(${LIBUSB_1_INCLUDE_DIRS} ${LIBUSB_1_LIBRARIES})
if(Freenect_FOUND)
    target_include_directories(KinectV1PointCloudProvider SYSTEM PUBLIC ${Freenect_INCLUDE_DIRS} ${LIBUSB_1_INCLUDE_DIRS})
endif()

set(EXE_SOURCES main.cpp KinectV1PointCloudProviderApp.h)
armarx_add_component_executable("${EXE_SOURCES}")
