#include "CoFusionParams.h"

using namespace armarx;

CoFusionParams::CoFusionParams()
{

}


#if USE_MASKFUSION == 1

std::unique_ptr<MaskFusion> CoFusionParams::makeCoFusion() const
{
    Resolution::setResolution(width, height);
    Intrinsics::setIntrinics(fx, fy, cx, cy);

    std::unique_ptr<MaskFusion> cofusion;

    cofusion.reset(new MaskFusion(
                       timeDelta,
                       countThresh,
                       errThresh,
                       covThresh,
                       closeLoops,
                       iclnuim,
                       reloc,
                       photoThresh,
                       initConfidenceGlobal,
                       initConfidenceObject,
                       depthCut,
                       icpThresh,
                       fastOdom,
                       fernThresh,
                       so3,
                       frameToFrameRGB,
                       modelSpawnOffset,
                       matchingType,
                       segmentationMethod,
                       exportDirectory,
                       exportSegmentationResults,
                       usePrecomputedMasksOnly,
                       frameQueueSize
                   ));

    return cofusion;
}


#else

std::unique_ptr<CoFusion> CoFusionParams::makeCoFusion() const
{
    Resolution::setResolution(width, height);
    Intrinsics::setIntrinics(fx, fy, cx, cy);

    std::unique_ptr<CoFusion> cofusion;

    cofusion.reset(new CoFusion(
                       timeDelta,
                       countThresh,
                       errThresh,
                       covThresh,
                       closeLoops,
                       iclnuim,
                       reloc,
                       photoThresh,
                       initConfidenceGlobal,
                       initConfidenceObject,
                       depthCut,
                       icpThresh,
                       fastOdom,
                       fernThresh,
                       so3,
                       frameToFrameRGB,
                       modelSpawnOffset,
                       matchingType,
                       exportDirectory,
                       exportSegmentationResults
                   ));

    return cofusion;
}

#endif

