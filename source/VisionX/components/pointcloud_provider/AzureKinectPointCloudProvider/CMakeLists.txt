armarx_component_set_name("AzureKinectPointCloudProvider")

find_package(k4a 1.2 QUIET)
message(STATUS "k4a found: ${k4a_FOUND} k4a version: ${k4a_VERSION}")
armarx_build_if(k4a_FOUND "Azure Kinect SDK not available")

set(COMPONENT_LIBS ArmarXCore ArmarXCoreObservers RobotAPIInterfaces  VisionXInterfaces VisionXTools VisionXCore
    VisionXPointCloud ImageToPointCloud 
    k4a::k4a
)

set(SOURCES AzureKinectPointCloudProvider.cpp)
set(HEADERS AzureKinectPointCloudProvider.h)
armarx_add_component("${SOURCES}" "${HEADERS}")
if(k4a_FOUND)
    target_include_directories(AzureKinectPointCloudProvider PUBLIC ${k4a_INCLUDE_DIR})
endif()

# add unit tests
add_subdirectory(test)
