/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::AzureKinectPointCloudProvider
 * @author     Mirko Wächter
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "AzureKinectPointCloudProvider.h"


#include <cstdio>
#include <filesystem>
#include <functional>

#include <Image/ImageProcessor.h>

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/time/ScopedStopWatch.h>
#include <ArmarXCore/core/time/StopWatch.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/util/CPPUtility/trace.h>
#include <Calibration/Calibration.h>
#include <VisionX/tools/ImageUtil.h>
#include <VisionX/tools/TypeMapping.h>



namespace
{
    /**
     * @brief Converts a k4a image to an IVT image.
     * @param colorImage
     * @param result
     */
    void
    k4a_to_ivt_image(const k4a::image& colorImage, ::CByteImage& result)
    {
        unsigned int cw = static_cast<unsigned int>(colorImage.get_width_pixels());
        unsigned int ch = static_cast<unsigned int>(colorImage.get_height_pixels());
        ARMARX_CHECK_EQUAL(static_cast<unsigned int>(result.width), cw);
        ARMARX_CHECK_EQUAL(static_cast<unsigned int>(result.height), ch);
        auto colorBuffer = reinterpret_cast<const unsigned char*>(colorImage.get_buffer());
        auto rgbBufferIVT = result.pixels;

        int indexIVT = 0;
        int indexK4A = 0;
        for (unsigned int y = 0; y < ch; ++y)
        {
            for (unsigned int x = 0; x < cw; ++x)
            {
                rgbBufferIVT[indexIVT] = colorBuffer[indexK4A + 2];
                rgbBufferIVT[indexIVT + 1] = colorBuffer[indexK4A + 1];
                rgbBufferIVT[indexIVT + 2] = colorBuffer[indexK4A + 0];
                indexIVT += 3;
                indexK4A += 4;
            }
        }
    }

    std::function<void(IceUtil::Time)>
    create_sw_callback(
        armarx::ManagedIceObject* obj,
        const std::string& description)
    {
        return [ = ](IceUtil::Time time)
        {
            std::string name = "duration " + description + " in [ms]";
            obj->setMetaInfo(name, new armarx::Variant{time.toMilliSecondsDouble()});
        };
    }

    /**
     * @brief Enum for time utils to use either system time or the time of the time provider.
     */
    enum TimestampType
    {
        use_system_time = true,
        use_provider_time = false
    };
}

namespace visionx
{
    void
    AzureKinectPointCloudProvider::onInitImageProvider()
    {
        config = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
        auto fps = getProperty<float>("framerate").getValue();
        if (fps == 5.0f)
        {
            config.camera_fps = K4A_FRAMES_PER_SECOND_5;
        }
        else if (fps == 15.0f)
        {
            config.camera_fps = K4A_FRAMES_PER_SECOND_15;
        }
        else if (fps == 30.0f)
        {
            config.camera_fps = K4A_FRAMES_PER_SECOND_30;
        }
        else
        {
            throw armarx::LocalException("Invalid framerate: ") << fps << " - Only framerates 5, 15 and 30 are "
                    << "supported by Azure Kinect.";
        }

        config.depth_mode = getProperty<k4a_depth_mode_t>("DepthMode");
        config.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
        config.color_resolution = getProperty<k4a_color_resolution_t>("ColorResolution");

        // This means that we'll only get captures that have both color and depth images, so we don't
        // need to check if the capture contains a particular type of image.
        config.synchronized_images_only = true;

        setNumberImages(2);
        auto depthDim = GetDepthDimensions(config.depth_mode);
        auto colorDim = GetColorDimensions(config.color_resolution);
        colorImageScalingFactor = std::max<double>((double)(depthDim.first) / colorDim.first, (double)(depthDim.second) / colorDim.second);
        visionx::ImageDimension scaledColorImageDim{(int)(colorDim.first * colorImageScalingFactor), (int)(colorDim.second * colorImageScalingFactor)};
        ARMARX_INFO << "Depth image size: " << depthDim.first << "x" << depthDim.second;
        ARMARX_INFO << "Color image size: " << colorDim.first << "x" << colorDim.second;
        alignedColorImage = k4a::image::create(
                                K4A_IMAGE_FORMAT_COLOR_BGRA32,
                                depthDim.first,
                                depthDim.second,
                                depthDim.first * 4 * static_cast<int32_t>(sizeof(uint8_t)));
        alignedDepthImage = k4a::image::create(
                                K4A_IMAGE_FORMAT_DEPTH16,
                                colorDim.first,
                                colorDim.second,
                                colorDim.first * 2 * static_cast<int32_t>(sizeof(uint8_t)));

        m_calibration_scale = static_cast<float>(colorDim.first) / depthDim.first;

        setImageFormat(
            visionx::ImageDimension(colorDim.first, colorDim.second),
            visionx::eRgb,
            visionx::eBayerPatternGr);
        resultDepthImage.reset(visionx::tools::createByteImage(getImageFormat(), visionx::eRgb));
        resultColorImage.reset(new CByteImage(colorDim.first, colorDim.second, CByteImage::eRGB24));

        alignedDepthImageScaled = k4a::image::create(
                                      K4A_IMAGE_FORMAT_DEPTH16,
                                      scaledColorImageDim.width,
                                      scaledColorImageDim.height,
                                      scaledColorImageDim.width * 2 * static_cast<int32_t>(sizeof(uint8_t)));

        xyzImage = k4a::image::create(
                       K4A_IMAGE_FORMAT_CUSTOM,
                       scaledColorImageDim.width,
                       scaledColorImageDim.height,
                       scaledColorImageDim.width * 3 * static_cast<int32_t>(sizeof(int16_t)));
        resizedIvtColorImage.reset(new CByteImage{scaledColorImageDim.width, scaledColorImageDim.height, CByteImage::eRGB24, false});

        pointcloud.reset(new pcl::PointCloud<CloudPointType>());
    }


    void
    AzureKinectPointCloudProvider::onConnectImageProvider()
    {
        ARMARX_DEBUG << "Connecting " << getName();

        pointcloudTask = armarx::RunningTask<AzureKinectPointCloudProvider>::pointer_type(
                             new armarx::RunningTask<AzureKinectPointCloudProvider>(
                                 this,
                                 &AzureKinectPointCloudProvider::runPointcloudPublishing));
        pointcloudTask->start();

        ARMARX_VERBOSE << "Connected " << getName();
    }


    void
    AzureKinectPointCloudProvider::onDisconnectImageProvider()
    {
        ARMARX_DEBUG << "Disconnecting " << getName();

        // Stop task
        {
            std::lock_guard<std::mutex> lock(pointcloudProcMutex);
            ARMARX_DEBUG << "Stopping pointcloud processing thread...";
            const bool wait_for_join = false;
            pointcloudTask->stop(wait_for_join);
            pointcloudProcSignal.notify_all();
            ARMARX_DEBUG << "Waiting for pointcloud processing thread to stop...";
            pointcloudTask->waitForStop();
            ARMARX_DEBUG << "Pointcloud processing thread stopped";
        }

        ARMARX_DEBUG << "Disconnected " << getName();
    }


    void
    AzureKinectPointCloudProvider::onInitCapturingPointCloudProvider()
    {
        this->setPointCloudSyncMode(eCaptureSynchronization);
        ARMARX_TRACE;
    }


    void
    AzureKinectPointCloudProvider::onExitCapturingPointCloudProvider()
    {
        ARMARX_INFO << "Closing Azure Kinect device";
        device.close();
        ARMARX_INFO << "Closed Azure Kinect device";
    }


    void
    AzureKinectPointCloudProvider::onStartCapture(float /* framesPerSecond */)
    {
        cloudFormat = getPointCloudFormat();

        //    setMetaInfo("Baseline", new Variant(std::to_string(calibration.calibrationRight.cameraParam.translation[0])
        //                                        + ", " + std::to_string(calibration.calibrationRight.cameraParam.translation[1]) +
        //                                        ", " + std::to_string(calibration.calibrationRight.cameraParam.translation[2])));

        // Check for devices.
        const uint32_t deviceCount = k4a::device::get_installed_count();
        if (deviceCount == 0)
        {
            getArmarXManager()->asyncShutdown();
            throw armarx::LocalException("No Azure Kinect devices detected!");
        }

        device = k4a::device::open(K4A_DEVICE_DEFAULT);
        auto depthDim = GetDepthDimensions(config.depth_mode);
        auto colorDim = GetColorDimensions(config.color_resolution);
        k4aCalibration = device.get_calibration(config.depth_mode, config.color_resolution);
        k4aCalibrationScaled = device.get_calibration(config.depth_mode, config.color_resolution);
        {
            auto colorImagescalingFactor =
                std::max<double>(static_cast<double>(depthDim.first) / colorDim.first,
                                 static_cast<double>(depthDim.second) / colorDim.second);
            auto& colorIntrinsics = k4aCalibrationScaled.color_camera_calibration.intrinsics;
            colorIntrinsics.parameters.param.cx *= colorImagescalingFactor;
            colorIntrinsics.parameters.param.cy *= colorImagescalingFactor;
            colorIntrinsics.parameters.param.fx *= colorImagescalingFactor;
            colorIntrinsics.parameters.param.fy *= colorImagescalingFactor;
            k4aCalibrationScaled.color_camera_calibration.resolution_width *= colorImagescalingFactor;
            k4aCalibrationScaled.color_camera_calibration.resolution_height *= colorImagescalingFactor;
            ARMARX_INFO << "Reducing color image by factor " << 1.0 / colorImagescalingFactor << " to "
                        << k4aCalibrationScaled.color_camera_calibration.resolution_width << "x"
                        << k4aCalibrationScaled.color_camera_calibration.resolution_height
                        << " for point cloud";
        }
        auto c = getStereoCalibration(Ice::Current());
        CCalibration* cLeft = tools::convert(c.calibrationLeft);
        cLeft->PrintCameraParameters();
        delete cLeft;
        CCalibration* cRight = tools::convert(c.calibrationRight);
        cRight->PrintCameraParameters();
        delete cRight;

        transformation = k4a::transformation(k4aCalibration);
        transformationScaled = k4a::transformation(k4aCalibrationScaled);

        device.start_cameras(&config);

        setMetaInfo("serialNumber", new armarx::Variant(device.get_serialnum()));
        setMetaInfo("rgbVersion", new armarx::Variant(VersionToString(device.get_version().rgb)));
        setMetaInfo("depthVersion", new armarx::Variant(VersionToString(device.get_version().depth)));
        setMetaInfo("depthSensorVersion", new armarx::Variant(VersionToString(device.get_version().depth_sensor)));
        setMetaInfo("audioVersion", new armarx::Variant(VersionToString(device.get_version().audio)));
    }


    void
    AzureKinectPointCloudProvider::onStopCapture()
    {
        // pass
    }


    bool
    AzureKinectPointCloudProvider::doCapture()
    {
        armarx::ScopedStopWatch sw_total{::create_sw_callback(this, "doCapture")};

        k4a::capture capture;
        const std::chrono::milliseconds timeout{1000};

        armarx::StopWatch sw_get_capture;
        if (device.get_capture(&capture, timeout))
        {
            ::create_sw_callback(this, "waiting for get_capture")(sw_get_capture.stop());

            setMetaInfo("temperature", new armarx::Variant(capture.get_temperature_c()));

            const k4a::image depthImage = capture.get_depth_image();
            const k4a::image colorImage = capture.get_color_image();

            auto realTime = IceUtil::Time::now();
            auto monotonicTime = IceUtil::Time::now(IceUtil::Time::Monotonic);
            auto clockDiff = realTime - monotonicTime;

            auto imageMonotonicTime = IceUtil::Time::microSeconds(
                                          std::chrono::duration_cast<std::chrono::microseconds>(
                                              depthImage.get_system_timestamp()).count());
            imagesTime = imageMonotonicTime + clockDiff;
            setMetaInfo("image age in [ms]",
                        new armarx::Variant{(realTime - imagesTime).toMilliSecondsDouble()});

            //  This function assumes that the image is made of depth pixels (i.e. uint16_t's),
            //  which is only true for IR/depth images.
            const k4a_image_format_t imageFormat = depthImage.get_format();
            if (imageFormat != K4A_IMAGE_FORMAT_DEPTH16 && imageFormat != K4A_IMAGE_FORMAT_IR16)
            {
                throw std::logic_error("Attempted to colorize a non-depth image!");
            }

            // Convert the k4a image to an IVT image.
            {
                armarx::ScopedStopWatch sw{::create_sw_callback(this, "convert k4a image to IVT")};
                ::k4a_to_ivt_image(colorImage, *resultColorImage);
            }

            // Provide data for pointcloud processing thread and signal to start processing.
            {
                // Acquire lock and write data needed by pointcloud thread (i.e.,
                // alignedDepthImageScaled and depthImageReady).
                {
                    std::lock_guard lock{pointcloudProcMutex};

                    // Resize the IVT image.
                    {
                        armarx::ScopedStopWatch sw{::create_sw_callback(this, "resize IVT image")};
                        ImageProcessor::Resize(resultColorImage.get(), resizedIvtColorImage.get());
                    }

                    // Transform depth image to scaled camera format (faked).
                    {
                        armarx::ScopedStopWatch sw{::create_sw_callback(this, "transform depth image to scaled "
                                                   "camera")};
                        transformationScaled.depth_image_to_color_camera(
                            depthImage,
                            &alignedDepthImageScaled);
                    }

                    // Signal that point cloud processing may proceed and reset processed flag.
                    depthImageReady = true;
                }

                ARMARX_DEBUG << "Notifying pointcloud thread.";
                pointcloudProcSignal.notify_one();
            }

            // Transform depth image to color camera format.
            {
                armarx::ScopedStopWatch sw{::create_sw_callback(this, "transform depth image to camera")};
                transformation.depth_image_to_color_camera(depthImage, &alignedDepthImage);
            }

            // Prepare result depth image.
            {
                armarx::ScopedStopWatch sw{::create_sw_callback(this, "prepare result depth image")};

                const int dw = alignedDepthImage.get_width_pixels();
                const int dh = alignedDepthImage.get_height_pixels();

                ARMARX_CHECK_EXPRESSION(resultDepthImage);
                ARMARX_CHECK_EQUAL(resultDepthImage->width, dw);
                ARMARX_CHECK_EQUAL(resultDepthImage->height, dh);

                auto resultDepthBuffer = resultDepthImage->pixels;
                const uint16_t* depthBuffer =
                    reinterpret_cast<const uint16_t*>(alignedDepthImage.get_buffer());

                int index = 0;
                int index2 = 0;
                for (int y = 0; y < dh; ++y)
                {
                    for (int x = 0; x < dw; ++x)
                    {
                        uint16_t depthValue = depthBuffer[index2];
                        unsigned char blueValue;
                        visionx::tools::depthValueToRGB(
                            depthValue,
                            resultDepthBuffer[index],
                            resultDepthBuffer[index + 1],
                            blueValue);
                        index += 3;
                        index2 += 1;
                    }
                }
            }

            // Broadcast RGB-D image.
            {
                armarx::ScopedStopWatch sw{::create_sw_callback(this, "broadcast RGB-D image")};
                CByteImage* images[2] = {resultColorImage.get(), resultDepthImage.get()};
                provideImages(images, imagesTime);
            }

            // Wait until `alignedDepthImageScaled` was processed and can be overridden again.
            {
                std::unique_lock signal_lock{pointcloudProcMutex};
                ARMARX_DEBUG << "Capturing thread waiting for signal...";
                pointcloudProcSignal.wait(signal_lock, [&] { return depthImageProcessed; });
                ARMARX_DEBUG << "Capturing thread received signal.";
            }

            return true;
        }
        else
        {
            ARMARX_INFO << deactivateSpam(30) << "Did not get frame until timeout of " << timeout;

            return false;
        }

    }


    void
    AzureKinectPointCloudProvider::runPointcloudPublishing()
    {
        ARMARX_DEBUG << "Started pointcloud processing task.";

        IceUtil::Time time;

        // Main pointcloud processing loop.
        while (not pointcloudTask->isStopped())
        {
            // Wait for data.
            std::unique_lock signal_lock{pointcloudProcMutex};
            ARMARX_DEBUG << "Pointcloud thread waiting for signal...";
            pointcloudProcSignal.wait(signal_lock,
                                      [&] { return pointcloudTask->isStopped() or depthImageReady; });
            ARMARX_DEBUG << "Pointcloud thread received signal.";

            // Assess timings, reset flags.
            const IceUtil::Time timestamp = imagesTime;
            depthImageReady = false;
            depthImageProcessed = false;

            // Transform depth image to pointcloud.
            {
                time = armarx::TimeUtil::GetTime(use_system_time);
                transformationScaled.depth_image_to_point_cloud(alignedDepthImageScaled,
                        K4A_CALIBRATION_TYPE_COLOR,
                        &xyzImage);
                ARMARX_DEBUG << "Transforming depth image to point cloud took "
                             << armarx::TimeUtil::GetTimeSince(time, use_system_time);
            }

            // Construct PCL pointcloud.
            {
                time = armarx::TimeUtil::GetTime(use_system_time);

                ARMARX_CHECK_EXPRESSION(pointcloud);
                pointcloud->width = static_cast<uint32_t>(alignedDepthImageScaled.get_width_pixels());
                pointcloud->height = static_cast<uint32_t>(alignedDepthImageScaled.get_height_pixels());
                pointcloud->is_dense = false;
                pointcloud->points.resize(pointcloud->width * pointcloud->height);
                auto k4aCloudBuffer = reinterpret_cast<const int16_t*>(xyzImage.get_buffer());
                auto colorBuffer = resizedIvtColorImage->pixels;
                size_t index = 0;
                float maxDepth = getProperty<float>("MaxDepth").getValue();
                for (auto& p : pointcloud->points)
                {
                    p.r = colorBuffer[index];
                    p.x = k4aCloudBuffer[index];
                    p.g = colorBuffer[++index];
                    p.y = k4aCloudBuffer[index];
                    p.b = colorBuffer[++index];
                    auto z = k4aCloudBuffer[index++];
                    p.z = z <= maxDepth && z != 0 ? z : std::numeric_limits<float>::quiet_NaN();
                }

                ARMARX_DEBUG << "Constructing point cloud took "
                             << armarx::TimeUtil::GetTimeSince(time, use_system_time);
            }

            // Notify capturing thread that data was processed and may be overridden.
            depthImageProcessed = true;
            signal_lock.unlock();
            ARMARX_DEBUG << "Notifying capturing thread...";
            pointcloudProcSignal.notify_one();

            // Broadcast PCL pointcloud.
            {
                time = armarx::TimeUtil::GetTime(use_system_time);
                pointcloud->header.stamp = static_cast<unsigned long>(timestamp.toMicroSeconds());
                providePointCloud(pointcloud);
                ARMARX_DEBUG << "Broadcasting pointcloud took "
                             << armarx::TimeUtil::GetTimeSince(time, use_system_time);
            }
        }

        ARMARX_DEBUG << "Stopped pointcloud processing task.";
    }


    std::string
    AzureKinectPointCloudProvider::getDefaultName() const
    {
        return "AzureKinectPointCloudProvider";
    }


    armarx::PropertyDefinitionsPtr
    AzureKinectPointCloudProvider::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new AzureKinectPointCloudProviderPropertyDefinitions(
                getConfigIdentifier()));
    }


    void
    AzureKinectPointCloudProvider::onInitComponent()
    {
        ImageProvider::onInitComponent();
        CapturingPointCloudProvider::onInitComponent();
    }


    void
    AzureKinectPointCloudProvider::onConnectComponent()
    {
        ImageProvider::onConnectComponent();
        CapturingPointCloudProvider::onConnectComponent();
    }


    void
    AzureKinectPointCloudProvider::onDisconnectComponent()
    {
        captureEnabled = false;

        ImageProvider::onDisconnectComponent();
        CapturingPointCloudProvider::onDisconnectComponent();
    }


    void
    AzureKinectPointCloudProvider::onExitComponent()
    {
        ImageProvider::onExitComponent();
        CapturingPointCloudProvider::onExitComponent();
    }


    StereoCalibration
    AzureKinectPointCloudProvider::getStereoCalibration(const Ice::Current&)
    {
        using Eigen::Map;
        using Eigen::Matrix4f;
        using Matrix3fRowMajor = Eigen::Matrix<float, 3, 3, Eigen::StorageOptions::RowMajor>;
        using visionx::tools::convertEigenMatToVisionX;

        const auto convert_calibration = [](const k4a_calibration_camera_t& k4a_calib,
                                            float scale = 1.)
        {
            MonocularCalibration calibration;

            const k4a_calibration_intrinsic_parameters_t& params = k4a_calib.intrinsics.parameters;
            calibration.cameraParam.principalPoint = {params.param.cx * scale,
                                                      params.param.cy* scale
                                                     };
            calibration.cameraParam.focalLength = {params.param.fx * scale,
                                                   params.param.fy* scale
                                                  };
            calibration.cameraParam.distortion = { params.param.k1, params.param.k2, params.param.k3,
                                                   params.param.k4, params.param.k5, params.param.k6
                                                 };
            calibration.cameraParam.width = k4a_calib.resolution_width;
            calibration.cameraParam.height = k4a_calib.resolution_height;
            const Matrix3fRowMajor rotation =
                Map<const Matrix3fRowMajor> {k4a_calib.extrinsics.rotation};
            calibration.cameraParam.rotation = convertEigenMatToVisionX(rotation);
            calibration.cameraParam.translation = { k4a_calib.extrinsics.translation,
                                                    k4a_calib.extrinsics.translation + 3
                                                  };

            return calibration;
        };

        StereoCalibration calibration;

        calibration.calibrationLeft = convert_calibration(k4aCalibration.color_camera_calibration);
        calibration.calibrationRight = convert_calibration(k4aCalibration.depth_camera_calibration,
                                       m_calibration_scale);
        calibration.rectificationHomographyLeft = convertEigenMatToVisionX(Matrix4f::Identity());
        calibration.rectificationHomographyRight = convertEigenMatToVisionX(Matrix4f::Identity());

        return calibration;
    }


    bool
    AzureKinectPointCloudProvider::getImagesAreUndistorted(const Ice::Current&)
    {
        return false;
    }


    std::string
    AzureKinectPointCloudProvider::getReferenceFrame(const Ice::Current&)
    {
        return getProperty<std::string>("frameName");
    }
}
