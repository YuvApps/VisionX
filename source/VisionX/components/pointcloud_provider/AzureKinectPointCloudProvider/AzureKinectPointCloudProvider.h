/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::AzureKinectPointCloudProvider
 * @author     Mirko Wächter
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <chrono>
#include <condition_variable>
#include <mutex>

#include <Eigen/Core>

#include <opencv2/opencv.hpp>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <VisionX/components/pointcloud_core/CapturingPointCloudProvider.h>
#include <VisionX/components/pointcloud_provider/ImageToPointCloud/DepthImageUtils.h>
#include <VisionX/interface/components/RGBDImageProvider.h>
#include <VisionX/core/CapturingImageProvider.h>

#include <k4a/k4a.h>
#include <k4a/k4a.hpp>


namespace visionx
{
    /**
     * @class AzureKinectPointCloudProviderPropertyDefinitions
     * @brief
     */
    class AzureKinectPointCloudProviderPropertyDefinitions:
        public visionx::CapturingPointCloudProviderPropertyDefinitions
    {
    public:
        AzureKinectPointCloudProviderPropertyDefinitions(std::string prefix):
            visionx::CapturingPointCloudProviderPropertyDefinitions(prefix)
        {
            defineOptionalProperty<k4a_color_resolution_t>("ColorResolution", K4A_COLOR_RESOLUTION_1080P, "Resolution of the RGB camera image.")
            .map("0", K4A_COLOR_RESOLUTION_OFF)       /** Color camera will be turned off */
            .map("720", K4A_COLOR_RESOLUTION_720P)    /** 1280x720  16:9 */
            .map("1080", K4A_COLOR_RESOLUTION_1080P)  /** 1920x1080 16:9 */
            .map("1440", K4A_COLOR_RESOLUTION_1440P)  /** 2560x1440 16:9 */
            .map("1536", K4A_COLOR_RESOLUTION_1536P)  /** 2048x1536 4:3  */
            .map("2160", K4A_COLOR_RESOLUTION_2160P)  /** 3840x2160 16:9 */
            .map("3072", K4A_COLOR_RESOLUTION_3072P); /** 4096x3072 4:3  */
            defineOptionalProperty<k4a_depth_mode_t>("DepthMode", K4A_DEPTH_MODE_NFOV_2X2BINNED, "Resolution/mode of the depth camera image.")
            .setCaseInsensitive(true)
            .map("OFF", K4A_DEPTH_MODE_OFF)
            .map("NFOV_2X2BINNED", K4A_DEPTH_MODE_NFOV_2X2BINNED)
            .map("NFOV_UNBINNED", K4A_DEPTH_MODE_NFOV_UNBINNED)
            .map("WFOV_2X2BINNED", K4A_DEPTH_MODE_WFOV_2X2BINNED)
            .map("WFOV_UNBINNED", K4A_DEPTH_MODE_WFOV_UNBINNED)
            .map("PASSIVE_IR", K4A_DEPTH_MODE_PASSIVE_IR);

            defineOptionalProperty<float>("MaxDepth", 6000.0, "Max. allowed depth value in mm. Depth values above this threshold will be set to nan.");
        }
    };

    /**
     * @defgroup Component-AzureKinectPointCloudProvider AzureKinectPointCloudProvider
     * @ingroup VisionX-Components
     * Provides support for Intel RealSense cameras for ArmarX.
     *
     * @class AzureKinectPointCloudProvider
     * @ingroup Component-AzureKinectPointCloudProvider
     * @brief Brief description of class AzureKinectPointCloudProvider.
     */
    class AzureKinectPointCloudProvider :
        virtual public visionx::RGBDPointCloudProviderInterface,
        virtual public visionx::CapturingPointCloudProvider,
        virtual public visionx::ImageProvider
    {
    public:
        using CloudPointType = pcl::PointXYZRGBA;
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        virtual std::string getDefaultName() const override;

    protected:

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // ManagedIceObject interface
    protected:
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;

        void onExitComponent() override;

        // StereoCalibrationInterface interface
    public:
        visionx::StereoCalibration getStereoCalibration(const Ice::Current& c = Ice::emptyCurrent) override;
        bool getImagesAreUndistorted(const ::Ice::Current& c = Ice::emptyCurrent) override;

        std::string getReferenceFrame(const Ice::Current& c = Ice::emptyCurrent) override;


    protected:
        void onStartCapture(float framesPerSecond) override;
        void onStopCapture() override;
        void onInitImageProvider() override;
        void onConnectImageProvider() override;
        void onDisconnectImageProvider() override;
        void onExitImageProvider() override {}

        void onInitCapturingPointCloudProvider() override;
        void onExitCapturingPointCloudProvider() override;
        bool doCapture() override;
        bool hasSharedMemorySupport(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }
        MetaPointCloudFormatPtr getDefaultPointCloudFormat()  override
        {
            MetaPointCloudFormatPtr info = new MetaPointCloudFormat();
            //info->frameId = getProperty<std::string>("frameId").getValue();
            info->type = PointContentType::eColoredPoints;
            ARMARX_CHECK_EXPRESSION(resizedIvtColorImage);
            ARMARX_INFO << "default pointcloud format: " << resizedIvtColorImage->width << ", " <<  resizedIvtColorImage->height;
            info->capacity = resizedIvtColorImage->width * resizedIvtColorImage->height * sizeof(ColoredPoint3D);// + info->frameId.size();
            info->size = info->capacity;
            return info;
        }

        void runPointcloudPublishing();

        // Gets the dimensions of the color images that the color camera will produce for a
        // given color resolution.
        static inline std::pair<int, int> GetColorDimensions(const k4a_color_resolution_t resolution)
        {
            switch (resolution)
            {
                case K4A_COLOR_RESOLUTION_720P:
                    return { 1280, 720 };
                case K4A_COLOR_RESOLUTION_2160P:
                    return { 3840, 2160 };
                case K4A_COLOR_RESOLUTION_1440P:
                    return { 2560, 1440 };
                case K4A_COLOR_RESOLUTION_1080P:
                    return { 1920, 1080 };
                case K4A_COLOR_RESOLUTION_3072P:
                    return { 4096, 3072 };
                case K4A_COLOR_RESOLUTION_1536P:
                    return { 2048, 1536 };

                default:
                    throw std::logic_error("Invalid color dimensions value!");
            }
        }

        // Gets the dimensions of the depth images that the depth camera will produce for a
        // given depth mode.
        static inline std::pair<int, int> GetDepthDimensions(const k4a_depth_mode_t depthMode)
        {
            switch (depthMode)
            {
                case K4A_DEPTH_MODE_NFOV_2X2BINNED:
                    return { 320, 288 };
                case K4A_DEPTH_MODE_NFOV_UNBINNED:
                    return { 640, 576 };
                case K4A_DEPTH_MODE_WFOV_2X2BINNED:
                    return { 512, 512 };
                case K4A_DEPTH_MODE_WFOV_UNBINNED:
                    return { 1024, 1024 };
                case K4A_DEPTH_MODE_PASSIVE_IR:
                    return { 1024, 1024 };

                default:
                    throw std::logic_error("Invalid depth dimensions value!");
            }
        }

        static std::string VersionToString(const k4a_version_t& version)
        {
            std::stringstream s;
            s << version.major << "." << version.minor << "." << version.iteration;
            return s.str();
        }

    private:

        IceUtil::Time imagesTime;

        std::mutex pointcloudProcMutex;
        std::condition_variable pointcloudProcSignal;
        bool depthImageReady;
        bool depthImageProcessed;
        armarx::RunningTask<visionx::AzureKinectPointCloudProvider>::pointer_type pointcloudTask;

        visionx::CByteImageUPtr resultDepthImage, resultColorImage, resizedIvtColorImage;
        pcl::PointCloud<CloudPointType>::Ptr pointcloud;
        MetaPointCloudFormatPtr cloudFormat;
        visionx::StereoCalibration calibration;

        float m_calibration_scale;

        k4a::device device;
        k4a_device_configuration_t config;
        k4a::calibration k4aCalibration;
        k4a::calibration k4aCalibrationScaled;
        k4a::transformation transformation;
        k4a::transformation transformationScaled;
        double colorImageScalingFactor = 1.0;
        k4a::image alignedColorImage, alignedDepthImage, alignedDepthImageScaled, xyzImage;

    };
}
