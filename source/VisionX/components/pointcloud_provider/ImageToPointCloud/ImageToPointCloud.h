/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ActiveVision::ArmarXObjects::ImageToPointCloud
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @author     Mirko Waechter ( waechter at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>


#include <VisionX/core/ImageProcessor.h>
#include <VisionX/components/pointcloud_core/CapturingPointCloudProvider.h>

#include <VisionX/tools/TypeMapping.h>
#include <VisionX/tools/ImageUtil.h>

#include <VisionX/interface/components/ImageToPointCloud.h>


#include <mutex>

#include <pcl/point_types.h>

#include "DepthImageUtils.h"

namespace armarx
{


    typedef pcl::PointXYZRGBL PointL;


    /**
     * @class ImageToPointCloudPropertyDefinitions
     * @brief
     */
    class ImageToPointCloudPropertyDefinitions:
        public visionx::ImageProcessorPropertyDefinitions
    {
    public:
        ImageToPointCloudPropertyDefinitions(std::string prefix):
            visionx::ImageProcessorPropertyDefinitions(prefix)
        {
            //            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
            //            defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");
            defineOptionalProperty<int>("PointCloudWidth", 640, "Must match the image width (cannot be read automaically from imageprovider as early as needed)");
            defineOptionalProperty<int>("PointCloudHeight", 480, "Must match the image height (cannot be read automaically from imageprovider as early as needed)");

            defineOptionalProperty<float>("VerticalViewAngle", 43.0f, "The vertical viewing angle of the camera. Only used when no calibration can be retrieved.");
            defineOptionalProperty<float>("HorizontalViewAngle", 57.0f, "The horizontal viewing angle of the camera. Only used when no calibration can be retrieved.");

            defineOptionalProperty<float>("PointCloudRotationZ", 180.0f, "Rotation around Z of the provided pointcloud", PropertyDefinitionBase::eModifiable);

            defineRequiredProperty<std::string>("ProviderName", "ImageProvider name. image[0]: rgb image, image[1]: depth image, image[2](optional): label integer");
            defineOptionalProperty<std::string>("CalibrationProviderName", "", "CalibrationProvider name. Optional. Useful if the images are processed and forwarded.");
        }
    };

    /**
     * @defgroup Component-ImageToPointCloud ImageToPointCloud
     * @ingroup VisionX-Components
     * A description of the component ImageToPointCloud.
     *
     * @class ImageToPointCloud
     * @ingroup Component-ImageToPointCloud
     * @brief Brief description of class ImageToPointCloud.
     *
     * Detailed description of class ImageToPointCloud.
     */
    class ImageToPointCloud :
        virtual public visionx::PointCloudProvider,
        virtual public visionx::ImageProcessor,
        virtual public armarx::ImageToPointCloudInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "ImageToPointCloud";
        }

        void onInitPointCloudProvider()
        {

        }

        void onExitPointCloudProvider()
        {

        }

    protected:

        void onInitImageProcessor() override;
        void onConnectImageProcessor() override;
        void onExitImageProcessor() override;
        void process() override;

        virtual visionx::MetaPointCloudFormatPtr getDefaultPointCloudFormat();

        void onInitComponent() override
        {
            visionx::PointCloudProvider::onInitComponent();
            visionx::ImageProcessor::onInitComponent();
        }

        void onConnectComponent() override
        {
            visionx::PointCloudProvider::onConnectComponent();
            visionx::ImageProcessor::onConnectComponent();
        }

        void onExitComponent() override
        {
            visionx::PointCloudProvider::onExitComponent();
            visionx::ImageProcessor::onExitComponent();
        }

        void onDisconnectComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions();

    private:
        //        armarx::DebugDrawerInterfacePrx debugDrawer;
        //        armarx::DebugObserverInterfacePrx debugObserver;
        visionx::StereoCalibrationInterfacePrx calibrationPrx;

        armarx::MetaInfoSizeBasePtr imageMetaInfo;

        float fovH;
        float fovV;

        armarx::Mutex mutex;

        std::string providerName;
        CByteImage** images;
        size_t numImages;
        visionx::ImageProviderInfo imageProviderInfo;
        DepthImageUtils depthImageUtils;
        pcl::PointCloud<PointL>::Ptr cloud, transformedCloud;
    };
}
