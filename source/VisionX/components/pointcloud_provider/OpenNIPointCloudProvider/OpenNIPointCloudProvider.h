/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::OpenNIPointCloudProvider
 * @author     David Schiebener (schiebener at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <VisionX/core/ImageProcessor.h>
#include <VisionX/components/pointcloud_core/CapturingPointCloudProvider.h>

#include <VisionX/interface/components/RGBDImageProvider.h>
//#include <VisionX/interface/components/Calibration.h>

#include <pcl/common/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/openni2_grabber.h>

#include <boost/thread/thread.hpp>

#include <ArmarXCore/core/time/TimeUtil.h>
#include <Eigen/Geometry>
#include <RobotAPI/interface/components/RobotHealthInterface.h>
#include <opencv2/opencv.hpp>

#include <Image/IplImageAdaptor.h>

namespace visionx
{

    /**
         * @class OpenNIPointCloudProviderPropertyDefinitions
         * @brief
         */
    class OpenNIPointCloudProviderPropertyDefinitions:
        public CapturingPointCloudProviderPropertyDefinitions
    {
    public:
        OpenNIPointCloudProviderPropertyDefinitions(std::string prefix):
            CapturingPointCloudProviderPropertyDefinitions(prefix)
        {
            defineOptionalProperty<Eigen::Vector2i>("dimensions", Eigen::Vector2i(640, 480), "")
            .map("320x240", Eigen::Vector2i(320, 240))
            .map("640x480", Eigen::Vector2i(640, 480))
            .map("800x600", Eigen::Vector2i(800, 600))
            .map("1280x1024", Eigen::Vector2i(1280, 1024))
            .map("1600x1200", Eigen::Vector2i(1600, 1200));

            defineOptionalProperty<std::string>("depthCameraCalibrationFile", "VisionX/camera_calibration/asus_xtion_depth.yaml", "");
            defineOptionalProperty<std::string>("RGBCameraCalibrationFile", "VisionX/camera_calibration/asus_xtion_rgb.yaml", "");

            defineOptionalProperty<bool>("EnableAutoExposure", true, "enable auto exposure.");
            defineOptionalProperty<bool>("EnableAutoWhiteBalance", true, "enable auto white balance.");

            defineOptionalProperty<std::string>("ReferenceFrameName", "DepthCamera", "Optional reference frame name.");
            defineOptionalProperty<std::string>("OpenNIDeviceId", "", "Device id of the openni device, e.g. empty for first device, or index in the format #n, e.g. 0");
            defineOptionalProperty<float>("CaptureTimeOffset", 16.0f, "In Milliseconds. Time offset between capturing the image on the hardware and receiving the image in this process.", armarx::PropertyDefinitionBase::eModifiable);
            defineOptionalProperty<std::string>("RobotHealthTopicName", "RobotHealthTopic", "Name of the RobotHealth topic");
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");

        }
    };

    /**
         * @class OpenNIPointCloudProvider
         * @ingroup VisionX-Components
         * @brief A brief description
         *
         * Detailed Description
         */
    class OpenNIPointCloudProvider :
        virtual public RGBDPointCloudProviderInterface,
        virtual public CapturingPointCloudProvider,
        virtual public ImageProvider
    {
    public:
        /**
             * @see armarx::ManagedIceObject::getDefaultName()
             */
        std::string getDefaultName() const override
        {
            return "OpenNIPointCloudProvider";
        }

    protected:
        /**
             * @see visionx::PointCloudProviderBase::onInitPointCloudProvider()
             */
        void onInitCapturingPointCloudProvider() override;

        /**
             * @see visionx::PointCloudProviderBase::onExitPointCloudProvider()
             */
        void onExitCapturingPointCloudProvider() override;

        /**
             * @see visionx::PointCloudProviderBase::onStartCapture()
             */
        void onStartCapture(float frameRate) override;

        /**
             * @see visionx::PointCloudProviderBase::onStopCapture()
             */
        void onStopCapture() override;

        /**
             * @see visionx::PointCloudProviderBase::doCapture()
            */
        bool doCapture() override;

        /**
             * @see visionx::CapturingImageProvider::onInitImageProvider()
             */
        void onInitImageProvider() override;

        /**
             * @see visionx::CapturingImageProvider::onExitImageProvider()
             */
        void onExitImageProvider() override { }

        bool hasSharedMemorySupport(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }

        /**
             * @see PropertyUser::createPropertyDefinitions()
             */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // mixed inherited stuff
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;


        StereoCalibration getStereoCalibration(const Ice::Current& c  = Ice::emptyCurrent) override;
        bool getImagesAreUndistorted(const ::Ice::Current& c = Ice::emptyCurrent) override
        {
            return false;
        }


        std::string getReferenceFrame(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return getProperty<std::string>("ReferenceFrameName");
        }

    private:
        void callbackFunctionForOpenNIGrabberPointCloudWithRGB(const pcl::PointCloud<pcl::PointXYZRGBA>::ConstPtr& newCloud);
        void callbackFunctionForOpenNIGrabberPointCloud(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr& newCloud);
        void callbackFunctionForOpenNIGrabberImage(const boost::shared_ptr<pcl::io::Image>& newImage);
        void grabImages(const boost::shared_ptr<pcl::io::Image>& RGBImage, const boost::shared_ptr<pcl::io::DepthImage>& depthImage, float reciprocalFocalLength);
        void grabDepthImage(const boost::shared_ptr<pcl::io::IRImage>& RGBImage, const boost::shared_ptr<pcl::io::DepthImage>& depthImage, float x);



        bool loadCalibrationFile(std::string fileName, visionx::CameraParameters& calibration);

        ImageProviderInterfacePrx imageProviderPrx;
        std::string providerName;
        CByteImage** rgbImages;
        int width, height;
        mutable boost::mutex syncMutex;

        pcl::io::OpenNI2Grabber::Ptr grabberInterface;
        boost::condition_variable condition;
        bool newPointCloudCapturingRequested, newImageCapturingRequested;
        long timeOfLastImageCapture, timeOfLastPointCloudCapture;
        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointcloudBuffer;
        StereoCalibration calibration;
        long captureTimeOffset;

        armarx::RobotHealthInterfacePrx robotHealthTopic;
        armarx::DebugObserverInterfacePrx debugObserver;
        IceUtil::Time lastReportTimestamp;
    };
}
