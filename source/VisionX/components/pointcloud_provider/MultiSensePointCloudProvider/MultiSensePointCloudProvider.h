/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::MultiSensePointCloudProvider
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

#include <VisionX/core/ImageProcessor.h>
#include <VisionX/components/pointcloud_core/CapturingPointCloudProvider.h>

#include <VisionX/interface/components/PointCloudAndImageAndCalibrationProviderInterface.h>

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/transforms.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/crop_box.h>

#include <opencv2/opencv.hpp>

#include <MultiSense/MultiSenseChannel.hh>
#include <MultiSense/MultiSenseTypes.hh>


#include <Image/IplImageAdaptor.h>

#include <Eigen/Core>

namespace armarx
{

    static Eigen::Vector4f stringToVector4f(std::string propertyValue)
    {
        Eigen::Vector4f vec;
        sscanf(propertyValue.c_str(), "%f, %f, %f, %f", &vec.data()[0], &vec.data()[1], &vec.data()[2], &vec.data()[3]);
        return vec;
    }


    /**
     * @class MultiSensePointCloudProviderPropertyDefinitions
     * @brief
     */
    class MultiSensePointCloudProviderPropertyDefinitions:
        public visionx::CapturingPointCloudProviderPropertyDefinitions
    {
    public:
        MultiSensePointCloudProviderPropertyDefinitions(std::string prefix):
            visionx::CapturingPointCloudProviderPropertyDefinitions(prefix)
        {
            PropertyDefinition<Eigen::Vector4f>::PropertyFactoryFunction f = &stringToVector4f;

            defineOptionalProperty<Eigen::Vector4f>("minPoint", Eigen::Vector4f(-5000, -1e+06, -0, 1), "").setFactory(f);
            defineOptionalProperty<Eigen::Vector4f>("maxPoint", Eigen::Vector4f(1000, 1e+08, 1e+08, 1), "").setFactory(f);
            defineOptionalProperty<std::string>("ipAddress", "10.66.171.21", "Description");
            defineOptionalProperty<bool>("enableLight", false, "Switch on the MultiSense LEDs on startup");
            defineOptionalProperty<bool>("useLidar", false, "Use the laser for depth information");
            defineOptionalProperty<std::string>("CalibrationFileName", "VisionX/examples/camera_multisense.txt", "Camera calibration file");
        }
    };

    /**
     * @defgroup Component-MultiSensePointCloudProvider MultiSensePointCloudProvider
     * @ingroup VisionX-Components
     * A description of the component MultiSensePointCloudProvider.
     *
     * @class MultiSensePointCloudProvider
     * @ingroup Component-MultiSensePointCloudProvider
     * @brief Brief description of class MultiSensePointCloudProvider.
     *
     * Detailed description of class MultiSensePointCloudProvider.
     */
    class MultiSensePointCloudProvider :
        virtual public visionx::CapturingPointCloudAndImageAndStereoCalibrationProviderInterface,
        virtual public visionx::CapturingPointCloudProvider,
        virtual public visionx::ImageProvider
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "MultiSensePointCloudProvider";
        }


        void disparityImageCallback(const crl::multisense::image::Header& header);

        void lumaImageCallback(const crl::multisense::image::Header& header);

        void chromaImageCallback(const crl::multisense::image::Header& header);

        void lidarScanCallback(const crl::multisense::lidar::Header& header);

    protected:
        /**
             * @see visionx::PointCloudProviderBase::onInitPointCloudProvider()
             */
        void onInitCapturingPointCloudProvider() override;

        /**
             * @see visionx::PointCloudProviderBase::onExitPointCloudProvider()
             */
        void onExitCapturingPointCloudProvider() override;

        /**
             * @see visionx::PointCloudProviderBase::onStartCapture()
             */
        void onStartCapture(float frameRate) override;

        /**
             * @see visionx::PointCloudProviderBase::onStopCapture()
             */
        void onStopCapture() override;

        /**
             * @see visionx::PointCloudProviderBase::doCapture()
            */
        bool doCapture() override;

        visionx::MetaPointCloudFormatPtr getDefaultPointCloudFormat() override;


        /**
             * @see visionx::CapturingImageProvider::onInitImageProvider()
             */
        void onInitImageProvider() override;

        /**
             * @see visionx::CapturingImageProvider::onExitImageProvider()
             */
        void onExitImageProvider() override { }


        /**
             * @see PropertyUser::createPropertyDefinitions()
             */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override
        {
            visionx::ImageProvider::onInitComponent();
            visionx::CapturingPointCloudProvider::onInitComponent();
        }

        void onConnectComponent() override
        {
            visionx::ImageProvider::onConnectComponent();
            visionx::CapturingPointCloudProvider::onConnectComponent();
        }

        void onDisconnectComponent() override
        {
            visionx::ImageProvider::onDisconnectComponent();
            visionx::CapturingPointCloudProvider::onDisconnectComponent();
        }

        void onExitComponent() override
        {
            visionx::ImageProvider::onExitComponent();
            visionx::CapturingPointCloudProvider::onExitComponent();
        }

        visionx::MonocularCalibration getMonocularCalibration(const ::Ice::Current& c = Ice::emptyCurrent) override;

        visionx::StereoCalibration getStereoCalibration(const Ice::Current& c  = Ice::emptyCurrent) override;

        bool getImagesAreUndistorted(const ::Ice::Current& c = Ice::emptyCurrent) override
        {
            return false;
        }

        bool hasSharedMemorySupport(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }




    private:

        pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointCloudData;

        crl::multisense::Channel* driver;

        boost::mutex dataMutex;

        crl::multisense::DataSource mask;

        bool hasNewDisparityData;
        bool hasNewColorData;

        std::vector<uint8_t> lumaData;
        bool hasNewLumaData;

        cv::Mat_<double> q_matrix;

        cv::Mat rgbImage;
        cv::Mat disparityImage;

        Eigen::Matrix4f cameraToSpindle;
        Eigen::Matrix4f laserToSpindle;

        pcl::CropBox<pcl::PointXYZRGBA> cropBoxFilter;


        visionx::StereoCalibration calibration;
        CByteImage** rgbImages;

    };
}

