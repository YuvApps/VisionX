/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PrimitiveVisualization
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PrimitiveVisualization.h"


#include <MemoryX/libraries/memorytypes/entity/EnvironmentalPrimitive.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>

#include <VirtualRobot/MathTools.h>

#include <pcl/point_types.h>
#include <pcl/common/colors.h>


using namespace armarx;


void PrimitiveVisualization::onInitComponent()
{
    usingTopic("SegmentedPointCloud");

    offeringTopic("DebugDrawerUpdates");

    usingProxy(getProperty<std::string>("WorkingMemoryName").getValue());

    primitiveColorFrame = {0.2, 0.4, 0.8, 0.7};
}


void PrimitiveVisualization::onConnectComponent()
{
    debugDrawer = getTopic<DebugDrawerInterfacePrx>("DebugDrawerUpdates");

    workingMemory = getProxy<memoryx::WorkingMemoryInterfacePrx>(getProperty<std::string>("WorkingMemoryName").getValue());
    environmentalPrimitiveSegment = workingMemory->getEnvironmentalPrimitiveSegment();

    clearDebugLayer();

}


void PrimitiveVisualization::onDisconnectComponent()
{

}


void PrimitiveVisualization::onExitComponent()
{

}


void PrimitiveVisualization::clearDebugLayer()
{
    debugDrawer->clearLayer(getLayerName());
    debugDrawer->clearLayer(getLayerName("_id"));
    debugDrawer->clearLayer(getLayerName("_frame"));
    debugDrawer->clearLayer(getLayerName("_grasp"));
    debugDrawer->clearLayer(getLayerName("_box"));
    debugDrawer->clearLayer(getLayerName("_plane_normals"));
}


void PrimitiveVisualization::reportNewPointCloudSegmentation(const Ice::Current& c)
{
    std::unique_lock<std::mutex> lock(mutex, std::try_to_lock);

    if (!lock.owns_lock())
    {
        ARMARX_INFO << deactivateSpam(3) << "unable to process new geometric primitives. already processing previous primitive set.";
        return;
    }

    clearDebugLayer();

    memoryx::SegmentLockBasePtr segmentLock = environmentalPrimitiveSegment->lockSegment();
    std::vector<memoryx::EnvironmentalPrimitiveBasePtr> primitives = environmentalPrimitiveSegment->getEnvironmentalPrimitives(MEMORYX_TOKEN(segmentLock));
    environmentalPrimitiveSegment->unlockSegment(segmentLock);

    for (memoryx::EnvironmentalPrimitiveBasePtr& primitive : primitives)
    {
        DrawColor color = getPrimitiveColor(primitive);
        visualizePrimitive(primitive, color);
    }
}


DrawColor PrimitiveVisualization::getPrimitiveColor(const memoryx::EnvironmentalPrimitiveBasePtr& primitive)
{
    DrawColor color;

    if (primitive->getLabel())
    {
        pcl::RGB c = pcl::GlasbeyLUT::at(primitive->getLabel() % pcl::GlasbeyLUT::size());
        color = {c.r / 255.0f, c.g / 255.0f, c.b / 255.0f, 0.9}; //primitive->getProbability()
    }
    else
    {
        color = {0.5, 0.5, 0.5, 0.5};
        //pcl::RGB c = pcl::GlasbeyLUT::at(std::stoi(primitive->getId()) % pcl::GlasbeyLUT::size());
        //color = {c.r / 255.0f, c.g / 255.0f, c.b / 255.0f, 0.5f};
    }


    // float alpha = std::max(0.1, std::min(0.6, elapsedTime / 60.0));
    //
    // alpha = 0.6;
    // color.a = alpha;

    return color;
}



void PrimitiveVisualization::visualizePrimitive(const memoryx::EnvironmentalPrimitiveBasePtr& primitive, DrawColor color)
{
    if (primitive->ice_isA(memoryx::PlanePrimitiveBase::ice_staticId()))
    {
        visualizePlane(memoryx::PlanePrimitiveBasePtr::dynamicCast(primitive), color);
    }
    else if (primitive->ice_isA(memoryx::SpherePrimitiveBase::ice_staticId()))
    {
        visualizeSphere(memoryx::SpherePrimitiveBasePtr::dynamicCast(primitive), color);
    }
    else if (primitive->ice_isA(memoryx::CylinderPrimitiveBase::ice_staticId()))
    {
        visualizeCylinder(memoryx::CylinderPrimitiveBasePtr::dynamicCast(primitive), color);
    }
    else if (primitive->ice_isA(memoryx::BoxPrimitiveBase::ice_staticId()))
    {
        visualizeBox(memoryx::BoxPrimitiveBasePtr::dynamicCast(primitive), color);
    }
    else if (primitive->ice_isA(memoryx::HigherSemanticStructureBase::ice_staticId()))
    {
        visualizeHigherSemanticStructure(memoryx::HigherSemanticStructureBasePtr::dynamicCast(primitive), color);
    }
    else
    {
        ARMARX_WARNING << "Cannot visualize unknown primitive type: " << primitive->getName();
        return;
    }

    primitiveLayer[primitive->getId()] = getLayerName();

    // debugDrawer->setTextVisu(getLayerName("_id"), primitive->getId(), primitive->getId(), primitive->getPose()->position, color, 18);
    // debugDrawer->setPoseVisu(getLayerName("_frame"), primitive->getId(), primitive->getPose());
}

void PrimitiveVisualization::visualizePlane(const memoryx::PlanePrimitiveBasePtr& plane, DrawColor color)
{
    memoryx::PlanePrimitivePtr p = memoryx::PlanePrimitivePtr::dynamicCast(plane);

    FramedPoseBasePtr pose = p->getPose();
    Vector3BasePtr dimensions = p->getOBBDimensions();

    if (getProperty<PLANE_VISUALIZATION_METHOD>("PlaneVisualizationMethod").getValue() == Obb)
    {
        // Old behavior: Visualize planes as boxes, while the depth of the box indicates the deviation of the
        // point cloud from the plane model

        debugDrawer->setBoxVisu(getLayerName(), p->getId(), pose, dimensions, color);
    }
    else if (getProperty<PLANE_VISUALIZATION_METHOD>("PlaneVisualizationMethod").getValue() == Rectangle)
    {
        Eigen::Matrix4f m = FramedPosePtr::dynamicCast(pose)->toEigen();
        Eigen::Vector3f d = Vector3Ptr::dynamicCast(dimensions)->toEigen();

        PolygonPointList points;
        points.push_back(new Vector3(Eigen::Vector3f((m * Eigen::Vector4f(d.x() / 2, d.y() / 2, 0, 1)).head(3))));
        points.push_back(new Vector3(Eigen::Vector3f((m * Eigen::Vector4f(d.x() / 2, -d.y() / 2, 0, 1)).head(3))));
        points.push_back(new Vector3(Eigen::Vector3f((m * Eigen::Vector4f(-d.x() / 2, -d.y() / 2, 0, 1)).head(3))));
        points.push_back(new Vector3(Eigen::Vector3f((m * Eigen::Vector4f(-d.x() / 2, d.y() / 2, 0, 1)).head(3))));

        debugDrawer->setPolygonVisu(getLayerName(), p->getId(), points, color, primitiveColorFrame, 2);
    }
    else if (getProperty<PLANE_VISUALIZATION_METHOD>("PlaneVisualizationMethod").getValue() == Hull)
    {
        debugDrawer->setPolygonVisu(getLayerName(), p->getId(), p->getGraspPoints(), color, primitiveColorFrame, 2);
    }
    else if (getProperty<PLANE_VISUALIZATION_METHOD>("PlaneVisualizationMethod").getValue() == Inlier)
    {
        DebugDrawer24BitColoredPointCloud cloud;
        cloud.points.reserve(p->getInliers().size());
        cloud.pointSize = 5;
        //cloud.transparency = color.a;
        DrawColor24Bit c = {static_cast<Ice::Byte>(color.r * 255), static_cast<Ice::Byte>(color.g * 255), static_cast<Ice::Byte>(color.b * 255)};
        DebugDrawer24BitColoredPointCloudElement e;
        for (const armarx::Vector3BasePtr& v : p->getInliers())
        {
            e.color = c;
            e.x = v->x;
            e.y = v->y;
            e.z = v->z;
            cloud.points.push_back(e);
        }
        debugDrawer->begin_set24BitColoredPointCloudVisu(getLayerName(), p->getId(), cloud);
    }

    // DrawColor hullColor = {1.0, 0.0, 0.0, 0.8};
    // int i = 0;
    // for (Vector3BasePtr & p : plane->getGraspPoints())
    // {
    //     debugDrawer->setSphereVisu(getLayerName("_grasp"), plane->getId() + "_" + std::to_string(i++), p, hullColor, 15.0);
    // }
    //    debugDrawer->setArrowVisu(getLayerName("_plane_normals"), p->getId(), p->getPose()->position, p->getPlaneNormal(), primitiveColorFrame, 25.0, 5.0);

}

void PrimitiveVisualization::visualizeSphere(const memoryx::SpherePrimitiveBasePtr& sphere, DrawColor color)
{
    memoryx::SpherePrimitivePtr p = memoryx::SpherePrimitivePtr::dynamicCast(sphere);

    Vector3BasePtr center = p->getSphereCenter();
    float radius = p->getSphereRadius();

    debugDrawer->setSphereVisu(getLayerName(), p->getId(), center, color, radius);
}

void PrimitiveVisualization::visualizeCylinder(const memoryx::CylinderPrimitiveBasePtr& cylinder, DrawColor color)
{
    memoryx::CylinderPrimitivePtr p = memoryx::CylinderPrimitivePtr::dynamicCast(cylinder);

    float radius = p->getCylinderRadius();
    float length = p->getLength();
    Vector3BasePtr center = p->getCylinderPoint();
    Vector3BasePtr direction = p->getCylinderAxisDirection();

    debugDrawer->setCylinderVisu(getLayerName(), p->getId(), center, direction, length, radius, color);
}


void PrimitiveVisualization::visualizeBox(const memoryx::BoxPrimitiveBasePtr& box, DrawColor color)
{
    color = {1.0, 0, 0.0, 0.9};

    memoryx::BoxPrimitivePtr p = memoryx::BoxPrimitivePtr::dynamicCast(box);
    memoryx::EntityRefList boxSides = p->getBoxSides();

    for (const memoryx::EntityRefBasePtr& r : boxSides)
    {
        memoryx::PlanePrimitiveBasePtr plane = memoryx::PlanePrimitiveBasePtr::dynamicCast(r->getEntity());
        if (plane)
        {
            //visualizePlane(memoryx::PlanePrimitiveBasePtr::dynamicCast(plane), sideColor);

            //int i = 0;
            //for (Vector3BasePtr & p : plane->getGraspPoints())
            //{
            //debugDrawer->setSphereVisu(getLayerName("_grasp"), plane->getId() + "_" + std::to_string(i++), p, getPrimitiveColor(box), 5.0);
            //}
        }
        else
        {
            ARMARX_WARNING << deactivateSpam(5) << "plane primitive no longer exists for box " << box->getId();
        }
    }

    debugDrawer->setBoxVisu(getLayerName("_box"), p->getId(), p->getPose(), p->getOBBDimensions(),  color);



    //    int i = 0;
    //    for (Vector3BasePtr & p : box->getGraspPoints())
    //    {
    //        debugDrawer->setSphereVisu(getLayerName("_grasp"), box->getId() + "_" + std::to_string(i++), p, color, 5.0);
    //    }

}

void PrimitiveVisualization::visualizeHigherSemanticStructure(const memoryx::HigherSemanticStructureBasePtr structure, DrawColor color)
{

    memoryx::HigherSemanticStructurePtr p = memoryx::HigherSemanticStructurePtr::dynamicCast(structure);
    color = {1.0, 1.0, 0.0, 1.0};
    ARMARX_LOG << "found structure with " <<  p->getPrimitives().size() << " primitives";

    std::vector<Vector3BasePtr> pointList;

    memoryx::EntityRefList primitives = p->getPrimitives();
    for (const memoryx::EntityRefBasePtr& r : primitives)
    {

        memoryx::EnvironmentalPrimitiveBasePtr primitive = memoryx::EnvironmentalPrimitiveBasePtr::dynamicCast(r->getEntity());

        if (!primitive)
        {
            ARMARX_WARNING << "primitive is null";
            continue;
        }

        visualizePrimitive(primitive, color);

        for (Vector3BasePtr& x : primitive->getGraspPoints())
        {
            pointList.push_back(x);
        }

    }
    ARMARX_LOG << " points " << pointList.size();
    color = {1.0, 0.0, 1.0, 0.5};
    debugDrawer->setPolygonVisu(getLayerName(), "points" + p->getId(), pointList, color, primitiveColorFrame, 20);
}


std::string PrimitiveVisualization::getLayerName(std::string suffix)
{
    return "primitive" + suffix;
}


armarx::PropertyDefinitionsPtr PrimitiveVisualization::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new PrimitiveVisualizationPropertyDefinitions(
            getConfigIdentifier()));
}

