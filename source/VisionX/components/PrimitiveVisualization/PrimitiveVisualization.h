/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::ArmarXObjects::PrimitiveVisualization
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <MemoryX/interface/components/WorkingMemoryInterface.h>
#include <MemoryX/core/memory/SegmentedMemory.h>
#include <MemoryX/libraries/memorytypes/segment/EnvironmentalPrimitiveSegment.h>

#include <VisionX/interface/components/PointCloudSegmenter.h>

#include <mutex>

namespace armarx
{

    enum PLANE_VISUALIZATION_METHOD
    {
        Hull, Inlier, Obb, Rectangle

    };

    /**
     * @class PrimitiveVisualizationPropertyDefinitions
     * @brief
     */
    class PrimitiveVisualizationPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        PrimitiveVisualizationPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "Name of WorkingMemory component");
            defineOptionalProperty<PLANE_VISUALIZATION_METHOD>("PlaneVisualizationMethod", Hull, "Visualize planes as boxes, as rectangles or as convex hulls")
            .map("Inlier", Inlier)
            .map("Hull", Hull)
            .map("OBB", Obb)
            .map("Box", Obb)
            .map("ConvexHull", Hull)
            .map("Rectangle", Rectangle);

        }
    };

    /**
     * @defgroup Component-PrimitiveVisualization PrimitiveVisualization
     * @ingroup VisionX-Components
     * A description of the component PrimitiveVisualization.
     *
     * @class PrimitiveVisualization
     * @ingroup Component-PrimitiveVisualization
     * @brief Brief description of class PrimitiveVisualization.
     *
     * Detailed description of class PrimitiveVisualization.
     */
    class PrimitiveVisualization :
        virtual public armarx::Component,
        virtual public visionx::PointCloudSegmentationListener
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "PrimitiveVisualization";
        }


        void reportNewPointCloudSegmentation(const Ice::Current& c = Ice::emptyCurrent) override;
    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    protected:


        DebugDrawerInterfacePrx debugDrawer;

        memoryx::WorkingMemoryInterfacePrx workingMemory;
        memoryx::EnvironmentalPrimitiveSegmentBasePrx environmentalPrimitiveSegment;


    private:

        void clearDebugLayer();

        std::mutex mutex;

        std::map<std::string, std::string> primitiveLayer;

        std::string getLayerName(std::string suffix = "");

        DrawColor getPrimitiveColor(const memoryx::EnvironmentalPrimitiveBasePtr& primitive);

        void visualizePrimitive(const memoryx::EnvironmentalPrimitiveBasePtr& primitive, DrawColor color);
        void visualizePlane(const memoryx::PlanePrimitiveBasePtr& plane, DrawColor color);
        void visualizeSphere(const memoryx::SpherePrimitiveBasePtr& sphere, DrawColor color);
        void visualizeCylinder(const memoryx::CylinderPrimitiveBasePtr& cylinder, DrawColor color);
        void visualizeBox(const memoryx::BoxPrimitiveBasePtr& box, DrawColor color);
        void visualizeHigherSemanticStructure(const memoryx::HigherSemanticStructureBasePtr structure, DrawColor color);

        DrawColor primitiveColorFrame;

    };
}

